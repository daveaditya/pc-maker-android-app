package com.maker.android.app.classes.hardware;

import android.os.Parcel;

import com.google.gson.JsonArray;
import com.google.gson.JsonDeserializationContext;
import com.google.gson.JsonDeserializer;
import com.google.gson.JsonElement;
import com.google.gson.JsonObject;
import com.google.gson.JsonParseException;
import com.maker.android.app.annotation.HardwareAnnotations;
import com.maker.android.app.classes.Attribute;
import com.maker.android.app.classes.Offer;
import com.maker.android.app.classes.SimpleHighlighter;
import com.maker.android.app.provider.PreserveContract;

import java.lang.reflect.Type;


@SuppressWarnings({"unused", "WeakerAccess"})
class WifiAdapter
        extends Hardware {

    /**
     * A {@link String} constant to use as tag in {@link android.util.Log}
     */
    private static final String LOG_TAG = WifiAdapter.class.getSimpleName();

    public static Deserialize sDeserialize;

    static {
        sDeserialize = new Deserialize();
    }

    @HardwareAnnotations.DisplayData(name = "Type", displayInHighlights = true)
    private String mType;

    @HardwareAnnotations.DisplayData(name = "Interface", displayInHighlights = true)
    private ConnectionInterface mInterface;

    @HardwareAnnotations.DisplayData(name = "Frequency")
    private String mFrequency;

    @HardwareAnnotations.DisplayData(name = "Dimensions", unit = "mm")
    private String mDimensions;

    @HardwareAnnotations.DisplayData(name = "Supported Network Standards")
    private String mNetworkStds;

    @HardwareAnnotations.DisplayData(name = "Supported OS")
    private String mSupportedOs;


    public static final Creator<WifiAdapter> CREATOR = new Creator<WifiAdapter>() {

        @Override
        public WifiAdapter createFromParcel(Parcel in) {
            return new WifiAdapter(in);
        }


        @Override
        public WifiAdapter[] newArray(int size) {
            return new WifiAdapter[size];
        }

    };


    private WifiAdapter(int id, String companyName, String model) {
        super(id, companyName, model);
        setHighlighter(new SimpleHighlighter());
    }


    WifiAdapter(Parcel in) {
        super(in);
    }


    @Override
    public int describeContents() {
        return 0;
    }


    @Override
    public PreserveContract.HardwareTypeEntry.HardwareType getHardwareType() {
        return PreserveContract.HardwareTypeEntry.HardwareType.WifiAdapter;
    }


    public String getType() {
        return mType;
    }

    public void setType(String type) {
        mType = type;
    }

    public ConnectionInterface getInterface() {
        return mInterface;
    }

    public void setInterface(ConnectionInterface anInterface) {
        mInterface = anInterface;
    }

    public String getFrequency() {
        return mFrequency;
    }

    public void setFrequency(String frequency) {
        mFrequency = frequency;
    }

    public String getDimensions() {
        return mDimensions;
    }

    public void setDimensions(String dimensions) {
        mDimensions = dimensions;
    }

    public String getNetworkStds() {
        return mNetworkStds;
    }

    public void setNetworkStds(String networkStds) {
        mNetworkStds = networkStds;
    }

    public String getSupportedOs() {
        return mSupportedOs;
    }

    public void setSupportedOs(String supportedOs) {
        mSupportedOs = supportedOs;
    }


    interface Builder {

        Builder setType(String type);

        Builder setInterface(ConnectionInterface connectionInterface);

        Builder setFrequency(String frequency);

        Builder setDimensions(String dimensions);

        Builder setNetworkStds(String networkStds);

        Builder setSupportedOs(String supportedOs);

        Builder setAttributes(Attribute attributes);

        Builder setOffers(Offer[] offers);

        WifiAdapter createWifiAdapter();

    }


    private static class WifiAdapterBuilder implements Builder {

        WifiAdapter mWifiAdapter;

        WifiAdapterBuilder(int id, String companyName, String model) {
            mWifiAdapter = new WifiAdapter(id, companyName, model);
        }

        @Override
        public Builder setType(String type) {
            mWifiAdapter.setType(type);
            return this;
        }

        @Override
        public Builder setInterface(ConnectionInterface connectionInterface) {
            mWifiAdapter.setInterface(connectionInterface);
            return this;
        }

        @Override
        public Builder setFrequency(String frequency) {
            mWifiAdapter.setFrequency(frequency);
            return this;
        }

        @Override
        public Builder setDimensions(String dimensions) {
            mWifiAdapter.setDimensions(dimensions);
            return this;
        }

        @Override
        public Builder setNetworkStds(String networkStds) {
            mWifiAdapter.setNetworkStds(networkStds);
            return this;
        }

        @Override
        public Builder setSupportedOs(String supportedOs) {
            mWifiAdapter.setSupportedOs(supportedOs);
            return this;
        }

        @Override
        public Builder setAttributes(Attribute attributes) {
            mWifiAdapter.setAttribute(attributes);
            return this;
        }

        @Override
        public Builder setOffers(Offer[] offers) {
            mWifiAdapter.setOffers(offers);
            return this;
        }

        @Override
        public WifiAdapter createWifiAdapter() {
            return mWifiAdapter;
        }

    }


    private static class Deserialize implements JsonDeserializer<WifiAdapter> {

        @Override
        public WifiAdapter deserialize(JsonElement json, Type typeOfT, JsonDeserializationContext context) throws JsonParseException {

            JsonObject jsonObject = json.getAsJsonObject();

            int wifiAdapterId = 0;
            String companyName = null, model = null;

            if (jsonObject.has("wifi_adapter_id") && !jsonObject.get("wifi_adapter_id").isJsonNull()) {
                wifiAdapterId = jsonObject.get("wifi_adapter_id").getAsInt();
            }

            if (jsonObject.has("company_name") && !jsonObject.get("company_name").isJsonNull()) {
                companyName = jsonObject.get("company_name").getAsString();
            }

            if (jsonObject.has("model") && !jsonObject.get("model").isJsonNull()) {
                model = jsonObject.get("model").getAsString();
            }

            Builder builder = new WifiAdapterBuilder(wifiAdapterId, companyName, model);

            if (jsonObject.has("type") && !jsonObject.get("type").isJsonNull()) {
                builder.setType(jsonObject.get("type").getAsString());
            }

            if (jsonObject.has("interface") && !jsonObject.get("interface").isJsonNull()) {
                builder.setInterface(ConnectionInterface.getEnumValue(jsonObject.get("interface").getAsString()));
            }

            if (jsonObject.has("frequency") && !jsonObject.get("frequency").isJsonNull()) {
                builder.setFrequency(jsonObject.get("frequency").getAsString());
            }

            if (jsonObject.has("dimensions") && !jsonObject.get("dimensions").isJsonNull()) {
                builder.setDimensions(jsonObject.get("dimensions").getAsString());
            }

            if (jsonObject.has("network_stds") && !jsonObject.get("network_stds").isJsonNull()) {
                builder.setNetworkStds(jsonObject.get("network_stds").getAsString());
            }

            if (jsonObject.has("supported_os") && !jsonObject.get("supported_os").isJsonNull()) {
                builder.setSupportedOs(jsonObject.get("supported_os").getAsString());
            }

            if (jsonObject.has("attributes") && !jsonObject.get("attributes").isJsonNull()) {
                builder.setAttributes(
                        Attribute.getAttributeFromJsonObject(jsonObject.get("attributes").getAsJsonObject())
                );
            }

            if (jsonObject.has("offers") && !jsonObject.get("offers").isJsonNull()) {
                JsonArray jsonArray = jsonObject.get("offers").getAsJsonArray();
                builder.setOffers(Offer.getOffersFromJsonArray(jsonArray));
            }

            return builder.createWifiAdapter();

        }

    }

}
