package com.maker.android.app.classes.hardware;


import android.os.Parcel;

import com.google.gson.JsonArray;
import com.google.gson.JsonDeserializationContext;
import com.google.gson.JsonDeserializer;
import com.google.gson.JsonElement;
import com.google.gson.JsonObject;
import com.google.gson.JsonParseException;
import com.maker.android.app.annotation.HardwareAnnotations;
import com.maker.android.app.classes.Attribute;
import com.maker.android.app.classes.Offer;
import com.maker.android.app.classes.SimpleHighlighter;
import com.maker.android.app.provider.PreserveContract;

import java.lang.reflect.Type;
import java.math.BigDecimal;


@SuppressWarnings({"unused", "WeakerAccess"})
class IntelProcessor
        extends Hardware {

    /**
     * A {@link String} constant to use as tag in {@link android.util.Log}
     */
    private static final String LOG_TAG = IntelProcessor.class.getSimpleName();

    public static Deserialize sDeserialize;

    static {
        sDeserialize = new Deserialize();
    }

    @HardwareAnnotations.DisplayData(name = "Base Frequency", displayInHighlights = true, unit = "GHz")
    private BigDecimal mBaseFrequency;

    @HardwareAnnotations.DisplayData(name = "Turbo Frequency", unit = "GHz")
    private BigDecimal mTurboFrequency;

    @HardwareAnnotations.DisplayData(name = "Cache Size", displayInHighlights = true)
    private String mCacheSize;

    @HardwareAnnotations.DisplayData(name = "Lithography", unit = "nm")
    private int mLithography;

    @HardwareAnnotations.DisplayData(name = "Number of Cores", displayInHighlights = true)
    private int mNoOfCores;

    @HardwareAnnotations.DisplayData(name = "Number of Threads", displayInHighlights = true)
    private int mNoOfThreads;

    @HardwareAnnotations.DisplayData(name = "Thermal Design Power", unit = "W")
    private int mThermalDesignPower;

    @HardwareAnnotations.DisplayData(name = "Maximum Memory Support", unit = "GB")
    private BigDecimal mMaxMemorySupport;

    @HardwareAnnotations.DisplayData(name = "Memory Type")
    private String mMemoryType;

    @HardwareAnnotations.DisplayData(name = "Number of Memory Channels")
    private int mNoOfMemoryChannels;

    @HardwareAnnotations.DisplayData(name = "Supports ECC Memory?")
    private boolean mHasEccMemory;

    @HardwareAnnotations.DisplayData(name = "Graphics Processor", displayInHighlights = true)
    private String mGraphicsProcessor;

    @HardwareAnnotations.DisplayData(name = "Graphics: Base Frequency", unit = "GHz")
    private BigDecimal mGBaseFrequency;

    @HardwareAnnotations.DisplayData(name = "Graphics: Maximum Frequency", unit = "GHz")
    private BigDecimal mGMaxFrequency;

    @HardwareAnnotations.DisplayData(name = "Maximum Video Memory", unit = "GB")
    private int mMaxVideoMemory;

    @HardwareAnnotations.DisplayData(name = "DirectX Version")
    private String mDirectXVer;

    @HardwareAnnotations.DisplayData(name = "OpenGL Version")
    private String mOpenGLVer;

    @HardwareAnnotations.DisplayData(name = "Graphics Output")
    private String mGraphicsOutput;

    @HardwareAnnotations.DisplayData(name = "PCIe Version")
    private ConnectionInterface mPcieRev;

    @HardwareAnnotations.DisplayData(name = "PCIe Configuration")
    private String mPcieConfig;

    @HardwareAnnotations.DisplayData(name = "Maximum PCIe Lanes")
    private int mMaxPcieLanes;

    @HardwareAnnotations.DisplayData(name = "Supported Sockets?")
    private String mSocketsSupport;

    @HardwareAnnotations.DisplayData(name = "Supports Intel Virtualization?")
    private boolean mHasIntelVirtualization;

    @HardwareAnnotations.DisplayData(name = "Supports Hyper Threading?")
    private boolean mHasHyperThreading;

    @HardwareAnnotations.DisplayData(name = "Turbo Boost Version")
    private BigDecimal mTurboBoostVersion;

    @HardwareAnnotations.DisplayData(name = "Has Stock Cooler?")
    private boolean mHasCooler;


    public static final Creator<IntelProcessor> CREATOR = new Creator<IntelProcessor>() {

        @Override
        public IntelProcessor createFromParcel(Parcel in) {
            return new IntelProcessor(in);
        }


        @Override
        public IntelProcessor[] newArray(int size) {
            return new IntelProcessor[size];
        }

    };


    private IntelProcessor(int id, String model) {
        super(id, "Intel", model);
        setHighlighter(new SimpleHighlighter());
    }


    IntelProcessor(Parcel in) {
        super(in);
    }


    @Override
    public int describeContents() {
        return 0;
    }


    @Override
    public PreserveContract.HardwareTypeEntry.HardwareType getHardwareType() {
        return PreserveContract.HardwareTypeEntry.HardwareType.IntelProcessor;
    }


    public BigDecimal getBaseFrequency() {
        return mBaseFrequency;
    }

    public void setBaseFrequency(BigDecimal baseFrequency) {
        mBaseFrequency = baseFrequency;
    }

    public BigDecimal getTurboFrequency() {
        return mTurboFrequency;
    }

    public void setTurboFrequency(BigDecimal turboFrequency) {
        mTurboFrequency = turboFrequency;
    }

    public String getCacheSize() {
        return mCacheSize;
    }

    public void setCacheSize(String cacheSize) {
        mCacheSize = cacheSize;
    }

    public int getLithography() {
        return mLithography;
    }

    public void setLithography(int lithography) {
        mLithography = lithography;
    }

    public int getNoOfCores() {
        return mNoOfCores;
    }

    public void setNoOfCores(int noOfCores) {
        mNoOfCores = noOfCores;
    }

    public int getNoOfThreads() {
        return mNoOfThreads;
    }

    public void setNoOfThreads(int noOfThreads) {
        mNoOfThreads = noOfThreads;
    }

    public int getThermalDesignPower() {
        return mThermalDesignPower;
    }

    public void setThermalDesignPower(int thermalDesignPower) {
        mThermalDesignPower = thermalDesignPower;
    }

    public BigDecimal getMaxMemorySupport() {
        return mMaxMemorySupport;
    }

    public void setMaxMemorySupport(BigDecimal maxMemorySupport) {
        mMaxMemorySupport = maxMemorySupport;
    }

    public String getMemoryType() {
        return mMemoryType;
    }

    public void setMemoryType(String memoryType) {
        mMemoryType = memoryType;
    }

    public int getNoOfMemoryChannels() {
        return mNoOfMemoryChannels;
    }

    public void setNoOfMemoryChannels(int noOfMemoryChannels) {
        mNoOfMemoryChannels = noOfMemoryChannels;
    }

    public boolean isHasEccMemory() {
        return mHasEccMemory;
    }

    public void setHasEccMemory(boolean hasEccMemory) {
        mHasEccMemory = hasEccMemory;
    }

    public String getGraphicsProcessor() {
        return mGraphicsProcessor;
    }

    public void setGraphicsProcessor(String graphicsProcessor) {
        mGraphicsProcessor = graphicsProcessor;
    }

    public BigDecimal getGBaseFrequency() {
        return mGBaseFrequency;
    }

    public void setGBaseFrequency(BigDecimal GBaseFrequency) {
        mGBaseFrequency = GBaseFrequency;
    }

    public BigDecimal getGMaxFrequency() {
        return mGMaxFrequency;
    }

    public void setGMaxFrequency(BigDecimal GMaxFrequency) {
        mGMaxFrequency = GMaxFrequency;
    }

    public int getMaxVideoMemory() {
        return mMaxVideoMemory;
    }

    public void setMaxVideoMemory(int maxVideoMemory) {
        mMaxVideoMemory = maxVideoMemory;
    }

    public String getDirectXVer() {
        return mDirectXVer;
    }

    public void setDirectXVer(String directXVer) {
        mDirectXVer = directXVer;
    }

    public String getOpenGLVer() {
        return mOpenGLVer;
    }

    public void setOpenGLVer(String openGLVer) {
        mOpenGLVer = openGLVer;
    }

    public String getGraphicsOutput() {
        return mGraphicsOutput;
    }

    public void setGraphicsOutput(String graphicsOutput) {
        mGraphicsOutput = graphicsOutput;
    }

    public ConnectionInterface getPcieRev() {
        return mPcieRev;
    }

    public void setPcieRev(ConnectionInterface pcieRev) {
        mPcieRev = pcieRev;
    }

    public String getPcieConfig() {
        return mPcieConfig;
    }

    public void setPcieConfig(String pcieConfig) {
        mPcieConfig = pcieConfig;
    }

    public int getMaxPcieLanes() {
        return mMaxPcieLanes;
    }

    public void setMaxPcieLanes(int maxPcieLanes) {
        mMaxPcieLanes = maxPcieLanes;
    }

    public String getSocketsSupport() {
        return mSocketsSupport;
    }

    public void setSocketsSupport(String socketsSupport) {
        mSocketsSupport = socketsSupport;
    }

    public boolean isHasIntelVirtualization() {
        return mHasIntelVirtualization;
    }

    public void setHasIntelVirtualization(boolean hasIntelVirtualization) {
        mHasIntelVirtualization = hasIntelVirtualization;
    }

    public boolean isHasHyperThreading() {
        return mHasHyperThreading;
    }

    public void setHasHyperThreading(boolean hasHyperThreading) {
        mHasHyperThreading = hasHyperThreading;
    }

    public BigDecimal getTurboBoostVersion() {
        return mTurboBoostVersion;
    }

    public void setTurboBoostVersion(BigDecimal turboBoostVersion) {
        mTurboBoostVersion = turboBoostVersion;
    }

    public boolean isHasCooler() {
        return mHasCooler;
    }

    public void setHasCooler(boolean hasCooler) {
        mHasCooler = hasCooler;
    }


    interface Builder {

        Builder setBaseFrequency(BigDecimal baseFrequency);

        Builder setTurboFrequency(BigDecimal turboFrequency);

        Builder setCacheSize(String cacheSize);

        Builder setLithography(int lithography);

        Builder setNoOfCores(int noOfCores);

        Builder setNoOfThreads(int noOfThreads);

        Builder setThermalDesignPower(int thermalDesignPower);

        Builder setMaxMemorySupport(BigDecimal maxMemorySupport);

        Builder setMemoryType(String memoryType);

        Builder setNoOfMemoryChannels(int noOfMemoryChannels);

        Builder setHasEccSupport(boolean hasEccSupport);

        Builder setGraphicsProcessor(String graphicsProcessor);

        Builder setGBaseFrequency(BigDecimal gBaseFrequency);

        Builder setGMaxFrequency(BigDecimal gMaxFrequency);

        Builder setMaxVideoFrequency(int maxVideoFrequency);

        Builder setDirectXVersion(String directXVersion);

        Builder setOpenGLVersion(String openGLVersion);

        Builder setGraphicsOutput(String graphicsOutput);

        Builder setPcieRev(ConnectionInterface pcieVer);

        Builder setPcieConfig(String pcieConfig);

        Builder setMaxPcieLanes(int maxPcieLanes);

        Builder setSocketsSupport(String socketsSupport);

        Builder setHasIntelVirtualization(boolean hasIntelVirtualization);

        Builder setHasHyperThreading(boolean hasHyperThreading);

        Builder setTurboBoostVersion(BigDecimal turboBoostVersion);

        Builder setHasCooler(boolean hasCooler);

        Builder setAttributes(Attribute attributes);

        Builder setOffers(Offer[] offers);

        IntelProcessor createIntelProcessor();

    }


    private static class IntelProcessorBuilder implements Builder {

        IntelProcessor mIntelProcessor;

        IntelProcessorBuilder(int id, String model) {
            mIntelProcessor = new IntelProcessor(id, model);
        }

        @Override
        public Builder setBaseFrequency(BigDecimal baseFrequency) {
            mIntelProcessor.setBaseFrequency(baseFrequency);
            return this;
        }

        @Override
        public Builder setTurboFrequency(BigDecimal turboFrequency) {
            mIntelProcessor.setTurboFrequency(turboFrequency);
            return this;
        }

        @Override
        public Builder setCacheSize(String cacheSize) {
            mIntelProcessor.setCacheSize(cacheSize);
            return this;
        }

        @Override
        public Builder setLithography(int lithography) {
            mIntelProcessor.setLithography(lithography);
            return this;
        }

        @Override
        public Builder setNoOfCores(int noOfCores) {
            mIntelProcessor.setNoOfCores(noOfCores);
            return this;
        }

        @Override
        public Builder setNoOfThreads(int noOfThreads) {
            mIntelProcessor.setNoOfThreads(noOfThreads);
            return this;
        }

        @Override
        public Builder setThermalDesignPower(int thermalDesignPower) {
            mIntelProcessor.setThermalDesignPower(thermalDesignPower);
            return this;
        }

        @Override
        public Builder setMaxMemorySupport(BigDecimal maxMemorySupport) {
            mIntelProcessor.setMaxMemorySupport(maxMemorySupport);
            return this;
        }

        @Override
        public Builder setMemoryType(String memoryType) {
            mIntelProcessor.setMemoryType(memoryType);
            return this;
        }

        @Override
        public Builder setNoOfMemoryChannels(int noOfMemoryChannels) {
            mIntelProcessor.setNoOfMemoryChannels(noOfMemoryChannels);
            return this;
        }

        @Override
        public Builder setHasEccSupport(boolean hasEccSupport) {
            mIntelProcessor.setHasEccMemory(hasEccSupport);
            return this;
        }

        @Override
        public Builder setGraphicsProcessor(String graphicsProcessor) {
            mIntelProcessor.setGraphicsProcessor(graphicsProcessor);
            return this;
        }

        @Override
        public Builder setGBaseFrequency(BigDecimal gBaseFrequency) {
            mIntelProcessor.setGBaseFrequency(gBaseFrequency);
            return this;
        }

        @Override
        public Builder setGMaxFrequency(BigDecimal gMaxFrequency) {
            mIntelProcessor.setGMaxFrequency(gMaxFrequency);
            return this;
        }

        @Override
        public Builder setMaxVideoFrequency(int maxVideoFrequency) {
            mIntelProcessor.setMaxVideoMemory(maxVideoFrequency);
            return this;
        }

        @Override
        public Builder setDirectXVersion(String directXVersion) {
            mIntelProcessor.setDirectXVer(directXVersion);
            return this;
        }

        @Override
        public Builder setOpenGLVersion(String openGLVersion) {
            mIntelProcessor.setOpenGLVer(openGLVersion);
            return this;
        }

        @Override
        public Builder setGraphicsOutput(String graphicsOutput) {
            mIntelProcessor.setGraphicsOutput(graphicsOutput);
            return this;
        }

        @Override
        public Builder setPcieRev(ConnectionInterface pcieVer) {
            mIntelProcessor.setPcieRev(pcieVer);
            return this;
        }

        @Override
        public Builder setPcieConfig(String pcieConfig) {
            mIntelProcessor.setPcieConfig(pcieConfig);
            return this;
        }

        @Override
        public Builder setMaxPcieLanes(int maxPcieLanes) {
            mIntelProcessor.setMaxPcieLanes(maxPcieLanes);
            return this;
        }

        @Override
        public Builder setSocketsSupport(String socketsSupport) {
            mIntelProcessor.setSocketsSupport(socketsSupport);
            return this;
        }

        @Override
        public Builder setHasIntelVirtualization(boolean hasIntelVirtualization) {
            mIntelProcessor.setHasIntelVirtualization(hasIntelVirtualization);
            return this;
        }

        @Override
        public Builder setHasHyperThreading(boolean hasHyperThreading) {
            mIntelProcessor.setHasHyperThreading(hasHyperThreading);
            return this;
        }

        @Override
        public Builder setTurboBoostVersion(BigDecimal turboBoostVersion) {
            mIntelProcessor.setTurboBoostVersion(turboBoostVersion);
            return this;
        }

        @Override
        public Builder setHasCooler(boolean hasCooler) {
            mIntelProcessor.setHasCooler(hasCooler);
            return this;
        }

        @Override
        public Builder setAttributes(Attribute attributes) {
            mIntelProcessor.setAttribute(attributes);
            return this;
        }

        @Override
        public Builder setOffers(Offer[] offers) {
            mIntelProcessor.setOffers(offers);
            return this;
        }

        @Override
        public IntelProcessor createIntelProcessor() {
            return mIntelProcessor;
        }

    }


    private static class Deserialize implements JsonDeserializer<IntelProcessor> {

        @Override
        public IntelProcessor deserialize(JsonElement json, Type typeOfT, JsonDeserializationContext context) throws JsonParseException {

            JsonObject jsonObject = json.getAsJsonObject();

            int intelProcessorId = 0;
            String model = null;

            if (jsonObject.has("intel_processor_id") && !jsonObject.get("intel_processor_id").isJsonNull()) {
                intelProcessorId = jsonObject.get("intel_processor_id").getAsInt();
            }

            if (jsonObject.has("model") && !jsonObject.get("model").isJsonNull()) {
                model = jsonObject.get("model").getAsString();
            }

            Builder builder = new IntelProcessorBuilder(intelProcessorId, model);

            if (jsonObject.has("base_frequency") && !jsonObject.get("base_frequency").isJsonNull()) {
                builder.setBaseFrequency(jsonObject.get("base_frequency").getAsBigDecimal());
            }

            if (jsonObject.has("turbo_frequency") && !jsonObject.get("turbo_frequency").isJsonNull()) {
                builder.setTurboFrequency(jsonObject.get("turbo_frequency").getAsBigDecimal());
            }

            if (jsonObject.has("cache_size") && !jsonObject.get("cache_size").isJsonNull()) {
                builder.setCacheSize(jsonObject.get("cache_size").getAsString());
            }

            if (jsonObject.has("lithography") && !jsonObject.get("lithography").isJsonNull()) {
                builder.setLithography(jsonObject.get("lithography").getAsInt());
            }

            if (jsonObject.has("no_of_cores") && !jsonObject.get("no_of_cores").isJsonNull()) {
                builder.setNoOfCores(jsonObject.get("no_of_cores").getAsInt());
            }

            if (jsonObject.has("no_of_threads") && !jsonObject.get("no_of_threads").isJsonNull()) {
                builder.setNoOfThreads(jsonObject.get("no_of_threads").getAsInt());
            }

            if (jsonObject.has("thermal_design_power") && !jsonObject.get("thermal_design_power").isJsonNull()) {
                builder.setThermalDesignPower(jsonObject.get("thermal_design_power").getAsInt());
            }

            if (jsonObject.has("max_memory_support") && !jsonObject.get("max_memory_support").isJsonNull()) {
                builder.setMaxMemorySupport(jsonObject.get("max_memory_support").getAsBigDecimal());
            }

            if (jsonObject.has("memory_type") && !jsonObject.get("memory_type").isJsonNull()) {
                builder.setMemoryType(jsonObject.get("memory_type").getAsString());
            }

            if (jsonObject.has("no_of_memory_channels") && !jsonObject.get("no_of_memory_channels").isJsonNull()) {
                builder.setNoOfMemoryChannels(jsonObject.get("no_of_memory_channels").getAsInt());
            }

            if (jsonObject.has("has_ecc_support") && !jsonObject.get("has_ecc_support").isJsonNull()) {
                builder.setHasEccSupport(jsonObject.get("has_ecc_support").getAsBoolean());
            }

            if (jsonObject.has("graphics_processor") && !jsonObject.get("graphics_processor").isJsonNull()) {
                builder.setGraphicsProcessor(jsonObject.get("graphics_processor").getAsString());
            }

            if (jsonObject.has("g_base_frequency") && !jsonObject.get("g_base_frequency").isJsonNull()) {
                builder.setGBaseFrequency(jsonObject.get("g_base_frequency").getAsBigDecimal());
            }

            if (jsonObject.has("g_max_frequency") && !jsonObject.get("g_max_frequency").isJsonNull()) {
                builder.setGMaxFrequency(jsonObject.get("g_max_frequency").getAsBigDecimal());
            }

            if (jsonObject.has("max_video_memory") && !jsonObject.get("max_video_memory").isJsonNull()) {
                builder.setMaxVideoFrequency(jsonObject.get("max_video_memory").getAsInt());
            }

            if (jsonObject.has("directX_ver") && !jsonObject.get("directX_ver").isJsonNull()) {
                builder.setDirectXVersion(jsonObject.get("directX_ver").getAsString());
            }

            if (jsonObject.has("openGL_ver") && !jsonObject.get("openGL_ver").isJsonNull()) {
                builder.setOpenGLVersion(jsonObject.get("openGL_ver").getAsString());
            }

            if (jsonObject.has("graphics_output") && !jsonObject.get("graphics_output").isJsonNull()) {
                builder.setGraphicsOutput(jsonObject.get("graphics_output").getAsString());
            }

            if (jsonObject.has("pcie_rev") && !jsonObject.get("pcie_rev").isJsonNull()) {
                builder.setPcieRev(ConnectionInterface.getEnumValue(jsonObject.get("pcie_rev").getAsString()));
            }

            if (jsonObject.has("pcie_config") && !jsonObject.get("pcie_config").isJsonNull()) {
                builder.setPcieConfig(jsonObject.get("pcie_config").getAsString());
            }

            if (jsonObject.has("max_pcie_lanes") && !jsonObject.get("max_pcie_lanes").isJsonNull()) {
                builder.setMaxPcieLanes(jsonObject.get("max_pcie_lanes").getAsInt());
            }

            if (jsonObject.has("sockets_support") && !jsonObject.get("sockets_support").isJsonNull()) {
                builder.setSocketsSupport(jsonObject.get("sockets_support").getAsString());
            }

            if (jsonObject.has("intel_virtualization") && !jsonObject.get("intel_virtualization").isJsonNull()) {
                builder.setHasIntelVirtualization(jsonObject.get("intel_virtualization").getAsBoolean());
            }

            if (jsonObject.has("hyper_threading") && !jsonObject.get("hyper_threading").isJsonNull()) {
                builder.setHasHyperThreading(jsonObject.get("hyper_threading").getAsBoolean());
            }

            if (jsonObject.has("turbo_boost_ver") && !jsonObject.get("turbo_boost_ver").isJsonNull()) {
                builder.setTurboBoostVersion(jsonObject.get("turbo_boost_ver").getAsBigDecimal());
            }

            if (jsonObject.has("has_cooler") && !jsonObject.get("has_cooler").isJsonNull()) {
                builder.setHasCooler(jsonObject.get("has_cooler").getAsBoolean());
            }

            if (jsonObject.has("attributes") && !jsonObject.get("attributes").isJsonNull()) {
                builder.setAttributes(
                        Attribute.getAttributeFromJsonObject(jsonObject.get("attributes").getAsJsonObject())
                );
            }

            if (jsonObject.has("offers") && !jsonObject.get("offers").isJsonNull()) {
                JsonArray jsonArray = jsonObject.get("offers").getAsJsonArray();
                builder.setOffers(Offer.getOffersFromJsonArray(jsonArray));
            }

            return builder.createIntelProcessor();

        }

    }

}
