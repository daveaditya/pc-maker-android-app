package com.maker.android.app.classes;

import android.graphics.Bitmap;
import android.os.Parcel;
import android.os.Parcelable;


@SuppressWarnings({"unused", "WeakerAccess"})
public class ComparedComponent
        implements Parcelable {

    /**
     * A {@link String} constant to use as tag in {@link android.util.Log}
     */
    private static final String LOG_TAG = ComparedComponent.class.getSimpleName();

    private String mType;
    private String mImageSrc;


    public static final Creator<ComparedComponent> CREATOR = new Creator<ComparedComponent>() {
        @Override
        public ComparedComponent createFromParcel(Parcel in) {
            return new ComparedComponent(in);
        }

        @Override
        public ComparedComponent[] newArray(int size) {
            return new ComparedComponent[size];
        }
    };


    public ComparedComponent(String imageSrc, Bitmap image, String type) {
        mImageSrc = imageSrc;
        mType = type;
    }


    protected ComparedComponent(Parcel in) {
        mImageSrc = in.readString();
        mType = in.readString();
    }


    public String getImageSrc() {
        return mImageSrc;
    }


    public String getType() {
        return mType;
    }


    @Override
    public int describeContents() {
        return 0;
    }


    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeString(mImageSrc);
        dest.writeString(mType);
    }

}
