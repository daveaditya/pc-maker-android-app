package com.maker.android.app.activity;

import android.content.ContentValues;
import android.content.Intent;
import android.database.sqlite.SQLiteConstraintException;
import android.graphics.Color;
import android.net.Uri;
import android.os.Bundle;
import android.os.Handler;
import android.os.Message;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.design.widget.BottomNavigationView;
import android.support.design.widget.Snackbar;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.LoaderManager;
import android.support.v4.content.Loader;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.widget.Toast;

import com.google.gson.Gson;
import com.google.gson.JsonObject;
import com.maker.android.app.R;
import com.maker.android.app.fragment.BuildSelectorDialogFragment;
import com.maker.android.app.fragment.ModelDetailsImageFragment;
import com.maker.android.app.fragment.ModelDetailsPriceFragment;
import com.maker.android.app.fragment.ModelDetailsSpecsFragment;
import com.maker.android.app.loader.AsyncDataDownloader;
import com.maker.android.app.provider.PreserveContract;
import com.maker.android.app.utils.Request;


/**
 * Requires
 * model id
 * model name
 * hardware type :- Display Names
 */
@SuppressWarnings({"unused"})
public class ModelDetailsActivity
        extends AppCompatActivity
        implements BuildSelectorDialogFragment.OnBuildNameSelected {

    /**
     * A {@link String} constant to use in {@link android.util.Log}
     */
    private static final String LOG_TAG = ModelDetailsActivity.class.getSimpleName();

    public static final String PARENT_CLASS = "ModelDetailsActivity.PARENT_CLASS";
    private static final String SPECS_FRAGMENT = "SPECS_FRAGMENT";
    private static final String IMAGE_FRAGMENT = "IMAGE_FRAGMENT";
    private static final String PRICE_FRAGMENT = "PRICE_FRAGMENT";
    private static final String BUILD_NAME_DIALOG = "BUILD_NAME_DIALOG";
    public static final String SHOW_DETAILS_DATA = "SHOW_DETAILS_DATA";
    public static final String TITLE = "TITLE";
    public static final String MODEL_TYPE = "MODEL_TYPE";
    public static final String MODEL_ID = "MODEL_ID";
    private static final int MODEL_DATA_LOADER = 5;

    private static Request mRequest;
    private static String mTitle;
    public static String mHardwareType;
    private static String mCurrentData;
    private BottomNavigationView mBottomNavigationView;
    private static FragmentManager mFragmentManager;
    private static final Bundle mArgs = new Bundle();
    private ModelDetailsSpecsFragment mModelDetailsSpecsFragment;
    private ModelDetailsImageFragment mModelDetailsImageFragment;
    private ModelDetailsPriceFragment mModelDetailsPriceFragment;


    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        MenuInflater inflater = new MenuInflater(getBaseContext());
        inflater.inflate(R.menu.model_detail_action_bar, menu);
        return super.onCreateOptionsMenu(menu);
    }


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_model_details);

        int modelId = getIntent().getIntExtra(MODEL_ID, 0);
        mTitle = getIntent().getStringExtra(TITLE);
        mHardwareType = getIntent().getStringExtra(MODEL_TYPE);

        String url
                = getResources()
                .getString(R.string.url_base_for_single_item_request,
                        PreserveContract.HardwareTypeEntry.HardwareType.getEnumFromName(mHardwareType).getEndPoint(), modelId);

        mRequest
                = new Request.RequestBuilder()
                .setURL(url)
                .setHttpType(Request.HttpRequestTypes.GET)
                .createRequest();

        // Gets a reference to the ToolBar and sets it as ActionBar
        Toolbar toolbar = (Toolbar) findViewById(R.id.activity_model_details_tb);
        toolbar.setTitle(mTitle);
        setSupportActionBar(toolbar);

        if (getSupportActionBar() != null) {
            getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        }

        mModelDetailsSpecsFragment = new ModelDetailsSpecsFragment();
        mModelDetailsImageFragment = new ModelDetailsImageFragment();
        mModelDetailsPriceFragment = new ModelDetailsPriceFragment();

        mFragmentManager = getSupportFragmentManager();

        mBottomNavigationView = (BottomNavigationView) findViewById(R.id.activity_model_details_bottom_navigation);
        mBottomNavigationView.setOnNavigationItemSelectedListener(new BottomNavigationView.OnNavigationItemSelectedListener() {
            @Override
            public boolean onNavigationItemSelected(@NonNull MenuItem item) {
                switch (item.getItemId()) {

                    case R.id.activity_detail_menu_specs:

                        mFragmentManager
                                .beginTransaction()
                                .replace(R.id.activity_model_details_fragment_show, mModelDetailsSpecsFragment, SPECS_FRAGMENT)
                                .commit();
                        return true;

                    case R.id.activity_detail_menu_image:

                        mFragmentManager
                                .beginTransaction()
                                .replace(R.id.activity_model_details_fragment_show, mModelDetailsImageFragment, IMAGE_FRAGMENT)
                                .commit();
                        return true;

                    case R.id.activity_detail_menu_prices:

                        mFragmentManager
                                .beginTransaction()
                                .replace(R.id.activity_model_details_fragment_show, mModelDetailsPriceFragment, PRICE_FRAGMENT)
                                .commit();
                        return true;

                    default:
                        return false;
                }
            }
        });


        getSupportLoaderManager()
                .initLoader(MODEL_DATA_LOADER, null, new ModelDataLoader())
                .forceLoad();
    }


    @Override
    public boolean onOptionsItemSelected(MenuItem item) {

        Intent intent;
        switch (item.getItemId()) {
            case R.id.activity_model_detail_menu_add_to_compare:
                addModelToCompare();
                return true;

            case R.id.activity_model_detail_menu_add_to_build:
                addModelToBuild();
                return true;

            case R.id.activity_model_detail_menu_goto_cart:
                intent = new Intent(getBaseContext(), ReviewBuildActivity.class);
                intent.putExtra(ReviewBuildActivity.PARENT_CLASS, ModelDetailsActivity.class.getName());
                startActivity(intent);
                return true;

            case R.id.activity_model_detail_menu_goto_compare:
                intent = new Intent(getBaseContext(), CompareActivity.class);
                intent.putExtra(CompareActivity.PARENT_CLASS, ModelDetailsActivity.class.getName());
                startActivity(intent);
                return true;

            default:
                return false;
        }

    }


    private void addModelToCompare() {

        ContentValues contentValues = new ContentValues();
        contentValues.put(PreserveContract.CompareEntry.COLUMN_COMPARE_HW_TYPE,
                PreserveContract.HardwareTypeEntry.HardwareType.getEnumFromName(mHardwareType).name());
        contentValues.put(PreserveContract.CompareEntry.COLUMN_COMPARE_MODEL_DETAILS, mCurrentData);

        try {
            final Uri uri = getContentResolver().insert(PreserveContract.CompareEntry.CONTENT_URI, contentValues);
            if (uri == null) {
                Toast.makeText(getBaseContext(), "Some error!!!", Toast.LENGTH_SHORT).show();
                return;
            }

            Snackbar snackbar = Snackbar.make(mBottomNavigationView, "Product added for comparison", Snackbar.LENGTH_LONG);
            snackbar.setAction("UNDO", new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    getContentResolver().delete(uri, null, null);
                }
            });
            snackbar.setActionTextColor(Color.parseColor("white"));
            snackbar.show();
        } catch (SQLiteConstraintException exc) {
            Toast.makeText(getBaseContext(), mTitle + " already added to compare!", Toast.LENGTH_SHORT).show();
        }

    }


    private void addModelToBuild() {
        BuildSelectorDialogFragment buildSelectorDialog = new BuildSelectorDialogFragment();
        buildSelectorDialog.show(getSupportFragmentManager(), BUILD_NAME_DIALOG);
    }


    @Override
    public void saveUnderSelectedBuildName(String buildName) {
        ContentValues contentValues = new ContentValues();
        contentValues.put(PreserveContract.BuildEntry.COLUMN_BUILD_NAME, buildName);
        contentValues.put(PreserveContract.BuildEntry.COLUMN_BUILD_HW_TYPE, mHardwareType);
        contentValues.put(PreserveContract.BuildEntry.COLUMN_BUILD_HW_DESC, mCurrentData);

        try {
            final Uri uri = getContentResolver().insert(PreserveContract.BuildEntry.CONTENT_URI, contentValues);
            if (uri == null) {
                Toast.makeText(getBaseContext(), "Some error!!!", Toast.LENGTH_SHORT).show();
                return;
            }

            Snackbar snackbar = Snackbar.make(mBottomNavigationView, "Product added to build", Snackbar.LENGTH_LONG);
            snackbar.setAction("UNDO", new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    getContentResolver().delete(uri, null, null);
                }
            });
            snackbar.setActionTextColor(Color.parseColor("white"));
            snackbar.show();
        } catch (SQLiteConstraintException exc) {
            Toast.makeText(getBaseContext(), mTitle + " already added to build!", Toast.LENGTH_SHORT).show();
        }
    }


    @Nullable
    @Override
    public Intent getParentActivityIntent() {
        Intent parentIntent = getIntent();
        String className = parentIntent.getStringExtra(PARENT_CLASS);

        Intent newIntent = null;
        try {
            //you need to define the class with package name
            newIntent = new Intent(getBaseContext(), Class.forName(className));

        } catch (ClassNotFoundException e) {
            e.printStackTrace();
        }
        return newIntent;
    }


    /**
     * A {@link android.support.v4.app.LoaderManager.LoaderCallbacks} class to interact with the
     * {@link LoaderManager} for downloading JSON data from Web Server.
     */
    private final class ModelDataLoader
            implements LoaderManager.LoaderCallbacks<String> {


        /**
         * Creates an {@link AsyncDataDownloader} for the given ID
         *
         * @param id   ID of the Loader
         * @param args {@link Bundle} containing data to pass to the {@link Loader}
         * @return An instance of {@link AsyncDataDownloader}
         */
        @Override
        public Loader<String> onCreateLoader(int id, Bundle args) {
            return new AsyncDataDownloader(getBaseContext(), mRequest);
        }


        /**
         * Called when the {@link AsyncDataDownloader} completes its task.
         *
         * @param loader {@link Loader} that finished the task
         * @param data   Data generated by the loader
         */
        @Override
        public void onLoadFinished(Loader<String> loader, String data) {

            try {
                // If nothing was returned from the server, do nothing
                if (data == null) {
                    return;
                }

                // Creates a GSON instance
                Gson gson = new Gson();

                // Creates a JSONObject from the response
                JsonObject jsonObject = gson.fromJson(data, JsonObject.class);

                // If the server response is FAILURE, show Toast and return
                if (jsonObject.getAsJsonObject("feedback").get("response").getAsString().equals(getResources().getString(R.string.server_failure))) {
                    Toast.makeText(getBaseContext(), "Internal Server Error!", Toast.LENGTH_SHORT).show();
                    return;
                }

                mCurrentData = jsonObject.getAsJsonArray("data").get(0).getAsJsonObject().toString();

                mArgs.putString(SHOW_DETAILS_DATA, mCurrentData);
                mArgs.putString(TITLE, mTitle);
                mArgs.putString(MODEL_TYPE, mHardwareType);

                mModelDetailsSpecsFragment.setArguments(mArgs);
                mModelDetailsImageFragment.setArguments(mArgs);
                mModelDetailsPriceFragment.setArguments(mArgs);

                final int WHAT = 1;
                @SuppressWarnings("HandlerLeak")
                Handler handler = new Handler() {
                    @Override
                    public void handleMessage(Message msg) {
                        if (msg.what == WHAT) changeFragment();
                    }
                };
                handler.sendEmptyMessage(WHAT);

                // Fetches JSONException that might occur while parsing JSON data
            } catch (NullPointerException exc) {
                exc.printStackTrace();
            }

        }

        /**
         * Removes the data references after the {@link Loader} is reset, if any.
         *
         * @param loader {@link Loader} which got reset
         */
        @Override
        public void onLoaderReset(Loader<String> loader) {
            mCurrentData = null;
        }

    }


    private void changeFragment() {
        mFragmentManager
                .beginTransaction()
                .add(R.id.activity_model_details_fragment_show, mModelDetailsSpecsFragment, SPECS_FRAGMENT)
                .commit();
    }

}