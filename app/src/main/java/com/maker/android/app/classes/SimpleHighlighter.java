package com.maker.android.app.classes;

import android.graphics.Typeface;
import android.text.SpannableStringBuilder;
import android.text.Spanned;
import android.text.style.StyleSpan;

import java.lang.reflect.Array;
import java.util.LinkedHashMap;
import java.util.Map;


@SuppressWarnings({"unused"})
public class SimpleHighlighter implements Highlights {

    /**
     * A {@link String} constant to use with {@link android.util.Log}
     */
    private static final String LOG_TAG = SimpleHighlighter.class.getSimpleName();


    /**
     * Returns the highlighted data gor the {@link CatalogueItem}
     *
     * @return {@link String} containing highlight data
     */
    @Override
    public SpannableStringBuilder getHighlights(LinkedHashMap<String, Object> data) {

        StringBuilder stringBuilder = new StringBuilder();

        for (Map.Entry<String, Object> e : data.entrySet()) {

            if (e.getValue() == null) {
                stringBuilder.append(e.getKey()).append(": ").append("N/A").append("\n");
                continue;
            }

            Class clazz = e.getValue().getClass();
            if (clazz.isArray()) {
                Object obj = e.getValue();
                stringBuilder.append(e.getKey()).append(":\n");
                for (int i = 0; i < Array.getLength(obj); i++) {
                    stringBuilder.append(Array.get(obj, i).toString()).append(" , ");
                }
                stringBuilder.delete(stringBuilder.length() - 2, stringBuilder.length());
                stringBuilder.append("\n");
            } else {
                stringBuilder.append(e.getKey()).append(": ").append(e.getValue().toString()).append("\n");
            }

        }

        stringBuilder = new StringBuilder(stringBuilder.toString().trim());

        SpannableStringBuilder spannableStringBuilder = new SpannableStringBuilder(stringBuilder.toString());
        for (Map.Entry<String, Object> e : data.entrySet()) {

            int pos = stringBuilder.toString().indexOf(e.getKey());
            spannableStringBuilder
                    .setSpan(new StyleSpan(Typeface.BOLD), pos, pos + e.getKey().length(), Spanned.SPAN_INCLUSIVE_INCLUSIVE);

        }

        return spannableStringBuilder;

    }

}
