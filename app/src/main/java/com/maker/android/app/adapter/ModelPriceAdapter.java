package com.maker.android.app.adapter;


import android.content.Context;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.net.Uri;
import android.support.v7.widget.CardView;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.bumptech.glide.Glide;
import com.maker.android.app.R;
import com.maker.android.app.classes.Offer;

import java.util.ArrayList;


@SuppressWarnings({"unused"})
public class ModelPriceAdapter extends RecyclerView.Adapter<ModelPriceAdapter.PriceViewHolder> {

    /**
     * A {@link String} constant to use as tag in {@link android.util.Log}
     */
    private static final String LOG_TAG = ModelPriceAdapter.class.getSimpleName();

    private Context mContext;
    private ArrayList<Offer> mOffersDataSet;


    public ModelPriceAdapter(Context context, ArrayList<Offer> offersDataSet) {
        mContext = context;
        mOffersDataSet = offersDataSet;
    }


    @Override
    public PriceViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {

        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.fragment_model_detail_price_item, parent, false);

        return new PriceViewHolder(view);
    }


    @Override
    public void onBindViewHolder(PriceViewHolder holder, int position) {

        final Offer currentItem = mOffersDataSet.get(position);

        holder.mTextViewCompanyName.setText(currentItem.getPartnerName());

        holder.mTextViewPrice.setText(currentItem.getFormattedPrice());

        Glide
                .with(mContext)
                .load(mContext.getString(R.string.url_base_for_partner_logo, currentItem.getPartnerName()))
                .into(holder.mImageViewLogo);

        holder.mCardView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                if (currentItem.getUrl() == null) {
                    Toast.makeText(mContext, "Link not present!", Toast.LENGTH_SHORT).show();
                    return;
                }

                Intent browserIntent = new Intent(Intent.ACTION_VIEW);

                // If a web browser is not available go to Google Play's chrome app page
                PackageManager packageManager = mContext.getPackageManager();
                if (browserIntent.resolveActivity(packageManager) == null) {
                    Uri marketUri = Uri.parse(
                            "market://search?q=pname:com.android.chrome"
                    );
                    Intent marketIntent = new Intent(Intent.ACTION_VIEW, marketUri);

                    if (marketIntent.resolveActivity(packageManager) == null) {
                        Toast.makeText(mContext, "Market Client Not Available!", Toast.LENGTH_SHORT).show();
                    }

                    mContext.startActivity(marketIntent);
                } else {
                    browserIntent.setData(Uri.parse(currentItem.getUrl()));
                    mContext.startActivity(browserIntent);
                }

            }
        });

    }


    @Override
    public int getItemCount() {
        return mOffersDataSet.size();
    }


    class PriceViewHolder extends RecyclerView.ViewHolder {

        CardView mCardView;
        ImageView mImageViewLogo;
        TextView mTextViewCompanyName;
        TextView mTextViewPrice;

        PriceViewHolder(View itemView) {
            super(itemView);

            mCardView = (CardView) itemView.findViewById(R.id.fragment_model_detail_price_cv);
            mImageViewLogo = (ImageView) itemView.findViewById(R.id.fragment_model_detail_price_imgv_logo);
            mTextViewCompanyName = (TextView) itemView.findViewById(R.id.fragment_model_detail_price_tv_name);
            mTextViewPrice = (TextView) itemView.findViewById(R.id.fragment_model_detail_price_tv_price);
        }

    }

}

