package com.maker.android.app.fragment;

import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v7.widget.DividerItemDecoration;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ProgressBar;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.google.gson.JsonObject;
import com.maker.android.app.R;
import com.maker.android.app.activity.ModelDetailsActivity;
import com.maker.android.app.adapter.ModelSpecsAdapter;
import com.maker.android.app.classes.hardware.Hardware;
import com.maker.android.app.classes.hardware.HardwareFactory;
import com.maker.android.app.provider.PreserveContract;

/**
 * A simple {@link Fragment} subclass.
 * to handle interaction events.
 */
@SuppressWarnings({"unused"})
public class ModelDetailsSpecsFragment extends Fragment {

    /**
     * A {@link String} constant to use in {@link android.util.Log}
     */
    private static final String LOG_TAG = ModelDetailsSpecsFragment.class.getSimpleName();


    public ModelDetailsSpecsFragment() {
        // Required empty public constructor
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_model_detail_specs, container, false);

        RecyclerView recyclerView = (RecyclerView) view.findViewById(R.id.fragment_model_detail_spec_rv);
        recyclerView.addItemDecoration(new DividerItemDecoration(getContext(), DividerItemDecoration.VERTICAL));
        ProgressBar progressBar = (ProgressBar) view.findViewById(R.id.fragment_model_detail_spec_pb_loading);

        String hardwareType = getArguments().getString(ModelDetailsActivity.MODEL_TYPE);

        try {

            PreserveContract.HardwareTypeEntry.HardwareType hwType =
                    PreserveContract.HardwareTypeEntry.HardwareType.getEnumFromName(hardwareType);

            // Gets the class associated with the current Hardware Type Component
            // No need to obtain enum since class names are stored in the database
            final Class clazz = HardwareFactory.getHardwareClass(hwType.name());

            // If null is returned throw new NullPointerException
            if (clazz == null) {
                throw new NullPointerException();
            }

            // Creates a GSON instance with specified TypeAdapter
            Gson gson = new GsonBuilder().registerTypeAdapter(clazz, clazz.getDeclaredField("sDeserialize").get(null)).create();

            // Creates a JSONObject from the response
            JsonObject jsonObject = gson.fromJson(getArguments().getString(ModelDetailsActivity.SHOW_DETAILS_DATA), JsonObject.class);

            Hardware product = gson.fromJson(
                    jsonObject, HardwareFactory.getHardwareTypeToken(hwType.name())
            );

            ModelSpecsAdapter specAdapter = new ModelSpecsAdapter(Hardware.getAsLinkedHashMap(product, false));

            // Sets the adapter of the RecyclerView to the newly created one
            recyclerView.setAdapter(specAdapter);

            // As data is downloaded and ready to be shown, make the ProgressBar GONE
            progressBar.setVisibility(View.GONE);

            // Make the RecyclerView visible
            recyclerView.setVisibility(View.VISIBLE);

            // Fetches JSONException that might occur while parsing JSON data
        } catch (IllegalAccessException | NoSuchFieldException | NullPointerException exc) {
            exc.printStackTrace();
        }

        return view;
    }

}
