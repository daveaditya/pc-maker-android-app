package com.maker.android.app.classes;


import android.os.Parcel;
import android.os.Parcelable;

import com.google.gson.annotations.SerializedName;


/**
 * Holds the data obtained from server when the components table is queried
 */
@SuppressWarnings({"unused", "WeakerAccess"})
public class CatalogueItem implements Parcelable {

    /**
     * A {@link String} constant to use as tag in {@link android.util.Log}
     */
    private static final String LOG_TAG = CatalogueItem.class.getSimpleName();

    /**
     * Stores the title of the {@link CatalogueItem}
     */
    @SerializedName("component_name")
    private String mTitle;

    /**
     * Stores the URL of the {@link CatalogueItem}
     */
    @SerializedName("img_src")
    private String mImageURL;
    /**
     * Stores the endpoint access name of the {@link CatalogueItem}
     */
    @SerializedName("endpoint")
    private String mEndpoint;
    /**
     * Generates instance of {@link CatalogueItem} from {@link Parcel}
     */
    public static final Creator<CatalogueItem> CREATOR = new Creator<CatalogueItem>() {

        /**
         * Creates a {@link CatalogueItem} object from {@link Parcel} using constructor
         *
         * @param in Parcel containing the data
         * @return {@link CatalogueItem} generated from the {@link Parcel}
         */
        @Override
        public CatalogueItem createFromParcel(Parcel in) {
            return new CatalogueItem(in);
        }


        /**
         * Creates an array of {@link CatalogueItem} of the specified size
         *
         * @param size Size of array to generate
         * @return An array of {@link CatalogueItem} objects
         */
        @Override
        public CatalogueItem[] newArray(int size) {
            return new CatalogueItem[size];
        }

    };


    /**
     * Constructs a {@link CatalogueItem} with given title and url
     *
     * @param title    Title of the {@link CatalogueItem}
     * @param endpoint Endpoint to access {@link CatalogueItem}
     * @param ImageURL ImageURL of the {@link CatalogueItem} image
     */
    public CatalogueItem(String title, String endpoint, String ImageURL) {
        mTitle = title;
        mEndpoint = endpoint;
        mImageURL = ImageURL;
    }


    /**
     * Constructs a {@link CatalogueItem} from {@link Parcel}
     *
     * @param in {@link Parcel} containing the data
     */
    protected CatalogueItem(Parcel in) {
        mTitle = in.readString();
        mImageURL = in.readString();
        mEndpoint = in.readString();
    }


    /**
     * Returns the title of {@link CatalogueItem}
     *
     * @return String title of {@link CatalogueItem}
     */
    public String getTitle() {
        return mTitle;
    }


    /**
     * Sets title of the {@link CatalogueItem}
     *
     * @param title {@link String} Title of the {@link CatalogueItem}
     */
    public void setTitle(String title) {
        mTitle = title;
    }


    /**
     * Returns the {@link String} representation of image url
     *
     * @return String url of the {@link CatalogueItem}'s image
     */
    public String getImageURL() {
        return mImageURL;
    }


    /**
     * Sets the {@link String} representation of the image url
     *
     * @param imageURL String url of the {@link CatalogueItem}'s image
     */
    public void setImageURL(String imageURL) {
        mImageURL = imageURL;
    }

    /**
     * Returns the endpoint access {@link String}
     *
     * @return Endpoint to access the {@link CatalogueItem} on server
     */
    public String getEndpoint() {
        return mEndpoint;
    }


    /**
     * Sets the endpoint access {@link String} of {@link CatalogueItem}
     *
     * @param endpoint {@link String} to access the {@link CatalogueItem} list
     */
    public void setEndpoint(String endpoint) {
        mEndpoint = endpoint;
    }


    /**
     * Describes the special object for {@link Parcelable}
     *
     * @return int 0 as nothing special is there
     */
    @Override
    public int describeContents() {
        return 0;
    }


    /**
     * Writes the {@link CatalogueItem} to a {@link Parcel}
     *
     * @param dest  {@link Parcel} to which data is to be written
     * @param flags Special flags if any
     */
    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeString(mTitle);
        dest.writeString(mImageURL);
        dest.writeString(mEndpoint);
    }

}
