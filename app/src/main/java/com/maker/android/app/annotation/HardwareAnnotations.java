package com.maker.android.app.annotation;

import java.lang.annotation.Annotation;
import java.lang.annotation.ElementType;
import java.lang.annotation.Inherited;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Target;


public class HardwareAnnotations {

    /**
     * Marker {@link Annotation} to declare that the FIELD should not be displayed to user
     */
    @Inherited
    @Retention(RetentionPolicy.RUNTIME)
    @Target(ElementType.FIELD)
    public @interface DontShowThis {
    }


    /**
     * Multi Value {@link Annotation} to represent the display name of the FIELD
     */
    @Inherited
    @Retention(RetentionPolicy.RUNTIME)
    @Target(ElementType.FIELD)
    public @interface DisplayData {
        String name() default "Something";

        String unit() default "Something";

        boolean displayInHighlights() default false;
    }

}