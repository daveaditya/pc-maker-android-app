package com.maker.android.app.classes.hardware;

enum Sockets {

    AMD_754, AMD_939, AMD_940, AM1, AM2, AM2_PLUS, AM3, AM3_PLUS, AM4, BGA_413, C32, FM_PLUS,
    FM1, FM2, FM2_PLUS, G34, LGA_1150, LGA_1151, LGA_1155, LGA_1156, LGA_1366, LGA_2011,
    LGA_2011_V3, LGA_2011_V3_NARROW, LGA_771, LGA_775, PGA_478;

    @Override
    public String toString() {
        switch (this) {
            case AMD_754:
                return "AMD 754";
            case AMD_939:
                return "AMD 939";
            case AMD_940:
                return "AMD 940";
            case AM1:
                return "AM1";
            case AM2:
                return "AM2";
            case AM2_PLUS:
                return "AM2+";
            case AM3:
                return "AM3";
            case AM3_PLUS:
                return "AM3+";
            case AM4:
                return "AM4";
            case BGA_413:
                return "BGA 413";
            case C32:
                return "C32";
            case FM_PLUS:
                return "FM+";
            case FM1:
                return "FM1";
            case FM2:
                return "FM2";
            case FM2_PLUS:
                return "FM2+";
            case G34:
                return "G34";
            case LGA_1150:
                return "LGA 1150";
            case LGA_1151:
                return "LGA 1151";
            case LGA_1155:
                return "LGA 1155";
            case LGA_1156:
                return "LGA 1156";
            case LGA_1366:
                return "LGA 1366";
            case LGA_2011:
                return "LGA 2011";
            case LGA_2011_V3:
                return "LGA 2011-v3";
            case LGA_2011_V3_NARROW:
                return "LGA 2011-v3 Narrow";
            case LGA_771:
                return "LGA 771";
            case LGA_775:
                return "LGA 775";
            case PGA_478:
                return "PGA 478";
            default:
                throw new EnumConstantNotPresentException(Sockets.class, "error");
        }

    }

    public static Sockets getEnumFromString(String name) {

        switch (name) {
            case "754":
                return AMD_754;
            case "939":
                return AMD_939;
            case "940":
                return AMD_940;
            case "AM1":
                return AM1;
            case "AM2":
                return AM2;
            case "AM2+":
                return AM2_PLUS;
            case "AM3":
                return AM3;
            case "AM3+":
                return AM3_PLUS;
            case "AM4":
                return AM4;
            case "BGA 413":
                return BGA_413;
            case "C32":
                return C32;
            case "FM+":
                return FM_PLUS;
            case "FM1":
                return FM1;
            case "FM2":
                return FM2;
            case "FM2+":
                return FM2_PLUS;
            case "G34":
                return G34;
            case "LGA 1150":
                return LGA_1150;
            case "LGA 1151":
                return LGA_1151;
            case "LGA 1155":
                return LGA_1155;
            case "LGA 1156":
                return LGA_1156;
            case "LGA 1366":
                return LGA_1366;
            case "LGA 2011":
                return LGA_2011;
            case "LGA 2011-v3":
                return LGA_2011_V3;
            case "LGA 2011-v3 Narrow":
                return LGA_2011_V3_NARROW;
            case "LGA 771":
                return LGA_771;
            case "LGA 775":
                return LGA_775;
            case "PGA 478":
                return PGA_478;
            default:
                throw new EnumConstantNotPresentException(Sockets.class, name);
        }

    }
}
