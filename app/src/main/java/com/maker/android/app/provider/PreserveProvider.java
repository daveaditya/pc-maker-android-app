package com.maker.android.app.provider;

import android.content.ContentProvider;
import android.content.ContentUris;
import android.content.ContentValues;
import android.content.UriMatcher;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.net.Uri;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.util.Log;

import java.io.IOException;


public class PreserveProvider extends ContentProvider {

    /**
     * A {@link String} constant to use as tag in {@link android.util.Log}
     */
    private static final String LOG_TAG = PreserveProvider.class.getSimpleName();

    /**
     * {@link UriMatcher} object to match all the objects
     */
    private static final UriMatcher sURI_MATCHER = new UriMatcher(UriMatcher.NO_MATCH);

    private static final int HW_TYPES = 100;

    private static final int HW_TYPES_ID = 101;

    private static final int MY_BUILDS = 200;

    private static final int MY_BUILDS_ID = 201;

    private static final int COMPARES = 300;

    private static final int COMPARES_ID = 301;

    private static final int COMPARES_HW_TYPE = 302;

    private static final int BUILDS = 400;

    private static final int BUILDS_ID = 401;

    private static final String SQL_COMPARED_HW = "SELECT " + PreserveContract.HardwareTypeEntry.TABLE_NAME + "." + PreserveContract.HardwareTypeEntry.COLUMN_HW_TYPE_NAME
            + ", " + PreserveContract.HardwareTypeEntry.TABLE_NAME + "." + PreserveContract.HardwareTypeEntry.COLUMN_HW_IMAGE_SRC
            + " FROM " + PreserveContract.HardwareTypeEntry.TABLE_NAME + " LEFT OUTER JOIN " + PreserveContract.CompareEntry.TABLE_NAME
            + " ON " + PreserveContract.HardwareTypeEntry.TABLE_NAME + "." + PreserveContract.HardwareTypeEntry.COLUMN_HW_TYPE_NAME + "="
            + PreserveContract.CompareEntry.TABLE_NAME + "." + PreserveContract.CompareEntry.COLUMN_COMPARE_HW_TYPE
            + " GROUP BY " + PreserveContract.CompareEntry.COLUMN_COMPARE_HW_TYPE
            + " HAVING " + PreserveContract.CompareEntry.COLUMN_COMPARE_MODEL_DETAILS + " IS NOT NULL";

    static {
        sURI_MATCHER.addURI(PreserveContract.CONTENT_AUTHORITY, PreserveContract.PATH_HARDWARE_TYPE, HW_TYPES);
        sURI_MATCHER.addURI(PreserveContract.CONTENT_AUTHORITY, PreserveContract.PATH_HARDWARE_TYPE + "/#", HW_TYPES_ID);

        sURI_MATCHER.addURI(PreserveContract.CONTENT_AUTHORITY, PreserveContract.PATH_BUILDS, BUILDS);
        sURI_MATCHER.addURI(PreserveContract.CONTENT_AUTHORITY, PreserveContract.PATH_BUILDS + "/#", BUILDS_ID);

        sURI_MATCHER.addURI(PreserveContract.CONTENT_AUTHORITY, PreserveContract.PATH_COMPARE, COMPARES);
        sURI_MATCHER.addURI(PreserveContract.CONTENT_AUTHORITY, PreserveContract.PATH_COMPARE + "/#", COMPARES_ID);
        sURI_MATCHER.addURI(PreserveContract.CONTENT_AUTHORITY, PreserveContract.PATH_COMPARE + "/HW_TYPE", COMPARES_HW_TYPE);

        sURI_MATCHER.addURI(PreserveContract.CONTENT_AUTHORITY, PreserveContract.PATH_MY_BUILDS, MY_BUILDS);
        sURI_MATCHER.addURI(PreserveContract.CONTENT_AUTHORITY, PreserveContract.PATH_MY_BUILDS + "/#", MY_BUILDS_ID);
    }

    /**
     * {@link PreserveHelper}
     */
    private PreserveHelper mDbHelper;

    @Override
    public boolean onCreate() {
        try {
            mDbHelper = new PreserveHelper(getContext());
        } catch (IOException exc) {
            Log.e(LOG_TAG, "Cannot create database!!!");
            exc.printStackTrace();
        }
        return true;
    }


    @Nullable
    @Override
    public Cursor query(@NonNull Uri uri, @Nullable String[] projection, @Nullable String selection, @Nullable String[] selectionArgs, @Nullable String sortOrder) {

        SQLiteDatabase database = mDbHelper.getReadableDatabase();

        Cursor cursor;

        switch (sURI_MATCHER.match(uri)) {

            case HW_TYPES:
                cursor = database.query(PreserveContract.HardwareTypeEntry.TABLE_NAME,
                        projection, selection, selectionArgs, null, null, sortOrder);
                break;
            case HW_TYPES_ID:
                selection = PreserveContract.HardwareTypeEntry._ID + "=?";
                selectionArgs = new String[]{
                        String.valueOf(ContentUris.parseId(uri))
                };

                cursor = database.query(PreserveContract.HardwareTypeEntry.TABLE_NAME,
                        projection, selection, selectionArgs, null, null, sortOrder);

                break;
            case BUILDS:
                cursor = database.query(PreserveContract.BuildEntry.TABLE_NAME,
                        projection, selection, selectionArgs, null, null, sortOrder);
                break;
            case BUILDS_ID:
                selection = PreserveContract.BuildEntry._ID + "=?";
                selectionArgs = new String[]{
                        String.valueOf(ContentUris.parseId(uri))
                };

                cursor = database.query(PreserveContract.BuildEntry.TABLE_NAME,
                        projection, selection, selectionArgs, null, null, sortOrder);

                break;
            case COMPARES:
                cursor = database.query(PreserveContract.CompareEntry.TABLE_NAME,
                        projection, selection, selectionArgs, null, null, sortOrder);
                break;
            case COMPARES_ID:
                selection = PreserveContract.CompareEntry._ID + "=?";
                selectionArgs = new String[]{
                        String.valueOf(ContentUris.parseId(uri))
                };

                cursor = database.query(PreserveContract.CompareEntry.TABLE_NAME,
                        projection, selection, selectionArgs, null, null, sortOrder);

                break;
            case COMPARES_HW_TYPE:
                cursor = database.rawQuery(SQL_COMPARED_HW, selectionArgs);
                break;
            case MY_BUILDS:
                cursor = database.query(PreserveContract.MyBuildsEntry.TABLE_NAME,
                        projection, selection, selectionArgs, null, null, sortOrder);
                break;
            case MY_BUILDS_ID:
                selection = PreserveContract.MyBuildsEntry._ID + "=?";
                selectionArgs = new String[]{
                        String.valueOf(ContentUris.parseId(uri))
                };

                cursor = database.query(PreserveContract.MyBuildsEntry.TABLE_NAME,
                        projection, selection, selectionArgs, null, null, sortOrder);

                break;

            default:
                throw new IllegalArgumentException("Cannot query unknown URI: " + uri);
        }

        if (cursor != null && getContext() != null) {
            try {
                cursor.setNotificationUri(getContext().getContentResolver(), uri);
            } catch (Exception exc) {
                exc.printStackTrace();
            }
        }

        return cursor;
    }


    @Nullable
    @Override
    public String getType(@NonNull Uri uri) {

        switch (sURI_MATCHER.match(uri)) {
            case HW_TYPES:
                return PreserveContract.HardwareTypeEntry.CONTENT_LIST_TYPE;
            case HW_TYPES_ID:
                return PreserveContract.HardwareTypeEntry.CONTENT_ITEM_TYPE;
            case BUILDS:
                return PreserveContract.BuildEntry.CONTENT_LIST_TYPE;
            case BUILDS_ID:
                return PreserveContract.BuildEntry.CONTENT_ITEM_TYPE;
            case COMPARES:
                return PreserveContract.CompareEntry.CONTENT_LIST_TYPE;
            case COMPARES_ID:
                return PreserveContract.CompareEntry.CONTENT_ITEM_TYPE;
            case COMPARES_HW_TYPE:
                return PreserveContract.CompareEntry.CONTENT_HW_TYPE_LIST;
            case MY_BUILDS:
                return PreserveContract.MyBuildsEntry.CONTENT_LIST_TYPE;
            case MY_BUILDS_ID:
                return PreserveContract.MyBuildsEntry.CONTENT_ITEM_TYPE;
            default:
                throw new IllegalStateException("Unknown URI: " + uri + " with match " + sURI_MATCHER.match(uri));
        }

    }


    @Nullable
    @Override
    public Uri insert(@NonNull Uri uri, @Nullable ContentValues values) {

        switch (sURI_MATCHER.match(uri)) {

            case BUILDS:
                return insertBuild(uri, values);
            case COMPARES:
                return insertCompare(uri, values);
            case MY_BUILDS:
                return insertMyBuild(uri, values);
            default:
                throw new IllegalArgumentException("Insertion is not supported for: " + uri);
        }

    }


    private Uri insertBuild(Uri uri, ContentValues contentValues) {

        String buildName = contentValues.getAsString(PreserveContract.BuildEntry.COLUMN_BUILD_NAME);
        if (buildName == null) {
            throw new IllegalArgumentException("Build idKey not provided");
        }

        String hardwareType = contentValues.getAsString(PreserveContract.BuildEntry.COLUMN_BUILD_HW_TYPE);
        if (hardwareType == null) {
            throw new IllegalArgumentException("Hardware Type not provided");
        }

        try {
            PreserveContract.HardwareTypeEntry.HardwareType.valueOf(hardwareType);
        } catch (EnumConstantNotPresentException exc) {
            throw new IllegalArgumentException("No Such Hardware Type");
        }

        String hardwareDesc = contentValues.getAsString(PreserveContract.BuildEntry.COLUMN_BUILD_HW_DESC);
        if (hardwareDesc == null) {
            throw new IllegalArgumentException("Hardware Description not provided");
        }

        SQLiteDatabase database = mDbHelper.getWritableDatabase();

        long id = database.insertOrThrow(PreserveContract.BuildEntry.TABLE_NAME, null, contentValues);

        if (id == -1) {
            Log.e(LOG_TAG, "Insert failed for: " + uri);
        }

        if (getContext() != null) {
            getContext().getContentResolver().notifyChange(uri, null);
        }

        return ContentUris.withAppendedId(uri, id);

    }


    private Uri insertCompare(Uri uri, ContentValues contentValues) {

        String hardwareType = contentValues.getAsString(PreserveContract.CompareEntry.COLUMN_COMPARE_HW_TYPE);
        if (hardwareType == null) {
            throw new IllegalArgumentException("Hardware Type not provided");
        }

        String modelDetails = contentValues.getAsString(PreserveContract.CompareEntry.COLUMN_COMPARE_MODEL_DETAILS);
        if (modelDetails == null) {
            throw new IllegalArgumentException("InSpecSheet details not provided");
        }

        SQLiteDatabase database = mDbHelper.getWritableDatabase();

        Long id = database.insertOrThrow(PreserveContract.CompareEntry.TABLE_NAME, null, contentValues);

        if (id == -1) {
            Log.e(LOG_TAG, "Insertion failed for: " + uri);
            return null;
        }

        if (getContext() != null) {
            getContext().getContentResolver().notifyChange(uri, null);
        }

        return ContentUris.withAppendedId(uri, id);

    }


    @Nullable
    private Uri insertMyBuild(Uri uri, ContentValues contentValues) {

        String buildName = contentValues.getAsString(PreserveContract.MyBuildsEntry.COLUMN_MY_BUILDS_NAME);
        if (buildName == null) {
            throw new IllegalArgumentException("Build idKey not provided");
        }

        SQLiteDatabase database = mDbHelper.getWritableDatabase();

        long id = database.insertOrThrow(PreserveContract.MyBuildsEntry.TABLE_NAME, null, contentValues);

        if (id == -1) {
            Log.e(LOG_TAG, "insertion failed for: " + uri);
            return null;
        }

        if (getContext() != null) {
            getContext().getContentResolver().notifyChange(uri, null);
        }

        return ContentUris.withAppendedId(uri, id);

    }


    @Override
    public int delete(@NonNull Uri uri, @Nullable String selection, @Nullable String[] selectionArgs) {

        SQLiteDatabase database = mDbHelper.getWritableDatabase();

        int rowsDeleted;

        switch (sURI_MATCHER.match(uri)) {

            case BUILDS:

                rowsDeleted = database.delete(PreserveContract.BuildEntry.TABLE_NAME, selection, selectionArgs);
                break;

            case BUILDS_ID:

                rowsDeleted = database.delete(PreserveContract.BuildEntry.TABLE_NAME, null, null);
                break;

            case COMPARES:

                rowsDeleted = database.delete(PreserveContract.CompareEntry.TABLE_NAME, selection, selectionArgs);
                break;

            case COMPARES_ID:

                rowsDeleted = database.delete(PreserveContract.CompareEntry.TABLE_NAME, null, null);

                break;

            case MY_BUILDS:

                rowsDeleted = database.delete(PreserveContract.MyBuildsEntry.TABLE_NAME, selection, selectionArgs);
                break;

            default:
                throw new IllegalArgumentException("Deletion is not supported for: " + uri);
        }


        if (rowsDeleted != 0 && getContext() != null) {
            getContext().getContentResolver().notifyChange(uri, null);
        }

        return rowsDeleted;
    }


    @Override
    public int update(@NonNull Uri uri, @Nullable ContentValues values, @Nullable String selection, @Nullable String[] selectionArgs) {

        switch (sURI_MATCHER.match(uri)) {

            case MY_BUILDS:
                return updateMyBuilds(uri, values, selection, selectionArgs);
            default:
                throw new IllegalArgumentException("Update not supported for: " + uri);

        }

    }


    private int updateMyBuilds(Uri uri, ContentValues contentValues, String selection, String[] selectionArgs) {

        String buildName = contentValues.getAsString(PreserveContract.MyBuildsEntry.COLUMN_MY_BUILDS_NAME);
        if (buildName == null) {
            throw new IllegalArgumentException("Name not provided.");
        }

        SQLiteDatabase database = mDbHelper.getWritableDatabase();

        int rowsUpdated = database.update(PreserveContract.MyBuildsEntry.TABLE_NAME,
                contentValues, selection, selectionArgs);

        if (rowsUpdated != 0 && getContext() != null) {
            getContext().getContentResolver().notifyChange(uri, null);
        }

        return rowsUpdated;
    }

}
