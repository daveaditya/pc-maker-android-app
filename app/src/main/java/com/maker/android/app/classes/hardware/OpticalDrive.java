package com.maker.android.app.classes.hardware;


import android.os.Parcel;

import com.google.gson.JsonArray;
import com.google.gson.JsonDeserializationContext;
import com.google.gson.JsonDeserializer;
import com.google.gson.JsonElement;
import com.google.gson.JsonObject;
import com.google.gson.JsonParseException;
import com.maker.android.app.annotation.HardwareAnnotations;
import com.maker.android.app.classes.Attribute;
import com.maker.android.app.classes.Offer;
import com.maker.android.app.classes.SimpleHighlighter;
import com.maker.android.app.provider.PreserveContract;


@SuppressWarnings({"unused", "WeakerAccess"})
class OpticalDrive
        extends Hardware {

    /**
     * A {@link String} constant to use as tag for {@link android.util.Log}
     */
    private static final String LOG_TAG = OpticalDrive.class.getSimpleName();

    @HardwareAnnotations.DontShowThis
    public static Deserialize sDeserialize;

    static {
        sDeserialize = new Deserialize();
    }

    @HardwareAnnotations.DisplayData(name = "Type", displayInHighlights = true)
    private String mType;

    @HardwareAnnotations.DisplayData(name = "Has Blu-Ray Support?")
    private boolean mHasBluray;

    @HardwareAnnotations.DisplayData(name = "Connector")
    private ConnectionInterface mConnector;

    @HardwareAnnotations.DisplayData(name = "Dimensions", unit = "mm")
    private String mDimensions;

    @HardwareAnnotations.DisplayData(name = "Supported Operating System")
    private String mSupportedOs;


    public static final Creator<OpticalDrive> CREATOR = new Creator<OpticalDrive>() {

        @Override
        public OpticalDrive createFromParcel(Parcel in) {
            return new OpticalDrive(in);
        }


        @Override
        public OpticalDrive[] newArray(int size) {
            return new OpticalDrive[size];
        }

    };


    private OpticalDrive(int id, String companyName, String model) {
        super(id, companyName, model);
        setHighlighter(new SimpleHighlighter());
    }


    OpticalDrive(Parcel in) {
        super(in);
    }


    @Override
    public int describeContents() {
        return 0;
    }


    @Override
    public PreserveContract.HardwareTypeEntry.HardwareType getHardwareType() {
        return PreserveContract.HardwareTypeEntry.HardwareType.OpticalDrive;
    }


    public String getType() {
        return mType;
    }

    public void setType(String type) {
        mType = type;
    }

    public boolean hasBluray() {
        return mHasBluray;
    }

    public void setHasBluray(boolean hasBluray) {
        mHasBluray = hasBluray;
    }

    public ConnectionInterface getConnector() {
        return mConnector;
    }

    public void setConnector(ConnectionInterface connector) {
        mConnector = connector;
    }

    public String getDimensions() {
        return mDimensions;
    }

    public void setDimensions(String dimensions) {
        mDimensions = dimensions;
    }

    public String getSupportedOs() {
        return mSupportedOs;
    }

    public void setSupportedOs(String supportedOs) {
        mSupportedOs = supportedOs;
    }


    interface Builder {

        Builder setType(String type);

        Builder setHasBluRay(boolean hasBluRay);

        Builder setConnector(ConnectionInterface connector);

        Builder setDimensions(String dimensions);

        Builder setSupportedOs(String supportedOs);

        Builder setAttributes(Attribute attributes);

        Builder setOffers(Offer[] offers);

        OpticalDrive createOpticalDrive();

    }


    private static class OpticalDriveBuilder implements Builder {

        OpticalDrive mOpticalDrive;

        OpticalDriveBuilder(int id, String companyName, String model) {
            mOpticalDrive = new OpticalDrive(id, companyName, model);
        }

        @Override
        public Builder setType(String type) {
            mOpticalDrive.setType(type);
            return this;
        }

        @Override
        public Builder setHasBluRay(boolean hasBluRay) {
            mOpticalDrive.setHasBluray(hasBluRay);
            return this;
        }

        @Override
        public Builder setConnector(ConnectionInterface connector) {
            mOpticalDrive.setConnector(connector);
            return this;
        }

        @Override
        public Builder setDimensions(String dimensions) {
            mOpticalDrive.setDimensions(dimensions);
            return this;
        }

        @Override
        public Builder setSupportedOs(String supportedOs) {
            mOpticalDrive.setSupportedOs(supportedOs);
            return this;
        }

        @Override
        public Builder setAttributes(Attribute attributes) {
            mOpticalDrive.setAttribute(attributes);
            return this;
        }

        @Override
        public Builder setOffers(Offer[] offers) {
            mOpticalDrive.setOffers(offers);
            return this;
        }

        @Override
        public OpticalDrive createOpticalDrive() {
            return mOpticalDrive;
        }

    }


    private static class Deserialize implements JsonDeserializer<OpticalDrive> {

        @Override
        public OpticalDrive deserialize(JsonElement json, java.lang.reflect.Type typeOfT, JsonDeserializationContext context) throws JsonParseException {

            JsonObject jsonObject = json.getAsJsonObject();

            int opticalDriveId = 0;
            String companyName = null, model = null;

            if (jsonObject.has("optical_drive_id") && !jsonObject.get("optical_drive_id").isJsonNull()) {
                opticalDriveId = jsonObject.get("optical_drive_id").getAsInt();
            }

            if (jsonObject.has("company_name") && !jsonObject.get("company_name").isJsonNull()) {
                companyName = jsonObject.get("company_name").getAsString();
            }

            if (jsonObject.has("model") && !jsonObject.get("model").isJsonNull()) {
                model = jsonObject.get("model").getAsString();
            }

            Builder builder = new OpticalDriveBuilder(opticalDriveId, companyName, model);

            if (jsonObject.has("type") && !jsonObject.get("type").isJsonNull()) {
                builder.setType(jsonObject.get("type").getAsString());
            }

            if (jsonObject.has("is_bluray") && !jsonObject.get("is_bluray").isJsonNull()) {
                builder.setHasBluRay(jsonObject.get("is_bluray").getAsBoolean());
            }

            if (jsonObject.has("connector") && !jsonObject.get("connector").isJsonNull()) {
                builder.setConnector(ConnectionInterface.getEnumValue(jsonObject.get("connector").getAsString()));
            }

            if (jsonObject.has("dimensions") && !jsonObject.get("dimensions").isJsonNull()) {
                builder.setDimensions(jsonObject.get("dimensions").getAsString());
            }

            if (jsonObject.has("supported_os") && !jsonObject.get("supported_os").isJsonNull()) {
                builder.setSupportedOs(jsonObject.get("supported_os").getAsString());
            }

            if (jsonObject.has("attributes") && !jsonObject.get("attributes").isJsonNull()) {
                builder.setAttributes(
                        Attribute.getAttributeFromJsonObject(jsonObject.get("attributes").getAsJsonObject())
                );
            }

            if (jsonObject.has("offers") && !jsonObject.get("offers").isJsonNull()) {
                JsonArray jsonArray = jsonObject.get("offers").getAsJsonArray();
                builder.setOffers(Offer.getOffersFromJsonArray(jsonArray));
            }

            return builder.createOpticalDrive();

        }

    }

}
