package com.maker.android.app.classes.hardware;

import android.os.Parcel;

import com.google.gson.JsonArray;
import com.google.gson.JsonDeserializationContext;
import com.google.gson.JsonDeserializer;
import com.google.gson.JsonElement;
import com.google.gson.JsonObject;
import com.google.gson.JsonParseException;
import com.maker.android.app.annotation.HardwareAnnotations;
import com.maker.android.app.classes.Attribute;
import com.maker.android.app.classes.Offer;
import com.maker.android.app.classes.SimpleHighlighter;
import com.maker.android.app.provider.PreserveContract;

import java.lang.reflect.Type;


@SuppressWarnings({"unused", "WeakerAccess"})
class Printer
        extends Hardware {

    /**
     * A {@link String} constant to use as tag for {@link android.util.Log}
     */
    private static final String LOG_TAG = Printer.class.getSimpleName();

    public static Deserialize sDeserialize;

    static {
        sDeserialize = new Deserialize();
    }

    @HardwareAnnotations.DisplayData(name = "Method", displayInHighlights = true)
    private String mMethod;

    @HardwareAnnotations.DisplayData(name = "Speed", unit = "ppm", displayInHighlights = true)
    private int mSpeed;

    @HardwareAnnotations.DisplayData(name = "Functions", displayInHighlights = true)
    private String mFunctions;

    @HardwareAnnotations.DisplayData(name = "Supported OS")
    private String mSupportedOs;

    @HardwareAnnotations.DisplayData(name = "Is USB Connectible?")
    private boolean mIsUsbConnectible;

    @HardwareAnnotations.DisplayData(name = "Has Network Support?")
    private boolean mHasNetworkSupport;

    @HardwareAnnotations.DisplayData(name = "Is WiFi Connectible?")
    private boolean mIsWifiConnectible;


    public static final Creator<Printer> CREATOR = new Creator<Printer>() {

        @Override
        public Printer createFromParcel(Parcel in) {
            return new Printer(in);
        }


        @Override
        public Printer[] newArray(int size) {
            return new Printer[size];
        }

    };


    private Printer(int id, String companyName, String model) {
        super(id, companyName, model);
        setHighlighter(new SimpleHighlighter());

    }


    Printer(Parcel in) {
        super(in);
    }


    @Override
    public int describeContents() {
        return 0;
    }


    @Override
    public PreserveContract.HardwareTypeEntry.HardwareType getHardwareType() {
        return PreserveContract.HardwareTypeEntry.HardwareType.Printer;
    }


    public String getMethod() {
        return mMethod;
    }

    public void setMethod(String method) {
        mMethod = method;
    }

    public int getSpeed() {
        return mSpeed;
    }

    public void setSpeed(int speed) {
        mSpeed = speed;
    }

    public String getFunctions() {
        return mFunctions;
    }

    public void setFunctions(String functions) {
        mFunctions = functions;
    }

    public String getSupportedOs() {
        return mSupportedOs;
    }

    public void setSupportedOs(String supportedOs) {
        mSupportedOs = supportedOs;
    }

    public boolean isUsbConnectible() {
        return mIsUsbConnectible;
    }

    public void setUsbConnectible(boolean usbConnectible) {
        mIsUsbConnectible = usbConnectible;
    }

    public boolean isHasNetworkSupport() {
        return mHasNetworkSupport;
    }

    public void setHasNetworkSupport(boolean hasNetworkSupport) {
        mHasNetworkSupport = hasNetworkSupport;
    }

    public boolean isWifiConnectible() {
        return mIsWifiConnectible;
    }

    public void setWifiConnectible(boolean wifiConnectible) {
        mIsWifiConnectible = wifiConnectible;
    }

    interface Builder {

        Builder setMethod(String method);

        Builder setSpeed(int speed);

        Builder setFunctions(String functions);

        Builder setSupportedOs(String os);

        Builder setIsUsbConnectible(boolean isUsbConnectible);

        Builder setHasNetworkSupport(boolean hasNetworkSupport);

        Builder setIsWifiConnectible(boolean isWifiConnectible);

        Builder setAttributes(Attribute attributes);

        Builder setOffers(Offer[] offers);

        Printer createPrinter();

    }


    private static class PrinterBuilder implements Builder {

        Printer mPrinter;

        PrinterBuilder(int id, String companyName, String model) {
            mPrinter = new Printer(id, companyName, model);
        }

        @Override
        public Builder setMethod(String method) {
            mPrinter.setMethod(method);
            return this;
        }

        @Override
        public Builder setSpeed(int speed) {
            mPrinter.setSpeed(speed);
            return this;
        }

        @Override
        public Builder setFunctions(String functions) {
            mPrinter.setFunctions(functions);
            return this;
        }

        @Override
        public Builder setSupportedOs(String os) {
            mPrinter.setSupportedOs(os);
            return this;
        }

        @Override
        public Builder setIsUsbConnectible(boolean isUsbConnectible) {
            mPrinter.setUsbConnectible(isUsbConnectible);
            return this;
        }

        @Override
        public Builder setHasNetworkSupport(boolean hasNetworkSupport) {
            mPrinter.setHasNetworkSupport(hasNetworkSupport);
            return this;
        }

        @Override
        public Builder setIsWifiConnectible(boolean isWifiConnectible) {
            mPrinter.setWifiConnectible(isWifiConnectible);
            return this;
        }

        @Override
        public Builder setAttributes(Attribute attributes) {
            mPrinter.setAttribute(attributes);
            return this;
        }

        @Override
        public Builder setOffers(Offer[] offers) {
            mPrinter.setOffers(offers);
            return this;
        }

        @Override
        public Printer createPrinter() {
            return mPrinter;
        }

    }


    private static class Deserialize implements JsonDeserializer<Printer> {

        @Override
        public Printer deserialize(JsonElement json, Type typeOfT, JsonDeserializationContext context) throws JsonParseException {

            JsonObject jsonObject = json.getAsJsonObject();

            int printerId = 0;
            String companyName = null, model = null;

            if (jsonObject.has("printer_id") && !jsonObject.get("printer_id").isJsonNull()) {
                printerId = jsonObject.get("printer_id").getAsInt();
            }

            if (jsonObject.has("company_name") && !jsonObject.get("company_name").isJsonNull()) {
                companyName = jsonObject.get("company_name").getAsString();
            }

            if (jsonObject.has("model") && !jsonObject.get("model").isJsonNull()) {
                model = jsonObject.get("model").getAsString();
            }

            Builder builder = new PrinterBuilder(printerId, companyName, model);

            if (jsonObject.has("method") && !jsonObject.get("method").isJsonNull()) {
                builder.setMethod(jsonObject.get("method").getAsString());
            }

            if (jsonObject.has("speed") && !jsonObject.get("speed").isJsonNull()) {
                builder.setSpeed(jsonObject.get("speed").getAsInt());
            }

            if (jsonObject.has("functions") && !jsonObject.get("functions").isJsonNull()) {
                builder.setFunctions(jsonObject.get("functions").getAsString());
            }

            if (jsonObject.has("supported_os") && !jsonObject.get("supported_os").isJsonNull()) {
                builder.setSupportedOs(jsonObject.get("supported_os").getAsString());
            }

            if (jsonObject.has("is_usb_connectible") && !jsonObject.get("is_usb_connectible").isJsonNull()) {
                builder.setIsUsbConnectible(jsonObject.get("is_usb_connectible").getAsBoolean());
            }

            if (jsonObject.has("has_network_support") && !jsonObject.get("has_network_support").isJsonNull()) {
                builder.setHasNetworkSupport(jsonObject.get("has_network_support").getAsBoolean());
            }

            if (jsonObject.has("is_wifi_connectible") && !jsonObject.get("is_wifi_connectible").isJsonNull()) {
                builder.setIsWifiConnectible(jsonObject.get("is_wifi_connectible").getAsBoolean());
            }

            if (jsonObject.has("attributes") && !jsonObject.get("attributes").isJsonNull()) {
                builder.setAttributes(
                        Attribute.getAttributeFromJsonObject(jsonObject.get("attributes").getAsJsonObject())
                );
            }

            if (jsonObject.has("offers") && !jsonObject.get("offers").isJsonNull()) {
                JsonArray jsonArray = jsonObject.get("offers").getAsJsonArray();
                builder.setOffers(Offer.getOffersFromJsonArray(jsonArray));
            }

            return builder.createPrinter();

        }

    }

}
