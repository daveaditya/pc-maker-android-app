package com.maker.android.app.activity;

import android.os.Bundle;
import android.support.design.widget.TabLayout;
import android.support.v4.view.ViewPager;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;

import com.maker.android.app.R;
import com.maker.android.app.adapter.TechSupportPagerAdapter;


@SuppressWarnings({"unused"})
public class TechSupportActivity extends AppCompatActivity {

    /**
     * A {@link String} constant to use as tag in {@link android.util.Log}
     */
    private static final String LOG_TAG = TechSupportActivity.class.getSimpleName();

    private TechSupportPagerAdapter mTechSupportPagerAdapter;
    private ViewPager mViewPager;
    private TabLayout mTabLayout;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_tech_support);

        // Gets a reference to the ToolBar and sets is as ActionBar
        Toolbar toolbar = (Toolbar) findViewById(R.id.activity_tech_support_tb);
        setSupportActionBar(toolbar);

        if (getSupportActionBar() != null) {
            getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        }

        mViewPager = (ViewPager) findViewById(R.id.activity_tech_support_vp);
        mTabLayout = (TabLayout) findViewById(R.id.activity_tech_support_tablayout);

        mTechSupportPagerAdapter = new TechSupportPagerAdapter(getSupportFragmentManager(), mTabLayout.getTabCount());
        mViewPager.setAdapter(mTechSupportPagerAdapter);

        mViewPager.addOnPageChangeListener(new ViewPager.OnPageChangeListener() {
            @Override
            public void onPageScrolled(int position, float positionOffset, int positionOffsetPixels) {

            }

            @Override
            public void onPageSelected(int position) {
                mTabLayout.setScrollPosition(position, 0f, true);
            }

            @Override
            public void onPageScrollStateChanged(int state) {

            }
        });

        mTabLayout.addOnTabSelectedListener(new TabLayout.OnTabSelectedListener() {
            @Override
            public void onTabSelected(TabLayout.Tab tab) {
                mViewPager.setCurrentItem(tab.getPosition());
            }

            @Override
            public void onTabUnselected(TabLayout.Tab tab) {

            }

            @Override
            public void onTabReselected(TabLayout.Tab tab) {

            }
        });

    }

}
