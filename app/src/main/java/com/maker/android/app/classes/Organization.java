package com.maker.android.app.classes;

import android.os.Parcel;
import android.os.Parcelable;

import com.google.gson.annotations.SerializedName;


@SuppressWarnings({"unused", "WeakerAccess"})
public class Organization
        implements Parcelable {

    /**
     * A {@link String} constant to use as tag in {@link android.util.Log}
     */
    private static final String LOG_TAG = Organization.class.getSimpleName();

    @SerializedName("organization_id")
    private int mId;

    @SerializedName("name")
    private String mName;

    @SerializedName("type")
    private String mType;

    @SerializedName("addr1")
    private String mAddr1;

    @SerializedName("addr2")
    private String mAddr2;

    @SerializedName("area")
    private String Area;

    @SerializedName("zipcode")
    private String mZipCode;

    @SerializedName("landline_no")
    private String mLandline;

    @SerializedName("mobile_no_1")
    private String mPrimaryMobile;

    @SerializedName("mobile_no_2")
    private String mSecondaryMobile;

    @SerializedName("email_id")
    private String mEmailId;

    @SerializedName("services")
    private String mServices;

    @SerializedName("description")
    private String mDescription;

    public static final Creator<Organization> CREATOR = new Creator<Organization>() {
        @Override
        public Organization createFromParcel(Parcel in) {
            return new Organization(in);
        }

        @Override
        public Organization[] newArray(int size) {
            return new Organization[size];
        }
    };


    protected Organization(Parcel in) {
        mId = in.readInt();
        mName = in.readString();
        mType = in.readString();
        mAddr1 = in.readString();
        mAddr2 = in.readString();
        Area = in.readString();
        mZipCode = in.readString();
        mLandline = in.readString();
        mPrimaryMobile = in.readString();
        mSecondaryMobile = in.readString();
        mEmailId = in.readString();
        mServices = in.readString();
        mDescription = in.readString();
    }

    public int getId() {
        return mId;
    }

    public void setId(int id) {
        mId = id;
    }

    public String getName() {
        return mName;
    }

    public void setName(String name) {
        mName = name;
    }

    public String getType() {
        return mType;
    }

    public void setType(String type) {
        mType = type;
    }

    public String getAddr1() {
        return mAddr1;
    }

    public void setAddr1(String addr1) {
        mAddr1 = addr1;
    }

    public String getAddr2() {
        return mAddr2;
    }

    public void setAddr2(String addr2) {
        mAddr2 = addr2;
    }

    public String getArea() {
        return Area;
    }

    public void setArea(String area) {
        Area = area;
    }

    public String getZipCode() {
        return mZipCode;
    }

    public void setZipCode(String zipCode) {
        mZipCode = zipCode;
    }

    public String getLandline() {
        return mLandline;
    }

    public void setLandline(String landline) {
        mLandline = landline;
    }

    public String getPrimaryMobile() {
        return mPrimaryMobile;
    }

    public void setPrimaryMobile(String primaryMobile) {
        mPrimaryMobile = primaryMobile;
    }

    public String getSecondaryMobile() {
        return mSecondaryMobile;
    }

    public void setSecondaryMobile(String secondaryMobile) {
        mSecondaryMobile = secondaryMobile;
    }

    public String getEmailId() {
        return mEmailId;
    }

    public void setEmailId(String emailId) {
        mEmailId = emailId;
    }

    public String getServices() {
        return mServices;
    }

    public void setServices(String services) {
        mServices = services;
    }

    public String getDescription() {
        return mDescription;
    }

    public void setDescription(String description) {
        mDescription = description;
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeInt(mId);
        dest.writeString(mName);
        dest.writeString(mType);
        dest.writeString(mAddr1);
        dest.writeString(mAddr2);
        dest.writeString(Area);
        dest.writeString(mZipCode);
        dest.writeString(mLandline);
        dest.writeString(mPrimaryMobile);
        dest.writeString(mSecondaryMobile);
        dest.writeString(mEmailId);
        dest.writeString(mServices);
        dest.writeString(mDescription);
    }
}
