package com.maker.android.app.activity;

import android.os.AsyncTask;
import android.os.Bundle;
import android.support.annotation.IdRes;
import android.support.design.widget.TextInputEditText;
import android.support.design.widget.TextInputLayout;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.text.Editable;
import android.text.TextWatcher;
import android.util.Log;
import android.view.View;
import android.widget.RadioGroup;
import android.widget.Spinner;
import android.widget.Toast;

import com.maker.android.app.R;
import com.maker.android.app.utils.DownloaderUtils;
import com.maker.android.app.utils.Request;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.HashMap;


@SuppressWarnings({"unused"})
public class SignupActivity extends AppCompatActivity {

    /**
     * A {@link String} constant to use as TAG in {@link Log}
     */
    private static final String LOG_TAG = SignupActivity.class.getSimpleName();

    private TextInputEditText mTIETName;
    private TextInputEditText mTIETUsername;
    private TextInputEditText mTIETPassword;
    private TextInputEditText mTIETEmailId;
    private TextInputEditText mTIETAddr1;
    private TextInputEditText mTIETAddr2;
    private TextInputEditText mTIETArea;
    private TextInputEditText mTIETZipCode;
    private TextInputEditText mTIETLandline;
    private TextInputEditText mTIETPrimaryMobile;
    private TextInputEditText mTIETSecondaryMobile;
    private TextInputEditText mTIETServices;
    private TextInputEditText mTIETDescription;

    private Spinner mSpinnerOrgTypeList;
    private RadioGroup mRBGAccountType;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_signup);

        // Gets a reference to the ToolBar and sets is as ActionBar
        Toolbar toolbar = (Toolbar) findViewById(R.id.activity_signup_tb);
        setSupportActionBar(toolbar);

        if (getSupportActionBar() != null) {
            getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        }

        mTIETName = (TextInputEditText) ((TextInputLayout) findViewById(R.id.activity_signup_til_name)).getEditText();
        mTIETUsername = (TextInputEditText) ((TextInputLayout) findViewById(R.id.activity_signup_til_username)).getEditText();
        mTIETPassword = (TextInputEditText) ((TextInputLayout) findViewById(R.id.activity_signup_til_password)).getEditText();
        mTIETEmailId = (TextInputEditText) ((TextInputLayout) findViewById(R.id.activity_signup_til_emailid)).getEditText();
        mTIETAddr1 = (TextInputEditText) ((TextInputLayout) findViewById(R.id.activity_signup_til_addr1)).getEditText();
        mTIETAddr2 = (TextInputEditText) ((TextInputLayout) findViewById(R.id.activity_signup_til_addr2)).getEditText();
        mTIETArea = (TextInputEditText) ((TextInputLayout) findViewById(R.id.activity_signup_til_area)).getEditText();
        mTIETZipCode = (TextInputEditText) ((TextInputLayout) findViewById(R.id.activity_signup_til_zipcode)).getEditText();
        mTIETLandline = (TextInputEditText) ((TextInputLayout) findViewById(R.id.activity_signup_til_landline)).getEditText();
        mTIETPrimaryMobile = (TextInputEditText) ((TextInputLayout) findViewById(R.id.activity_signup_til_primary_mobile)).getEditText();
        mTIETSecondaryMobile = (TextInputEditText) ((TextInputLayout) findViewById(R.id.activity_signup_til_secondary_mobile)).getEditText();
        mTIETServices = (TextInputEditText) ((TextInputLayout) findViewById(R.id.activity_signup_til_services)).getEditText();
        mTIETDescription = (TextInputEditText) ((TextInputLayout) findViewById(R.id.activity_signup_til_description)).getEditText();

        mSpinnerOrgTypeList = (Spinner) findViewById(R.id.activity_signup_spinner_org_list);
        mRBGAccountType = (RadioGroup) findViewById(R.id.activity_signup_rbgrp_type_account);

        mTIETPrimaryMobile.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {
                if (s.length() > 9) {
                    mTIETSecondaryMobile.setEnabled(true);
                }
            }

            @Override
            public void afterTextChanged(Editable s) {

            }
        });

        mRBGAccountType.setOnCheckedChangeListener(new RadioGroup.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(RadioGroup group, @IdRes int checkedId) {
                if (checkedId == R.id.activity_signup_rb_organization) {
                    findViewById(R.id.activity_signup_tv_org_type).setVisibility(View.VISIBLE);
                    findViewById(R.id.activity_signup_spinner_org_list).setVisibility(View.VISIBLE);
                } else {
                    findViewById(R.id.activity_signup_tv_org_type).setVisibility(View.GONE);
                    findViewById(R.id.activity_signup_spinner_org_list).setVisibility(View.GONE);
                }
            }
        });
    }


    public void clearForm(View view) {
        mTIETName.setText("");
        mTIETUsername.setText("");
        mTIETPassword.setText("");
        mTIETEmailId.setText("");
        mTIETAddr1.setText("");
        mTIETAddr2.setText("");
        mTIETArea.setText("");
        mTIETZipCode.setText("");
        mTIETLandline.setText("");
        mTIETPrimaryMobile.setText("");
        mTIETSecondaryMobile.setText("");
        mTIETServices.setText("");
        mTIETDescription.setText("");

        mRBGAccountType.clearCheck();
    }


    public void registerUser(View view) {

        HashMap<String, Object> data = new HashMap<>();

        String name = mTIETName.getText().toString();
        if (name.isEmpty()) {
            mTIETName.setError("Please enter name");
            return;
        }
        data.put("name", name);

        String username = mTIETUsername.getText().toString();
        if (username.isEmpty() || username.length() < 6) {
            mTIETUsername.setError("Length of username should be greater than 6");
            return;
        }
        data.put("username", username);

        String password = mTIETPassword.getText().toString();
        if (password.isEmpty() || password.length() < 8) {
            mTIETPassword.setError("Length of password must be greater than 8");
            return;
        }
        data.put("password", password);

        String requestUrl;
        String orgType;
        switch (mRBGAccountType.getCheckedRadioButtonId()) {
            case R.id.activity_signup_rb_technician:
                requestUrl = getString(R.string.url_technician_signup);
                Toast.makeText(getBaseContext(), "Make sure you have entered full name", Toast.LENGTH_SHORT).show();
                break;
            case R.id.activity_signup_rb_organization:
                requestUrl = getString(R.string.url_organization_signup);
                orgType = (String) mSpinnerOrgTypeList.getSelectedItem();
                if (orgType == null) {
                    Toast.makeText(getBaseContext(), "Select an organization type", Toast.LENGTH_SHORT).show();
                    return;
                }
                data.put("type", orgType);
                break;
            default:
                Toast.makeText(getBaseContext(), "Please select an account type", Toast.LENGTH_SHORT).show();
                return;
        }

        String emailId = mTIETEmailId.getText().toString();
        if (emailId.isEmpty()) {
            mTIETEmailId.setError("Please enter your email address");
            return;
        }
        data.put("email_id", emailId);

        String addr1 = mTIETAddr1.getText().toString();
        if (addr1.isEmpty()) {
            mTIETAddr1.setError("Please enter address line 1");
            return;
        }
        data.put("addr1", addr1);

        String addr2 = mTIETAddr2.getText().toString();
        data.put("addr2", addr2);

        String area = mTIETArea.getText().toString();
        if (area.isEmpty()) {
            mTIETArea.setError("Please enter area");
            return;
        }
        data.put("area", area);

        String zipCode = mTIETZipCode.getText().toString();
        data.put("zipcode", zipCode);

        String landline = mTIETLandline.getText().toString();
        data.put("landline_no", landline);

        String primaryMobile = mTIETPrimaryMobile.getText().toString();
        if (primaryMobile.isEmpty() || primaryMobile.length() < 10) {
            mTIETPrimaryMobile.setError("Please enter at least one correct mobile no.");
        }
        data.put("mobile_no_1", primaryMobile);

        String secondaryMobile = mTIETSecondaryMobile.getText().toString();
        data.put("mobile_no_2", secondaryMobile);

        String services = mTIETServices.getText().toString();
        if (services.isEmpty()) {
            services = null;
        }
        data.put("services", services);

        String description = mTIETDescription.getText().toString();
        if (description.isEmpty()) {
            description = null;
        }
        data.put("description", description);


        Request request = new Request.RequestBuilder()
                .setHttpType(Request.HttpRequestTypes.POST)
                .setURL(requestUrl)
                .setData(data)
                .createRequest();

        new AsyncRegister().execute(request);
    }


    private class AsyncRegister extends AsyncTask<Request, Void, String> {

        @Override
        protected String doInBackground(Request... params) {
            try {
                return DownloaderUtils.performNetworkRequest(params[0]);
            } catch (Exception exc) {
                exc.printStackTrace();
                return null;
            }
        }


        @Override
        protected void onPostExecute(String s) {

            try {

                // If the String obtained by the request is null some error might have occurred
                // Print and return
                if (s == null) {
                    Toast.makeText(getBaseContext(), getResources().getString(R.string.internal_server_error), Toast.LENGTH_SHORT).show();
                    return;
                }

                Log.e(LOG_TAG, s);

                // Gets the feedback object from the response
                JSONObject feedback = new JSONObject(s).getJSONObject("feedback");

                // Gets the response string from feedback i.e. SUCCESS or FAILURE
                String response = feedback.getString("response");
                if (response.equals(getResources().getString(R.string.server_success))) {

                    // Set RESULT_OK as result and finish
                    setResult(RESULT_OK);
                    finish();

                    // If response is fail
                } else {
                    // If the response_type is InvalidUsername show error for username
                    if (feedback.getString("error_type").equals("UsernameTaken")) {
                        mTIETUsername.setError(getResources().getString(R.string.activity_start_invalid_username));
                        // If the response is InvalidPassword show error for password
                    } else if (feedback.getString("error_type").equals("InvalidPassword")) {
                        Toast.makeText(getBaseContext(), "Account creation failed", Toast.LENGTH_SHORT).show();
                    } else if (feedback.getString("error_type").equals("InvalidArgument")) {
                        Toast.makeText(getBaseContext(), "Some error occurred", Toast.LENGTH_SHORT).show();
                    }
                }

            } catch (JSONException exc) {
                exc.printStackTrace();
            }

        }

    }


}
