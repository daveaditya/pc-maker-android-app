package com.maker.android.app.fragment;

import android.app.Activity;
import android.content.Intent;
import android.database.Cursor;
import android.net.Uri;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v4.app.LoaderManager;
import android.support.v4.content.CursorLoader;
import android.support.v4.content.Loader;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;
import android.widget.Toast;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.maker.android.app.R;
import com.maker.android.app.activity.ReviewBuildActivity;
import com.maker.android.app.adapter.BuildDetailsAdapter;
import com.maker.android.app.classes.Organization;
import com.maker.android.app.classes.hardware.Hardware;
import com.maker.android.app.classes.hardware.HardwareFactory;
import com.maker.android.app.provider.PreserveContract;

import java.util.ArrayList;
import java.util.LinkedHashMap;
import java.util.Map;


@SuppressWarnings({"unused"})
public class BuildDetailsFragment
        extends Fragment {

    /**
     * A {@link String} constant to use as tag in {@link android.util.Log}
     */
    private static final String LOG_TAG = BuildDetailsFragment.class.getSimpleName();
    private static final String DEALER_SELECT_DIALOG = "DEALER_SELECT_DIALOG";
    private static final int BUILD_DETAILS_LOADER = 13;
    private static final int REQUEST_DEALER_CONTACT = 51;

    private LinkedHashMap<String, ArrayList<Hardware>> mBuildData;
    private RecyclerView mRecyclerViewBuild;
    private BuildDetailsAdapter mBuildDetailsAdapter;
    private TextView mTextViewEmpty;


    public BuildDetailsFragment() {
    }


    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_build_details, container, false);

        mBuildData = new LinkedHashMap<>();
        mRecyclerViewBuild = (RecyclerView) view.findViewById(R.id.fragment_build_details_rv);
        mTextViewEmpty = (TextView) view.findViewById(R.id.fragment_build_details_tv_empty);

        getActivity()
                .getSupportLoaderManager()
                .restartLoader(BUILD_DETAILS_LOADER, null, new AsyncBuildDetailsLoader())
                .forceLoad();

        return view;
    }


    @Override
    public void onCreateOptionsMenu(Menu menu, MenuInflater inflater) {
        super.onCreateOptionsMenu(menu, inflater);
        menu.findItem(R.id.activity_review_build_action_prev_saved).setVisible(true);
        menu.findItem(R.id.activity_review_build_action_dealer).setVisible(true);
    }


    @Override
    public boolean onOptionsItemSelected(MenuItem item) {

        switch (item.getItemId()) {

            case R.id.activity_review_build_action_delete:

                int rowsDeleted
                        = getContext().
                        getContentResolver()
                        .delete(PreserveContract.BuildEntry.CONTENT_URI,
                                PreserveContract.BuildEntry.COLUMN_BUILD_NAME + "=?",
                                new String[]{
                                        getArguments().getString(ReviewBuildActivity.SELECTED_BUILD)
                                });

                if (rowsDeleted == -1) {
                    Toast.makeText(getContext(), "Some error!", Toast.LENGTH_SHORT).show();
                }

                BuildsListFragment buildsListFragment = new BuildsListFragment();
                buildsListFragment.setHasOptionsMenu(true);

                getActivity()
                        .getSupportFragmentManager()
                        .beginTransaction()
                        .replace(R.id.activity_build_fragment, buildsListFragment)
                        .commit();
                return true;

            case R.id.activity_review_build_action_prev_saved:

                BuildsListFragment buildsListFragmentNew = new BuildsListFragment();
                buildsListFragmentNew.setHasOptionsMenu(true);

                getActivity()
                        .getSupportFragmentManager()
                        .beginTransaction()
                        .replace(R.id.activity_build_fragment, buildsListFragmentNew)
                        .commit();
                return true;

            case R.id.activity_review_build_action_dealer:

                DealerDialogFragment dealerDialogFragment = new DealerDialogFragment();
                dealerDialogFragment.setTargetFragment(this, REQUEST_DEALER_CONTACT);
                dealerDialogFragment.show(getActivity().getSupportFragmentManager(), DEALER_SELECT_DIALOG);
                return true;

            default:
                return false;

        }

    }


    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {

        if (requestCode == DealerDialogFragment.REQUEST_CODE && resultCode == Activity.RESULT_OK) {

            String selectedMode = data.getStringExtra(DealerDialogFragment.SELECTED_MODE);
            Organization organization = data.getParcelableExtra(DealerDialogFragment.SELECTED_ORG);
            switch (selectedMode) {
                case DealerDialogFragment.EMAIL_MODE:
                    sendMail(organization.getEmailId());
                    break;
                case DealerDialogFragment.PHONE_MODE:
                    dialNumber(organization.getPrimaryMobile());
                    break;
            }

        }

    }


    private void dialNumber(String number) {
        Intent intent = new Intent(Intent.ACTION_DIAL, Uri.parse("tel:" + number));
        if (intent.resolveActivity(getContext().getPackageManager()) != null) {
            startActivity(intent);
        } else {
            Toast.makeText(getContext(), "Dialer not present", Toast.LENGTH_SHORT).show();
        }
    }


    private void sendMail(String emailId) {
        Intent intent = new Intent(Intent.ACTION_SENDTO);
        intent.setData(Uri.parse("mailto:"));
        intent.putExtra(Intent.EXTRA_EMAIL, new String[]{emailId});
        StringBuilder bodyBuilder = new StringBuilder();

        for (Map.Entry<String, ArrayList<Hardware>> component : mBuildData.entrySet()) {
            bodyBuilder.append("Component Name: ").append(component.getKey());
            for (int i = 0; i < component.getValue().size(); i++) {
                bodyBuilder
                        .append("\n")
                        .append(component.getValue().get(i).getModelOverviewToSend())
                        .append("\n\n");
            }
        }
        bodyBuilder.append("\n\nSent using PC Maker & Services");

        intent.putExtra(Intent.EXTRA_TEXT, bodyBuilder.toString());
        if (intent.resolveActivity(getContext().getPackageManager()) != null) {
            startActivity(intent);
        } else {
            Toast.makeText(getContext(), "Mail client not present", Toast.LENGTH_SHORT).show();
        }
    }


    public interface OpenModelDetails {
        void openModelDetails(int id, String hardwareType, String model);
    }


    private class AsyncBuildDetailsLoader implements LoaderManager.LoaderCallbacks<Cursor> {

        @Override
        public Loader<Cursor> onCreateLoader(int id, Bundle args) {
            return new CursorLoader(
                    getContext(),
                    PreserveContract.BuildEntry.CONTENT_URI,
                    null,
                    PreserveContract.BuildEntry.COLUMN_BUILD_NAME + "=?",
                    new String[]{
                            getArguments().getString(ReviewBuildActivity.SELECTED_BUILD)
                    },
                    null
            );
        }

        @Override
        public void onLoadFinished(Loader<Cursor> loader, Cursor data) {

            if (data.getCount() == 0) {
                mTextViewEmpty.setVisibility(View.VISIBLE);
                mRecyclerViewBuild.setVisibility(View.GONE);
                return;
            }

            mBuildData.clear();
            while (data.moveToNext()) {

                String hwType = data.getString(data.getColumnIndex(PreserveContract.BuildEntry.COLUMN_BUILD_HW_TYPE));
                String jsonData = data.getString(data.getColumnIndex(PreserveContract.BuildEntry.COLUMN_BUILD_HW_DESC));
                String processorKey = "Processor";

                if (!mBuildData.containsKey(hwType)) {
                    ArrayList<Hardware> hwData = new ArrayList<>();
                    try {
                        // Gets the class associated with the current HardwareType Component
                        // No need to call HardwareType.getEnumFromValue() since class is stored in database
                        final Class clazz = HardwareFactory.getHardwareClass(hwType);

                        // If null is returned throw new NullPointerException
                        if (clazz == null) {
                            throw new NullPointerException();
                        }

                        // Creates a GSON instance with specified TypeAdapter
                        Gson gson = new GsonBuilder().registerTypeAdapter(clazz, clazz.getDeclaredField("sDeserialize").get(null)).create();
                        Hardware hardware = gson.fromJson(jsonData, HardwareFactory.getHardwareTypeToken(hwType));

                        hwData.add(hardware);
                        if (hwType.equals("IntelProcessor") || hwType.equals("AmdProcessor")) {
                            ArrayList<Hardware> temp = mBuildData.get("Processor");
                            if (temp != null) {
                                hwData.addAll(0, temp);
                            }
                            mBuildData.put(processorKey, hwData);

                        } else {
                            mBuildData.put(hwType, hwData);
                        }
                    } catch (NoSuchFieldException | IllegalAccessException exc) {
                        exc.printStackTrace();
                        return;
                    }
                } else {
                    ArrayList<Hardware> hwData;
                    hwData = mBuildData.get(hwType);

                    try {

                        final Class clazz = HardwareFactory.getHardwareClass(hwType);

                        // If null is returned throw new NullPointerException
                        if (clazz == null) {
                            throw new NullPointerException();
                        }

                        // Creates a GSON instance with specified TypeAdapter
                        Gson gson = new GsonBuilder().registerTypeAdapter(clazz, clazz.getDeclaredField("sDeserialize").get(null)).create();
                        Hardware hardware = gson.fromJson(jsonData, HardwareFactory.getHardwareTypeToken(hwType));

                        hwData.add(hardware);
                        mBuildData.put(hwType, hwData);

                    } catch (NoSuchFieldException | IllegalAccessException exc) {
                        exc.printStackTrace();
                        return;
                    }
                }
            }

            mBuildDetailsAdapter = new BuildDetailsAdapter(getContext(), mBuildData);
            mRecyclerViewBuild.setAdapter(mBuildDetailsAdapter);
            mRecyclerViewBuild.setVisibility(View.VISIBLE);
        }

        @Override
        public void onLoaderReset(Loader<Cursor> loader) {
            mBuildData.clear();
            if (mBuildDetailsAdapter != null) {
                mBuildDetailsAdapter.notifyDataSetChanged();
            }
        }
    }

}
