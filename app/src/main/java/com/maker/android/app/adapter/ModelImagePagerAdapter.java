package com.maker.android.app.adapter;

import android.content.Context;
import android.support.constraint.ConstraintLayout;
import android.support.v4.view.PagerAdapter;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;

import com.bumptech.glide.Glide;
import com.maker.android.app.R;

import java.util.ArrayList;


@SuppressWarnings({"unused"})
public class ModelImagePagerAdapter extends PagerAdapter {

    /**
     * A {@link String} constant to use as tag in {@link android.util.Log}
     */
    private static final String LOG_TAG = ModelImagePagerAdapter.class.getSimpleName();

    private Context mContext;
    private LayoutInflater mLayoutInflater;
    private ArrayList<String> mStringUrlArrayList;


    public ModelImagePagerAdapter(Context context, ArrayList<String> stringUrlArrayList) {
        mContext = context;
        mLayoutInflater = (LayoutInflater) mContext.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        mStringUrlArrayList = stringUrlArrayList;
    }


    @Override
    public Object instantiateItem(ViewGroup container, int position) {

        View view = mLayoutInflater.inflate(R.layout.fragment_model_detail_image_item, container, false);

        Glide
                .with(mContext)
                .load(mStringUrlArrayList.get(position))
                .into((ImageView) view.findViewById(R.id.fragment_model_detail_imgv));

        container.addView(view);

        return view;
    }


    @Override
    public int getCount() {
        return mStringUrlArrayList.size();
    }


    @Override
    public boolean isViewFromObject(View view, Object object) {
        return view == object;
    }

    @Override
    public void destroyItem(ViewGroup container, int position, Object object) {
        container.removeView((ConstraintLayout) object);
    }

}
