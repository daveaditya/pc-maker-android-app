package com.maker.android.app.fragment;

import android.content.Context;
import android.database.Cursor;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v4.app.LoaderManager;
import android.support.v4.content.CursorLoader;
import android.support.v4.content.Loader;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.maker.android.app.R;
import com.maker.android.app.adapter.CompareListAdapter;
import com.maker.android.app.classes.ComparedComponent;
import com.maker.android.app.provider.PreserveContract;

import java.util.ArrayList;

@SuppressWarnings({"unused"})
public class CompareListFragment
        extends Fragment {

    /**
     * {@link String} constant to use as tag for {@link android.util.Log}
     */
    private static final String LOG_TAG = CompareListFragment.class.getSimpleName();

    private static final int COMPARE_COMPONENT_LOADER = 8;

    private OnComparedComponentSelected mOnComparedComponentSelected;
    private TextView mTextViewEmpty;
    private RecyclerView mRecyclerViewComparedItems;
    private ArrayList<ComparedComponent> mComparedComponentList;
    private CompareListAdapter mCompareListAdapter;

    public CompareListFragment() {
    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        try {
            mOnComparedComponentSelected = (OnComparedComponentSelected) context;
        } catch (ClassCastException exc) {
            throw new ClassCastException("Activity should implement OnComparedComponentSelected");
        }
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {

        View view = inflater.inflate(R.layout.fragment_compare_list, container, false);

        mTextViewEmpty = (TextView) view.findViewById(R.id.fragment_compare_list_tv_empty);
        mRecyclerViewComparedItems = (RecyclerView) view.findViewById(R.id.fragment_compare_list_rv);
        mComparedComponentList = new ArrayList<>();
        mCompareListAdapter = new CompareListAdapter(getContext(), mOnComparedComponentSelected, mComparedComponentList);
        mRecyclerViewComparedItems.setAdapter(mCompareListAdapter);

        getActivity()
                .getSupportLoaderManager()
                .restartLoader(COMPARE_COMPONENT_LOADER, null, new AsyncListLoader());

        return view;

    }


    @Override
    public void onCreateOptionsMenu(Menu menu, MenuInflater inflater) {
        super.onCreateOptionsMenu(menu, inflater);
        menu.findItem(R.id.activity_compare_menu_action_select_category).setVisible(false);
    }


    @Override
    public boolean onOptionsItemSelected(MenuItem item) {

        switch (item.getItemId()) {
            case R.id.activity_compare_menu_action_discard:
                getContext().getContentResolver().delete(PreserveContract.CompareEntry.CONTENT_URI, "1", null);
                return true;

            default:
                return false;
        }

    }

    public interface OnComparedComponentSelected {

        void getComparedComponent(String component);

    }

    private final class AsyncListLoader implements LoaderManager.LoaderCallbacks<Cursor> {

        @Override
        public Loader<Cursor> onCreateLoader(int id, Bundle args) {
            return new CursorLoader(
                    getContext(),
                    PreserveContract.CompareEntry.CONTENT_URI.buildUpon().appendPath("HW_TYPE").build(),
                    new String[]{PreserveContract.CompareEntry.COLUMN_COMPARE_HW_TYPE},
                    null,
                    null,
                    null
            );
        }


        @Override
        public void onLoadFinished(Loader<Cursor> loader, Cursor data) {

            if (data != null) {

                if (data.getCount() == 0) {
                    mRecyclerViewComparedItems.setVisibility(View.GONE);
                    mTextViewEmpty.setVisibility(View.VISIBLE);
                } else {

                    mComparedComponentList.clear();
                    while (data.moveToNext()) {
                        ComparedComponent item = new ComparedComponent(
                                data.getString(data.getColumnIndex(PreserveContract.HardwareTypeEntry.COLUMN_HW_IMAGE_SRC)), null,
                                PreserveContract.HardwareTypeEntry.HardwareType.valueOf(data.getString(data.getColumnIndex(PreserveContract.HardwareTypeEntry.COLUMN_HW_TYPE_NAME))).toString()
                        );
                        mComparedComponentList.add(item);
                        mCompareListAdapter.notifyDataSetChanged();
                    }

                    mRecyclerViewComparedItems.setVisibility(View.VISIBLE);

                }

            }
        }


        @Override
        public void onLoaderReset(Loader<Cursor> loader) {
            mComparedComponentList.clear();
            if (mCompareListAdapter != null) {
                mCompareListAdapter.notifyDataSetChanged();
            }
        }

    }

}
