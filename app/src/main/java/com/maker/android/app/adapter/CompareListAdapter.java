package com.maker.android.app.adapter;

import android.content.Context;
import android.support.v7.widget.CardView;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.bumptech.glide.Glide;
import com.maker.android.app.R;
import com.maker.android.app.classes.ComparedComponent;
import com.maker.android.app.fragment.CompareListFragment;
import com.maker.android.app.provider.PreserveContract;
import com.maker.android.app.utils.MyUtils;

import java.util.ArrayList;


@SuppressWarnings({"unused"})
public class CompareListAdapter extends RecyclerView.Adapter<CompareListAdapter.ComparedComponentHolder> {

    /**
     * A {@link String} constant to use as tag in {@link android.util.Log}
     */
    private static final String LOG_TAG = CompareListAdapter.class.getSimpleName();

    private Context mContext;
    private ArrayList<ComparedComponent> mDataset;
    private CompareListFragment.OnComparedComponentSelected mOnComparedComponentSelected;

    public CompareListAdapter(Context context, CompareListFragment.OnComparedComponentSelected comparedComponentSelected
            , ArrayList<ComparedComponent> dataset) {
        mContext = context;
        mDataset = dataset;
        mOnComparedComponentSelected = comparedComponentSelected;
    }

    @Override
    public ComparedComponentHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View itemView = LayoutInflater.from(parent.getContext()).inflate(R.layout.fragment_compare_list_item, parent, false);
        return new ComparedComponentHolder(itemView);
    }

    @Override
    public void onBindViewHolder(final ComparedComponentHolder holder, int position) {

        final ComparedComponent currentItem = mDataset.get(position);

        holder.mTextViewType.setText(MyUtils.createWordString(currentItem.getType()));

        Glide
                .with(mContext)
                .load(mContext.getString(R.string.url_base_for_images) + currentItem.getImageSrc())
                .into(holder.mImageViewHardwareImg);

        holder.mCardView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                mOnComparedComponentSelected.getComparedComponent(
                        PreserveContract.HardwareTypeEntry.HardwareType.getEnumFromName(currentItem.getType()).name()
                );
            }
        });

    }

    @Override
    public int getItemCount() {
        return mDataset.size();
    }

    class ComparedComponentHolder extends RecyclerView.ViewHolder {

        TextView mTextViewType;
        ImageView mImageViewHardwareImg;
        CardView mCardView;

        ComparedComponentHolder(View itemView) {
            super(itemView);
            mTextViewType = (TextView) itemView.findViewById(R.id.fragment_compare_list_item_hardware_label);
            mImageViewHardwareImg = (ImageView) itemView.findViewById(R.id.fragment_compare_list_item_hardware_image);
            mCardView = (CardView) itemView.findViewById(R.id.fragment_compare_list_item_cv);
        }
    }

}

