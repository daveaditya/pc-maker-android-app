package com.maker.android.app.adapter;

import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.maker.android.app.R;
import com.maker.android.app.classes.Spec;

import java.util.ArrayList;
import java.util.LinkedHashMap;


@SuppressWarnings({"unused"})
public class ModelSpecsAdapter extends RecyclerView.Adapter<ModelSpecsAdapter.SpecViewHolder> {

    /**
     * A {@link String} constant to use as tag in {@link android.util.Log}
     */
    private static final String LOG_TAG = ModelSpecsAdapter.class.getSimpleName();

    private ArrayList<Spec> mDataSet;

    public ModelSpecsAdapter(LinkedHashMap<String, Object> data) {
        mDataSet = Spec.getArrayList(data);
    }


    @Override
    public SpecViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {

        // Creates a new View using the component_item.xml Layout
        View itemView = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.fragment_model_detail_spec_item, parent, false);

        // Creates a new ComponentViewHolder from the itemView
        // Returns the newly created itemViewHolder
        return new SpecViewHolder(itemView);

    }


    @Override
    public void onBindViewHolder(SpecViewHolder holder, int position) {

        Spec currentItem = mDataSet.get(holder.getAdapterPosition());

        holder.textViewTitle.setText(currentItem.getTitle());
        holder.textViewDetail.setText(currentItem.getDetails());

    }


    @Override
    public int getItemCount() {
        return mDataSet.size();
    }


    class SpecViewHolder extends RecyclerView.ViewHolder {

        TextView textViewTitle;

        TextView textViewDetail;

        SpecViewHolder(View itemView) {
            super(itemView);

            textViewTitle = (TextView) itemView.findViewById(R.id.fragment_model_detail_spec_tv_title);
            textViewDetail = (TextView) itemView.findViewById(R.id.fragment_model_detail_spec_tv_detail);
        }

    }

}

