package com.maker.android.app.classes.hardware;

import android.os.Parcel;

import com.google.gson.JsonArray;
import com.google.gson.JsonDeserializationContext;
import com.google.gson.JsonDeserializer;
import com.google.gson.JsonElement;
import com.google.gson.JsonObject;
import com.google.gson.JsonParseException;
import com.maker.android.app.annotation.HardwareAnnotations;
import com.maker.android.app.classes.Attribute;
import com.maker.android.app.classes.Offer;
import com.maker.android.app.classes.SimpleHighlighter;
import com.maker.android.app.provider.PreserveContract;

import java.lang.reflect.Type;
import java.math.BigDecimal;


@SuppressWarnings({"unused", "WeakerAccess"})
class Motherboard
        extends Hardware {

    /**
     * A {@link String} constant to use as tag for {@link android.util.Log}
     */
    private static final String LOG_TAG = Motherboard.class.getSimpleName();

    public static Deserialize sDeserialize;


    static {
        sDeserialize = new Deserialize();
    }


    @HardwareAnnotations.DisplayData(name = "Chipset", displayInHighlights = true)
    private String mChipsetModel;

    @HardwareAnnotations.DisplayData(name = "Form Factor", displayInHighlights = true)
    private FormFactor mFormFactor;

    @HardwareAnnotations.DisplayData(name = "Number of Memory Slots")
    private int mNoOfMemorySlots;

    @HardwareAnnotations.DisplayData(name = "Memory Type")
    private ConnectionInterface mMemoryType;

    @HardwareAnnotations.DisplayData(name = "Max Supported Memory", unit = "GB")
    private BigDecimal mMaxMemory;

    @HardwareAnnotations.DisplayData(name = "Has ECC Memory Support?")
    private boolean hasEccMemory;

    @HardwareAnnotations.DisplayData(name = "SLI / Crossfire Support")
    private String mSliCrossfireSupport;

    @HardwareAnnotations.DisplayData(name = "Number of SATA 3 Gbps Ports")
    private int mNoOfSata3Gbps;

    @HardwareAnnotations.DisplayData(name = "Number of SATA 6 Gbps Ports")
    private int mNoOfSata6Gbps;

    @HardwareAnnotations.DisplayData(name = "Number of SATA Express Ports")
    private int mNoOfSataExpress;

    @HardwareAnnotations.DisplayData(name = "Number of M2 Ports")
    private int mNoOfM2Ports;

    @HardwareAnnotations.DisplayData(name = "Number of M SATA Ports")
    private int mNoOfMSataPorts;

    @HardwareAnnotations.DisplayData(name = "Number of PCIe x16 Slots")
    private int mNoOfPciex16Slots;

    @HardwareAnnotations.DisplayData(name = "Number of PCIe x8 Slots")
    private int mNoOfPciex8Slots;

    @HardwareAnnotations.DisplayData(name = "Number of PCI Slots")
    private int mNoOfPciSlots;

    @HardwareAnnotations.DisplayData(name = "Has Built-in WiFi")
    private boolean mHasBuiltInWifi;

    @HardwareAnnotations.DisplayData(name = "Has Built-in Bluetooth")
    private boolean mHasBuiltInBluetooth;

    @HardwareAnnotations.DisplayData(name = "Color")
    private String mColor;

    @HardwareAnnotations.DisplayData(name = "Supported Sockets")
    private Sockets[] mSupportedSockets;

    @HardwareAnnotations.DisplayData(name = "Supported USB Versions")
    private ConnectionInterface[] mSupportedUsbVersions;


    public static final Creator<Motherboard> CREATOR = new Creator<Motherboard>() {

        @Override
        public Motherboard createFromParcel(Parcel in) {
            return new Motherboard(in);
        }


        @Override
        public Motherboard[] newArray(int size) {
            return new Motherboard[size];
        }

    };


    private Motherboard(int id, String companyName, String model) {
        super(id, companyName, model);
        setHighlighter(new SimpleHighlighter());
    }


    Motherboard(Parcel in) {
        super(in);
    }


    @Override
    public int describeContents() {
        return 0;
    }


    @Override
    public PreserveContract.HardwareTypeEntry.HardwareType getHardwareType() {
        return PreserveContract.HardwareTypeEntry.HardwareType.Motherboard;
    }


    public String getChipsetModel() {
        return mChipsetModel;
    }

    public void setChipsetModel(String chipsetModel) {
        mChipsetModel = chipsetModel;
    }

    public FormFactor getFormFactor() {
        return mFormFactor;
    }

    public void setFormFactor(FormFactor formFactor) {
        mFormFactor = formFactor;
    }

    public int getNoOfMemorySlots() {
        return mNoOfMemorySlots;
    }

    public void setNoOfMemorySlots(int noOfMemorySlots) {
        mNoOfMemorySlots = noOfMemorySlots;
    }

    public ConnectionInterface getMemoryType() {
        return mMemoryType;
    }

    public void setMemoryType(ConnectionInterface memoryType) {
        mMemoryType = memoryType;
    }

    public BigDecimal getMaxMemory() {
        return mMaxMemory;
    }

    public void setMaxMemory(BigDecimal maxMemory) {
        mMaxMemory = maxMemory;
    }

    public boolean isHasEccMemory() {
        return hasEccMemory;
    }

    public void setHasEccMemory(boolean hasEccMemory) {
        this.hasEccMemory = hasEccMemory;
    }

    public String getSliCrossfireSupport() {
        return mSliCrossfireSupport;
    }

    public void setSliCrossfireSupport(String sliCrossfireSupport) {
        mSliCrossfireSupport = sliCrossfireSupport;
    }

    public int getNoOfSata3Gbps() {
        return mNoOfSata3Gbps;
    }

    public void setNoOfSata3Gbps(int noOfSata3Gbps) {
        mNoOfSata3Gbps = noOfSata3Gbps;
    }

    public int getNoOfSata6Gbps() {
        return mNoOfSata6Gbps;
    }

    public void setNoOfSata6Gbps(int noOfSata6Gbps) {
        mNoOfSata6Gbps = noOfSata6Gbps;
    }

    public int getNoOfSataExpress() {
        return mNoOfSataExpress;
    }

    public void setNoOfSataExpress(int noOfSataExpress) {
        mNoOfSataExpress = noOfSataExpress;
    }

    public int getNoOfM2Ports() {
        return mNoOfM2Ports;
    }

    public void setNoOfM2Ports(int noOfM2Ports) {
        mNoOfM2Ports = noOfM2Ports;
    }

    public int getNoOfMSataPorts() {
        return mNoOfMSataPorts;
    }

    public void setNoOfMSataPorts(int noOfMSataPorts) {
        mNoOfMSataPorts = noOfMSataPorts;
    }

    public int getNoOfPciex16Slots() {
        return mNoOfPciex16Slots;
    }

    public void setNoOfPciex16Slots(int noOfPciex16Slots) {
        mNoOfPciex16Slots = noOfPciex16Slots;
    }

    public int getNoOfPciex8Slots() {
        return mNoOfPciex8Slots;
    }

    public void setNoOfPciex8Slots(int noOfPciex8Slots) {
        mNoOfPciex8Slots = noOfPciex8Slots;
    }

    public int getNoOfPciSlots() {
        return mNoOfPciSlots;
    }

    public void setNoOfPciSlots(int noOfPciSlots) {
        mNoOfPciSlots = noOfPciSlots;
    }

    public boolean isHasBuiltInWifi() {
        return mHasBuiltInWifi;
    }

    public void setHasBuiltInWifi(boolean hasBuiltInWifi) {
        mHasBuiltInWifi = hasBuiltInWifi;
    }

    public boolean isHasBuiltInBluetooth() {
        return mHasBuiltInBluetooth;
    }

    public void setHasBuiltInBluetooth(boolean hasBuiltInBluetooth) {
        mHasBuiltInBluetooth = hasBuiltInBluetooth;
    }

    public String getColor() {
        return mColor;
    }

    public void setColor(String color) {
        mColor = color;
    }

    public Sockets[] getSupportedSockets() {
        return mSupportedSockets;
    }

    public void setSupportedSockets(Sockets[] sockets) {
        mSupportedSockets = sockets;
    }

    public ConnectionInterface[] getSupportedUsbVersions() {
        return mSupportedUsbVersions;
    }

    public void setSupportedUsbVersions(ConnectionInterface[] supportedUsbVersions) {
        mSupportedUsbVersions = supportedUsbVersions;
    }


    enum FormFactor {

        AT("AT"), ATX("ATX"), EATX("EATX"), FLEX_ATX("Flex ATX"), HPTX("HPTX"),
        MICRO_ATX("Micro ATX"), MINI_ATX("Mini ATX"), MINI_DTX("Mini DTX"), MINI_ITX("Mini ITX"),
        NANO_ITX("Nano ITX"), PICO_ITX("Pico ITX"), SSI_CEB("SSI CEB"), SSI_EEB("SSI EEB"),
        SSI_MEB("SSI MEB"), THIN_MINI_ITX("Thin Mini ITX"), XL_ATX("XL ATX");

        String description;

        FormFactor(String description) {
            this.description = description;
        }

        public static Motherboard.FormFactor getEnumValue(String string) throws EnumConstantNotPresentException {

            switch (string) {
                case "AT":
                    return AT;
                case "ATX":
                    return ATX;
                case "EATX":
                    return EATX;
                case "Flex ATX":
                    return FLEX_ATX;
                case "HPTX":
                    return HPTX;
                case "Micro ATX":
                    return MICRO_ATX;
                case "Mini ATX":
                    return MINI_ATX;
                case "Mini DTX":
                    return MINI_DTX;
                case "Mini ITX":
                    return MINI_ITX;
                case "Nano ITX":
                    return NANO_ITX;
                case "Pico ITX":
                    return PICO_ITX;
                case "SSI CEB":
                    return SSI_CEB;
                case "SSI EEB":
                    return SSI_EEB;
                case "SSI MEB":
                    return SSI_MEB;
                case "Thin Mini ITX":
                    return THIN_MINI_ITX;
                case "XL ATX":
                    return XL_ATX;
                default:
                    throw new EnumConstantNotPresentException(FormFactor.class, string);
            }

        }

        @Override
        public String toString() {
            return description;
        }

    }


    interface Builder {

        Builder setChipsetModel(String chipsetModel);

        Builder setFormFactor(FormFactor formFactor);

        Builder setNoOfMemorySlots(int noOfMemorySlots);

        Builder setMemoryType(ConnectionInterface memoryType);

        Builder setMaxMemory(BigDecimal maxMemory);

        Builder setHasEccMemory(boolean hasEccMemory);

        Builder setSliCrossfireSupport(String sliCrossfireSupport);

        Builder setNoOfSata3Gbps(int noOfSata3Gbps);

        Builder setNoOfSata6Gbps(int noOfSata6Gbps);

        Builder setNoOfSataExpress(int noOfSataExpress);

        Builder setNoOfM2Ports(int noOfM2Ports);

        Builder setNoOfMSataPorts(int noOfMSataPorts);

        Builder setNoOfPciex16Slots(int noOfPciex16Slots);

        Builder setNoOfPciex8Slots(int noOfPciex8Slots);

        Builder setNoOfPciSlots(int noOfPciSlots);

        Builder setHasBuiltinWifi(boolean hasBuiltinWifi);

        Builder setHasBuiltinBluetooth(boolean hasBuiltinBluetooth);

        Builder setColor(String color);

        Builder setSupportedSockets(Sockets[] supportedSockets);

        Builder setSupportedUsbVersions(ConnectionInterface[] supportedUsbVersions);

        Builder setAttributes(Attribute attributes);

        Builder setOffers(Offer[] offers);

        Motherboard createMotherboard();

    }


    private static class MotherboardBuilder implements Builder {

        Motherboard mMotherboard;

        MotherboardBuilder(int id, String companyName, String model) {
            mMotherboard = new Motherboard(id, companyName, model);
        }

        @Override
        public Builder setChipsetModel(String chipsetModel) {
            mMotherboard.setChipsetModel(chipsetModel);
            return this;
        }

        @Override
        public Builder setFormFactor(FormFactor formFactor) {
            mMotherboard.setFormFactor(formFactor);
            return null;
        }

        @Override
        public Builder setNoOfMemorySlots(int noOfMemorySlots) {
            mMotherboard.setNoOfMemorySlots(noOfMemorySlots);
            return this;
        }

        @Override
        public Builder setMemoryType(ConnectionInterface memoryType) {
            mMotherboard.setMemoryType(memoryType);
            return this;
        }

        @Override
        public Builder setMaxMemory(BigDecimal maxMemory) {
            mMotherboard.setMaxMemory(maxMemory);
            return this;
        }

        @Override
        public Builder setHasEccMemory(boolean hasEccMemory) {
            mMotherboard.setHasEccMemory(hasEccMemory);
            return this;
        }

        @Override
        public Builder setSliCrossfireSupport(String sliCrossfireSupport) {
            mMotherboard.setSliCrossfireSupport(sliCrossfireSupport);
            return this;
        }

        @Override
        public Builder setNoOfSata3Gbps(int noOfSata3Gbps) {
            mMotherboard.setNoOfSata3Gbps(noOfSata3Gbps);
            return this;
        }

        @Override
        public Builder setNoOfSata6Gbps(int noOfSata6Gbps) {
            mMotherboard.setNoOfSata6Gbps(noOfSata6Gbps);
            return this;
        }

        @Override
        public Builder setNoOfSataExpress(int noOfSataExpress) {
            mMotherboard.setNoOfSataExpress(noOfSataExpress);
            return this;
        }

        @Override
        public Builder setNoOfM2Ports(int noOfM2Ports) {
            mMotherboard.setNoOfM2Ports(noOfM2Ports);
            return this;
        }

        @Override
        public Builder setNoOfMSataPorts(int noOfMSataPorts) {
            mMotherboard.setNoOfMSataPorts(noOfMSataPorts);
            return this;
        }

        @Override
        public Builder setNoOfPciex16Slots(int noOfPciex16Slots) {
            mMotherboard.setNoOfPciex16Slots(noOfPciex16Slots);
            return this;
        }

        @Override
        public Builder setNoOfPciex8Slots(int noOfPciex8Slots) {
            mMotherboard.setNoOfPciex8Slots(noOfPciex8Slots);
            return this;
        }

        @Override
        public Builder setNoOfPciSlots(int noOfPciSlots) {
            mMotherboard.setNoOfPciSlots(noOfPciSlots);
            return this;
        }

        @Override
        public Builder setHasBuiltinWifi(boolean hasBuiltinWifi) {
            mMotherboard.setHasBuiltInWifi(hasBuiltinWifi);
            return this;
        }

        @Override
        public Builder setHasBuiltinBluetooth(boolean hasBuiltinBluetooth) {
            mMotherboard.setHasBuiltInBluetooth(hasBuiltinBluetooth);
            return this;
        }

        @Override
        public Builder setColor(String color) {
            mMotherboard.setColor(color);
            return this;
        }

        @Override
        public Builder setSupportedSockets(Sockets[] supportedSockets) {
            mMotherboard.setSupportedSockets(supportedSockets);
            return this;
        }

        @Override
        public Builder setSupportedUsbVersions(ConnectionInterface[] supportedUsbVersions) {
            mMotherboard.setSupportedUsbVersions(supportedUsbVersions);
            return this;
        }

        @Override
        public Builder setAttributes(Attribute attributes) {
            mMotherboard.setAttribute(attributes);
            return this;
        }

        @Override
        public Builder setOffers(Offer[] offers) {
            mMotherboard.setOffers(offers);
            return this;
        }

        @Override
        public Motherboard createMotherboard() {
            return mMotherboard;
        }

    }


    private static class Deserialize implements JsonDeserializer<Motherboard> {

        @Override
        public Motherboard deserialize(JsonElement json, Type typeOfT, JsonDeserializationContext context) throws JsonParseException {

            JsonObject jsonObject = json.getAsJsonObject();

            int motherboardId = 0;
            String companyName = null, model = null;

            if (jsonObject.has("motherboard_id") && !jsonObject.get("motherboard_id").isJsonNull()) {
                motherboardId = jsonObject.get("motherboard_id").getAsInt();
            }

            if (jsonObject.has("company_name") && !jsonObject.get("company_name").isJsonNull()) {
                companyName = jsonObject.get("company_name").getAsString();
            }

            if (jsonObject.has("model") && !jsonObject.get("model").isJsonNull()) {
                model = jsonObject.get("model").getAsString();
            }

            Builder builder = new MotherboardBuilder(motherboardId, companyName, model);

            if (jsonObject.has("chipset_model") && !jsonObject.get("chipset_model").isJsonNull()) {
                builder.setChipsetModel(jsonObject.get("chipset_model").getAsString());
            }


            if (jsonObject.has("form_factor") && !jsonObject.get("form_factor").isJsonNull()) {
                builder.setFormFactor(FormFactor.getEnumValue(jsonObject.get("form_factor").getAsString()));
            }

            if (jsonObject.has("no_of_memory_slots") && !jsonObject.get("no_of_memory_slots").isJsonNull()) {
                builder.setNoOfMemorySlots(jsonObject.get("no_of_memory_slots").getAsInt());
            }

            if (jsonObject.has("memory_type") && !jsonObject.get("memory_type").isJsonNull()) {
                builder.setMemoryType(ConnectionInterface.getEnumValue(jsonObject.get("memory_type").getAsString()));
            }

            if (jsonObject.has("max_memory") && !jsonObject.get("max_memory").isJsonNull()) {
                builder.setMaxMemory(jsonObject.get("max_memory").getAsBigDecimal());
            }

            if (jsonObject.has("has_ecc_support") && !jsonObject.get("has_ecc_support").isJsonNull()) {
                builder.setHasEccMemory(jsonObject.get("has_ecc_support").getAsBoolean());
            }

            if (jsonObject.has("sli_crossfire_support") && !jsonObject.get("sli_crossfire_support").isJsonNull()) {
                builder.setSliCrossfireSupport(jsonObject.get("sli_crossfire_support").getAsString());
            }

            if (jsonObject.has("no_of_sata_3gbps") && !jsonObject.get("no_of_sata_3gbps").isJsonNull()) {
                builder.setNoOfSata3Gbps(jsonObject.get("no_of_sata_3gbps").getAsInt());
            }

            if (jsonObject.has("no_of_sata_6gbps") && !jsonObject.get("no_of_sata_6gbps").isJsonNull()) {
                builder.setNoOfSata6Gbps(jsonObject.get("no_of_sata_6gbps").getAsInt());
            }

            if (jsonObject.has("no_of_sata_express") && !jsonObject.get("no_of_sata_express").isJsonNull()) {
                builder.setNoOfSataExpress(jsonObject.get("no_of_sata_express").getAsInt());
            }

            if (jsonObject.has("no_of_m2_ports") && !jsonObject.get("no_of_m2_ports").isJsonNull()) {
                builder.setNoOfM2Ports(jsonObject.get("no_of_m2_ports").getAsInt());
            }

            if (jsonObject.has("no_of_msata_ports") && !jsonObject.get("no_of_msata_ports").isJsonNull()) {
                builder.setNoOfMSataPorts(jsonObject.get("no_of_msata_ports").getAsInt());
            }

            if (jsonObject.has("no_of_pcie_x16_slots") && !jsonObject.get("no_of_pcie_x16_slots").isJsonNull()) {
                builder.setNoOfPciex16Slots(jsonObject.get("no_of_pcie_x16_slots").getAsInt());
            }

            if (jsonObject.has("no_of_pcie_x8_slots") && !jsonObject.get("no_of_pcie_x8_slots").isJsonNull()) {
                builder.setNoOfPciex8Slots(jsonObject.get("no_of_pcie_x8_slots").getAsInt());
            }

            if (jsonObject.has("no_of_pci_slots") && !jsonObject.get("no_of_pci_slots").isJsonNull()) {
                builder.setNoOfPciSlots(jsonObject.get("no_of_pci_slots").getAsInt());
            }

            if (jsonObject.has("has_builtin_wifi") && !jsonObject.get("has_builtin_wifi").isJsonNull()) {
                builder.setHasBuiltinWifi(jsonObject.get("has_builtin_wifi").getAsBoolean());
            }

            if (jsonObject.has("has_builtin_bluetooth") && !jsonObject.get("has_builtin_bluetooth").isJsonNull()) {
                builder.setHasBuiltinBluetooth(jsonObject.get("has_builtin_bluetooth").getAsBoolean());
            }

            if (jsonObject.has("color")) {
                if (jsonObject.get("color").isJsonNull()) {
                    builder.setColor("N/A");
                } else {
                    builder.setColor(jsonObject.get("color").getAsString());
                }
            }

            if (jsonObject.has("supported_sockets") && !jsonObject.get("supported_sockets").isJsonNull()) {

                JsonArray jsonArray = jsonObject.get("supported_sockets").getAsJsonArray();

                Sockets[] sockets = new Sockets[jsonArray.size()];
                for (int i = 0; i < jsonArray.size(); i++) {
                    sockets[i] =
                            Sockets.getEnumFromString(jsonArray.get(i).getAsString());
                }

                builder.setSupportedSockets(sockets);
            }

            if (jsonObject.has("supported_usb_vers") && !jsonObject.get("supported_usb_vers").isJsonNull()) {

                JsonArray jsonArray = jsonObject.get("supported_usb_vers").getAsJsonArray();

                ConnectionInterface[] usbVers = new ConnectionInterface[jsonArray.size()];
                for (int i = 0; i < jsonArray.size(); i++) {
                    usbVers[i] =
                            ConnectionInterface.getEnumValue(jsonArray.get(i).getAsString());
                }

                builder.setSupportedUsbVersions(usbVers);
            }

            if (jsonObject.has("attributes") && !jsonObject.get("attributes").isJsonNull()) {
                builder.setAttributes(
                        Attribute.getAttributeFromJsonObject(jsonObject.get("attributes").getAsJsonObject())
                );
            }

            if (jsonObject.has("offers") && !jsonObject.get("offers").isJsonNull()) {
                JsonArray jsonArray = jsonObject.get("offers").getAsJsonArray();
                builder.setOffers(Offer.getOffersFromJsonArray(jsonArray));
            }

            return builder.createMotherboard();

        }

    }

}
