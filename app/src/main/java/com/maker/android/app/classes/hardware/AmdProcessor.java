package com.maker.android.app.classes.hardware;


import android.os.Parcel;

import com.google.gson.JsonArray;
import com.google.gson.JsonDeserializationContext;
import com.google.gson.JsonDeserializer;
import com.google.gson.JsonElement;
import com.google.gson.JsonObject;
import com.google.gson.JsonParseException;
import com.maker.android.app.annotation.HardwareAnnotations;
import com.maker.android.app.classes.Attribute;
import com.maker.android.app.classes.Offer;
import com.maker.android.app.classes.SimpleHighlighter;
import com.maker.android.app.provider.PreserveContract;

import java.lang.reflect.Type;
import java.math.BigDecimal;


@SuppressWarnings({"unused", "WeakerAccess"})
class AmdProcessor
        extends Hardware {

    /**
     * A {@link String} constant to use as tag in {@link android.util.Log}
     */
    private static final String LOG_TAG = AmdProcessor.class.getSimpleName();

    public static Deserialize sDeserialize;

    static {
        sDeserialize = new Deserialize();
    }

    @HardwareAnnotations.DisplayData(name = "Line")
    private String mLine;

    @HardwareAnnotations.DisplayData(name = "Family")
    private String mFamily;

    @HardwareAnnotations.DisplayData(name = "CMOS")
    private String mCmos;

    @HardwareAnnotations.DisplayData(name = "PCIe Revision")
    private ConnectionInterface mPcieRev;

    @HardwareAnnotations.DisplayData(name = "Is Unlocked?")
    private boolean mUnlocked;

    @HardwareAnnotations.DisplayData(name = "L2 Cache Size")
    private String mL2CacheSize;

    @HardwareAnnotations.DisplayData(name = "Package")
    private String mPackage;

    @HardwareAnnotations.DisplayData(name = "Number of CPU Cores", displayInHighlights = true)
    private int mNoOfCpuCores;

    @HardwareAnnotations.DisplayData(name = "Number of Threads")
    private int mNoOfThreads;

    @HardwareAnnotations.DisplayData(name = "Base Frequency", displayInHighlights = true, unit = "GHz")
    private BigDecimal mBaseFrequency;

    @HardwareAnnotations.DisplayData(name = "Turbo Frequency", unit = "GHz")
    private BigDecimal mTurboFrequency;

    @HardwareAnnotations.DisplayData(name = "Thermal Power Design", unit = "W")
    private int mThermalPowerDesign;

    @HardwareAnnotations.DisplayData(name = "Memory Interface")
    private String mMemoryInterface;

    @HardwareAnnotations.DisplayData(name = "Memory Speed", unit = "MHz")
    private int mMemorySpeed;

    @HardwareAnnotations.DisplayData(name = "Number of Memory Channels")
    private int mNoOfMemoryChannels;

    @HardwareAnnotations.DisplayData(name = "Graphics Processor", displayInHighlights = true)
    private String mGraphicsProcessor;

    @HardwareAnnotations.DisplayData(name = "DirectX Version")
    private BigDecimal mDirectXVer;

    @HardwareAnnotations.DisplayData(name = "Graphics Frequency", unit = "MHz")
    private BigDecimal mGFrequency;

    @HardwareAnnotations.DisplayData(name = "Graphics Core Count")
    private int mGraphicsCoreCount;

    @HardwareAnnotations.DisplayData(name = "AMD Virtualization")
    private boolean mHasAmdVirtualization;

    @HardwareAnnotations.DisplayData(name = "Has Cooler?")
    private boolean mHasCooler;

    @HardwareAnnotations.DisplayData(name = "Has DisplayPort")
    private boolean mHasDisplayPort;

    @HardwareAnnotations.DisplayData(name = "Has HDMI?")
    private boolean mHasHdmi;


    AmdProcessor(int id, String model) {
        super(id, "AMD", model);
        setHighlighter(new SimpleHighlighter());
    }


    AmdProcessor(Parcel in) {
        super(in);
    }


    public static final Creator<AmdProcessor> CREATOR = new Creator<AmdProcessor>() {

        @Override
        public AmdProcessor createFromParcel(Parcel in) {
            return new AmdProcessor(in);
        }


        @Override
        public AmdProcessor[] newArray(int size) {
            return new AmdProcessor[size];
        }

    };


    @Override
    public int describeContents() {
        return 0;
    }


    @Override
    public PreserveContract.HardwareTypeEntry.HardwareType getHardwareType() {
        return PreserveContract.HardwareTypeEntry.HardwareType.AmdProcessor;
    }

    public String getLine() {
        return mLine;
    }

    public void setLine(String line) {
        mLine = line;
    }

    public String getFamily() {
        return mFamily;
    }

    public void setFamily(String family) {
        mFamily = family;
    }

    public String getCmos() {
        return mCmos;
    }

    public void setCmos(String cmos) {
        mCmos = cmos;
    }

    public ConnectionInterface getPcieRev() {
        return mPcieRev;
    }

    public void setPcieRev(ConnectionInterface pcieRev) {
        mPcieRev = pcieRev;
    }

    public boolean isUnlocked() {
        return mUnlocked;
    }

    public void setUnlocked(boolean unlocked) {
        mUnlocked = unlocked;
    }

    public String getL2CacheSize() {
        return mL2CacheSize;
    }

    public void setL2CacheSize(String l2CacheSize) {
        mL2CacheSize = l2CacheSize;
    }

    public String getPackage() {
        return mPackage;
    }

    public void setPackage(String aPackage) {
        mPackage = aPackage;
    }

    public int getNoOfCpuCores() {
        return mNoOfCpuCores;
    }

    public void setNoOfCpuCores(int noOfCpuCores) {
        mNoOfCpuCores = noOfCpuCores;
    }

    public int getNoOfThreads() {
        return mNoOfThreads;
    }

    public void setNoOfThreads(int noOfThreads) {
        mNoOfThreads = noOfThreads;
    }

    public BigDecimal getBaseFrequency() {
        return mBaseFrequency;
    }

    public void setBaseFrequency(BigDecimal baseFrequency) {
        mBaseFrequency = baseFrequency;
    }

    public BigDecimal getTurboFrequency() {
        return mTurboFrequency;
    }

    public void setTurboFrequency(BigDecimal turboFrequency) {
        mTurboFrequency = turboFrequency;
    }

    public int getThermalPowerDesign() {
        return mThermalPowerDesign;
    }

    public void setThermalPowerDesign(int thermalPowerDesign) {
        mThermalPowerDesign = thermalPowerDesign;
    }

    public String getMemoryInterface() {
        return mMemoryInterface;
    }

    public void setMemoryInterface(String memoryInterface) {
        mMemoryInterface = memoryInterface;
    }

    public int getMemorySpeed() {
        return mMemorySpeed;
    }

    public void setMemorySpeed(int memorySpeed) {
        mMemorySpeed = memorySpeed;
    }

    public int getNoOfMemoryChannels() {
        return mNoOfMemoryChannels;
    }

    public void setNoOfMemoryChannels(int noOfMemoryChannels) {
        mNoOfMemoryChannels = noOfMemoryChannels;
    }

    public String getGraphicsProcessor() {
        return mGraphicsProcessor;
    }

    public void setGraphicsProcessor(String graphicsProcessor) {
        mGraphicsProcessor = graphicsProcessor;
    }

    public BigDecimal getDirectXVer() {
        return mDirectXVer;
    }

    public void setDirectXVer(BigDecimal directXVer) {
        mDirectXVer = directXVer;
    }

    public BigDecimal getGFrequency() {
        return mGFrequency;
    }

    public void setGFrequency(BigDecimal GFrequency) {
        mGFrequency = GFrequency;
    }

    public int getGraphicsCoreCount() {
        return mGraphicsCoreCount;
    }

    public void setGraphicsCoreCount(int graphicsCoreCount) {
        mGraphicsCoreCount = graphicsCoreCount;
    }

    public boolean hasAmdVirtualization() {
        return mHasAmdVirtualization;
    }

    public void setHasAmdVirtualization(boolean hasAmdVirtualization) {
        mHasAmdVirtualization = hasAmdVirtualization;
    }

    public boolean hasCooler() {
        return mHasCooler;
    }

    public void setHasCooler(boolean hasCooler) {
        mHasCooler = hasCooler;
    }

    public boolean hasDisplayPort() {
        return mHasDisplayPort;
    }

    public void setHasDisplayPort(boolean hasDisplayPort) {
        mHasDisplayPort = hasDisplayPort;
    }

    public boolean hasHdmi() {
        return mHasHdmi;
    }

    public void setHasHdmi(boolean hasHdmi) {
        mHasHdmi = hasHdmi;
    }

    interface Builder {

        Builder setLine(String line);

        Builder setFamily(String family);

        Builder setCmos(String cmos);

        Builder setPcieRev(ConnectionInterface connectionInterface);

        Builder setUnlocked(boolean unlocked);

        Builder setL2CacheSize(String l2CacheSize);

        Builder setPackage(String packageProcessor);

        Builder setNoOfCpuCores(int noOfCpuCores);

        Builder setNoOfThreads(int noOfThreads);

        Builder setBaseFrequency(BigDecimal baseFrequency);

        Builder setTurboFrequency(BigDecimal turboFrequency);

        Builder setThermalPowerDesign(int thermalPowerDesign);

        Builder setMemoryInterface(String memoryInterface);

        Builder setMemorySpeed(int memorySpeed);

        Builder setNoOfMemoryChannels(int noOfMemoryChannels);

        Builder setGraphicsProcessor(String graphicsProcessor);

        Builder setDirectXVersion(BigDecimal directXVersion);

        Builder setGFrequency(BigDecimal gFrequency);

        Builder setGraphicsCoreCount(int graphicsCoreCount);

        Builder setHasAmdVirtualization(boolean hasAmdVirtualization);

        Builder setHasCooler(boolean hasCooler);

        Builder setHasDisplayPort(boolean hasDisplayPort);

        Builder setHasHdmi(boolean hasHdmi);

        Builder setAttributes(Attribute attributes);

        Builder setOffers(Offer[] offers);

        AmdProcessor createAmdProcessor();

    }


    private static class AmdProcessorBuilder implements Builder {

        AmdProcessor mAmdProcessor;

        AmdProcessorBuilder(int id, String model) {
            mAmdProcessor = new AmdProcessor(id, model);
        }

        @Override
        public Builder setLine(String line) {
            mAmdProcessor.setLine(line);
            return this;
        }

        @Override
        public Builder setFamily(String family) {
            mAmdProcessor.setFamily(family);
            return this;
        }

        @Override
        public Builder setCmos(String cmos) {
            mAmdProcessor.setCmos(cmos);
            return this;
        }

        @Override
        public Builder setPcieRev(ConnectionInterface connectionInterface) {
            mAmdProcessor.setPcieRev(connectionInterface);
            return this;
        }

        @Override
        public Builder setUnlocked(boolean unlocked) {
            mAmdProcessor.setUnlocked(unlocked);
            return this;
        }

        @Override
        public Builder setL2CacheSize(String l2CacheSize) {
            mAmdProcessor.setL2CacheSize(l2CacheSize);
            return this;
        }

        @Override
        public Builder setPackage(String packageProcessor) {
            mAmdProcessor.setPackage(packageProcessor);
            return this;
        }

        @Override
        public Builder setNoOfCpuCores(int noOfCpuCores) {
            mAmdProcessor.setNoOfCpuCores(noOfCpuCores);
            return this;
        }

        @Override
        public Builder setNoOfThreads(int noOfThreads) {
            mAmdProcessor.setNoOfThreads(noOfThreads);
            return this;
        }

        @Override
        public Builder setBaseFrequency(BigDecimal baseFrequency) {
            mAmdProcessor.setBaseFrequency(baseFrequency);
            return this;
        }

        @Override
        public Builder setTurboFrequency(BigDecimal turboFrequency) {
            mAmdProcessor.setTurboFrequency(turboFrequency);
            return this;
        }

        @Override
        public Builder setThermalPowerDesign(int thermalPowerDesign) {
            mAmdProcessor.setThermalPowerDesign(thermalPowerDesign);
            return this;
        }

        @Override
        public Builder setMemoryInterface(String memoryInterface) {
            mAmdProcessor.setMemoryInterface(memoryInterface);
            return this;
        }

        @Override
        public Builder setMemorySpeed(int memorySpeed) {
            mAmdProcessor.setMemorySpeed(memorySpeed);
            return this;
        }

        @Override
        public Builder setNoOfMemoryChannels(int noOfMemoryChannels) {
            mAmdProcessor.setNoOfMemoryChannels(noOfMemoryChannels);
            return this;
        }

        @Override
        public Builder setGraphicsProcessor(String graphicsProcessor) {
            mAmdProcessor.setGraphicsProcessor(graphicsProcessor);
            return this;
        }

        @Override
        public Builder setDirectXVersion(BigDecimal directXVersion) {
            mAmdProcessor.setDirectXVer(directXVersion);
            return this;
        }

        @Override
        public Builder setGFrequency(BigDecimal gFrequency) {
            mAmdProcessor.setGFrequency(gFrequency);
            return this;
        }

        @Override
        public Builder setGraphicsCoreCount(int graphicsCoreCount) {
            mAmdProcessor.setGraphicsCoreCount(graphicsCoreCount);
            return this;
        }

        @Override
        public Builder setHasAmdVirtualization(boolean hasAmdVirtualization) {
            mAmdProcessor.setHasAmdVirtualization(hasAmdVirtualization);
            return this;
        }

        @Override
        public Builder setHasCooler(boolean hasCooler) {
            mAmdProcessor.setHasCooler(hasCooler);
            return this;
        }

        @Override
        public Builder setHasDisplayPort(boolean hasDisplayPort) {
            mAmdProcessor.setHasDisplayPort(hasDisplayPort);
            return this;
        }

        @Override
        public Builder setHasHdmi(boolean hasHdmi) {
            mAmdProcessor.setHasHdmi(hasHdmi);
            return this;
        }

        @Override
        public Builder setAttributes(Attribute attributes) {
            mAmdProcessor.setAttribute(attributes);
            return this;
        }

        @Override
        public Builder setOffers(Offer[] offers) {
            mAmdProcessor.setOffers(offers);
            return this;
        }

        @Override
        public AmdProcessor createAmdProcessor() {
            return mAmdProcessor;
        }

    }


    private static class Deserialize implements JsonDeserializer<AmdProcessor> {

        @Override
        public AmdProcessor deserialize(JsonElement json, Type typeOfT, JsonDeserializationContext context) throws JsonParseException {

            JsonObject jsonObject = json.getAsJsonObject();

            int amdProcessorId = 0;
            String model = null;

            if (jsonObject.has("amd_processor_id") && !jsonObject.get("amd_processor_id").isJsonNull()) {
                amdProcessorId = jsonObject.get("amd_processor_id").getAsInt();
            }

            if (jsonObject.has("model") && !jsonObject.get("model").isJsonNull()) {
                model = jsonObject.get("model").getAsString();
            }

            Builder builder = new AmdProcessorBuilder(amdProcessorId, model);

            if (jsonObject.has("line") && !jsonObject.get("line").isJsonNull()) {
                builder.setLine(jsonObject.get("line").getAsString());
            }

            if (jsonObject.has("family") && !jsonObject.get("family").isJsonNull()) {
                builder.setFamily(jsonObject.get("family").getAsString());
            }

            if (jsonObject.has("cmos") && !jsonObject.get("cmos").isJsonNull()) {
                builder.setCmos(jsonObject.get("cmos").getAsString());
            }

            if (jsonObject.has("pcie_rev") && !jsonObject.get("pcie_rev").isJsonNull()) {
                builder.setPcieRev(ConnectionInterface.getEnumValue(jsonObject.get("pcie_rev").getAsString()));
            }

            if (jsonObject.has("unlocked") && !jsonObject.get("unlocked").isJsonNull()) {
                builder.setUnlocked(jsonObject.get("unlocked").getAsBoolean());
            }

            if (jsonObject.has("l2_cache_size") && !jsonObject.get("l2_cache_size").isJsonNull()) {
                builder.setL2CacheSize(jsonObject.get("l2_cache_size").getAsString());
            }

            if (jsonObject.has("package") && !jsonObject.get("package").isJsonNull()) {
                builder.setPackage(jsonObject.get("package").getAsString());
            }

            if (jsonObject.has("no_of_cpu_cores") && !jsonObject.get("no_of_cpu_cores").isJsonNull()) {
                builder.setNoOfCpuCores(jsonObject.get("no_of_cpu_cores").getAsInt());
            }

            if (jsonObject.has("no_of_threads") && !jsonObject.get("no_of_threads").isJsonNull()) {
                builder.setNoOfThreads(jsonObject.get("no_of_threads").getAsInt());
            }

            if (jsonObject.has("base_frequency") && !jsonObject.get("base_frequency").isJsonNull()) {
                builder.setBaseFrequency(jsonObject.get("base_frequency").getAsBigDecimal());
            }

            if (jsonObject.has("turbo_frequency") && !jsonObject.get("turbo_frequency").isJsonNull()) {
                builder.setTurboFrequency(jsonObject.get("turbo_frequency").getAsBigDecimal());
            }

            if (jsonObject.has("thermal_design_power") && !jsonObject.get("thermal_design_power").isJsonNull()) {
                builder.setThermalPowerDesign(jsonObject.get("thermal_design_power").getAsInt());
            }

            if (jsonObject.has("memory_interface") && !jsonObject.get("memory_interface").isJsonNull()) {
                builder.setMemoryInterface(jsonObject.get("memory_interface").getAsString());
            }

            if (jsonObject.has("memory_speed") && !jsonObject.get("memory_speed").isJsonNull()) {
                builder.setMemorySpeed(jsonObject.get("memory_speed").getAsInt());
            }

            if (jsonObject.has("no_of_memory_channels") && !jsonObject.get("no_of_memory_channels").isJsonNull()) {
                builder.setNoOfMemoryChannels(jsonObject.get("no_of_memory_channels").getAsInt());
            }

            if (jsonObject.has("graphics_processor") && !jsonObject.get("graphics_processor").isJsonNull()) {
                builder.setGraphicsProcessor(jsonObject.get("graphics_processor").getAsString());
            }

            if (jsonObject.has("directX_ver") && !jsonObject.get("directX_ver").isJsonNull()) {
                builder.setDirectXVersion(jsonObject.get("directX_ver").getAsBigDecimal());
            }

            if (jsonObject.has("g_frequency") && !jsonObject.get("g_frequency").isJsonNull()) {
                builder.setGFrequency(jsonObject.get("g_frequency").getAsBigDecimal());
            }

            if (jsonObject.has("graphics_core_count") && !jsonObject.get("graphics_core_count").isJsonNull()) {
                builder.setGraphicsCoreCount(jsonObject.get("graphics_core_count").getAsInt());
            }

            if (jsonObject.has("amd_virtualization") && !jsonObject.get("amd_virtualization").isJsonNull()) {
                builder.setHasAmdVirtualization(jsonObject.get("amd_virtualization").getAsBoolean());
            }

            if (jsonObject.has("has_cooler") && !jsonObject.get("has_cooler").isJsonNull()) {
                builder.setHasCooler(jsonObject.get("has_cooler").getAsBoolean());
            }

            if (jsonObject.has("has_displayport") && !jsonObject.get("has_displayport").isJsonNull()) {
                builder.setHasDisplayPort(jsonObject.get("has_displayport").getAsBoolean());
            }

            if (jsonObject.has("has_hdmi") && !jsonObject.get("has_hdmi").isJsonNull()) {
                builder.setHasHdmi(jsonObject.get("has_hdmi").getAsBoolean());
            }

            if (jsonObject.has("attributes") && !jsonObject.get("attributes").isJsonNull()) {
                builder.setAttributes(
                        Attribute.getAttributeFromJsonObject(jsonObject.get("attributes").getAsJsonObject())
                );
            }

            if (jsonObject.has("offers") && !jsonObject.get("offers").isJsonNull()) {
                JsonArray jsonArray = jsonObject.get("offers").getAsJsonArray();
                builder.setOffers(Offer.getOffersFromJsonArray(jsonArray));
            }

            return builder.createAmdProcessor();

        }

    }

}
