package com.maker.android.app.adapter;


import android.annotation.SuppressLint;
import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.maker.android.app.R;
import com.maker.android.app.classes.Spec;

import java.util.ArrayList;
import java.util.LinkedHashMap;


@SuppressWarnings({"unused"})
@SuppressLint("InflateParams")
public class CompareDetailsAdapter extends RecyclerView.Adapter<CompareDetailsAdapter.CompareDetailsHolder> {

    /**
     * A {@link String} constant to use as tag in {@link android.util.Log}
     */
    private static final String LOG_TAG = CompareDetailsAdapter.class.getSimpleName();

    private Context mContext;
    private ArrayList<ArrayList<Spec>> mDataset;
    private ArrayList<String> mKeys;


    public CompareDetailsAdapter(Context context, ArrayList<LinkedHashMap<String, Object>> dataset) {
        mContext = context;
        mKeys = new ArrayList<>(dataset.get(0).keySet());
        mKeys.remove("Company Name");
        mKeys.add(0, "Company Name");
        mDataset = new ArrayList<>();
        for (int i = 0; i < dataset.size(); i++) {
            mDataset.add(Spec.getArrayList(dataset.get(i)));
        }
    }

    @Override
    public CompareDetailsHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = LayoutInflater
                .from(parent.getContext())
                .inflate(R.layout.fragment_compare_details_item, parent, false);
        return new CompareDetailsHolder(view);
    }

    @Override
    public void onBindViewHolder(CompareDetailsHolder holder, int position) {

        String currentSpec = mKeys.get(position);
        if (currentSpec.equals("Model") && position + 1 < mKeys.size()) {
            currentSpec = mKeys.get(position + 1);
        }
        holder.mTextViewDetailsTitle.setText(currentSpec);

        LayoutInflater layoutInflater
                = (LayoutInflater) mContext.getSystemService(Context.LAYOUT_INFLATER_SERVICE);

        holder.mLinearLayoutModelContainer.removeAllViews();

        String model = "", detail = "";
        for (ArrayList<Spec> componentSpecs : mDataset) {

            // Find model name and spec
            for (Spec spec : componentSpecs) {

                if (spec.getTitle().equals("Model")) {
                    model = spec.getDetails();
                }

                if (spec.getTitle().equals(currentSpec)) {
                    detail = spec.getDetails();
                }
            }

            View item = layoutInflater.inflate(R.layout.compare_sub_item, null);
            ((TextView) item.findViewById(R.id.compare_item_model_title)).setText(model);
            ((TextView) item.findViewById(R.id.compare_item_model_detail)).setText(detail);
            holder.mLinearLayoutModelContainer.addView(item);

        }

    }


    @Override
    public int getItemCount() {
        return mKeys.size() - 1;
    }


    class CompareDetailsHolder extends RecyclerView.ViewHolder {

        TextView mTextViewDetailsTitle;
        LinearLayout mLinearLayoutModelContainer;

        CompareDetailsHolder(View itemView) {
            super(itemView);

            mTextViewDetailsTitle
                    = (TextView) itemView.findViewById(R.id.fragment_compare_detail_item_title);
            mLinearLayoutModelContainer
                    = (LinearLayout) itemView.findViewById(R.id.fragment_compare_details_item_ll_holder);
        }

    }


}
