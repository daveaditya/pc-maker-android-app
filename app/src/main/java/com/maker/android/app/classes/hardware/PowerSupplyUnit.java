package com.maker.android.app.classes.hardware;

import android.os.Parcel;

import com.google.gson.JsonArray;
import com.google.gson.JsonDeserializationContext;
import com.google.gson.JsonDeserializer;
import com.google.gson.JsonElement;
import com.google.gson.JsonObject;
import com.google.gson.JsonParseException;
import com.maker.android.app.annotation.HardwareAnnotations;
import com.maker.android.app.classes.Attribute;
import com.maker.android.app.classes.Offer;
import com.maker.android.app.classes.SimpleHighlighter;
import com.maker.android.app.provider.PreserveContract;

import java.lang.reflect.Type;


@SuppressWarnings({"unused", "WeakerAccess"})
class PowerSupplyUnit
        extends Hardware {

    /**
     * A {@link String} constant to use as tag in {@link android.util.Log}
     */
    private static final String LOG_TAG = PowerSupplyUnit.class.getSimpleName();

    public static Deserialize sDeserialize;

    static {
        sDeserialize = new Deserialize();
    }


    @HardwareAnnotations.DisplayData(name = "Form Factor", displayInHighlights = true)
    private FormFactor mFormFactor;

    @HardwareAnnotations.DisplayData(name = "Fan Size", unit = "mm")
    private int mFanSize;

    @HardwareAnnotations.DisplayData(name = "Length", unit = "mm")
    private int mLength;

    @HardwareAnnotations.DisplayData(name = "Width", unit = "mm")
    private int mWidth;

    @HardwareAnnotations.DisplayData(name = "Height", unit = "mm")
    private int mHeight;

    @HardwareAnnotations.DisplayData(name = "Power", unit = "W")
    private int mPower;


    public static final Creator<PowerSupplyUnit> CREATOR = new Creator<PowerSupplyUnit>() {

        @Override
        public PowerSupplyUnit createFromParcel(Parcel in) {
            return new PowerSupplyUnit(in);
        }


        @Override
        public PowerSupplyUnit[] newArray(int size) {
            return new PowerSupplyUnit[size];
        }

    };


    private PowerSupplyUnit(int id, String companyName, String model) {
        super(id, companyName, model);
        setHighlighter(new SimpleHighlighter());
    }


    PowerSupplyUnit(Parcel in) {
        super(in);
    }


    @Override
    public int describeContents() {
        return 0;
    }


    @Override
    public PreserveContract.HardwareTypeEntry.HardwareType getHardwareType() {
        return PreserveContract.HardwareTypeEntry.HardwareType.PowerSupplyUnit;
    }

    public FormFactor getFormFactor() {
        return mFormFactor;
    }

    public void setFormFactor(FormFactor formFactor) {
        mFormFactor = formFactor;
    }

    public int getFanSize() {
        return mFanSize;
    }

    public void setFanSize(int fanSize) {
        mFanSize = fanSize;
    }

    public int getLength() {
        return mLength;
    }

    public void setLength(int length) {
        mLength = length;
    }

    public int getWidth() {
        return mWidth;
    }

    public void setWidth(int width) {
        mWidth = width;
    }

    public int getHeight() {
        return mHeight;
    }

    public void setHeight(int height) {
        mHeight = height;
    }

    public int getPower() {
        return mPower;
    }

    public void setPower(int power) {
        mPower = power;
    }


    enum FormFactor {

        ATX("ATX"), ATX_PS2("ATX PS2"), ATX_BTX("ATX/BTX"), ATX12V("ATX12V"), ATX12V_V2_3("ATX12V V2.3"),
        EPS_12V("EPS12V"), FLEX_ATX("Flex ATX"), MICRO_ATX("Micro ATX"), MINI_ITX("Mini ITX"), SFX("SFX"),
        SFX_L("SFX-L"), SFX12V("SFX12V"), TFX12V("TFX12V");

        String description;


        FormFactor(String description) {
            this.description = description;
        }

        public static FormFactor getEnumValue(String string) throws EnumConstantNotPresentException {

            switch (string) {

                case "ATX":
                    return ATX;
                case "ATX PS2":
                    return ATX_PS2;
                case "ATX/BTX":
                    return ATX_BTX;
                case "ATX12V":
                    return ATX12V;
                case "ATX12V V2.3":
                    return ATX12V_V2_3;
                case "EPS12V":
                    return EPS_12V;
                case "Flex ATX":
                    return FLEX_ATX;
                case "Micro ATX":
                    return MICRO_ATX;
                case "Mini ITX":
                    return MINI_ITX;
                case "SFX":
                    return SFX;
                case "SFX-L":
                    return SFX_L;
                case "SFX12V":
                    return SFX12V;
                case "TFX12V":
                    return TFX12V;
                default:
                    throw new EnumConstantNotPresentException(FormFactor.class, string);
            }

        }

        @Override
        public String toString() {
            return description;
        }

    }


    interface Builder {

        Builder setFormFactor(FormFactor formFactor);

        Builder setFanSize(int fanSize);

        Builder setLength(int length);

        Builder setWidth(int width);

        Builder setHeight(int height);

        Builder setPower(int power);

        Builder setAttributes(Attribute attributes);

        Builder setOffers(Offer[] offers);

        PowerSupplyUnit createPowerSupplyUnit();

    }


    private static class PowerSupplyUnitBuilder implements Builder {

        PowerSupplyUnit mPowerSupplyUnit;

        PowerSupplyUnitBuilder(int id, String companyName, String model) {
            mPowerSupplyUnit = new PowerSupplyUnit(id, companyName, model);
        }

        @Override
        public Builder setFormFactor(FormFactor formFactor) {
            mPowerSupplyUnit.setFormFactor(formFactor);
            return this;
        }

        @Override
        public Builder setFanSize(int fanSize) {
            mPowerSupplyUnit.setFanSize(fanSize);
            return this;
        }

        @Override
        public Builder setLength(int length) {
            mPowerSupplyUnit.setLength(length);
            return this;
        }

        @Override
        public Builder setWidth(int width) {
            mPowerSupplyUnit.setWidth(width);
            return this;
        }

        @Override
        public Builder setHeight(int height) {
            mPowerSupplyUnit.setHeight(height);
            return this;
        }

        @Override
        public Builder setPower(int power) {
            mPowerSupplyUnit.setPower(power);
            return this;
        }

        @Override
        public Builder setAttributes(Attribute attributes) {
            mPowerSupplyUnit.setAttribute(attributes);
            return this;
        }

        @Override
        public Builder setOffers(Offer[] offers) {
            mPowerSupplyUnit.setOffers(offers);
            return this;
        }

        @Override
        public PowerSupplyUnit createPowerSupplyUnit() {
            return mPowerSupplyUnit;
        }

    }


    private static class Deserialize implements JsonDeserializer<PowerSupplyUnit> {

        @Override
        public PowerSupplyUnit deserialize(JsonElement json, Type typeOfT, JsonDeserializationContext context) throws JsonParseException {

            JsonObject jsonObject = json.getAsJsonObject();

            int powerSupplyUnitId = 0;
            String companyName = null, model = null;

            if (jsonObject.has("power_supply_unit_id") && !jsonObject.get("power_supply_unit_id").isJsonNull()) {
                powerSupplyUnitId = jsonObject.get("power_supply_unit_id").getAsInt();
            }

            if (jsonObject.has("company_name") && !jsonObject.get("company_name").isJsonNull()) {
                companyName = jsonObject.get("company_name").getAsString();
            }

            if (jsonObject.has("model") && !jsonObject.get("model").isJsonNull()) {
                model = jsonObject.get("model").getAsString();
            }

            Builder builder = new PowerSupplyUnitBuilder(powerSupplyUnitId, companyName, model);

            if (jsonObject.has("form_factor") && !jsonObject.get("form_factor").isJsonNull()) {
                builder.setFormFactor(FormFactor.getEnumValue(jsonObject.get("form_factor").getAsString()));
            }

            if (jsonObject.has("fan_size") && !jsonObject.get("fan_size").isJsonNull()) {
                builder.setFanSize(jsonObject.get("fan_size").getAsInt());
            }

            if (jsonObject.has("length") && !jsonObject.get("length").isJsonNull()) {
                builder.setLength(jsonObject.get("length").getAsInt());
            }

            if (jsonObject.has("width") && !jsonObject.get("width").isJsonNull()) {
                builder.setWidth(jsonObject.get("width").getAsInt());
            }

            if (jsonObject.has("height") && !jsonObject.get("height").isJsonNull()) {
                builder.setHeight(jsonObject.get("height").getAsInt());
            }

            if (jsonObject.has("power") && !jsonObject.get("power").isJsonNull()) {
                builder.setPower(jsonObject.get("power").getAsInt());
            }

            if (jsonObject.has("attributes") && !jsonObject.get("attributes").isJsonNull()) {
                builder.setAttributes(
                        Attribute.getAttributeFromJsonObject(jsonObject.get("attributes").getAsJsonObject())
                );
            }

            if (jsonObject.has("offers") && !jsonObject.get("offers").isJsonNull()) {
                JsonArray jsonArray = jsonObject.get("offers").getAsJsonArray();
                builder.setOffers(Offer.getOffersFromJsonArray(jsonArray));
            }

            return builder.createPowerSupplyUnit();

        }

    }

}
