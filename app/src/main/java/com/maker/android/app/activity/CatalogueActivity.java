package com.maker.android.app.activity;

import android.content.Intent;
import android.os.Bundle;
import android.support.v4.app.LoaderManager;
import android.support.v4.content.Loader;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.widget.ProgressBar;

import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;
import com.maker.android.app.R;
import com.maker.android.app.adapter.CatalogueAdapter;
import com.maker.android.app.classes.CatalogueItem;
import com.maker.android.app.loader.AsyncDataDownloader;
import com.maker.android.app.utils.Request;
import com.maker.android.app.utils.Request.RequestBuilder;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;


@SuppressWarnings({"unused"})
public class CatalogueActivity
        extends AppCompatActivity
        implements CatalogueAdapter.CatalogueClickListener.OpenModelListActivity {

    /**
     * A {@link String} constant to use as TAG in {@link Log}
     */
    private static final String LOG_TAG = CatalogueActivity.class.getSimpleName();

    /**
     * A {@link String} constant to for sending request to {@link ModelListActivity}
     */

    public static final String MODEL_LIST_REQUEST = "MODEL_LIST_REQUEST";

    /**
     * A {@link String} constant to send the title of {@link CatalogueItem}
     * to {@link ModelListActivity}
     */
    public static final String MODEL_TITLE = "MODEL_TITLE";

    /**
     * A {@link String} constant to be used for passing {@link Request} object
     */
    private static final String REQUEST = "REQUEST";

    /**
     * An int constant to be used as ID for the {@link AsyncDataLoader}
     */
    private static final int HARDWARE_LIST_LOADER = 0;

    /**
     * Used to store reference to the {@link RecyclerView}
     */
    private RecyclerView mRecyclerView;

    private ArrayList<CatalogueItem> mCatalogueItems;

    /**
     * {@link android.support.v7.widget.RecyclerView.Adapter} to be used with mRecyclerView
     */
    private CatalogueAdapter mCatalogueAdapter;

    /**
     * Used to store reference to the {@link ProgressBar}
     */
    private ProgressBar mProgressBar;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_catalogue);

        // Gets a reference to the ToolBar and sets is as ActionBar
        Toolbar toolbar = (Toolbar) findViewById(R.id.activity_catalogue_tb);
        setSupportActionBar(toolbar);

        if (getSupportActionBar() != null) {
            getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        }

        // Initialize the ArrayList
        mCatalogueItems = new ArrayList<>();

        // Gets a reference to the RecyclerView and sets its adapter to null
        mRecyclerView = (RecyclerView) findViewById(R.id.activity_catalogue_rv_list);

        // Gets a reference to the ProgressBar and sets visibility to VISIBLE
        // as the data would not be available at the instant when activity starts
        mProgressBar = (ProgressBar) findViewById(R.id.activity_catalogue_pb_loading);

        // Creates a new Bundle instance to send to the AsyncDataLoader
        Bundle args = new Bundle();

        // Puts the Request object inside the bundle
        args.putParcelable(REQUEST,
                new RequestBuilder()
                        .setURL(getResources().getString(R.string.url_component_list))
                        .setHttpType(Request.HttpRequestTypes.GET)
                        .createRequest()
        );

        // Calls the LoaderManager to fetch data from Web Server
        getSupportLoaderManager()
                .initLoader(HARDWARE_LIST_LOADER, args, new AsyncDataLoader())
                .forceLoad();
    }


    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        MenuInflater menuInflater = new MenuInflater(getBaseContext());
        menuInflater.inflate(R.menu.catalogue_action_bar, menu);
        return super.onCreateOptionsMenu(menu);
    }


    @Override
    public boolean onOptionsItemSelected(MenuItem item) {

        switch (item.getItemId()) {
            case R.id.activity_catalogue_action_help:
                Intent intent = new Intent(getBaseContext(), TechSupportActivity.class);
                startActivity(intent);
                return true;

            case R.id.activity_catalogue_action_goto_cart:
                intent = new Intent(getBaseContext(), ReviewBuildActivity.class);
                intent.putExtra(ReviewBuildActivity.PARENT_CLASS, ModelListActivity.class.getName());
                startActivity(intent);
                return true;

            case R.id.activity_catalogue_action_goto_compare:
                intent = new Intent(getBaseContext(), CompareActivity.class);
                intent.putExtra(CompareActivity.PARENT_CLASS, ModelListActivity.class.getName());
                startActivity(intent);
                return true;

            default:
                return false;
        }

    }


    /**
     * A callback method
     *
     * @param hardwareRequest Type of hardware requested
     * @param title Title for the {@link ModelListActivity}
     */
    @Override
    public void modelListActivityStarter(Request hardwareRequest, String title, String endpoint) {

        // Create new intent with ModelListActivity as target
        Intent intent = new Intent(getBaseContext(), ModelListActivity.class);

        // Put Request and CatalogueItem's Title in the Intent
        intent.putExtra(MODEL_LIST_REQUEST, hardwareRequest);
        intent.putExtra(MODEL_TITLE, title);

        // Start the ModelListActivity
        startActivity(intent);

    }

    /**
     * A {@link android.support.v4.app.LoaderManager.LoaderCallbacks} class to interact with the
     * {@link LoaderManager} for downloading JSON data from Web Server
     */
    private final class AsyncDataLoader implements LoaderManager.LoaderCallbacks<String> {

        /**
         * Creates an {@link AsyncDataDownloader} for the given ID
         *
         * @param id   ID of the Loader
         * @param args {@link Bundle} containing data to pass to the {@link Loader}
         * @return An instance of {@link AsyncDataDownloader}
         */
        @Override
        public Loader<String> onCreateLoader(int id, Bundle args) {

            // Gets the Request object from the Bundle
            Request request = args.getParcelable(REQUEST);

            // Creates and returns a new AsyncDataDownloader
            return new AsyncDataDownloader(getBaseContext(), request);
        }


        /**
         * Called when the {@link AsyncDataDownloader} completes its task. Here JSON parsing is done
         * and an {@link ArrayList<CatalogueItem>} is created which is them passed to fetch images
         *
         * @param loader {@link Loader} that finished the task
         * @param data   Data generated by the loader
         */
        @Override
        public void onLoadFinished(Loader<String> loader, String data) {

            try {
                // If the data is null, return
                if (data == null) {
                    return;
                }

                // Creates a JSONObject from the response
                JSONObject jsonObject = new JSONObject(data);

                // If the response is server_failure return, else continue parsing JSON data
                if (jsonObject.getJSONObject("feedback").getString("response").equals(getResources().getString(R.string.server_failure))) {
                    return;
                }

                // Creates an empty ArrayList of CatalogueItem to store the retrieved data
                Gson gson = new Gson();

                @SuppressWarnings("unchecked")
                ArrayList<CatalogueItem> result = gson.fromJson(
                        jsonObject.getJSONArray("data").toString(),
                        new TypeToken<ArrayList<CatalogueItem>>() {
                        }.getType()
                );

                // Add the result to our main ArrayList
                mCatalogueItems.addAll(result);

                // Creates a new CatalogueAdapter with obtained data
                mCatalogueAdapter = new CatalogueAdapter(CatalogueActivity.this, mCatalogueItems);

                // Sets the Adapter of the RecyclerView to the newly created CatalogueAdapter
                mRecyclerView.setAdapter(mCatalogueAdapter);

                // As data fetching is complete make the ProgressBar invisible
                mProgressBar.setVisibility(View.GONE);

                // Make the RecyclerView visible
                mRecyclerView.setVisibility(View.VISIBLE);

                // Fetches JSONException, if any
            } catch (JSONException exc) {
                exc.printStackTrace();
            }
        }


        /**
         * Removes the data references after the {@link Loader} is reset, if any.
         *
         * @param loader {@link Loader} which got reset
         */
        @Override
        public void onLoaderReset(Loader<String> loader) {
            mCatalogueItems.clear();
            if (mCatalogueAdapter != null) {
                mCatalogueAdapter.notifyDataSetChanged();
            }
        }

    }

}
