package com.maker.android.app.activity;

import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.Menu;
import android.view.MenuInflater;

import com.maker.android.app.R;
import com.maker.android.app.fragment.CompareDetailsFragment;
import com.maker.android.app.fragment.CompareListFragment;


@SuppressWarnings({"unused"})
public class CompareActivity
        extends AppCompatActivity
        implements CompareListFragment.OnComparedComponentSelected {

    /**
     * A {@link String} constant to use as tag in {@link android.util.Log}
     */
    private static final String LOG_TAG = CompareActivity.class.getSimpleName();

    public static final String PARENT_CLASS = "CompareActivity.PARENT_CLASS";
    public static final String SELECTED_COMPONENT = "SELECTED_COMPONENT";

    private CompareDetailsFragment mCompareDetailsFragment;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_compare);

        Toolbar toolbar = (Toolbar) findViewById(R.id.activity_model_compare_tb);
        toolbar.setTitle("Comparator");
        setSupportActionBar(toolbar);

        if (getSupportActionBar() != null) {
            getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        }

        CompareListFragment compareListFragment = new CompareListFragment();
        mCompareDetailsFragment = new CompareDetailsFragment();

        compareListFragment.setHasOptionsMenu(true);
        mCompareDetailsFragment.setHasOptionsMenu(true);

        getSupportFragmentManager()
                .beginTransaction()
                .add(R.id.activity_compare_fragment, compareListFragment)
                .commit();
    }


    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        MenuInflater menuInflater = new MenuInflater(getBaseContext());
        menuInflater.inflate(R.menu.compare_action_bar, menu);
        return super.onCreateOptionsMenu(menu);
    }


    @Nullable
    @Override
    public Intent getParentActivityIntent() {
        Intent parentIntent = getIntent();
        String className = parentIntent.getStringExtra(PARENT_CLASS);

        Intent newIntent = null;
        try {
            //you need to define the class with package name
            newIntent = new Intent(getBaseContext(), Class.forName(className));

        } catch (ClassNotFoundException e) {
            e.printStackTrace();
        }
        return newIntent;
    }


    @Override
    public void getComparedComponent(String component) {
        Bundle args = new Bundle();
        args.putString(SELECTED_COMPONENT, component);
        mCompareDetailsFragment.setArguments(args);
        getSupportFragmentManager()
                .beginTransaction()
                .replace(R.id.activity_compare_fragment, mCompareDetailsFragment)
                .commit();
    }

}
