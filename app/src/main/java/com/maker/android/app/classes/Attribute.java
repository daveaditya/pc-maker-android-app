package com.maker.android.app.classes;


import com.google.common.base.CharMatcher;
import com.google.gson.JsonObject;


@SuppressWarnings({"unused"})
public class Attribute {

    /**
     * A {@link String} constant to use as tag in {@link android.util.Log}
     */
    private static final String LOG_TAG = Attribute.class.getSimpleName();

    private String mHomeUrl;
    private double mRating;
    private String[] mImageUrls;


    public Attribute(String homeUrl, double rating, String[] imageUrls) {
        mHomeUrl = homeUrl;
        mRating = rating;
        mImageUrls = imageUrls;
    }


    public String getHomeUrl() {
        return mHomeUrl;
    }


    public double getRating() {
        return mRating;
    }


    public String[] getImageUrls() {
        return mImageUrls;
    }


    public static Attribute getAttributeFromJsonObject(JsonObject data) {

        String url = null;
        if (!data.get("link").isJsonNull()) {
            url = CharMatcher.is('"').trimFrom(data.get("link").getAsString());
        }

        double rating = 0;
        if (!data.get("rating").isJsonNull()) {
            rating = data.get("rating").getAsDouble();
        }

        String[] imgUrls = null;
        if (!data.get("img_srcs").isJsonNull()) {
            String[] temp = data.get("img_srcs").getAsString().split(",");
            imgUrls = new String[temp.length];

            for (int i = 0; i < temp.length; i++) {
                imgUrls[i] = CharMatcher.is('"').trimFrom(temp[i]);
            }
        }

        return new Attribute(url, rating, imgUrls);

    }

}
