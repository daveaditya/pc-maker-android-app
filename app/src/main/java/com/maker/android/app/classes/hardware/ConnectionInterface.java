package com.maker.android.app.classes.hardware;

public enum ConnectionInterface {

    BIT_128_DDR3("128-bit DDR3"), BIT_128_GDDR5("128-bit GDDR5"), BIT_192_GDDR5("192-bit GDDR5"),
    BIT_256_GDDR5("256-bit GDDR5"), PIN_3("3-Pin"), JACK_3_5_MM("3.5mm Jack"),
    BIT_384_GDDR5("384-bit GDDR5"), PIN_4("4-Pin"), PIN_4_PWM("4-Pin (PWM)"), BIT_512_GDDR5("512-bit GDDR5"),
    BIT_64_DDR2("64-bit DDR2"), BIT_64_DDR3("64-bit DDR3"), AGP("AGP"), BLUETOOTH("Bluetooth"),
    DDR("DDR"), DDR1("DDR1"), DDR2("DDR2"), DDR3("DDR3"), DDR3L("DDR3L"), DDR4("DDR4"), GDDR("GDDR"),
    GDDR2("GDDR2"), GDDR3("GDDR3"), GDDR4("GDDR4"), GDDR5("GDDR5"), GDDR5X("GDDR5X"),
    HIGH_BANDWIDTH_MEMORY_HBM("High Bandwidth Memory (HBM)"), NFC("NFC"), PCI("PCI"), PCI_EXPRESS("PCI Express"),
    PCIE_1_0_X1("PCIe 1.0 x1"), PCIE_1_0_X16("PCIe 1.0 x16"), PCIE_1_0_X4("PCIe 1.0 x4"),
    PCIE_1_0A("PCIe 1.0a"), PCIE_1_1("PCIe 1.1"), PCIE_2_0("PCIe 2.0"), PCIE_2_0_X1("PCIe 2.0 x1"),
    PCIE_2_0_X16("PCIe 2.0 x16"), PCIE_2_0_X8("PCIe 2.0 x8"), PCIE_2_0_3_0("PCIe 2.0/3.0"),
    PCIE_2_1("PCIe 2.1"), PCIE_2_1_X16("PCIe 2.1 x16"), PCIE_3_0("PCIe 3.0"), PCIE_3_0_X16("PCIe 3.0 x16"),
    PCIE_3_0_X8("PCIe 3.0 x8"), PCIE_3_1("PCIe 3.1"), PCIE_4_0("PCIe 4.0"), PCIE_NVME_3_0_X4("PCIe NVMe 3.0 x4"),
    PCIE_X16("PCIe x16"), PCIE_X8("PCIe x8"), PS_2("PS/2"), SATA("SATA"), SATA_1_5_GBPS("SATA 1.5Gb/s"),
    SATA_3_0_GBPS("SATA 3.0Gb/s"), SATA_6_0_GBPS("SATA 6.0Gb/s"), SATA_4_PIN("SATA and 4-Pin"),
    USB("USB"), USB_PS_2("USB & PS/2"), USB_1_0("USB 1.0"), USB_1_1("USB 1.1"), USB_2_0("USB 2.0"),
    USB_3_0("USB 3.0"), USB_3_1("USB 3.1"), USB_3_1_GEN1("USB 3.1 Gen 1"), USB_3_1_GEN2("USB 3.1 Gen 2"),
    USB_TYPE_C("USB Type C");

    String description;


    ConnectionInterface(String description) {
        this.description = description;
    }

    public static ConnectionInterface getEnumValue(String string) throws EnumConstantNotPresentException {

        switch (string) {
            case "128-bit DDR3":
                return BIT_128_DDR3;
            case "128-bit GDDR5":
                return BIT_128_GDDR5;
            case "192-bit GDDR5":
                return BIT_192_GDDR5;
            case "256-bit GDDR5":
                return BIT_256_GDDR5;
            case "3-Pin":
                return PIN_3;
            case "3.5mm Jack":
                return JACK_3_5_MM;
            case "384-bit GDDR5":
                return BIT_384_GDDR5;
            case "4-Pin":
                return PIN_4;
            case "4-Pin (PWM)":
                return PIN_4_PWM;
            case "512-bit GDDR5":
                return BIT_512_GDDR5;
            case "64-bit DDR2":
                return BIT_64_DDR2;
            case "64-bit DDR3":
                return BIT_64_DDR3;
            case "AGP":
                return AGP;
            case "Bluetooth":
                return BLUETOOTH;
            case "DDR":
                return DDR;
            case "DDR1":
                return DDR1;
            case "DDR2":
                return DDR2;
            case "DDR3":
                return DDR3;
            case "DDR3L":
                return DDR3L;
            case "DDR4":
                return DDR4;
            case "GDDR":
                return GDDR;
            case "GDDR2":
                return GDDR2;
            case "GDDR3":
                return GDDR3;
            case "GDDR4":
                return GDDR4;
            case "GDDR5":
                return GDDR5;
            case "GDDR5X":
                return GDDR5X;
            case "High Bandwidth Memory (HBM)":
                return HIGH_BANDWIDTH_MEMORY_HBM;
            case "NFC":
                return NFC;
            case "PCI":
                return PCI;
            case "PCI Express":
                return PCI_EXPRESS;
            case "PCIe 1.0 x1":
                return PCIE_1_0_X1;
            case "PCIe 1.0 x16":
                return PCIE_1_0_X16;
            case "PCIe 1.0 x4":
                return PCIE_1_0_X4;
            case "PCIe 1.0a":
                return PCIE_1_0A;
            case "PCIe 1.1":
                return PCIE_1_1;
            case "PCIe 2.0":
                return PCIE_2_0;
            case "PCIe 2.0 x1":
                return PCIE_2_0_X1;
            case "PCIe 2.0 x16":
                return PCIE_2_0_X16;
            case "PCIe 2.0 x8":
                return PCIE_2_0_X8;
            case "PCIe 2.0/3.0":
                return PCIE_2_0_3_0;
            case "PCIe 2.1":
                return PCIE_2_1;
            case "PCIe 2.1 x16":
                return PCIE_2_1_X16;
            case "PCIe 3.0":
                return PCIE_3_0;
            case "PCIe 3.0 x16":
                return PCIE_3_0_X16;
            case "PCIe 3.0 x8":
                return PCIE_3_0_X8;
            case "PCIe 3.1":
                return PCIE_3_1;
            case "PCIe 4.0":
                return PCIE_4_0;
            case "PCIe NVMe 3.0 x4":
                return PCIE_NVME_3_0_X4;
            case "PCIe x16":
                return PCIE_X16;
            case "PCIe x8":
                return PCIE_X8;
            case "PS/2":
                return PS_2;
            case "SATA":
                return SATA;
            case "SATA 1.5Gb/s":
                return SATA_1_5_GBPS;
            case "SATA 3.0Gb/s":
                return SATA_3_0_GBPS;
            case "SATA 6.0Gb/s":
                return SATA_6_0_GBPS;
            case "SATA and 4-Pin":
                return SATA_4_PIN;
            case "USB":
                return USB;
            case "USB & PS/2":
                return USB_PS_2;
            case "USB 1.0":
                return USB_1_0;
            case "USB 1.1":
                return USB_1_1;
            case "USB 2.0":
                return USB_2_0;
            case "USB 3.0":
                return USB_3_0;
            case "USB 3.1":
                return USB_3_1;
            case "USB 3.1 Gen 1":
                return USB_3_1_GEN1;
            case "USB 3.1 Gen 2":
                return USB_3_1_GEN2;
            case "USB Type C":
                return USB_TYPE_C;
            default:
                throw new EnumConstantNotPresentException(ConnectionInterface.class, string);
        }

    }

    @Override
    public String toString() {
        return description;
    }

}
