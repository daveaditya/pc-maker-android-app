package com.maker.android.app.utils;

import android.util.Log;


@SuppressWarnings({"unused", "WeakerAccess"})
public class OtherUtils {

    public static void largeLog(String tag, String content) {
        if (content.length() > 4000) {
            Log.d(tag, content.substring(0, 4000));
            largeLog(tag, content.substring(4000));
        } else {
            Log.d(tag, content);
        }
    }

}
