package com.maker.android.app.adapter;


import android.content.Context;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageButton;
import android.widget.ProgressBar;
import android.widget.TextView;

import com.maker.android.app.R;
import com.maker.android.app.classes.Organization;
import com.maker.android.app.fragment.DealerDialogFragment;

import java.util.ArrayList;


@SuppressWarnings({"unused", "WeakerAccess"})
public class DealerSelectAdapter extends RecyclerView.Adapter<RecyclerView.ViewHolder> {

    /**
     * A {@link String} constant to use as tag in {@link android.util.Log}
     */
    private static final String LOG_TAG = DealerSelectAdapter.class.getSimpleName();

    private static final int ITEM_VIEW = 0;
    private static final int PROGRESS_VIEW = 1;

    private Context mContext;
    private ArrayList<Organization> mDataset;
    private OnDealerSelected mOnDealerSelected;

    private int mVisibleThreshold = 2;
    private int mLastVisibleItem, mTotalItemCount;
    private boolean mLoading;
    private boolean mHasNewData;

    private OnLoadMoreListener mOnLoadMoreListener;


    public interface OnLoadMoreListener {
        void loadMore();
    }


    public interface OnDealerSelected {
        void getSelectedDealer(String mode, Organization organization);
    }


    public DealerSelectAdapter(Context context, RecyclerView recyclerView, ArrayList<Organization> organizations, OnDealerSelected onDealerSelected) {
        mContext = context;
        mDataset = organizations;
        mOnDealerSelected = onDealerSelected;

        if (recyclerView.getLayoutManager() instanceof LinearLayoutManager) {

            final LinearLayoutManager linearLayoutManager
                    = (LinearLayoutManager) recyclerView.getLayoutManager();

            recyclerView.addOnScrollListener(new RecyclerView.OnScrollListener() {
                @Override
                public void onScrolled(RecyclerView recyclerView, int dx, int dy) {
                    super.onScrolled(recyclerView, dx, dy);

                    mTotalItemCount = linearLayoutManager.getItemCount();
                    mLastVisibleItem = linearLayoutManager.findLastCompletelyVisibleItemPosition();

                    if (!isLoading() && mTotalItemCount <= (mLastVisibleItem + mVisibleThreshold) && hasNewData()) {
                        if (mOnLoadMoreListener != null) {
                            mOnLoadMoreListener.loadMore();
                        }
                        setLoading(true);
                    }
                }
            });

        }
    }


    public boolean isLoading() {
        return mLoading;
    }

    public void setLoading(boolean loading) {
        this.mLoading = loading;
    }

    public boolean hasNewData() {
        return mHasNewData;
    }

    public void setHasNewData(boolean hasNewData) {
        mHasNewData = hasNewData;
    }

    public void setOnLoadMoreListener(OnLoadMoreListener onLoadMoreListener) {
        mOnLoadMoreListener = onLoadMoreListener;
    }


    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        // Creates a new view with the activity_model_list_item.xml layout
        View itemView;
        switch (viewType) {
            case ITEM_VIEW:
                itemView = LayoutInflater.from(mContext).inflate(R.layout.fragment_dialog_dealer_item, parent, false);

                // Returns a new DealerSelectHolder with itemView as its view
                return new DealerSelectHolder(itemView);

            case PROGRESS_VIEW:
                itemView = LayoutInflater
                        .from(parent.getContext())
                        .inflate(R.layout.progress_item, parent, false);

                // Returns a new ProgressViewHolder with itemView as its view
                return new ProgressViewHolder(itemView);

            default:
                return null;
        }
    }

    @Override
    public void onBindViewHolder(RecyclerView.ViewHolder holder, int position) {

        if (holder instanceof DealerSelectHolder) {
            final Organization organization = mDataset.get(position);
            ((DealerSelectHolder) holder).mTextViewName.setText(organization.getName());
            ((DealerSelectHolder) holder).mTextViewArea.setText(organization.getArea());
            ((DealerSelectHolder) holder).mImageButtonCall.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    mOnDealerSelected.getSelectedDealer(DealerDialogFragment.PHONE_MODE, organization);
                }
            });
            ((DealerSelectHolder) holder).mImageButtonMail.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    mOnDealerSelected.getSelectedDealer(DealerDialogFragment.EMAIL_MODE, organization);
                }
            });
        } else if (holder instanceof ProgressViewHolder) {
            ((ProgressViewHolder) holder).progressBar.setIndeterminate(true);
        }

    }


    @Override
    public int getItemCount() {
        return mDataset.size();
    }


    @Override
    public int getItemViewType(int position) {
        return mDataset.get(position) != null ? ITEM_VIEW : PROGRESS_VIEW;
    }


    private class DealerSelectHolder extends RecyclerView.ViewHolder {

        TextView mTextViewName;
        TextView mTextViewArea;
        ImageButton mImageButtonCall;
        ImageButton mImageButtonMail;

        DealerSelectHolder(View itemView) {
            super(itemView);
            mTextViewName = (TextView) itemView.findViewById(R.id.fragment_dialog_dealer_item_tv_name);
            mTextViewArea = (TextView) itemView.findViewById(R.id.fragment_dialog_dealer_item_tv_area);
            mImageButtonCall = (ImageButton) itemView.findViewById(R.id.fragment_dialog_dealer_item_imgbtn_call);
            mImageButtonMail = (ImageButton) itemView.findViewById(R.id.fragment_dialog_dealer_item_imgbtn_mail);
        }

    }


    private class ProgressViewHolder extends RecyclerView.ViewHolder {

        ProgressBar progressBar;

        ProgressViewHolder(View itemView) {
            super(itemView);
            progressBar = (ProgressBar) itemView.findViewById(R.id.progress_item_pb);
        }

    }

}
