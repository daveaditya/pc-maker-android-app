package com.maker.android.app.fragment;

import android.os.Bundle;
import android.os.Handler;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v4.app.LoaderManager;
import android.support.v4.content.Loader;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ProgressBar;
import android.widget.TextView;
import android.widget.Toast;

import com.google.gson.Gson;
import com.google.gson.JsonObject;
import com.google.gson.reflect.TypeToken;
import com.maker.android.app.R;
import com.maker.android.app.adapter.TechSupportAdapter;
import com.maker.android.app.classes.Organization;
import com.maker.android.app.loader.AsyncDataDownloader;
import com.maker.android.app.utils.Request;

import java.util.ArrayList;


@SuppressWarnings({"unused"})
public class OrganizationTechSupportFragment
        extends Fragment {

    /**
     * A {@link String} constant to use as tag in {@link android.util.Log}
     */
    private static final String LOG_TAG = OrganizationTechSupportFragment.class.getSimpleName();

    private static final String ORG_REQUEST = "ORG_REQUEST";
    private static final int ORG_LOADER = 15;

    private ArrayList<Organization> mData;
    private TechSupportAdapter mTechSupportAdapter;
    private RecyclerView mRecyclerView;
    private TextView mTextViewEmpty;
    private ProgressBar mProgressBarLoading;

    private String mNextPage = null;


    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {

        View view = inflater.inflate(R.layout.fragment_tech_support_organization, container, false);
        mRecyclerView = (RecyclerView) view.findViewById(R.id.fragment_tech_support_org_rv);
        mRecyclerView.setHasFixedSize(true);

        mTextViewEmpty = (TextView) view.findViewById(R.id.fragment_tech_support_org_tv_empty);
        mProgressBarLoading = (ProgressBar) view.findViewById(R.id.fragment_tech_support_org_pb_loading);

        mData = new ArrayList<>();
        mTechSupportAdapter = new TechSupportAdapter(getContext(), mRecyclerView, mData);
        mTechSupportAdapter.setOnLoadMoreListener(new TechSupportAdapter.OnLoadMoreListener() {
            @Override
            public void loadMore() {
                mData.add(null);
                mRecyclerView.post(new Runnable() {
                    @Override
                    public void run() {
                        mTechSupportAdapter.notifyItemInserted(mData.size() - 1);
                    }
                });
                loadMoreData();
            }
        });
        mRecyclerView.setAdapter(mTechSupportAdapter);

        Bundle args = new Bundle();
        args.putParcelable(ORG_REQUEST, new Request.RequestBuilder()
                .setHttpType(Request.HttpRequestTypes.GET)
                .setURL(getString(R.string.url_for_technician_list))
                .createRequest());

        getActivity()
                .getSupportLoaderManager()
                .initLoader(ORG_LOADER, args, new AsyncOrganizationLoader())
                .forceLoad();
        return view;

    }


    private void loadMoreData() {
        Bundle args = new Bundle();
        args.putParcelable(ORG_REQUEST,
                new Request.RequestBuilder().setURL(mNextPage).setHttpType(Request.HttpRequestTypes.GET).createRequest());

        getActivity()
                .getSupportLoaderManager()
                .restartLoader(ORG_LOADER, args, new AsyncOrganizationLoader())
                .forceLoad();
    }



    private class AsyncOrganizationLoader implements LoaderManager.LoaderCallbacks<String> {

        @Override
        public Loader<String> onCreateLoader(int id, Bundle args) {
            return new AsyncDataDownloader(getContext(), (Request) args.getParcelable(ORG_REQUEST));
        }

        @Override
        public void onLoadFinished(Loader<String> loader, String data) {

            // If the data is null, return
            if (data == null) {
                return;
            }

            Gson gson = new Gson();

            // Creates a JSONObject from the response
            JsonObject jsonObject = gson.fromJson(data, JsonObject.class);

            if (!jsonObject.getAsJsonObject("feedback").get("error_type").isJsonNull()) {
                if (jsonObject.getAsJsonObject("feedback").get("error_type").getAsString().equals("EmptyResultSet")) {

                    if (mNextPage == null) {
                        mProgressBarLoading.setVisibility(View.GONE);
                        mTextViewEmpty.setVisibility(View.VISIBLE);
                        return;
                    }

                    mNextPage = null;
                    mTechSupportAdapter.setHasNewData(false);
                    if (mData.size() != 0) {
                        mData.remove(mData.size() - 1);
                        mTechSupportAdapter.notifyItemRemoved(mData.size());
                    }
                    Toast.makeText(getContext(), "No More Data to Load", Toast.LENGTH_SHORT).show();
                    return;
                }
            }

            // If the server response is FAILURE, show Toast and return
            if (jsonObject.getAsJsonObject("feedback").get("response").getAsString().equals(getResources().getString(R.string.server_failure))) {
                Log.e(LOG_TAG, jsonObject.toString());
                Toast.makeText(getContext(), "Internal Server Error!", Toast.LENGTH_SHORT).show();
                return;
            }

            // Gets the url for the next page
            if (!jsonObject.getAsJsonObject("feedback").get("next_page").isJsonNull()) {
                mNextPage = jsonObject.getAsJsonObject("feedback").get("next_page").getAsString();
                mTechSupportAdapter.setHasNewData(true);
            }

            final ArrayList<Organization> result = gson.fromJson(
                    jsonObject.getAsJsonArray("data").toString(),
                    new TypeToken<ArrayList<Organization>>() {
                    }.getType()
            );

            new Handler().postDelayed(
                    new Runnable() {
                        @Override
                        public void run() {
                            if (mData.size() != 0) {
                                mData.remove(mData.size() - 1);
                                mTechSupportAdapter.notifyItemRemoved(mData.size());
                            }

                            // Creates an empty ArrayList to store CatalogueItem objects
                            mData.addAll(result);

                            // Create a notify dataset change event
                            mTechSupportAdapter.notifyDataSetChanged();

                            mTechSupportAdapter.setLoading(false);

                            // As data is downloaded and ready to be shown, make the ProgressBar GONE
                            mProgressBarLoading.setVisibility(View.GONE);

                            // Make the RecyclerView visible
                            mRecyclerView.setVisibility(View.VISIBLE);
                        }
                    }, 500
            );

        }

        @Override
        public void onLoaderReset(Loader<String> loader) {
            mData.clear();
            if (mTechSupportAdapter != null) {
                mTechSupportAdapter.notifyDataSetChanged();
            }
        }
    }


}
