package com.maker.android.app.utils;

import android.support.annotation.NonNull;

import java.util.regex.Matcher;
import java.util.regex.Pattern;


public class MyUtils {

    /**
     * A Utility method to convert uppercase, lowercase or mixed case strings to title case
     * Example: cat - Cat, CAT - Cat
     *
     * @param stmt String to be converted
     * @return Modified string
     */
    @NonNull
    public static String createWordString(String stmt) {

        // Create a StringBuilder from the input argument
        StringBuilder stringBuilder = new StringBuilder(stmt.toLowerCase());

        // Keeps track whether the previous character was space or not.
        // Since a starting word has no space before it, make this variable's value true
        boolean prevSpace = true;

        // For every character if the current character is space make the prevSpace variable true.
        // If the character is not space and also if prevSpace is true and current character is
        // in lower case make it uppercase
        for (int i = 0; i < stringBuilder.length(); i++) {
            char ch = stringBuilder.charAt(i);
            if (ch == ' ') {
                prevSpace = true;
                continue;
            }
            if (prevSpace && Character.isLowerCase(ch)) {
                stringBuilder.setCharAt(i, Character.toUpperCase(ch));
                prevSpace = false;
            }
        }

        // Return the modified string
        return stringBuilder.toString();
    }


    /**
     * Removes duplicates words from given {@link String}
     *
     * @param string {@link String} containing duplicate words
     * @return duplicates free {@link String}
     */
    public static String removeDuplicates(String string) {

        String duplicatePattern = "\\b([\\w\\s']+) \\1\\b";
        Pattern p = Pattern.compile(duplicatePattern);
        Matcher m = p.matcher(string);
        if (m.matches()) {
            return (m.group(1));
        }
        return (string);
    }

}
