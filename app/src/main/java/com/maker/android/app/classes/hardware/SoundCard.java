package com.maker.android.app.classes.hardware;

import android.os.Parcel;

import com.google.gson.JsonArray;
import com.google.gson.JsonDeserializationContext;
import com.google.gson.JsonDeserializer;
import com.google.gson.JsonElement;
import com.google.gson.JsonObject;
import com.google.gson.JsonParseException;
import com.maker.android.app.annotation.HardwareAnnotations;
import com.maker.android.app.classes.Attribute;
import com.maker.android.app.classes.Offer;
import com.maker.android.app.classes.SimpleHighlighter;
import com.maker.android.app.provider.PreserveContract;

import java.lang.reflect.Type;


@SuppressWarnings({"unused", "WeakerAccess"})
class SoundCard
        extends Hardware {

    /**
     * A {@link String} constant to use as tag in {@link android.util.Log}
     */
    private static final String LOG_TAG = SoundCard.class.getSimpleName();

    public static Deserialize sDeserialize;

    static {
        sDeserialize = new Deserialize();
    }

    @HardwareAnnotations.DisplayData(name = "Chipset", displayInHighlights = true)
    private String mChipset;

    @HardwareAnnotations.DisplayData(name = "Type", displayInHighlights = true)
    private String mType;

    @HardwareAnnotations.DisplayData(name = "Interface")
    private String mInterface;

    @HardwareAnnotations.DisplayData(name = "Connectivity")
    private String mConnectivity;

    @HardwareAnnotations.DisplayData(name = "Supported OS")
    private String mSupportedOs;


    private SoundCard(int id, String companyName, String model) {
        super(id, companyName, model);
        setHighlighter(new SimpleHighlighter());
    }


    public static final Creator<SoundCard> CREATOR = new Creator<SoundCard>() {

        @Override
        public SoundCard createFromParcel(Parcel in) {
            return new SoundCard(in);
        }


        @Override
        public SoundCard[] newArray(int size) {
            return new SoundCard[size];
        }

    };


    SoundCard(Parcel in) {
        super(in);
    }


    @Override
    public int describeContents() {
        return 0;
    }


    @Override
    public PreserveContract.HardwareTypeEntry.HardwareType getHardwareType() {
        return PreserveContract.HardwareTypeEntry.HardwareType.SoundCard;
    }


    public String getChipset() {
        return mChipset;
    }

    public void setChipset(String chipset) {
        mChipset = chipset;
    }

    public String getType() {
        return mType;
    }

    public void setType(String type) {
        mType = type;
    }

    public String getInterface() {
        return mInterface;
    }

    public void setInterface(String anInterface) {
        mInterface = anInterface;
    }

    public String getConnectivity() {
        return mConnectivity;
    }

    public void setConnectivity(String connectivity) {
        mConnectivity = connectivity;
    }

    public String getSupportedOs() {
        return mSupportedOs;
    }

    public void setSupportedOs(String supportedOs) {
        mSupportedOs = supportedOs;
    }


    interface Builder {

        Builder setChipset(String chipset);

        Builder setType(String type);

        Builder setInterface(String aInterface);

        Builder setConnectivity(String connectivity);

        Builder setSupportedOs(String supportedOs);

        Builder setAttributes(Attribute attributes);

        Builder setOffers(Offer[] offers);

        SoundCard createSoundCard();

    }


    private static class SoundCardBuilder implements Builder {

        SoundCard mSoundCard;

        SoundCardBuilder(int id, String companyName, String model) {
            mSoundCard = new SoundCard(id, companyName, model);
        }

        @Override
        public Builder setChipset(String chipset) {
            mSoundCard.setChipset(chipset);
            return this;
        }

        @Override
        public Builder setType(String type) {
            mSoundCard.setType(type);
            return this;
        }

        @Override
        public Builder setInterface(String aInterface) {
            mSoundCard.setInterface(aInterface);
            return this;
        }

        @Override
        public Builder setConnectivity(String connectivity) {
            mSoundCard.setConnectivity(connectivity);
            return this;
        }

        @Override
        public Builder setSupportedOs(String supportedOs) {
            mSoundCard.setSupportedOs(supportedOs);
            return this;
        }

        @Override
        public Builder setAttributes(Attribute attributes) {
            mSoundCard.setAttribute(attributes);
            return this;
        }

        @Override
        public Builder setOffers(Offer[] offers) {
            mSoundCard.setOffers(offers);
            return this;
        }

        @Override
        public SoundCard createSoundCard() {
            return mSoundCard;
        }

    }


    private static class Deserialize implements JsonDeserializer<SoundCard> {

        @Override
        public SoundCard deserialize(JsonElement json, Type typeOfT, JsonDeserializationContext context) throws JsonParseException {

            JsonObject jsonObject = json.getAsJsonObject();

            int soundCardId = 0;
            String companyName = null, model = null;

            if (jsonObject.has("sound_card_id") && !jsonObject.get("sound_card_id").isJsonNull()) {
                soundCardId = jsonObject.get("sound_card_id").getAsInt();
            }

            if (jsonObject.has("company_name") && !jsonObject.get("company_name").isJsonNull()) {
                companyName = jsonObject.get("company_name").getAsString();
            }

            if (jsonObject.has("model") && !jsonObject.get("model").isJsonNull()) {
                model = jsonObject.get("model").getAsString();
            }

            Builder builder = new SoundCardBuilder(soundCardId, companyName, model);

            if (jsonObject.has("chipset") && !jsonObject.get("chipset").isJsonNull()) {
                builder.setChipset(jsonObject.get("chipset").getAsString());
            }

            if (jsonObject.has("type") && !jsonObject.get("type").isJsonNull()) {
                builder.setType(jsonObject.get("type").getAsString());
            }

            if (jsonObject.has("interface") && !jsonObject.get("interface").isJsonNull()) {
                builder.setInterface(jsonObject.get("interface").getAsString());
            }

            if (jsonObject.has("connectivity") && !jsonObject.get("connectivity").isJsonNull()) {
                builder.setConnectivity(jsonObject.get("connectivity").getAsString());
            }

            if (jsonObject.has("supported_os") && !jsonObject.get("supported_os").isJsonNull()) {
                builder.setSupportedOs(jsonObject.get("supported_os").getAsString());
            }

            if (jsonObject.has("attributes") && !jsonObject.get("attributes").isJsonNull()) {
                builder.setAttributes(
                        Attribute.getAttributeFromJsonObject(jsonObject.get("attributes").getAsJsonObject())
                );
            }

            if (jsonObject.has("offers") && !jsonObject.get("offers").isJsonNull()) {
                JsonArray jsonArray = jsonObject.get("offers").getAsJsonArray();
                builder.setOffers(Offer.getOffersFromJsonArray(jsonArray));
            }

            return builder.createSoundCard();

        }

    }

}
