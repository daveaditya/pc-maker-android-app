package com.maker.android.app.adapter;

import android.content.Context;
import android.content.Intent;
import android.net.Uri;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageButton;
import android.widget.ProgressBar;
import android.widget.TextView;
import android.widget.Toast;

import com.maker.android.app.R;
import com.maker.android.app.classes.Organization;
import com.maker.android.app.classes.Technician;

import java.util.ArrayList;


@SuppressWarnings({"unused", "WeakerAccess"})
public class TechSupportAdapter extends RecyclerView.Adapter<RecyclerView.ViewHolder> {

    /**
     * A {@link String} constant to use as tag in {@link android.util.Log}
     */
    private static final String LOG_TAG = TechSupportAdapter.class.getSimpleName();

    private static final int PROGRESS_VIEW = 0;
    private static final int ITEM_VIEW = 1;

    private Context mContext;
    private ArrayList<?> mDataset;

    private int mVisibleThreshold = 2;
    private int mLastVisibleItem, mTotalItemCount;
    private boolean mLoading;
    private boolean mHasNewData;

    private OnLoadMoreListener mOnLoadMoreListener;

    public interface OnLoadMoreListener {
        void loadMore();
    }


    public TechSupportAdapter(Context context, RecyclerView recyclerView, ArrayList<?> dataset) {
        mContext = context;
        mDataset = dataset;

        if (recyclerView.getLayoutManager() instanceof LinearLayoutManager) {

            final LinearLayoutManager linearLayoutManager
                    = (LinearLayoutManager) recyclerView.getLayoutManager();

            recyclerView.addOnScrollListener(new RecyclerView.OnScrollListener() {
                @Override
                public void onScrolled(RecyclerView recyclerView, int dx, int dy) {
                    super.onScrolled(recyclerView, dx, dy);

                    mTotalItemCount = linearLayoutManager.getItemCount();
                    mLastVisibleItem = linearLayoutManager.findLastCompletelyVisibleItemPosition();

                    if (!isLoading() && mTotalItemCount <= (mLastVisibleItem + mVisibleThreshold) && hasNewData()) {
                        if (mOnLoadMoreListener != null) {
                            mOnLoadMoreListener.loadMore();
                        }
                        setLoading(true);
                    }
                }
            });

        }
    }

    public boolean isLoading() {
        return mLoading;
    }

    public void setLoading(boolean loading) {
        this.mLoading = loading;
    }

    public boolean hasNewData() {
        return mHasNewData;
    }

    public void setHasNewData(boolean hasNewData) {
        mHasNewData = hasNewData;
    }

    public void setOnLoadMoreListener(OnLoadMoreListener onLoadMoreListener) {
        mOnLoadMoreListener = onLoadMoreListener;
    }

    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {

        // Creates a new view with the activity_model_list_item.xml layout
        View itemView;
        switch (viewType) {
            case ITEM_VIEW:
                itemView = LayoutInflater.from(mContext).inflate(R.layout.activity_tech_support_item, parent, false);

                // Returns a new ModelListHolder with itemView as its view
                return new TechSupportHolder(itemView);

            case PROGRESS_VIEW:
                itemView = LayoutInflater
                        .from(parent.getContext())
                        .inflate(R.layout.progress_item, parent, false);

                // Returns a new ModelListHolder with itemView as its view
                return new ProgressViewHolder(itemView);

            default:
                return null;
        }

    }

    @Override
    public void onBindViewHolder(RecyclerView.ViewHolder holder, int position) {

        if (holder instanceof TechSupportHolder) {
            if (mDataset.get(position) instanceof Organization) {
                final Organization organization = (Organization) mDataset.get(position);
                ((TechSupportHolder) holder).mTextViewName.setText(organization.getName());

                if (organization.getArea() != null) {
                    ((TechSupportHolder) holder).mTextViewArea.setText(organization.getArea());
                } else {
                    ((TechSupportHolder) holder).mTextViewArea.setText("N/A");
                }

                ((TechSupportHolder) holder).mImageButtonCall.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        dialNumber(organization.getPrimaryMobile());
                    }
                });

                ((TechSupportHolder) holder).mImageButtonMail.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        sendMail(organization.getEmailId());
                    }
                });

            } else if (mDataset.get(position) instanceof Technician) {
                final Technician technician = (Technician) mDataset.get(position);
                ((TechSupportHolder) holder).mTextViewName.setText(technician.getName());

                if (technician.getArea() != null) {
                    ((TechSupportHolder) holder).mTextViewArea.setText(technician.getArea());
                } else {
                    ((TechSupportHolder) holder).mTextViewArea.setText("N/A");
                }

                ((TechSupportHolder) holder).mImageButtonCall.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        dialNumber(technician.getPrimaryMobile());
                    }
                });

                ((TechSupportHolder) holder).mImageButtonMail.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        sendMail(technician.getEmailId());
                    }
                });
            }
        } else if (holder instanceof ProgressViewHolder) {
            ((ProgressViewHolder) holder).progressBar.setIndeterminate(true);
        }

    }


    @Override
    public int getItemCount() {
        return mDataset.size();
    }


    @Override
    public int getItemViewType(int position) {
        return mDataset.get(position) != null ? ITEM_VIEW : PROGRESS_VIEW;
    }


    private class TechSupportHolder extends RecyclerView.ViewHolder {

        TextView mTextViewName;
        TextView mTextViewArea;
        ImageButton mImageButtonCall;
        ImageButton mImageButtonMail;

        TechSupportHolder(View itemView) {
            super(itemView);
            mTextViewName = (TextView) itemView.findViewById(R.id.activity_tech_support_tv_name);
            mTextViewArea = (TextView) itemView.findViewById(R.id.activity_tech_support_tv_area);
            mImageButtonCall = (ImageButton) itemView.findViewById(R.id.activity_tech_support_imgbtn_call);
            mImageButtonMail = (ImageButton) itemView.findViewById(R.id.activity_tech_support_imgbtn_mail);
        }

    }


    /**
     * Holds the {@link ProgressBar} to show loading of data
     */
    private class ProgressViewHolder extends RecyclerView.ViewHolder {

        ProgressBar progressBar;

        ProgressViewHolder(View itemView) {
            super(itemView);
            progressBar = (ProgressBar) itemView.findViewById(R.id.progress_item_pb);
        }

    }


    private void dialNumber(String number) {
        Intent intent = new Intent(Intent.ACTION_DIAL, Uri.parse("tel:" + number));
        if (intent.resolveActivity(mContext.getPackageManager()) != null) {
            mContext.startActivity(intent);
        } else {
            Toast.makeText(mContext, "Dialer not present", Toast.LENGTH_SHORT).show();
        }
    }


    private void sendMail(String emailId) {
        Intent intent = new Intent(Intent.ACTION_SENDTO);
        intent.setData(Uri.parse("mailto:"));
        intent.putExtra(Intent.EXTRA_EMAIL, new String[]{emailId});
        intent.putExtra(Intent.EXTRA_TEXT, "Sent using PC Maker & Services");
        if (intent.resolveActivity(mContext.getPackageManager()) != null) {
            mContext.startActivity(intent);
        } else {
            Toast.makeText(mContext, "Mail client not present", Toast.LENGTH_SHORT).show();
        }
    }

}
