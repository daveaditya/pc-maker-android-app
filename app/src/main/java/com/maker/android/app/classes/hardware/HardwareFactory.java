package com.maker.android.app.classes.hardware;


import android.support.annotation.Nullable;

import com.google.gson.reflect.TypeToken;

import java.lang.reflect.Type;
import java.util.ArrayList;

/**
 *
 */
public final class HardwareFactory {

    /**
     * Returns {@link Class} for the given hardware type using its {@link String} representation
     *
     * @param hardwareType {@link String} representation of the hardware
     * @return Class corresponding to the hardware
     */
    @Nullable
    public static Class getHardwareClass(String hardwareType) {

        try {
            switch (hardwareType) {

                case "Cabinet":
                    return Class.forName("com.maker.android.app.classes.hardware.Cabinet");
                case "ChasisCooler":
                    return Class.forName("com.maker.android.app.classes.hardware.ChasisCooler");
                case "CpuCooler":
                    return Class.forName("com.maker.android.app.classes.hardware.CpuCooler");
                case "GraphicsCard":
                    return Class.forName("com.maker.android.app.classes.hardware.GraphicsCard");
                case "Headphone":
                    return Class.forName("com.maker.android.app.classes.hardware.Headphone");
                case "KeyboardMouse":
                    return Class.forName("com.maker.android.app.classes.hardware.KeyboardMouse");
                case "Monitor":
                    return Class.forName("com.maker.android.app.classes.hardware.Monitor");
                case "Motherboard":
                    return Class.forName("com.maker.android.app.classes.hardware.Motherboard");
                case "MousePad":
                    return Class.forName("com.maker.android.app.classes.hardware.MousePad");
                case "OperatingSystem":
                    return Class.forName("com.maker.android.app.classes.hardware.OperatingSystem");
                case "OpticalDrive":
                    return Class.forName("com.maker.android.app.classes.hardware.OpticalDrive");
                case "PowerSupplyUnit":
                    return Class.forName("com.maker.android.app.classes.hardware.PowerSupplyUnit");
                case "Printer":
                    return Class.forName("com.maker.android.app.classes.hardware.Printer");
                case "IntelProcessor":
                    return Class.forName("com.maker.android.app.classes.hardware.IntelProcessor");
                case "AmdProcessor":
                    return Class.forName("com.maker.android.app.classes.hardware.AmdProcessor");
                case "Ram":
                    return Class.forName("com.maker.android.app.classes.hardware.Ram");
                case "SecondaryStorage":
                    return Class.forName("com.maker.android.app.classes.hardware.SecondaryStorage");
                case "SoundCard":
                    return Class.forName("com.maker.android.app.classes.hardware.SoundCard");
                case "Speaker":
                    return Class.forName("com.maker.android.app.classes.hardware.Speaker");
                case "UninterruptiblePowerSupply":
                    return Class.forName("com.maker.android.app.classes.hardware.UninterruptiblePowerSupply");
                case "Webcam":
                    return Class.forName("com.maker.android.app.classes.hardware.Webcam");
                case "WifiAdapter":
                    return Class.forName("com.maker.android.app.classes.hardware.WifiAdapter");
            }
        } catch (ClassNotFoundException e) {
            e.printStackTrace();
        }

        return null;

    }


    /**
     * Returns the {@link Type} of {@link ArrayList<Hardware>} using the {@link String} representation
     * of the {@link com.maker.android.app.provider.PreserveContract.HardwareTypeEntry.HardwareType}
     * @param hardwareType {@link String} representation of Hardware
     * @return Type associated with ArrayList of Hardware
     */
    @Nullable
    public static Type getArrayListTypeToken(String hardwareType) {

        switch (hardwareType) {

            case "Cabinet":
                return new TypeToken<ArrayList<Cabinet>>() {
                }.getType();
            case "ChasisCooler":
                return new TypeToken<ArrayList<ChasisCooler>>() {
                }.getType();
            case "CpuCooler":
                return new TypeToken<ArrayList<CpuCooler>>() {
                }.getType();
            case "GraphicsCard":
                return new TypeToken<ArrayList<GraphicsCard>>() {
                }.getType();
            case "Headphone":
                return new TypeToken<ArrayList<Headphone>>() {
                }.getType();
            case "KeyboardMouse":
                return new TypeToken<ArrayList<KeyboardMouse>>() {
                }.getType();
            case "Monitor":
                return new TypeToken<ArrayList<Monitor>>() {
                }.getType();
            case "Motherboard":
                return new TypeToken<ArrayList<Motherboard>>() {
                }.getType();
            case "MousePad":
                return new TypeToken<ArrayList<MousePad>>() {
                }.getType();
            case "OperatingSystem":
                return new TypeToken<ArrayList<OperatingSystem>>() {
                }.getType();
            case "OpticalDrive":
                return new TypeToken<ArrayList<OpticalDrive>>() {
                }.getType();
            case "PowerSupplyUnit":
                return new TypeToken<ArrayList<PowerSupplyUnit>>() {
                }.getType();
            case "Printer":
                return new TypeToken<ArrayList<Printer>>() {
                }.getType();
            case "IntelProcessor":
                return new TypeToken<ArrayList<IntelProcessor>>() {
                }.getType();
            case "AmdProcessor":
                return new TypeToken<ArrayList<AmdProcessor>>() {
                }.getType();
            case "Ram":
                return new TypeToken<ArrayList<Ram>>() {
                }.getType();
            case "SecondaryStorage":
                return new TypeToken<ArrayList<SecondaryStorage>>() {
                }.getType();
            case "SoundCard":
                return new TypeToken<ArrayList<SoundCard>>() {
                }.getType();
            case "Speaker":
                return new TypeToken<ArrayList<Speaker>>() {
                }.getType();
            case "UninterruptiblePowerSupply":
                return new TypeToken<ArrayList<UninterruptiblePowerSupply>>() {
                }.getType();
            case "Webcam":
                return new TypeToken<ArrayList<Webcam>>() {
                }.getType();
            case "WifiAdapter":
                return new TypeToken<ArrayList<WifiAdapter>>() {
                }.getType();
            default:
                return null;
        }

    }


    /**
     * Returns the {@link Type} object from the {@link String} representation of
     * the {@link com.maker.android.app.provider.PreserveContract.HardwareTypeEntry.HardwareType}
     *
     * @param hardwareType {@link String} representation of the hardware
     * @return Type associated with the Hardware
     */
    @Nullable
    public static Type getHardwareTypeToken(String hardwareType) {

        switch (hardwareType) {

            case "Cabinet":
                return new TypeToken<Cabinet>() {
                }.getType();
            case "ChasisCooler":
                return new TypeToken<ChasisCooler>() {
                }.getType();
            case "CpuCooler":
                return new TypeToken<CpuCooler>() {
                }.getType();
            case "GraphicsCard":
                return new TypeToken<GraphicsCard>() {
                }.getType();
            case "Headphone":
                return new TypeToken<Headphone>() {
                }.getType();
            case "KeyboardMouse":
                return new TypeToken<KeyboardMouse>() {
                }.getType();
            case "Monitor":
                return new TypeToken<Monitor>() {
                }.getType();
            case "Motherboard":
                return new TypeToken<Motherboard>() {
                }.getType();
            case "MousePad":
                return new TypeToken<MousePad>() {
                }.getType();
            case "OperatingSystem":
                return new TypeToken<OperatingSystem>() {
                }.getType();
            case "OpticalDrive":
                return new TypeToken<OpticalDrive>() {
                }.getType();
            case "PowerSupplyUnit":
                return new TypeToken<PowerSupplyUnit>() {
                }.getType();
            case "Printer":
                return new TypeToken<Printer>() {
                }.getType();
            case "IntelProcessor":
                return new TypeToken<IntelProcessor>() {
                }.getType();
            case "AmdProcessor":
                return new TypeToken<AmdProcessor>() {
                }.getType();
            case "Ram":
                return new TypeToken<Ram>() {
                }.getType();
            case "SecondaryStorage":
                return new TypeToken<SecondaryStorage>() {
                }.getType();
            case "SoundCard":
                return new TypeToken<SoundCard>() {
                }.getType();
            case "Speaker":
                return new TypeToken<Speaker>() {
                }.getType();
            case "UninterruptiblePowerSupply":
                return new TypeToken<UninterruptiblePowerSupply>() {
                }.getType();
            case "Webcam":
                return new TypeToken<Webcam>() {
                }.getType();
            case "WifiAdapter":
                return new TypeToken<WifiAdapter>() {
                }.getType();
            default:
                return null;
        }

    }

}
