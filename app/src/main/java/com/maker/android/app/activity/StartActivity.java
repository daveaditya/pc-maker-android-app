package com.maker.android.app.activity;

import android.app.Activity;
import android.content.Intent;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.design.widget.TextInputLayout;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.View;
import android.widget.EditText;
import android.widget.RadioGroup;
import android.widget.Toast;

import com.maker.android.app.R;
import com.maker.android.app.utils.DownloaderUtils;
import com.maker.android.app.utils.Request;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.HashMap;


@SuppressWarnings({"unused", "FieldCanBeLocal", "HandlerLeak"})
public class StartActivity
        extends AppCompatActivity {

    /**
     * A {@link String} constant to use as TAG in {@link Log}
     */
    private static final String LOG_TAG = StartActivity.class.getSimpleName();

    /**
     * A counter to keep track of number of login attempts
     */
    private static int login_attempts = 0;

    private static final int SIGNUP_REQUEST_CODE = 101;

    EditText mEditTextUsername;
    EditText mEditTextPassword;
    RadioGroup mRadioGroupAccountType;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_start);

        // Gets the View using IDs
        mEditTextUsername = ((TextInputLayout) findViewById(R.id.activity_start_til_username)).getEditText();
        mEditTextPassword = ((TextInputLayout) findViewById(R.id.activity_start_til_password)).getEditText();
        mRadioGroupAccountType = (RadioGroup) findViewById(R.id.activity_start_rb_grp_type_account);
    }


    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        if (requestCode == SIGNUP_REQUEST_CODE && resultCode == Activity.RESULT_OK) {
            Toast.makeText(getBaseContext(), "Account creation successful", Toast.LENGTH_SHORT).show();
        }
    }

    /**
     * Go to the {@link CatalogueActivity} when Browse and Create Build textview
     * is clicked
     *
     * @param view The view that was clicked
     */
    public void gotoCatalogueActivity(View view) {

        // Create an intent with target as the CatalogueActivity
        Intent intent = new Intent(getBaseContext(), CatalogueActivity.class);

        // Start the CatalogueActivity
        startActivity(intent);

    }


    /**
     * Go to the {@link SignupActivity} when Signup button is clicked
     *
     * @param view The view that was clicked
     */
    public void gotoSignupActivity(View view) {

        // Create an intent with target as the CatalogueActivity
        Intent intent = new Intent(getBaseContext(), SignupActivity.class);

        // Start the CatalogueActivity
        startActivityForResult(intent, SIGNUP_REQUEST_CODE);

    }


    /**
     * Verify the credentials and authenticate the login attempt
     *
     * @param view The view that was clicked
     */
    public void loginRequest(View view) {

        HashMap<String, Object> credentials = new HashMap<>();
        // If the username if not entered display error
        String username = mEditTextUsername.getText().toString();
        if (username.isEmpty()) {
            mEditTextUsername.setError("Username not entered!");
        }
        credentials.put("username", username);

        // If the password is not entered display error
        String password = mEditTextPassword.getText().toString();
        if (password.isEmpty()) {
            mEditTextPassword.setError("Password not entered!");
        }
        credentials.put("password", password);

        // String object to store the url
        String url;
        switch (mRadioGroupAccountType.getCheckedRadioButtonId()) {
            // If technician is selected as account type, use the corresponding string resource
            case R.id.activity_start_rb_technician:
                url = getResources().getString(R.string.url_technician_login);
                break;
            // If organization is selected as account type, use the corresponding string resource
            case R.id.activity_start_rb_organization:
                url = getResources().getString(R.string.url_organization_login);
                break;
            // If every thing fails return
            default:
                return;
        }

        login_attempts++;

        Request request = new Request.RequestBuilder()
                .setHttpType(Request.HttpRequestTypes.POST)
                .setURL(url)
                .setData(credentials)
                .createRequest();

        // Since every field is entered, authenticate the user with the server
        new AsyncLogin().execute(request);
    }


    /**
     * An {@link AsyncTask} for authenticating the user credentials
     */
    private class AsyncLogin extends AsyncTask<Request, Void, String> {

        @Override
        protected String doInBackground(Request... params) {
            try {
                // Perform the POST request and return the String data
                return DownloaderUtils.performNetworkRequest(params[0]);
            } catch (Exception exc) {
                exc.printStackTrace();
                return null;
            }
        }


        @Override
        protected void onPostExecute(String s) {

            try {

                // If the String obtained by the request is null some error might have occurred
                // Print and return
                if (s == null) {
                    Toast.makeText(getBaseContext(), getResources().getString(R.string.internal_server_error), Toast.LENGTH_SHORT).show();
                    return;
                }

                // Gets the feedback object from the response
                JSONObject feedback = new JSONObject(s).getJSONObject("feedback");

                // Gets the response string from feedback i.e. SUCCESS or FAILURE
                String response = feedback.getString("response");
                if (response.equals(getResources().getString(R.string.server_success))) {
                    // Create an intent with target as the AccountActivity
                    Intent intent = new Intent(getBaseContext(), AccountHomeActivity.class);

                    // Start the CatalogueActivity
                    startActivity(intent);
                    // If response is fail
                } else {
                    // If the response_type is InvalidUsername show error for username
                    if (feedback.getString("error_type").equals("InvalidUsername")) {
                        mEditTextUsername.setError(getResources().getString(R.string.activity_start_invalid_username));
                        // If the response is InvalidPassword show error for password
                    } else if (feedback.getString("error_type").equals("InvalidPassword")) {
                        mEditTextPassword.setError(getResources().getString(R.string.activity_start_invalid_password));
                    }

                    // If the number of login attempts are greater than 3 disable the login button
                    if (login_attempts > 3) {
                        findViewById(R.id.activity_start_btn_login).setEnabled(false);
                        Toast.makeText(getBaseContext(), getResources().getString(R.string.activity_start_no_more_attempts), Toast.LENGTH_SHORT).show();
                    }
                }
            } catch (JSONException exc) {
                exc.printStackTrace();
            }

        }

    }

}
