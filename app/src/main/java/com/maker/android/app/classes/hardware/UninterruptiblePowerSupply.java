package com.maker.android.app.classes.hardware;

import android.os.Parcel;

import com.google.gson.JsonArray;
import com.google.gson.JsonDeserializationContext;
import com.google.gson.JsonDeserializer;
import com.google.gson.JsonElement;
import com.google.gson.JsonObject;
import com.google.gson.JsonParseException;
import com.maker.android.app.annotation.HardwareAnnotations;
import com.maker.android.app.classes.Attribute;
import com.maker.android.app.classes.Offer;
import com.maker.android.app.classes.SimpleHighlighter;
import com.maker.android.app.provider.PreserveContract;

import java.lang.reflect.Type;


@SuppressWarnings({"unused", "WeakerAccess"})
class UninterruptiblePowerSupply
        extends Hardware {

    /**
     * A {@link String} constant to use as tag in {@link android.util.Log}
     */
    private static final String LOG_TAG = UninterruptiblePowerSupply.class.getSimpleName();

    public static Deserialize sDeserialize;

    static {
        sDeserialize = new Deserialize();
    }

    @HardwareAnnotations.DisplayData(name = "Dimensions", unit = "mm")
    private String mDimensions;

    @HardwareAnnotations.DisplayData(name = "Capacity", displayInHighlights = true)
    private String mCapacity;

    @HardwareAnnotations.DisplayData(name = "Battery", displayInHighlights = true)
    private String mBattery;

    @HardwareAnnotations.DisplayData(name = "Recharge Time")
    private String mRechargeTime;


    public static final Creator<UninterruptiblePowerSupply> CREATOR = new Creator<UninterruptiblePowerSupply>() {

        @Override
        public UninterruptiblePowerSupply createFromParcel(Parcel in) {
            return new UninterruptiblePowerSupply(in);
        }


        @Override
        public UninterruptiblePowerSupply[] newArray(int size) {
            return new UninterruptiblePowerSupply[size];
        }

    };


    private UninterruptiblePowerSupply(int id, String companyName, String model) {
        super(id, companyName, model);
        setHighlighter(new SimpleHighlighter());
    }


    UninterruptiblePowerSupply(Parcel in) {
        super(in);
    }


    @Override
    public int describeContents() {
        return 0;
    }


    @Override
    public PreserveContract.HardwareTypeEntry.HardwareType getHardwareType() {
        return PreserveContract.HardwareTypeEntry.HardwareType.UninterruptiblePowerSupply;
    }


    public String getDimensions() {
        return mDimensions;
    }

    public void setDimensions(String dimensions) {
        mDimensions = dimensions;
    }

    public String getCapacity() {
        return mCapacity;
    }

    public void setCapacity(String capacity) {
        mCapacity = capacity;
    }

    public String getBattery() {
        return mBattery;
    }

    public void setBattery(String battery) {
        mBattery = battery;
    }

    public String getRechargeTime() {
        return mRechargeTime;
    }

    public void setRechargeTime(String rechargeTime) {
        mRechargeTime = rechargeTime;
    }


    interface Builder {

        Builder setDimensions(String dimensions);

        Builder setCapacity(String capacity);

        Builder setBattery(String battery);

        Builder setRechargeTime(String rechargeTime);

        Builder setAttributes(Attribute attributes);

        Builder setOffers(Offer[] offers);

        UninterruptiblePowerSupply createUninterruptiblePowerSupply();

    }


    private static class UninterruptiblePowerSupplyBuilder implements Builder {

        UninterruptiblePowerSupply mUninterruptiblePowerSupply;

        UninterruptiblePowerSupplyBuilder(int id, String companyName, String model) {
            mUninterruptiblePowerSupply = new UninterruptiblePowerSupply(id, companyName, model);
        }

        @Override
        public Builder setDimensions(String dimensions) {
            mUninterruptiblePowerSupply.setDimensions(dimensions);
            return this;
        }

        @Override
        public Builder setCapacity(String capacity) {
            mUninterruptiblePowerSupply.setCapacity(capacity);
            return this;
        }

        @Override
        public Builder setBattery(String battery) {
            mUninterruptiblePowerSupply.setBattery(battery);
            return this;
        }

        @Override
        public Builder setRechargeTime(String rechargeTime) {
            mUninterruptiblePowerSupply.setRechargeTime(rechargeTime);
            return this;
        }

        @Override
        public Builder setAttributes(Attribute attributes) {
            mUninterruptiblePowerSupply.setAttribute(attributes);
            return this;
        }

        @Override
        public Builder setOffers(Offer[] offers) {
            mUninterruptiblePowerSupply.setOffers(offers);
            return this;
        }

        @Override
        public UninterruptiblePowerSupply createUninterruptiblePowerSupply() {
            return mUninterruptiblePowerSupply;
        }

    }


    private static class Deserialize implements JsonDeserializer<UninterruptiblePowerSupply> {

        @Override
        public UninterruptiblePowerSupply deserialize(JsonElement json, Type typeOfT, JsonDeserializationContext context) throws JsonParseException {

            JsonObject jsonObject = json.getAsJsonObject();

            int upsId = 0;
            String companyName = null, model = null;

            if (jsonObject.has("ups_id") && !jsonObject.get("ups_id").isJsonNull()) {
                upsId = jsonObject.get("ups_id").getAsInt();
            }

            if (jsonObject.has("company_name") && !jsonObject.get("company_name").isJsonNull()) {
                companyName = jsonObject.get("company_name").getAsString();
            }

            if (jsonObject.has("model") && !jsonObject.get("model").isJsonNull()) {
                model = jsonObject.get("model").getAsString();
            }

            Builder builder = new UninterruptiblePowerSupplyBuilder(upsId, companyName, model);

            if (jsonObject.has("dimensions") && !jsonObject.get("dimensions").isJsonNull()) {
                builder.setDimensions(jsonObject.get("dimensions").getAsString());
            }

            if (jsonObject.has("capacity") && !jsonObject.get("capacity").isJsonNull()) {
                builder.setCapacity(jsonObject.get("capacity").getAsString());
            }

            if (jsonObject.has("battery") && !jsonObject.get("battery").isJsonNull()) {
                builder.setBattery(jsonObject.get("battery").getAsString());
            }

            if (jsonObject.has("recharge_time") && !jsonObject.get("recharge_time").isJsonNull()) {
                builder.setRechargeTime(jsonObject.get("recharge_time").getAsString());
            }

            if (jsonObject.has("attributes") && !jsonObject.get("attributes").isJsonNull()) {
                builder.setAttributes(
                        Attribute.getAttributeFromJsonObject(jsonObject.get("attributes").getAsJsonObject())
                );
            }

            if (jsonObject.has("offers") && !jsonObject.get("offers").isJsonNull()) {
                JsonArray jsonArray = jsonObject.get("offers").getAsJsonArray();
                builder.setOffers(Offer.getOffersFromJsonArray(jsonArray));
            }

            return builder.createUninterruptiblePowerSupply();

        }

    }

}
