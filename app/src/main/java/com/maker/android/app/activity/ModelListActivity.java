package com.maker.android.app.activity;

import android.content.ContentValues;
import android.content.Intent;
import android.database.sqlite.SQLiteConstraintException;
import android.graphics.Color;
import android.net.Uri;
import android.os.Bundle;
import android.os.Handler;
import android.support.design.widget.Snackbar;
import android.support.v4.app.LoaderManager;
import android.support.v4.app.NavUtils;
import android.support.v4.content.Loader;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.widget.ProgressBar;
import android.widget.Toast;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.google.gson.JsonArray;
import com.google.gson.JsonObject;
import com.maker.android.app.R;
import com.maker.android.app.adapter.ModelListAdapter;
import com.maker.android.app.classes.hardware.Hardware;
import com.maker.android.app.classes.hardware.HardwareFactory;
import com.maker.android.app.fragment.BuildSelectorDialogFragment;
import com.maker.android.app.loader.AsyncDataDownloader;
import com.maker.android.app.provider.PreserveContract;
import com.maker.android.app.utils.Request;
import com.maker.android.app.utils.Request.RequestBuilder;

import java.util.ArrayList;


public class ModelListActivity
        extends AppCompatActivity
        implements ModelListAdapter.OnAddOptionClicked,
        ModelListAdapter.OnDetailsClicked,
        BuildSelectorDialogFragment.OnBuildNameSelected {

    /**
     * A {@link String} constant to use as TAG in {@link Log}
     */
    @SuppressWarnings("unused")
    private static final String LOG_TAG = ModelListActivity.class.getSimpleName();

    /**
     * A {@link String} constant to represent the {@link Request}
     */
    private static final String MODEL_LIST_REQUEST = "MODEL_LIST_REQUEST";

    private static final String BUILD_NAME_DIALOG = "BUILD_NAME_DIALOG";

    /**
     * A {@link String} constant to represent the {@link AsyncDataDownloader}
     */
    private static int MODEL_LIST_CONTINUOUS_LOADER = 500;

    /**
     * Title of the {@link android.app.Activity}
     */
    private static String mTitle = null;

    private String mNextPage = null;

    private Request mRequest;

    /**
     * Used to hold a reference to the loading {@link ProgressBar}
     */
    private ProgressBar mProgressBar;

    /**
     * Used to hold a reference to the {@link RecyclerView}
     */
    private RecyclerView mRecyclerView;

    /**
     * Used to hold a reference to the {@link ModelListAdapter} used by
     * the {@link RecyclerView}
     */
    private ModelListAdapter mModelListAdapter;

    private ArrayList<Hardware> mHardwareArrayList;

    private String mDownloadedData = "";

    private String mDesiredComponent;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_model_list);

        if (savedInstanceState == null) {
            // Retrieve the Request from calling intent
            mRequest = getIntent().getParcelableExtra(CatalogueActivity.MODEL_LIST_REQUEST);

            // Retrieve title from intent
            mTitle = getIntent().getStringExtra(CatalogueActivity.MODEL_TITLE);
        }

        // Gets a reference to the ToolBar and set it as ActionBar
        Toolbar toolbar = (Toolbar) findViewById(R.id.activity_model_list_tb);
        toolbar.setTitle(mTitle);
        setSupportActionBar(toolbar);

        if (getSupportActionBar() != null) {
            getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        }

        // Gets a reference to the RecyclerView
        mRecyclerView = (RecyclerView) findViewById(R.id.activity_model_list_rv_list);
        mRecyclerView.setHasFixedSize(true);

        // Gets a reference to the ProgressBar
        mProgressBar = (ProgressBar) findViewById(R.id.activity_model_list_pb_loading);

        // Initialize ArrayList
        mHardwareArrayList = new ArrayList<>();

        // Create Model List Adapter for the RecyclerView
        mModelListAdapter
                = new ModelListAdapter(ModelListActivity.this, mRecyclerView, mHardwareArrayList);
        mRecyclerView.setAdapter(mModelListAdapter);

        mModelListAdapter.setOnLoadMoreListener(new ModelListAdapter.OnLoadMoreListener() {
            @Override
            public void loadMore() {
                mHardwareArrayList.add(null);
                mRecyclerView.post(new Runnable() {
                    @Override
                    public void run() {
                        mModelListAdapter.notifyItemInserted(mHardwareArrayList.size() - 1);
                    }
                });

                loadMoreData();
            }
        });

        // Create a new Bundle to send to the ModelDataLoader
        Bundle args = new Bundle();

        // Puts the Request into the args Bundle
        args.putParcelable(MODEL_LIST_REQUEST, mRequest);

        // Calls the ModelDataLoader, with id as MODEL_LIST_CONTINUOUS_LOADER and args Bundle
        getSupportLoaderManager()
                .initLoader(MODEL_LIST_CONTINUOUS_LOADER, args, new ModelDataLoader())
                .forceLoad();

    }


    private void loadMoreData() {

        Bundle args = new Bundle();
        args.putParcelable(MODEL_LIST_REQUEST, new RequestBuilder().setURL(mNextPage).setHttpType(Request.HttpRequestTypes.GET).createRequest());

        getSupportLoaderManager()
                .restartLoader(MODEL_LIST_CONTINUOUS_LOADER, args, new ModelDataLoader())
                .forceLoad();

    }


    @Override
    protected void onSaveInstanceState(Bundle outState) {
        super.onSaveInstanceState(outState);
        outState.putParcelable(MODEL_LIST_REQUEST, mRequest);
    }


    @Override
    protected void onRestoreInstanceState(Bundle savedInstanceState) {
        super.onRestoreInstanceState(savedInstanceState);
        mRequest = savedInstanceState.getParcelable(MODEL_LIST_REQUEST);
    }


    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        MenuInflater menuInflater = new MenuInflater(getBaseContext());
        menuInflater.inflate(R.menu.model_list_action_bar, menu);
        return super.onCreateOptionsMenu(menu);
    }


    @Override
    public boolean onOptionsItemSelected(MenuItem item) {

        Intent intent;
        switch (item.getItemId()) {
            case android.R.id.home:
                NavUtils.navigateUpFromSameTask(this);
                return true;
            case R.id.activity_model_list_action_goto_cart:
                intent = new Intent(getBaseContext(), ReviewBuildActivity.class);
                intent.putExtra(ReviewBuildActivity.PARENT_CLASS, ModelListActivity.class.getName());
                startActivity(intent);
                return true;
            case R.id.activity_model_list_action_goto_compare:
                intent = new Intent(getBaseContext(), CompareActivity.class);
                intent.putExtra(CompareActivity.PARENT_CLASS, ModelListActivity.class.getName());
                startActivity(intent);
                return true;
            default:
                return false;
        }

    }

    @Override
    public void getSelectedItem(String inType, int id) {

        Gson gson = new Gson();
        JsonArray jsonArray = gson.fromJson(mDownloadedData, JsonArray.class);

        String idKey = PreserveContract.HardwareTypeEntry.HardwareType.getIdKey(
                PreserveContract.HardwareTypeEntry.HardwareType.getEnumFromName(mTitle)
        );

        for (int i = 0; i < jsonArray.size(); i++) {
            JsonObject jsonObject = jsonArray.get(i).getAsJsonObject();
            if (jsonObject.get(idKey).getAsInt() == id) {
                mDesiredComponent = jsonObject.toString();
            }
        }

        switch (inType) {
            case ModelListAdapter.ADD_TO_BUILD:

                BuildSelectorDialogFragment buildSelectorDialog = new BuildSelectorDialogFragment();
                buildSelectorDialog.show(getSupportFragmentManager(), BUILD_NAME_DIALOG);
                break;

            case ModelListAdapter.ADD_TO_COMPARE:

                ContentValues contentValues = new ContentValues();
                contentValues.put(PreserveContract.CompareEntry.COLUMN_COMPARE_HW_TYPE,
                        PreserveContract.HardwareTypeEntry.HardwareType.getEnumFromName(mTitle).name());
                contentValues.put(PreserveContract.CompareEntry.COLUMN_COMPARE_MODEL_DETAILS, mDesiredComponent);

                try {
                    final Uri uri = getContentResolver().insert(PreserveContract.CompareEntry.CONTENT_URI, contentValues);
                    if (uri == null) {
                        Toast.makeText(getBaseContext(), "Some error!!!", Toast.LENGTH_SHORT).show();
                        return;
                    }

                    Snackbar snackbar = Snackbar.make(mRecyclerView, "Product added for comparison", Snackbar.LENGTH_LONG);
                    snackbar.setAction("UNDO", new View.OnClickListener() {
                        @Override
                        public void onClick(View v) {
                            getContentResolver().delete(uri, null, null);
                        }
                    });
                    snackbar.setActionTextColor(Color.parseColor("white"));
                    snackbar.show();
                } catch (SQLiteConstraintException exc) {
                    Toast.makeText(getBaseContext(), mTitle + " already added to compare!", Toast.LENGTH_SHORT).show();
                }
                break;
        }

    }

    @Override
    public void saveUnderSelectedBuildName(String buildName) {
        ContentValues contentValues = new ContentValues();
        contentValues.put(PreserveContract.BuildEntry.COLUMN_BUILD_NAME, buildName);
        contentValues.put(PreserveContract.BuildEntry.COLUMN_BUILD_HW_TYPE,
                PreserveContract.HardwareTypeEntry.HardwareType.getEnumFromName(mTitle).name());
        contentValues.put(PreserveContract.BuildEntry.COLUMN_BUILD_HW_DESC, mDesiredComponent);

        try {
            final Uri uri = getContentResolver().insert(PreserveContract.BuildEntry.CONTENT_URI, contentValues);
            if (uri == null) {
                Toast.makeText(getBaseContext(), "Some error!!!", Toast.LENGTH_SHORT).show();
                return;
            }

            Snackbar snackbar = Snackbar.make(mRecyclerView, "Product added to build", Snackbar.LENGTH_LONG);
            snackbar.setAction("UNDO", new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    getContentResolver().delete(uri, null, null);
                }
            });
            snackbar.setActionTextColor(Color.parseColor("white"));
            snackbar.show();
        } catch (SQLiteConstraintException exc) {
            Toast.makeText(getBaseContext(), mTitle + " already added to build!", Toast.LENGTH_SHORT).show();
        }
    }

    @Override
    public void openModelDetailsActivity(int id, String hardwareType, String model) {

        Intent intent = new Intent(getBaseContext(), ModelDetailsActivity.class);
        intent.putExtra(ModelDetailsActivity.PARENT_CLASS, ModelListActivity.class.getName());
        intent.putExtra(ModelDetailsActivity.MODEL_ID, id);
        intent.putExtra(ModelDetailsActivity.MODEL_TYPE, hardwareType);
        intent.putExtra(ModelDetailsActivity.TITLE, model);
        startActivity(intent);

    }


    /**
     * A {@link android.support.v4.app.LoaderManager.LoaderCallbacks} class to interact with the
     * {@link LoaderManager} for downloading JSON data from Web Server. And creates {@link Hardware}
     * object from the returned data and sets {@link android.support.v7.widget.RecyclerView.Adapter}
     * for the {@link RecyclerView}
     */
    private final class ModelDataLoader
            implements LoaderManager.LoaderCallbacks<String> {


        /**
         * Creates an {@link AsyncDataDownloader} for the given ID
         *
         * @param id   ID of the Loader
         * @param args {@link Bundle} containing data to pass to the {@link Loader}
         * @return An instance of {@link AsyncDataDownloader}
         */
        @Override
        public Loader<String> onCreateLoader(int id, Bundle args) {
            Request request = args.getParcelable(MODEL_LIST_REQUEST);
            assert request != null;
            return new AsyncDataDownloader(getBaseContext(), request);
        }


        /**
         * Called when the {@link AsyncDataDownloader} completes its task. Here JSON parsing is done
         * and an {@link ArrayList<Hardware>} is created and the {@link RecyclerView}'s adapter is set
         *
         * @param loader {@link Loader} that finished the task
         * @param data   Data generated by the loader
         */
        @Override
        public void onLoadFinished(Loader<String> loader, String data) {

            try {

                // If nothing was returned from the server, do nothing
                if (data == null) {
                    return;
                }

                // Gets the class associated with the current CatalogueItem Component
                final Class clazz = HardwareFactory.getHardwareClass(PreserveContract.HardwareTypeEntry.HardwareType.getEnumFromName(mTitle).name());

                // If null is returned throw new NullPointerException
                if (clazz == null) {
                    throw new NullPointerException();
                }

                // Creates a GSON instance with specified TypeAdapter
                Gson gson = new GsonBuilder().registerTypeAdapter(clazz, clazz.getDeclaredField("sDeserialize").get(null)).create();

                // Creates a JSONObject from the response
                JsonObject jsonObject = gson.fromJson(data, JsonObject.class);

                if (!jsonObject.getAsJsonObject("feedback").get("error_type").isJsonNull()) {
                    if (jsonObject.getAsJsonObject("feedback").get("error_type").getAsString().equals("EmptyResultSet")) {
                        mNextPage = null;
                        mModelListAdapter.setHasNewData(false);
                        if (mHardwareArrayList.size() != 0) {
                            mHardwareArrayList.remove(mHardwareArrayList.size() - 1);
                            mModelListAdapter.notifyItemRemoved(mHardwareArrayList.size());
                        }
                        Toast.makeText(getBaseContext(), "No More Data to Load", Toast.LENGTH_SHORT).show();
                        return;
                    }
                }

                // If the server response is FAILURE, show Toast and return
                if (jsonObject.getAsJsonObject("feedback").get("response").getAsString().equals(getResources().getString(R.string.server_failure))) {
                    Log.e(LOG_TAG, jsonObject.toString());
                    Toast.makeText(getBaseContext(), "Internal Server Error!", Toast.LENGTH_SHORT).show();
                    return;
                }

                // Gets the url for the next page
                if (!jsonObject.getAsJsonObject("feedback").get("next_page").isJsonNull()) {
                    mNextPage = jsonObject.getAsJsonObject("feedback").get("next_page").getAsString();
                    mModelListAdapter.setHasNewData(true);
                }

                // Store the downloaded data
                if (!mDownloadedData.equals("")) {
                    JsonArray jsonArray = gson.fromJson(mDownloadedData, JsonArray.class);
                    jsonArray.addAll(jsonObject.getAsJsonArray("data"));
                    mDownloadedData = jsonArray.toString();
                } else {
                    mDownloadedData = jsonObject.getAsJsonArray("data").toString();
                }

                final ArrayList<? extends Hardware> result = gson.fromJson(
                        jsonObject.getAsJsonArray("data").toString(),
                        HardwareFactory.getArrayListTypeToken(PreserveContract.HardwareTypeEntry.HardwareType.getEnumFromName(mTitle).name()));

                new Handler().postDelayed(
                        new Runnable() {
                            @Override
                            public void run() {
                                if (mHardwareArrayList.size() != 0) {
                                    mHardwareArrayList.remove(mHardwareArrayList.size() - 1);
                                    mModelListAdapter.notifyItemRemoved(mHardwareArrayList.size());
                                }

                                // Creates an empty ArrayList to store CatalogueItem objects
                                mHardwareArrayList.addAll(result);

                                // Create a notify dataset change event
                                mModelListAdapter.notifyDataSetChanged();

                                mModelListAdapter.setLoading(false);

                                // As data is downloaded and ready to be shown, make the ProgressBar GONE
                                mProgressBar.setVisibility(View.GONE);

                                // Make the RecyclerView visible
                                mRecyclerView.setVisibility(View.VISIBLE);
                            }
                        }, 500
                );

                // Fetches JSONException that might occur while parsing JSON data
            } catch (IllegalAccessException | NoSuchFieldException | NullPointerException exc) {
                exc.printStackTrace();
            }

        }


        /**
         * Removes the data references after the {@link Loader} is reset, if any.
         *
         * @param loader {@link Loader} which got reset
         */
        @Override
        public void onLoaderReset(Loader<String> loader) {
            mHardwareArrayList.clear();
            mModelListAdapter.notifyDataSetChanged();
        }

    }

}


