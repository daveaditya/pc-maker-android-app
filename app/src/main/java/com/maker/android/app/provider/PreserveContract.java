package com.maker.android.app.provider;

import android.content.ContentResolver;
import android.net.Uri;
import android.provider.BaseColumns;
import android.support.annotation.Nullable;

import org.jetbrains.annotations.Contract;

@SuppressWarnings("all")
public class PreserveContract {

    static final String CONTENT_AUTHORITY = "com.maker.android.pcmaker.preserveprovider";
    static final String PATH_BUILDS = "builds";
    static final String PATH_COMPARE = "compare";
    static final String PATH_HARDWARE_TYPE = "hardware_types";
    static final String PATH_MY_BUILDS = "my_builds";
    private static final Uri BASE_CONTENT_URI = Uri.parse("content://" + CONTENT_AUTHORITY);


    public static final class BuildEntry implements BaseColumns {

        public static final String TABLE_NAME = "builds_data";

        public static final String _ID = BaseColumns._ID;

        public static final String COLUMN_BUILD_NAME = "build_name";

        public static final String COLUMN_BUILD_HW_TYPE = "hardware_type";

        public static final String COLUMN_BUILD_HW_DESC = "hardware_desc";

        public static final Uri CONTENT_URI = Uri.withAppendedPath(BASE_CONTENT_URI, PATH_BUILDS);

        public static final String CONTENT_LIST_TYPE =
                ContentResolver.CURSOR_DIR_BASE_TYPE + "/" + CONTENT_AUTHORITY + "/" + PATH_BUILDS;

        public static final String CONTENT_ITEM_TYPE =
                ContentResolver.CURSOR_ITEM_BASE_TYPE + "/" + CONTENT_AUTHORITY + "/" + PATH_BUILDS;

    }


    public static final class CompareEntry implements BaseColumns {

        public static final String TABLE_NAME = "compare";

        public static final String _ID = BaseColumns._ID;

        public static final String COLUMN_COMPARE_HW_TYPE = "hardware_type";

        public static final String COLUMN_COMPARE_MODEL_DETAILS = "model_details";

        public static final Uri CONTENT_URI = Uri.withAppendedPath(BASE_CONTENT_URI, PATH_COMPARE);

        public static final String CONTENT_LIST_TYPE =
                ContentResolver.CURSOR_DIR_BASE_TYPE + "/" + CONTENT_AUTHORITY + "/" + PATH_COMPARE;

        public static final String CONTENT_ITEM_TYPE =
                ContentResolver.CURSOR_ITEM_BASE_TYPE + "/" + CONTENT_AUTHORITY + "/" + PATH_COMPARE;

        public static final String CONTENT_HW_TYPE_LIST =
                ContentResolver.CURSOR_DIR_BASE_TYPE + "/" + CONTENT_AUTHORITY + "/HW_TYPE";

    }


    public static final class HardwareTypeEntry implements BaseColumns {

        public static final String TABLE_NAME = "hardware_types";

        public static final String _ID = BaseColumns._ID;

        public static final String COLUMN_HW_TYPE_NAME = "type";

        public static final String COLUMN_HW_IMAGE_SRC = "image_src";

        public static final Uri CONTENT_URI = Uri.withAppendedPath(BASE_CONTENT_URI, PATH_HARDWARE_TYPE);

        public static final String CONTENT_LIST_TYPE =
                ContentResolver.CURSOR_DIR_BASE_TYPE + "/" + CONTENT_AUTHORITY + "/" + PATH_HARDWARE_TYPE;

        public static final String CONTENT_ITEM_TYPE =
                ContentResolver.CURSOR_ITEM_BASE_TYPE + "/" + CONTENT_AUTHORITY + "/" + PATH_HARDWARE_TYPE;

        public enum HardwareType {
            Cabinet("cabinet_id", "cabinets"), ChasisCooler("chasis_cooler_id", "chasis_coolers"),
            CpuCooler("cpu_cooler_id", "cpu_coolers"), GraphicsCard("graphics_card_id", "graphics_cards"),
            Headphone("headphone_id", "headphones"), KeyboardMouse("key_mouse_id", "keyboard_mouses"),
            Monitor("monitor_id", "monitors"), Motherboard("motherboard_id", "motherboards"),
            MousePad("mouse_pad_id", "mouse_pads"), OperatingSystem("operating_system_id", "operating_systems"),
            OpticalDrive("optical_drive_id", "optical_drives"), PowerSupplyUnit("power_supply_unit_id", "psus"),
            Printer("printer_id", "printers"), IntelProcessor("intel_processor_id", "intel_processors"),
            AmdProcessor("amd_processor_id", "amd_processors"), Ram("ram_id", "rams"),
            SecondaryStorage("storage_id", "storages"), SoundCard("sound_card_id", "sound_cards"),
            Speaker("speaker_id", "speakers"), UninterruptiblePowerSupply("ups_id", "ups"),
            Webcam("webcam_id", "webcams"), WifiAdapter("wifi_adapter_id", "wifi_adapters");

            private String idKey;
            private String endPoint;

            HardwareType(String idKey, String endPoint) {
                this.idKey = idKey;
                this.endPoint = endPoint;
            }

            public static String getIdKey(HardwareType hardwareType) {
                return hardwareType.idKey;
            }

            public static HardwareType getEnumFromName(String name) {

                switch (name) {
                    case "Cabinet":
                        return Cabinet;
                    case "Chasis Cooler":
                        return ChasisCooler;
                    case "CPU Cooler":
                        return CpuCooler;
                    case "Graphics Card":
                        return GraphicsCard;
                    case "Headphone":
                        return Headphone;
                    case "Keyboard & Mouse":
                        return KeyboardMouse;
                    case "Monitor":
                        return Monitor;
                    case "Motherboard":
                        return Motherboard;
                    case "Mouse Pad":
                        return MousePad;
                    case "Operating System":
                        return OperatingSystem;
                    case "Optical Drive":
                        return OpticalDrive;
                    case "Power Supply Unit":
                        return PowerSupplyUnit;
                    case "Printer":
                        return Printer;
                    case "Intel Processor":
                        return IntelProcessor;
                    case "AMD Processor":
                        return AmdProcessor;
                    case "RAM":
                        return Ram;
                    case "Secondary Storage":
                        return SecondaryStorage;
                    case "Sound Card":
                        return SoundCard;
                    case "Speaker":
                        return Speaker;
                    case "Uninterruptible Power Supply":
                        return UninterruptiblePowerSupply;
                    case "Webcam":
                        return Webcam;
                    case "WiFi Adapter":
                        return WifiAdapter;
                    default:
                        throw new EnumConstantNotPresentException(HardwareType.class, name);
                }

            }

            public String getEndPoint() {
                return endPoint;
            }

            ;


            @Nullable
            @Contract(pure = true)
            @Override
            public String toString() {

                switch (this) {
                    case Cabinet:
                        return "Cabinet";
                    case ChasisCooler:
                        return "Chasis Cooler";
                    case CpuCooler:
                        return "CPU Cooler";
                    case GraphicsCard:
                        return "Graphics Card";
                    case Headphone:
                        return "Headphone";
                    case KeyboardMouse:
                        return "Keyboard & Mouse";
                    case Monitor:
                        return "Monitor";
                    case Motherboard:
                        return "Motherboard";
                    case MousePad:
                        return "Mouse Pad";
                    case OperatingSystem:
                        return "Operating System";
                    case OpticalDrive:
                        return "Optical Drive";
                    case PowerSupplyUnit:
                        return "Power Supply Unit";
                    case Printer:
                        return "Printer";
                    case IntelProcessor:
                        return "Intel Processor";
                    case AmdProcessor:
                        return "AMD Processor";
                    case Ram:
                        return "RAM";
                    case SecondaryStorage:
                        return "Secondary Storage";
                    case SoundCard:
                        return "Sound Card";
                    case Speaker:
                        return "Speaker";
                    case UninterruptiblePowerSupply:
                        return "Uninterruptible Power Supply";
                    case Webcam:
                        return "Webcam";
                    case WifiAdapter:
                        return "WiFi Adapter";
                    default:
                        return null;
                }

            }

        }

    }


    public static final class MyBuildsEntry implements BaseColumns {

        public static final String TABLE_NAME = "my_builds";

        public static final String _ID = BaseColumns._ID;

        public static final String COLUMN_MY_BUILDS_NAME = "build_name";

        public static final String COLUMN_MY_BUILDS_DATE_TIME = "date_time";

        public static final Uri CONTENT_URI = Uri.withAppendedPath(BASE_CONTENT_URI, PATH_MY_BUILDS);

        public static final String CONTENT_LIST_TYPE =
                ContentResolver.CURSOR_DIR_BASE_TYPE + "/" + CONTENT_AUTHORITY + "/" + PATH_MY_BUILDS;

        public static final String CONTENT_ITEM_TYPE =
                ContentResolver.CURSOR_ITEM_BASE_TYPE + "/" + CONTENT_AUTHORITY + "/" + PATH_MY_BUILDS;

    }
}
