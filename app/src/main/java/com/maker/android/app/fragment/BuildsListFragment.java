package com.maker.android.app.fragment;

import android.content.Context;
import android.database.Cursor;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v4.app.LoaderManager;
import android.support.v4.content.CursorLoader;
import android.support.v4.content.Loader;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.ListView;
import android.widget.TextView;

import com.maker.android.app.R;
import com.maker.android.app.provider.PreserveContract;

@SuppressWarnings({"unused"})
public class BuildsListFragment extends Fragment {

    /**
     * A {@link String} constant to use as tag in {@link android.util.Log}
     */
    private static final String LOG_TAG = BuildsListFragment.class.getSimpleName();

    private static final int MY_BUILD_LIST_LOADER = 11;

    private ListView mListViewBuildList;
    private TextView mTextViewEmpty;
    private ArrayAdapter<String> mBuildAdapter;
    private OnBuildSelected mOnBuildSelected;

    public BuildsListFragment() {
    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        try {
            mOnBuildSelected = (OnBuildSelected) context;
        } catch (ClassCastException exc) {
            exc.printStackTrace();
        }
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_build_list, container, false);

        mListViewBuildList = (ListView) view.findViewById(R.id.fragment_build_list_lv);
        mTextViewEmpty = (TextView) view.findViewById(R.id.fragment_build_list_tv_empty);
        mBuildAdapter = new ArrayAdapter<>(getContext(), android.R.layout.simple_list_item_1);
        mListViewBuildList.setAdapter(mBuildAdapter);

        mListViewBuildList.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                mOnBuildSelected.getSelectedBuild(
                        mBuildAdapter.getItem(position)
                );
            }
        });

        getActivity()
                .getSupportLoaderManager()
                .restartLoader(MY_BUILD_LIST_LOADER, null, new AsyncBuildsLoader())
                .forceLoad();

        return view;
    }


    @Override
    public void onCreateOptionsMenu(Menu menu, MenuInflater inflater) {
        super.onCreateOptionsMenu(menu, inflater);
        menu.findItem(R.id.activity_review_build_action_prev_saved).setVisible(false);
        menu.findItem(R.id.activity_review_build_action_dealer).setVisible(false);
    }


    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {

            case R.id.activity_review_build_action_delete:

                item.setTitle("Discard All");
                getContext().getContentResolver().delete(PreserveContract.MyBuildsEntry.CONTENT_URI, "1", null);
                return true;

            default:
                return false;

        }

    }

    public interface OnBuildSelected {
        void getSelectedBuild(String buildName);
    }

    private class AsyncBuildsLoader implements LoaderManager.LoaderCallbacks<Cursor> {

        @Override
        public Loader<Cursor> onCreateLoader(int id, Bundle args) {
            return new CursorLoader(
                    getContext(),
                    PreserveContract.MyBuildsEntry.CONTENT_URI,
                    new String[]{
                            PreserveContract.MyBuildsEntry.COLUMN_MY_BUILDS_NAME
                    },
                    null,
                    null,
                    null
            );
        }

        @Override
        public void onLoadFinished(Loader<Cursor> loader, Cursor data) {

            if (data.getCount() == 0) {
                mTextViewEmpty.setVisibility(View.VISIBLE);
                mListViewBuildList.setVisibility(View.GONE);
                return;
            }

            mBuildAdapter.clear();
            while (data.moveToNext()) {
                mBuildAdapter.add(data.getString(data.getColumnIndex(PreserveContract.MyBuildsEntry.COLUMN_MY_BUILDS_NAME)));
                mBuildAdapter.notifyDataSetChanged();
            }

            mListViewBuildList.setVisibility(View.VISIBLE);

        }

        @Override
        public void onLoaderReset(Loader<Cursor> loader) {
            mBuildAdapter.clear();
            mBuildAdapter.notifyDataSetChanged();
        }
    }

}

