package com.maker.android.app.classes.hardware;

import android.os.Parcel;

import com.google.gson.JsonArray;
import com.google.gson.JsonDeserializationContext;
import com.google.gson.JsonDeserializer;
import com.google.gson.JsonElement;
import com.google.gson.JsonObject;
import com.google.gson.JsonParseException;
import com.maker.android.app.annotation.HardwareAnnotations;
import com.maker.android.app.classes.Attribute;
import com.maker.android.app.classes.Offer;
import com.maker.android.app.classes.SimpleHighlighter;
import com.maker.android.app.provider.PreserveContract;

import java.lang.reflect.Type;
import java.math.BigDecimal;


@SuppressWarnings({"unused", "WeakerAccess"})
class SecondaryStorage
        extends Hardware {

    /**
     * A {@link String} constant to use as tag in {@link android.util.Log}
     */
    private static final String LOG_TAG = SecondaryStorage.class.getSimpleName();

    public static Deserialize sDeserialize;

    static {
        sDeserialize = new Deserialize();
    }

    @HardwareAnnotations.DisplayData(name = "Type", displayInHighlights = true)
    private String mType;

    @HardwareAnnotations.DisplayData(name = "Technology", displayInHighlights = true)
    private String mTechnology;

    @HardwareAnnotations.DisplayData(name = "Capacity", displayInHighlights = true, unit = "GB")
    private BigDecimal mCapacity;

    @HardwareAnnotations.DisplayData(name = "RPM", unit = "rpm")
    private String mRpm;

    @HardwareAnnotations.DisplayData(name = "Interface")
    private ConnectionInterface mInterface;

    @HardwareAnnotations.DisplayData(name = "Form Factor")
    private String mFormFactor;

    public static final Creator<SecondaryStorage> CREATOR = new Creator<SecondaryStorage>() {

        @Override
        public SecondaryStorage createFromParcel(Parcel in) {
            return new SecondaryStorage(in);
        }


        @Override
        public SecondaryStorage[] newArray(int size) {
            return new SecondaryStorage[size];
        }

    };


    private SecondaryStorage(int id, String companyName, String model) {
        super(id, companyName, model);
        setHighlighter(new SimpleHighlighter());
    }


    SecondaryStorage(Parcel in) {
        super(in);
    }


    @Override
    public int describeContents() {
        return 0;
    }


    @Override
    public PreserveContract.HardwareTypeEntry.HardwareType getHardwareType() {
        return PreserveContract.HardwareTypeEntry.HardwareType.SecondaryStorage;
    }


    public String getType() {
        return mType;
    }

    public void setType(String type) {
        mType = type;
    }

    public String getTechnology() {
        return mTechnology;
    }

    public void setTechnology(String technology) {
        mTechnology = technology;
    }

    public BigDecimal getCapacity() {
        return mCapacity;
    }

    public void setCapacity(BigDecimal capacity) {
        mCapacity = capacity;
    }

    public String getRpm() {
        return mRpm;
    }

    public void setRpm(String rpm) {
        mRpm = rpm;
    }

    public ConnectionInterface getInterface() {
        return mInterface;
    }

    public void setInterface(ConnectionInterface anInterface) {
        mInterface = anInterface;
    }

    public String getFormFactor() {
        return mFormFactor;
    }

    public void setFormFactor(String formFactor) {
        mFormFactor = formFactor;
    }

    interface Builder {

        Builder setType(String type);

        Builder setTechnology(String technology);

        Builder setCapacity(BigDecimal capacity);

        Builder setRpm(String rpm);

        Builder setInterface(ConnectionInterface connectionInterface);

        Builder setFormFactor(String formFactor);

        Builder setAttributes(Attribute attributes);

        Builder setOffers(Offer[] offers);

        SecondaryStorage createSecondaryStorage();

    }


    private static class SecondaryStorageBuilder implements Builder {

        SecondaryStorage mSecondaryStorage;

        SecondaryStorageBuilder(int id, String companyName, String model) {
            mSecondaryStorage = new SecondaryStorage(id, companyName, model);
        }

        @Override
        public Builder setType(String type) {
            mSecondaryStorage.setType(type);
            return this;
        }

        @Override
        public Builder setTechnology(String technology) {
            mSecondaryStorage.setTechnology(technology);
            return this;
        }

        @Override
        public Builder setCapacity(BigDecimal capacity) {
            mSecondaryStorage.setCapacity(capacity);
            return this;
        }

        @Override
        public Builder setRpm(String rpm) {
            mSecondaryStorage.setRpm(rpm);
            return this;
        }

        @Override
        public Builder setInterface(ConnectionInterface connectionInterface) {
            mSecondaryStorage.setInterface(connectionInterface);
            return this;
        }

        @Override
        public Builder setFormFactor(String formFactor) {
            mSecondaryStorage.setFormFactor(formFactor);
            return this;
        }

        @Override
        public Builder setAttributes(Attribute attributes) {
            mSecondaryStorage.setAttribute(attributes);
            return this;
        }

        @Override
        public Builder setOffers(Offer[] offers) {
            mSecondaryStorage.setOffers(offers);
            return this;
        }

        @Override
        public SecondaryStorage createSecondaryStorage() {
            return mSecondaryStorage;
        }

    }


    private static class Deserialize implements JsonDeserializer<SecondaryStorage> {

        @Override
        public SecondaryStorage deserialize(JsonElement json, Type typeOfT, JsonDeserializationContext context) throws JsonParseException {

            JsonObject jsonObject = json.getAsJsonObject();

            int storageId = 0;
            String companyName = null, model = null;

            if (jsonObject.has("storage_id") && !jsonObject.get("storage_id").isJsonNull()) {
                storageId = jsonObject.get("storage_id").getAsInt();
            }

            if (jsonObject.has("company_name") && !jsonObject.get("company_name").isJsonNull()) {
                companyName = jsonObject.get("company_name").getAsString();
            }

            if (jsonObject.has("model") && !jsonObject.get("model").isJsonNull()) {
                model = jsonObject.get("model").getAsString();
            }

            Builder builder = new SecondaryStorageBuilder(storageId, companyName, model);

            if (jsonObject.has("type") && !jsonObject.get("type").isJsonNull()) {
                builder.setType(jsonObject.get("type").getAsString());
            }

            if (jsonObject.has("technology") && !jsonObject.get("technology").isJsonNull()) {
                builder.setTechnology(jsonObject.get("technology").getAsString());
            }

            if (jsonObject.has("capacity") && !jsonObject.get("capacity").isJsonNull()) {
                builder.setCapacity(jsonObject.get("capacity").getAsBigDecimal());
            }

            if (jsonObject.has("rpm") && !jsonObject.get("rpm").isJsonNull()) {
                builder.setRpm(jsonObject.get("rpm").getAsString());
            }

            if (jsonObject.has("interface") && !jsonObject.get("interface").isJsonNull()) {
                builder.setInterface(ConnectionInterface.getEnumValue(jsonObject.get("interface").getAsString()));
            }

            if (jsonObject.has("form_factor") && !jsonObject.get("form_factor").isJsonNull()) {
                builder.setFormFactor(jsonObject.get("form_factor").getAsString());
            }

            if (jsonObject.has("attributes") && !jsonObject.get("attributes").isJsonNull()) {
                builder.setAttributes(
                        Attribute.getAttributeFromJsonObject(jsonObject.get("attributes").getAsJsonObject())
                );
            }

            if (jsonObject.has("offers") && !jsonObject.get("offers").isJsonNull()) {
                JsonArray jsonArray = jsonObject.get("offers").getAsJsonArray();
                builder.setOffers(Offer.getOffersFromJsonArray(jsonArray));
            }

            return builder.createSecondaryStorage();

        }

    }

}
