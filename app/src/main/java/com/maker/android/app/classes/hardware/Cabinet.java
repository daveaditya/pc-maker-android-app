package com.maker.android.app.classes.hardware;


import android.os.Bundle;
import android.os.Parcel;
import android.os.Parcelable;

import com.google.gson.JsonArray;
import com.google.gson.JsonDeserializationContext;
import com.google.gson.JsonDeserializer;
import com.google.gson.JsonElement;
import com.google.gson.JsonObject;
import com.google.gson.JsonParseException;
import com.maker.android.app.annotation.HardwareAnnotations.DisplayData;
import com.maker.android.app.annotation.HardwareAnnotations.DontShowThis;
import com.maker.android.app.classes.Attribute;
import com.maker.android.app.classes.Offer;
import com.maker.android.app.classes.SimpleHighlighter;
import com.maker.android.app.provider.PreserveContract;

import java.util.ArrayList;


@SuppressWarnings({"unused", "WeakerAccess"})
public class Cabinet
        extends Hardware {

    /**
     * A {@link String} constant to use with {@link android.util.Log}
     */
    @DontShowThis
    private static final String LOG_TAG = Cabinet.class.getSimpleName();

    @DontShowThis
    private static final String FANS_PARCEL = "FANS_PARCEL";

    @DontShowThis
    public static Deserialize sDeserialize;

    static {
        sDeserialize = new Deserialize();
    }

    @DisplayData(name = "Type", displayInHighlights = true)
    private Type mType;

    @DisplayData(name = "Color", displayInHighlights = true)
    private String mColor;

    @DisplayData(name = "No. of 2.5\" bays")
    private int noOfBays2_5;

    @DisplayData(name = "No. of 3.5\" bays")
    private int noOfBays3_5;

    @DisplayData(name = "No. of 5.25\" bays")
    private int noOfBays5_25;

    @DisplayData(name = "Maximum Expansion slots")
    private int maxNoOfExpansionSlots;

    @DisplayData(name = "Maximum CPU Cooler Height", unit = "mm")
    private int maxCpuCoolerHeight;

    @DisplayData(name = "Maximum GPU length", unit = "mm")
    private int maxGpuLength;

    @DisplayData(name = "Maximum PSU length", unit = "mm")
    private int maxPsuLength;

    @DisplayData(name = "Fan Support")
    private CabinetFan[] mFans;

    @DisplayData(name = "Supported Motherboard Form Factors")
    private Motherboard.FormFactor[] mMbdFormFactors;

    @DisplayData(name = "Supported PSU Form Factors")
    private PowerSupplyUnit.FormFactor[] mPsuFormFactors;

    @DisplayData(name = "Supported USB versions")
    private ConnectionInterface[] mSupportedUsbVers;


    public Cabinet(int id, String companyName, String model) {
        super(id, companyName, model);
        setHighlighter(new SimpleHighlighter());
    }


    protected Cabinet(Parcel in) {
        super(in);
        mType = Type.values()[in.readInt()];
        mColor = in.readString();
        noOfBays2_5 = in.readInt();
        noOfBays3_5 = in.readInt();
        noOfBays5_25 = in.readInt();
        maxNoOfExpansionSlots = in.readInt();
        maxCpuCoolerHeight = in.readInt();
        maxGpuLength = in.readInt();
        maxPsuLength = in.readInt();

        mFans = (CabinetFan[]) in.readBundle(Cabinet.class.getClassLoader()).getParcelableArray(FANS_PARCEL);

        Motherboard.FormFactor[] mbdFormFactors = Motherboard.FormFactor.values();
        mMbdFormFactors = new Motherboard.FormFactor[in.readInt()];
        for (int i = 0; i < mMbdFormFactors.length; i++) {
            mMbdFormFactors[i] = mbdFormFactors[in.readInt()];
        }

        mPsuFormFactors = new PowerSupplyUnit.FormFactor[in.readInt()];
        PowerSupplyUnit.FormFactor[] psuFormFactors = PowerSupplyUnit.FormFactor.values();
        for (int i = 0; i < mPsuFormFactors.length; i++) {
            mPsuFormFactors[i] = psuFormFactors[in.readInt()];
        }

        mSupportedUsbVers = new ConnectionInterface[in.readInt()];
        ConnectionInterface[] connInterfaces = ConnectionInterface.values();
        for (int i = 0; i < mSupportedUsbVers.length; i++) {
            mSupportedUsbVers[i] = connInterfaces[in.readInt()];
        }

    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        super.writeToParcel(dest, flags);
        dest.writeInt(mType.ordinal());
        dest.writeString(mColor);
        dest.writeInt(noOfBays2_5);
        dest.writeInt(noOfBays3_5);
        dest.writeInt(noOfBays5_25);
        dest.writeInt(maxNoOfExpansionSlots);
        dest.writeInt(maxCpuCoolerHeight);
        dest.writeInt(maxGpuLength);
        dest.writeInt(maxPsuLength);

        Bundle fans = new Bundle();
        fans.putParcelableArray(FANS_PARCEL, mFans);
        dest.writeBundle(fans);

        dest.writeInt(mMbdFormFactors.length);
        for (Motherboard.FormFactor formFactor : mMbdFormFactors) {
            dest.writeInt(formFactor.ordinal());
        }
        dest.writeInt(mPsuFormFactors.length);
        for (PowerSupplyUnit.FormFactor formFactor : mPsuFormFactors) {
            dest.writeInt(formFactor.ordinal());
        }
        dest.writeInt(mSupportedUsbVers.length);
        for (ConnectionInterface connectionInterface : mSupportedUsbVers) {
            dest.writeInt(connectionInterface.ordinal());
        }
    }

    @DontShowThis
    public static final Creator<Cabinet> CREATOR = new Creator<Cabinet>() {

        @Override
        public Cabinet createFromParcel(Parcel in) {
            return new Cabinet(in);
        }

        @Override
        public Cabinet[] newArray(int size) {
            return new Cabinet[size];
        }

    };

    @Override
    public int describeContents() {
        return 0;
    }

    public Type getType() {
        return mType;
    }

    public void setType(Type type) {
        mType = type;
    }

    public String getColor() {
        return mColor;
    }

    public void setColor(String color) {
        mColor = color;
    }

    public int getNoOfBays2_5() {
        return noOfBays2_5;
    }

    public void setNoOfBays2_5(int noOfBays2_5) {
        this.noOfBays2_5 = noOfBays2_5;
    }

    public int getNoOfBays3_5() {
        return noOfBays3_5;
    }

    public void setNoOfBays3_5(int noOfBays3_5) {
        this.noOfBays3_5 = noOfBays3_5;
    }

    public int getNoOfBays5_25() {
        return noOfBays5_25;
    }

    public void setNoOfBays5_25(int noOfBays5_25) {
        this.noOfBays5_25 = noOfBays5_25;
    }

    public int getMaxNoOfExpansionSlots() {
        return maxNoOfExpansionSlots;
    }

    public void setMaxNoOfExpansionSlots(int maxNoOfMExpansionSlots) {
        this.maxNoOfExpansionSlots = maxNoOfMExpansionSlots;
    }

    public int getMaxCpuCoolerHeight() {
        return maxCpuCoolerHeight;
    }

    public void setMaxCpuCoolerHeight(int maxCpuCoolerHeight) {
        this.maxCpuCoolerHeight = maxCpuCoolerHeight;
    }

    public int getMaxGpuLength() {
        return maxGpuLength;
    }

    public void setMaxGpuLength(int maxGpuLength) {
        this.maxGpuLength = maxGpuLength;
    }

    public int getMaxPsuLength() {
        return maxPsuLength;
    }

    public void setMaxPsuLength(int maxPsuLength) {
        this.maxPsuLength = maxPsuLength;
    }

    public CabinetFan[] getFans() {
        return mFans;
    }

    public void setFans(CabinetFan[] fans) {
        mFans = fans;
    }

    public Motherboard.FormFactor[] getMbdFormFactors() {
        return mMbdFormFactors;
    }

    public void setMbdFormFactors(Motherboard.FormFactor[] mbdFormFactors) {
        mMbdFormFactors = mbdFormFactors;
    }

    public PowerSupplyUnit.FormFactor[] getPsuFormFactors() {
        return mPsuFormFactors;
    }

    public void setPsuFormFactors(PowerSupplyUnit.FormFactor[] psuFormFactors) {
        mPsuFormFactors = psuFormFactors;
    }

    public ConnectionInterface[] getSupportedUsbVers() {
        return mSupportedUsbVers;
    }

    public void setSupportedUsbVers(ConnectionInterface[] supportedUsbVers) {
        mSupportedUsbVers = supportedUsbVers;
    }


    @Override
    public PreserveContract.HardwareTypeEntry.HardwareType getHardwareType() {
        return PreserveContract.HardwareTypeEntry.HardwareType.Cabinet;
    }


    public enum Type {

        ATX_DESKTOP("ATX Desktop"), ATX_FULL_TOWER("ATX Full Tower"),
        ATX_MID_TOWER("ATX Mid Tower"), ATX_MINI_TOWER("ATX Mini Tower"),
        ATX_TEST_BENCH("ATX Test Bench"), HTPC("HTPC"), LAN_BOX("Lan Box"),
        MICROATX_DESKTOP("MicroATX Desktop"), MICROATX_MID_TOWER("MicroATX Mid Tower"),
        MICROATX_MINI_TOWER("MicroATX Mini Tower"), MICROATX_SLIM("MicroATX Slim"),
        MINI_ITX_DESKTOP("Mini ITX Desktop"), MINI_ITX_TEST_BENCH("Mini ITX Test Bench"),
        MINI_ITX_TOWER("Mini ITX Tower");

        String description;

        Type(String s) {
            description = s;
        }

        public static Type getEnumValue(String string) throws EnumConstantNotPresentException {

            switch (string) {
                case "ATX Desktop":
                    return ATX_DESKTOP;
                case "ATX Full Tower":
                    return ATX_FULL_TOWER;
                case "ATX Mid Tower":
                    return ATX_MID_TOWER;
                case "ATX Mini Tower":
                    return ATX_MINI_TOWER;
                case "ATX Test Bench":
                    return ATX_TEST_BENCH;
                case "HTPC":
                    return HTPC;
                case "Lan Box":
                    return LAN_BOX;
                case "MicroATX Desktop":
                    return MICROATX_DESKTOP;
                case "MicroATX Mid Tower":
                    return MICROATX_MID_TOWER;
                case "MicroATX Mini Tower":
                    return MICROATX_MINI_TOWER;
                case "MicroATX Slim":
                    return MICROATX_SLIM;
                case "Mini ITX Desktop":
                    return MINI_ITX_DESKTOP;
                case "Mini ITX Test Bench":
                    return MINI_ITX_TEST_BENCH;
                case "Mini ITX Tower":
                    return MINI_ITX_TOWER;
                default:
                    throw new EnumConstantNotPresentException(Type.class, string);
            }

        }

        @Override
        public String toString() {
            return description;
        }
    }


    private static class Deserialize implements JsonDeserializer<Cabinet> {

        @Override
        public Cabinet deserialize(JsonElement json, java.lang.reflect.Type typeOfT, JsonDeserializationContext context) throws JsonParseException {

            JsonObject jsonObject = json.getAsJsonObject();

            int cabinetId = 0;
            String companyName = null, model = null;

            if (jsonObject.has("cabinet_id") && !jsonObject.get("cabinet_id").isJsonNull()) {
                cabinetId = jsonObject.get("cabinet_id").getAsInt();
            }

            if (jsonObject.has("company_name") && !jsonObject.get("company_name").isJsonNull()) {
                companyName = jsonObject.get("company_name").getAsString();
            }

            if (jsonObject.has("model") && !jsonObject.get("model").isJsonNull()) {
                model = jsonObject.get("model").getAsString();
            }

            Builder builder = new CabinetBuilder(cabinetId, companyName, model);

            if (jsonObject.has("type") && !jsonObject.get("type").isJsonNull()) {
                builder.setType(Type.getEnumValue(jsonObject.get("type").getAsString()));
            }

            if (jsonObject.has("color")) {
                if (jsonObject.get("color").isJsonNull()) {
                    builder.setColor("N/A");
                } else {
                    builder.setColor(jsonObject.get("color").getAsString());
                }
            }

            if (jsonObject.has("no_of_bay_2_5") && !jsonObject.get("no_of_bay_2_5").isJsonNull()) {
                builder.setNoOfBays2_5(jsonObject.get("no_of_bay_2_5").getAsInt());
            }

            if (jsonObject.has("no_of_bay_3_5") && !jsonObject.get("no_of_bay_3_5").isJsonNull()) {
                builder.setNoOfBays3_5(jsonObject.get("no_of_bay_3_5").getAsInt());
            }

            if (jsonObject.has("no_of_bay_5_25") && !jsonObject.get("no_of_bay_5_25").isJsonNull()) {
                builder.setNoOfBays5_25(jsonObject.get("no_of_bay_5_25").getAsInt());
            }

            if (jsonObject.has("max_no_of_expansion_slots") && !jsonObject.get("max_no_of_expansion_slots").isJsonNull()) {
                builder.setMaxNoOfExpansionSlots(jsonObject.get("max_no_of_expansion_slots").getAsInt());
            }

            if (jsonObject.has("max_cpu_cooler_height") && !jsonObject.get("max_cpu_cooler_height").isJsonNull()) {
                builder.setMaxCpuCoolerHeight(jsonObject.get("max_cpu_cooler_height").getAsInt());
            }

            if (jsonObject.has("max_gpu_length") && !jsonObject.get("max_gpu_length").isJsonNull()) {
                builder.setMaxGpuLength(jsonObject.get("max_gpu_length").getAsInt());
            }

            if (jsonObject.has("max_psu_length") && !jsonObject.get("max_psu_length").isJsonNull()) {
                builder.setMaxPsuLength(jsonObject.get("max_psu_length").getAsInt());
            }

            ArrayList<CabinetFan> cabinetFanArrayList = null;

            String[] fanDataKeys = {
                    "fan_front", "fan_rear", "fan_side", "fan_top", "fan_bottom", "fan_internal",
                    "fan_mid", "radiator_front", "radiator_rear", "radiator_top", "radiator_top",
                    "radiator_bottom", "radiator_side"
            };

            for (String fanKey : fanDataKeys) {

                if (jsonObject.has(fanKey) && !jsonObject.get(fanKey).isJsonNull()) {

                    if (cabinetFanArrayList == null) {
                        cabinetFanArrayList = new ArrayList<>();
                    }

                    CabinetFan.Type type
                            = CabinetFan.Type.getEnumValue(fanKey.split("_")[0]);
                    CabinetFan.Location location
                            = CabinetFan.Location.getEnumValue(fanKey.split("_")[1]);

                    cabinetFanArrayList.addAll(
                            CabinetFan.getCabinetFanArrayList(type, location, jsonObject.get(fanKey).getAsString()));
                }

            }

            if (cabinetFanArrayList != null) {
                CabinetFan[] data = new CabinetFan[cabinetFanArrayList.size()];
                builder.setFans(cabinetFanArrayList.toArray(data));
            }

            if (jsonObject.has("mbd_form_factors") && !jsonObject.get("mbd_form_factors").isJsonNull()) {

                JsonArray jsonArray = jsonObject.get("mbd_form_factors").getAsJsonArray();

                Motherboard.FormFactor[] formFactors = new Motherboard.FormFactor[jsonArray.size()];
                for (int i = 0; i < jsonArray.size(); i++) {
                    formFactors[i] =
                            Motherboard.FormFactor.getEnumValue(jsonArray.get(i).getAsString());
                }

                builder.setMbdFormFactors(formFactors);
            }

            if (jsonObject.has("psu_form_factors") && !jsonObject.get("psu_form_factors").isJsonNull()) {

                JsonArray jsonArray = jsonObject.get("psu_form_factors").getAsJsonArray();

                PowerSupplyUnit.FormFactor[] formFactors = new PowerSupplyUnit.FormFactor[jsonArray.size()];
                for (int i = 0; i < jsonArray.size(); i++) {
                    formFactors[i] =
                            PowerSupplyUnit.FormFactor.getEnumValue(jsonArray.get(i).getAsString());
                }

                builder.setPsuFormFactors(formFactors);
            }

            if (jsonObject.has("supported_usb_vers") && !jsonObject.get("supported_usb_vers").isJsonNull()) {

                JsonArray jsonArray = jsonObject.get("supported_usb_vers").getAsJsonArray();

                ConnectionInterface[] usbVers = new ConnectionInterface[jsonArray.size()];
                for (int i = 0; i < jsonArray.size(); i++) {
                    usbVers[i] =
                            ConnectionInterface.getEnumValue(jsonArray.get(i).getAsString());
                }

                builder.setSupportedUsbVers(usbVers);
            }

            if (jsonObject.has("attributes") && !jsonObject.get("attributes").isJsonNull()) {
                builder.setAttributes(
                        Attribute.getAttributeFromJsonObject(jsonObject.get("attributes").getAsJsonObject())
                );
            }

            if (jsonObject.has("offers") && !jsonObject.get("offers").isJsonNull()) {
                JsonArray jsonArray = jsonObject.get("offers").getAsJsonArray();
                builder.setOffers(Offer.getOffersFromJsonArray(jsonArray));
            }

            return builder.createCabinet();

        }

    }


    private static class CabinetFan
            implements Parcelable {

        private Type mType;

        private Location mLocation;
        private int mSize;
        private int mQuantity;

        public static final Creator<CabinetFan> CREATOR = new Creator<CabinetFan>() {
            @Override
            public CabinetFan createFromParcel(Parcel in) {
                return new CabinetFan(in);
            }

            @Override
            public CabinetFan[] newArray(int size) {
                return new CabinetFan[size];
            }
        };

        CabinetFan(Type type, Location location, int size, int quantity) {
            mType = type;
            mLocation = location;
            mSize = size;
            mQuantity = quantity;
        }

        protected CabinetFan(Parcel in) {
            mType = Type.values()[in.readInt()];
            mLocation = Location.values()[in.readInt()];
            mSize = in.readInt();
            mQuantity = in.readInt();
        }

        @Override
        public int describeContents() {
            return 0;
        }

        @Override
        public void writeToParcel(Parcel dest, int flags) {
            dest.writeInt(mType.ordinal());
            dest.writeInt(mLocation.ordinal());
            dest.writeInt(mSize);
            dest.writeInt(mQuantity);
        }

        public Type getType() {
            return mType;
        }

        public Location getLocation() {
            return mLocation;
        }

        public int getSize() {
            return mSize;
        }

        public int getQuantity() {
            return mQuantity;
        }

        @Override
        public String toString() {
            return getLocation()
                    + ": " + getType()
                    + " , " + String.valueOf(getSize()) + " mm x "
                    + String.valueOf(getQuantity());
        }

        enum Type {

            FAN("Simple Fan"), RADIATOR("Radiator");

            String description;

            Type(String description) {
                this.description = description;
            }

            public static Type getEnumValue(String string) throws EnumConstantNotPresentException {

                if (string.equals("fan")) {
                    return FAN;
                } else if (string.equals("radiator")) {
                    return RADIATOR;
                }

                throw new EnumConstantNotPresentException(Type.class, string);

            }

            @Override
            public String toString() {
                return description;
            }

        }

        enum Location {

            FRONT("Front"), REAR("Rear"), SIDE("Side"), TOP("Top"), BOTTOM("Bottom"),
            INTERNAL("Internal"), MID("Mid");

            String description;

            Location(String description) {
                this.description = description;
            }

            public static Location getEnumValue(String string) throws EnumConstantNotPresentException {

                switch (string) {

                    case "front":
                        return FRONT;
                    case "rear":
                        return REAR;
                    case "side":
                        return SIDE;
                    case "top":
                        return TOP;
                    case "bottom":
                        return BOTTOM;
                    case "internal":
                        return INTERNAL;
                    case "mid":
                        return MID;
                    default:
                        throw new EnumConstantNotPresentException(Location.class, string);
                }

            }

            @Override
            public String toString() {
                return description;
            }

        }

        static ArrayList<CabinetFan> getCabinetFanArrayList(Type type, Location location, String fanData) {
            ArrayList<CabinetFan> data = new ArrayList<>();
            String[] fans = fanData.split(",");
            for (String fan : fans) {
                String[] single = fan.split("x");
                data.add(
                        new CabinetFan(type, location,
                                Integer.valueOf(single[0].trim()), Integer.valueOf(single[1].trim()))
                );
            }
            return data;
        }

    }


    interface Builder {

        Builder setType(Type type);

        Builder setColor(String color);

        Builder setNoOfBays2_5(int noOfBays2_5);

        Builder setNoOfBays3_5(int noOfBays3_5);

        Builder setNoOfBays5_25(int noOfBays5_25);

        Builder setMaxNoOfExpansionSlots(int maxNoOfExpansionSlots);

        Builder setMaxCpuCoolerHeight(int maxCpuCoolerHeight);

        Builder setMaxGpuLength(int maxGpuLength);

        Builder setMaxPsuLength(int maxPsuLength);

        Builder setFans(CabinetFan[] fans);

        Builder setMbdFormFactors(Motherboard.FormFactor[] mbdFormFactors);

        Builder setPsuFormFactors(PowerSupplyUnit.FormFactor[] psuFormFactors);

        Builder setSupportedUsbVers(ConnectionInterface[] supportedUsbVers);

        Builder setAttributes(Attribute attribute);

        Builder setOffers(Offer[] offers);

        Cabinet createCabinet();

    }


    private static class CabinetBuilder implements Builder {

        private Cabinet mCabinet;


        CabinetBuilder(int id, String companyName, String model) {
            mCabinet = new Cabinet(id, companyName, model);
        }


        @Override
        public Builder setType(Type type) {
            mCabinet.setType(type);
            return this;
        }

        @Override
        public Builder setColor(String color) {
            mCabinet.setColor(color);
            return this;
        }

        @Override
        public Builder setNoOfBays2_5(int noOfBays2_5) {
            mCabinet.setNoOfBays2_5(noOfBays2_5);
            return this;
        }

        @Override
        public Builder setNoOfBays3_5(int noOfBays3_5) {
            mCabinet.setNoOfBays3_5(noOfBays3_5);
            return this;
        }

        @Override
        public Builder setNoOfBays5_25(int noOfBays5_25) {
            mCabinet.setNoOfBays5_25(noOfBays5_25);
            return this;
        }

        @Override
        public Builder setMaxNoOfExpansionSlots(int maxNoOfExpansionSlots) {
            mCabinet.setMaxNoOfExpansionSlots(maxNoOfExpansionSlots);
            return this;
        }

        @Override
        public Builder setMaxCpuCoolerHeight(int maxCpuCoolerHeight) {
            mCabinet.setMaxCpuCoolerHeight(maxCpuCoolerHeight);
            return this;
        }

        @Override
        public Builder setMaxGpuLength(int maxGpuLength) {
            mCabinet.setMaxGpuLength(maxGpuLength);
            return this;
        }

        @Override
        public Builder setMaxPsuLength(int maxPsuLength) {
            mCabinet.setMaxPsuLength(maxPsuLength);
            return this;
        }

        @Override
        public Builder setFans(CabinetFan[] fans) {
            mCabinet.setFans(fans);
            return this;
        }

        @Override
        public Builder setMbdFormFactors(Motherboard.FormFactor[] mbdFormFactors) {
            mCabinet.setMbdFormFactors(mbdFormFactors);
            return this;
        }

        @Override
        public Builder setPsuFormFactors(PowerSupplyUnit.FormFactor[] psuFormFactors) {
            mCabinet.setPsuFormFactors(psuFormFactors);
            return this;
        }

        @Override
        public Builder setSupportedUsbVers(ConnectionInterface[] supportedUsbVers) {
            mCabinet.setSupportedUsbVers(supportedUsbVers);
            return this;
        }


        @Override
        public Builder setAttributes(Attribute attribute) {
            mCabinet.setAttribute(attribute);
            return this;
        }


        @Override
        public Builder setOffers(Offer[] offers) {
            mCabinet.setOffers(offers);
            return this;
        }

        @Override
        public Cabinet createCabinet() {
            return mCabinet;
        }

    }

}