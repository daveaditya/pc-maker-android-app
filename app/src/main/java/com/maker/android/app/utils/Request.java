package com.maker.android.app.utils;


import android.os.Bundle;
import android.os.Parcel;
import android.os.Parcelable;

import java.util.HashMap;


/**
 * Stores the URL to request, type of request and the data to send along with the request
 */
@SuppressWarnings({"unused", "WeakerAccess"})
public class Request implements Parcelable {

    /**
     * A {@link String} constant to use as tag in {@link android.util.Log}
     */
    private static final String LOG_TAG = Request.class.getSimpleName();

    /**
     * A {@link String} constant to represent the {@link HashMap} mData in the {@link Bundle}
     */
    private static final String HASHMAP_DATA = "HASHMAP_DATA";

    /**
     * Stores the URL to send HTTP request to
     */
    private String mURL;

    /**
     * Stores the {@link HttpRequestTypes} for the URL
     */
    private HttpRequestTypes mType;

    /**
     * Stores the data to send along with the request
     */
    private HashMap<String, Object> mData;


    private Request() {
    }


    /**
     * Construct a {@link Request} object from a {@link Parcel}
     *
     * @param in {@link Parcel} containing the data to make {@link Request} object
     */
    protected Request(Parcel in) {
        mURL = in.readString();
        mType = HttpRequestTypes.valueOf(in.readString());
        Bundle hashMapData = in.readBundle(getClass().getClassLoader());
        //noinspection unchecked
        mData = (HashMap<String, Object>) hashMapData.getSerializable(HASHMAP_DATA);
    }


    /**
     * Generated instance of {@link Request} from the {@link Parcel}
     */
    public static final Creator<Request> CREATOR = new Creator<Request>() {

        /**
         * Creates a {@link Request} instance from {@link Parcel}
         *
         * @param in Parcel containing the data
         * @return {@link Request} object made from the {@link Parcel}
         */
        @Override
        public Request createFromParcel(Parcel in) {
            return new Request(in);
        }


        /**
         * Creates an array of {@link Request} of given size
         *
         * @param size Size of the array
         * @return An array of {@link Request} of given size
         */
        @Override
        public Request[] newArray(int size) {
            return new Request[size];
        }

    };


    /**
     * Returns the destination URL
     *
     * @return String url to which request is to be made
     */
    public String getURL() {
        return mURL;
    }


    /**
     * Sets the destination URL of the {@link Request}
     *
     * @param URL String url of the destination
     */
    private void setURL(String URL) {
        mURL = URL;
    }


    /**
     * Returns the {@link HttpRequestTypes} of the {@link Request}
     *
     * @return A {@link HttpRequestTypes} representing the type of HTTP request
     */
    public HttpRequestTypes getType() {
        return mType;
    }


    /**
     * Sets the {@link HttpRequestTypes} of the {@link Request} object
     *
     * @param type The {@link HttpRequestTypes} of the {@link Request} object
     */
    private void setHttpType(HttpRequestTypes type) {
        mType = type;
    }


    /**
     * Returns the data that is to be sent along with the HTTP request
     *
     * @return {@link HashMap} containing the data to be sent
     */
    public HashMap<String, Object> getData() {
        return mData;
    }

    /**
     * Sets the {@link HashMap} data to be sent along with the request
     *
     * @param data {@link HashMap} of data to be sent
     */
    public void setData(HashMap<String, Object> data) {
        mData = data;
    }


    /**
     * Describes the special objects for {@link Parcelable}
     *
     * @return int 0 as nothing is special
     */
    @Override
    public int describeContents() {
        return 0;
    }


    /**
     * Creates a {@link Parcel} for the {@link Request}
     *
     * @param dest  {@link Parcel} in which data is to be written
     * @param flags Special flags if any
     */
    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeString(mURL);
        dest.writeString(mType.toString());

        Bundle hashMapData = new Bundle();
        hashMapData.putSerializable(HASHMAP_DATA, mData);

        dest.writeBundle(hashMapData);
    }


    /**
     * Constants for the valid HTTP Requests
     */
    public enum HttpRequestTypes {
        GET, PUT, POST, DELETE
    }


    interface Builder {

        Builder setURL(String uRl);

        Builder setHttpType(Request.HttpRequestTypes type);

        Builder setData(HashMap<String, Object> data);

        Request createRequest();

    }

    /**
     * A class to build {@link Request} object
     */
    public static final class RequestBuilder implements Builder {

        private Request mRequest;


        /**
         * {@link RequestBuilder} Constructor
         */
        public RequestBuilder() {
            mRequest = new Request();
        }


        /**
         * Sets the destination URL of the {@link Request}
         *
         * @param URL String url of the destination
         */
        public Request.RequestBuilder setURL(String URL) {
            mRequest.setURL(URL);
            return this;
        }


        /**
         * Sets the {@link HttpRequestTypes} of the {@link Request} object
         *
         * @param type The {@link HttpRequestTypes} of the {@link Request} object
         */
        public Request.RequestBuilder setHttpType(HttpRequestTypes type) {
            mRequest.setHttpType(type);
            return this;
        }


        /**
         * Sets the {@link HashMap} data to be sent along with the request
         *
         * @param data {@link HashMap} of data to be sent
         */
        public Request.RequestBuilder setData(HashMap<String, Object> data) {
            mRequest.setData(data);
            return this;
        }

        @Override
        public Request createRequest() {
            return mRequest;
        }

    }

}
