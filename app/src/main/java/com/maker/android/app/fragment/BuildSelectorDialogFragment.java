package com.maker.android.app.fragment;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteConstraintException;
import android.graphics.Color;
import android.net.Uri;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.design.widget.Snackbar;
import android.support.design.widget.TextInputEditText;
import android.support.design.widget.TextInputLayout;
import android.support.v4.app.DialogFragment;
import android.support.v4.app.LoaderManager;
import android.support.v4.content.CursorLoader;
import android.support.v4.content.Loader;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;

import com.maker.android.app.R;
import com.maker.android.app.provider.PreserveContract;


@SuppressWarnings({"unused"})
public class BuildSelectorDialogFragment extends DialogFragment {

    /**
     * A {@link String} constant to use as tag in {@link android.util.Log}
     */
    private static final String LOG_TAG = BuildSelectorDialogFragment.class.getSimpleName();

    private static final int BUILD_LIST_LOADER = 12;

    private OnBuildNameSelected mOnBuildNameSelected;
    private ListView mListViewBuilds;
    private TextView mTextViewEmpty;
    private TextInputLayout mTextInputLayoutName;
    private ArrayAdapter<String> mBuildNameArrayAdapter;

    public BuildSelectorDialogFragment() {
    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        try {
            mOnBuildNameSelected = (OnBuildNameSelected) context;
        } catch (ClassCastException exc) {
            throw new ClassCastException("Must implement OnBuildNameSelected");
        }
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        final View view = inflater.inflate(R.layout.fragment_dialog_select_build, container, false);

        mListViewBuilds = (ListView) view.findViewById(R.id.fragment_dialog_select_build_lv);
        Button buttonCancel = (Button) view.findViewById(R.id.fragment_dialog_select_build_cancel);
        Button buttonAddNew = (Button) view.findViewById(R.id.fragment_dialog_select_build_btn_add_new);
        mTextViewEmpty = (TextView) view.findViewById(R.id.fragment_dialog_build_select_tv_empty);
        mTextInputLayoutName = (TextInputLayout) view.findViewById(R.id.fragment_dialog_build_select_til_new);
        mBuildNameArrayAdapter = new ArrayAdapter<>(getContext(), android.R.layout.simple_list_item_1);

        buttonAddNew.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                TextInputEditText textInputEditText =
                        (TextInputEditText) mTextInputLayoutName.getEditText();

                if (textInputEditText != null) {
                    String input = textInputEditText.getText().toString();
                    if (input.equals("") || input.length() < 6) {
                        textInputEditText.setError("Name should be greater than 6 characters and unique");
                        return;
                    }

                    ContentValues contentValues = new ContentValues();
                    contentValues.put(PreserveContract.MyBuildsEntry.COLUMN_MY_BUILDS_NAME, input);

                    try {
                        final Uri uri = getActivity()
                                .getContentResolver()
                                .insert(PreserveContract.MyBuildsEntry.CONTENT_URI, contentValues);

                        if (uri == null) {
                            Toast.makeText(getContext(), "Some error!!!", Toast.LENGTH_SHORT).show();
                            return;
                        }

                        Snackbar snackbar = Snackbar.make(view, "New build created", Snackbar.LENGTH_LONG);
                        snackbar.setAction("UNDO", new View.OnClickListener() {
                            @Override
                            public void onClick(View v) {
                                getActivity().getContentResolver().delete(uri, null, null);
                            }
                        });
                        snackbar.setActionTextColor(Color.parseColor("white"));
                        snackbar.show();
                    } catch (SQLiteConstraintException exc) {
                        Toast.makeText(getContext(), "Name already taken!", Toast.LENGTH_SHORT).show();
                    }
                }
            }
        });

        buttonCancel.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dismiss();
            }
        });

        mListViewBuilds.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                mOnBuildNameSelected.saveUnderSelectedBuildName(mBuildNameArrayAdapter.getItem(position));
                dismiss();
            }
        });

        getActivity()
                .getSupportLoaderManager()
                .restartLoader(BUILD_LIST_LOADER, null, new AsyncBuildListLoader())
                .forceLoad();

        getDialog().setCanceledOnTouchOutside(false);
        return view;
    }

    public interface OnBuildNameSelected {
        void saveUnderSelectedBuildName(String buildName);
    }

    private class AsyncBuildListLoader implements LoaderManager.LoaderCallbacks<Cursor> {

        @Override
        public Loader<Cursor> onCreateLoader(int id, Bundle args) {
            return new CursorLoader(
                    getContext(),
                    PreserveContract.MyBuildsEntry.CONTENT_URI,
                    new String[]{
                            PreserveContract.MyBuildsEntry.COLUMN_MY_BUILDS_NAME
                    },
                    null,
                    null,
                    null
            );
        }

        @Override
        public void onLoadFinished(Loader<Cursor> loader, Cursor data) {

            if (data.getCount() == 0) {
                mListViewBuilds.setVisibility(View.INVISIBLE);
                mTextViewEmpty.setVisibility(View.VISIBLE);
                return;
            }

            mBuildNameArrayAdapter.clear();
            while (data.moveToNext()) {
                mBuildNameArrayAdapter.add(data.getString(data.getColumnIndex(PreserveContract.MyBuildsEntry.COLUMN_MY_BUILDS_NAME)));
                mBuildNameArrayAdapter.notifyDataSetChanged();
            }
            mListViewBuilds.setAdapter(mBuildNameArrayAdapter);
            mTextViewEmpty.setVisibility(View.INVISIBLE);
            mListViewBuilds.setVisibility(View.VISIBLE);

        }

        @Override
        public void onLoaderReset(Loader<Cursor> loader) {
            mBuildNameArrayAdapter.clear();
            mBuildNameArrayAdapter.notifyDataSetChanged();
        }
    }


}
