package com.maker.android.app.activity;

import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;

import com.maker.android.app.R;


/**
 * Todo: Display Account information
 * Todo: Account Settings - Change Password, Add Technician, Remove Technician, Delete Account
 */
@SuppressWarnings({"unused"})
public class AccountHomeActivity extends AppCompatActivity {

    /**
     * A {@link String} constant to use as TAG in {@link Log}
     */
    private static final String LOG_TAG = AccountHomeActivity.class.getSimpleName();

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_account_home);

        // Gets a reference to the ToolBar and sets is as ActionBar
        Toolbar toolbar = (Toolbar) findViewById(R.id.activity_account_home_tb);
        setSupportActionBar(toolbar);

        if (getSupportActionBar() != null) {
            getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        }

    }


    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        MenuInflater menuInflater = new MenuInflater(getBaseContext());
        menuInflater.inflate(R.menu.accounts_action_bar, menu);
        return super.onCreateOptionsMenu(menu);
    }


    @Override
    public boolean onOptionsItemSelected(MenuItem item) {

        switch (item.getItemId()) {

            case R.id.account_home_action_settings:

                Intent intent = new Intent(getBaseContext(), AccountSettingsActivity.class);
                startActivity(intent);
                break;

        }

        return super.onOptionsItemSelected(item);
    }
}
