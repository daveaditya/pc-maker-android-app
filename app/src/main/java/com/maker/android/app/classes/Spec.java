package com.maker.android.app.classes;

import java.lang.reflect.Array;
import java.util.ArrayList;
import java.util.LinkedHashMap;
import java.util.Map;


@SuppressWarnings({"unused"})
public class Spec {

    /**
     * A {@link String} constant to use in {@link android.util.Log}
     */
    private static final String LOG_TAG = Spec.class.getSimpleName();

    private String mTitle;
    private String mDetails;


    private Spec(String title, String details) {
        mTitle = title;
        mDetails = details;
    }


    public String getTitle() {
        return mTitle;
    }


    public String getDetails() {
        return mDetails;
    }


    public static ArrayList<Spec> getArrayList(LinkedHashMap<String, Object> data) {

        ArrayList<Spec> toSend = new ArrayList<>();

        toSend.add(
                new Spec("Company Name", data.get("Company Name").toString())
        );
        data.remove("Company Name");

        toSend.add(
                new Spec("Model", data.get("Model").toString())
        );
        data.remove("Model");

        for (Map.Entry<String, Object> tuple : data.entrySet()) {

            if (tuple.getValue() == null) {
                toSend.add(new Spec(tuple.getKey(), "N/A"));
                continue;
            }

            StringBuilder detail = new StringBuilder();
            if (tuple.getValue().getClass().isArray()) {
                Object obj = tuple.getValue();
                for (int i = 0; i < Array.getLength(obj); i++) {
                    detail.append(Array.get(obj, i).toString()).append("; ");
                }
                detail.delete(detail.length() - 2, detail.length());
            } else {
                detail.append(tuple.getValue().toString());
            }

            toSend.add(
                    new Spec(tuple.getKey(), detail.toString())
            );
        }

        return toSend;

    }

}
