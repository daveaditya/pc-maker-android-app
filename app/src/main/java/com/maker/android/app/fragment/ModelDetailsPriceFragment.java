package com.maker.android.app.fragment;

import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ProgressBar;
import android.widget.TextView;

import com.google.gson.Gson;
import com.google.gson.JsonArray;
import com.google.gson.JsonObject;
import com.maker.android.app.R;
import com.maker.android.app.activity.ModelDetailsActivity;
import com.maker.android.app.adapter.ModelPriceAdapter;
import com.maker.android.app.classes.Offer;

import java.util.ArrayList;
import java.util.Arrays;

/**
 * A simple {@link Fragment} subclass.
 * to handle interaction events.
 */
@SuppressWarnings({"unused"})
public class ModelDetailsPriceFragment extends Fragment {

    /**
     * A {@link String} constant to use in {@link android.util.Log}
     */
    private static final String LOG_TAG = ModelDetailsPriceFragment.class.getSimpleName();


    public ModelDetailsPriceFragment() {
        // Required empty public constructor
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {

        View view = inflater.inflate(R.layout.fragment_model_detail_prices, container, false);

        RecyclerView recyclerView = (RecyclerView) view.findViewById(R.id.fragment_model_detail_price_rv);
        ProgressBar progressBarLoading = (ProgressBar) view.findViewById(R.id.fragment_model_detail_price_pb_loading);
        TextView textViewNotAvailable = (TextView) view.findViewById(R.id.fragment_model_detail_price_tv_not_available);

        try {
            // Creates a GSON instance with specified TypeAdapter
            Gson gson = new Gson();

            // Creates a JSONObject from the response
            JsonObject jsonObject = gson.fromJson(getArguments().getString(ModelDetailsActivity.SHOW_DETAILS_DATA), JsonObject.class);

            if (jsonObject.has("offers") && !jsonObject.get("offers").isJsonNull()) {
                JsonArray offersJsonArray = jsonObject.getAsJsonArray("offers");
                ArrayList<Offer> offersArrayList
                        = new ArrayList<>(Arrays.asList(Offer.getOffersFromJsonArray(offersJsonArray)));
                progressBarLoading.setVisibility(View.GONE);
                recyclerView.setAdapter(new ModelPriceAdapter(getContext(), offersArrayList));
                recyclerView.setVisibility(View.VISIBLE);
            } else {
                progressBarLoading.setVisibility(View.GONE);
                textViewNotAvailable.setVisibility(View.VISIBLE);
            }

            // Fetches JSONException that might occur while parsing JSON data
        } catch (NullPointerException exc) {
            exc.printStackTrace();
        }

        return view;
    }

}
