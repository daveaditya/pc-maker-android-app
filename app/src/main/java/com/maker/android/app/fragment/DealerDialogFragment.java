package com.maker.android.app.fragment;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.os.Handler;
import android.support.annotation.Nullable;
import android.support.v4.app.DialogFragment;
import android.support.v4.app.LoaderManager;
import android.support.v4.content.Loader;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ProgressBar;
import android.widget.TextView;
import android.widget.Toast;

import com.google.gson.Gson;
import com.google.gson.JsonObject;
import com.google.gson.reflect.TypeToken;
import com.maker.android.app.R;
import com.maker.android.app.adapter.DealerSelectAdapter;
import com.maker.android.app.classes.Organization;
import com.maker.android.app.loader.AsyncDataDownloader;
import com.maker.android.app.utils.Request;

import java.util.ArrayList;


@SuppressWarnings({"unused"})
public class DealerDialogFragment
        extends DialogFragment {

    /**
     * A {@link String} constant to use as tag in {@link android.util.Log}
     */
    private static final String LOG_TAG = DealerDialogFragment.class.getSimpleName();

    public static final int REQUEST_CODE = 11;
    public static final String SELECTED_MODE = "SELECTED_MODE";
    public static final String SELECTED_ORG = "SELECTED_ORG";

    public static final String EMAIL_MODE = "EMAIL";
    public static final String PHONE_MODE = "PHONE";

    private static final String DEALER_REQUEST = "DEALER_REQUEST";
    private static final int DEALER_LIST_LOADER = 33;

    private ArrayList<Organization> mData;
    private DealerSelectAdapter mDealerSelectAdapter;
    private RecyclerView mRecyclerView;
    private TextView mTextViewEmpty;
    private ProgressBar mProgressBarLoading;
    private Button mButtonCancel;

    private String mNextPage = null;

    public DealerDialogFragment() {

    }


    @Nullable
    @Override
    public View onCreateView(final LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_dialog_dealer, container, false);
        getDialog().setCanceledOnTouchOutside(false);

        mButtonCancel = (Button) view.findViewById(R.id.fragment_dialog_dealer_btn_cancel);
        mButtonCancel.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dismiss();
            }
        });
        mTextViewEmpty = (TextView) view.findViewById(R.id.fragment_dialog_dealer_tv_empty);
        mProgressBarLoading = (ProgressBar) view.findViewById(R.id.fragment_dialog_dealer_pb_loading);

        mRecyclerView = (RecyclerView) view.findViewById(R.id.fragment_dialog_dealer_rv);
        mRecyclerView.setHasFixedSize(true);

        DealerSelectAdapter.OnDealerSelected onDealerSelected = new DealerSelectAdapter.OnDealerSelected() {
            @Override
            public void getSelectedDealer(String mode, Organization organization) {
                Intent intent = new Intent();
                intent.putExtra(SELECTED_MODE, mode);
                intent.putExtra(SELECTED_ORG, organization);
                getTargetFragment()
                        .onActivityResult(REQUEST_CODE, Activity.RESULT_OK, intent);
                dismiss();
            }
        };
        mData = new ArrayList<>();
        mDealerSelectAdapter = new DealerSelectAdapter(getContext(), mRecyclerView, mData, onDealerSelected);
        mDealerSelectAdapter.setOnLoadMoreListener(new DealerSelectAdapter.OnLoadMoreListener() {
            @Override
            public void loadMore() {
                mData.add(null);
                mRecyclerView.post(new Runnable() {
                    @Override
                    public void run() {
                        mDealerSelectAdapter.notifyItemInserted(mData.size() - 1);
                    }
                });
                loadMoreData();
            }
        });
        mRecyclerView.setAdapter(mDealerSelectAdapter);

        Bundle args = new Bundle();
        args.putParcelable(DEALER_REQUEST, new Request.RequestBuilder()
                .setHttpType(Request.HttpRequestTypes.GET)
                .setURL(getString(R.string.url_for_organization_list))
                .createRequest());

        getActivity()
                .getSupportLoaderManager()
                .restartLoader(DEALER_LIST_LOADER, args, new AsyncDealerLoader())
                .forceLoad();
        return view;
    }


    private void loadMoreData() {
        Bundle args = new Bundle();
        args.putParcelable(DEALER_REQUEST,
                new Request.RequestBuilder().setURL(mNextPage).setHttpType(Request.HttpRequestTypes.GET).createRequest());

        getActivity()
                .getSupportLoaderManager()
                .restartLoader(DEALER_LIST_LOADER, args, new AsyncDealerLoader())
                .forceLoad();
    }


    private class AsyncDealerLoader implements LoaderManager.LoaderCallbacks<String> {

        @Override
        public Loader<String> onCreateLoader(int id, Bundle args) {
            return new AsyncDataDownloader(getContext(), (Request) args.getParcelable(DEALER_REQUEST));
        }

        @Override
        public void onLoadFinished(Loader<String> loader, String data) {

            // If the data is null, return
            if (data == null) {
                return;
            }

            Gson gson = new Gson();

            // Creates a JSONObject from the response
            JsonObject jsonObject = gson.fromJson(data, JsonObject.class);

            if (!jsonObject.getAsJsonObject("feedback").get("error_type").isJsonNull()) {
                if (jsonObject.getAsJsonObject("feedback").get("error_type").getAsString().equals("EmptyResultSet")) {

                    if (mNextPage == null) {
                        mProgressBarLoading.setVisibility(View.GONE);
                        mTextViewEmpty.setVisibility(View.VISIBLE);
                        return;
                    }

                    mNextPage = null;
                    mDealerSelectAdapter.setHasNewData(false);
                    if (mData.size() != 0) {
                        mData.remove(mData.size() - 1);
                        mDealerSelectAdapter.notifyItemRemoved(mData.size());
                    }
                    Toast.makeText(getContext(), "No More Data to Load", Toast.LENGTH_SHORT).show();
                    return;
                }
            }

            // If the server response is FAILURE, show Toast and return
            if (jsonObject.getAsJsonObject("feedback").get("response").getAsString().equals(getResources().getString(R.string.server_failure))) {
                Log.e(LOG_TAG, jsonObject.toString());
                Toast.makeText(getContext(), "Internal Server Error!", Toast.LENGTH_SHORT).show();
                return;
            }

            // Gets the url for the next page
            if (!jsonObject.getAsJsonObject("feedback").get("next_page").isJsonNull()) {
                mNextPage = jsonObject.getAsJsonObject("feedback").get("next_page").getAsString();
                mDealerSelectAdapter.setHasNewData(true);
            }

            final ArrayList<Organization> result = gson.fromJson(
                    jsonObject.getAsJsonArray("data").toString(),
                    new TypeToken<ArrayList<Organization>>() {
                    }.getType()
            );

            new Handler().postDelayed(
                    new Runnable() {
                        @Override
                        public void run() {
                            if (mData.size() != 0) {
                                mData.remove(mData.size() - 1);
                                mDealerSelectAdapter.notifyItemRemoved(mData.size());
                            }

                            for (int i = 0; i < result.size(); i++) {
                                if (result.get(i).getType().equals("InfoTech")) {
                                    continue;
                                }
                                mData.add(result.get(i));
                            }

                            // Create a notify dataset change event
                            mDealerSelectAdapter.notifyDataSetChanged();

                            mDealerSelectAdapter.setLoading(false);

                            // As data is downloaded and ready to be shown, make the ProgressBar GONE
                            mProgressBarLoading.setVisibility(View.GONE);

                            // Make the RecyclerView visible
                            mRecyclerView.setVisibility(View.VISIBLE);

                            mButtonCancel.setVisibility(View.VISIBLE);
                        }
                    }, 500
            );

        }

        @Override
        public void onLoaderReset(Loader<String> loader) {
            mData.clear();
            if (mDealerSelectAdapter != null) {
                mDealerSelectAdapter.notifyDataSetChanged();
            }
        }
    }

}
