package com.maker.android.app.classes.hardware;


import android.os.Parcel;

import com.google.gson.JsonArray;
import com.google.gson.JsonDeserializationContext;
import com.google.gson.JsonDeserializer;
import com.google.gson.JsonElement;
import com.google.gson.JsonObject;
import com.google.gson.JsonParseException;
import com.maker.android.app.annotation.HardwareAnnotations;
import com.maker.android.app.classes.Attribute;
import com.maker.android.app.classes.Offer;
import com.maker.android.app.classes.SimpleHighlighter;
import com.maker.android.app.provider.PreserveContract;

import java.lang.reflect.Type;

@SuppressWarnings({"unused private", "WeakerAccess"})
class OperatingSystem
        extends Hardware {

    private static final String LOG_TAG = OperatingSystem.class.getSimpleName();

    public static Deserialize sDeserialize;

    static {
        sDeserialize = new Deserialize();
    }

    @HardwareAnnotations.DisplayData(name = "Name")
    private String mName;

    @HardwareAnnotations.DisplayData(name = "Type")
    private String mType;

    @HardwareAnnotations.DisplayData(name = "Bit", displayInHighlights = true, unit = "bit")
    private String mBit;

    @HardwareAnnotations.DisplayData(name = "Edition", displayInHighlights = true)
    private String mEdition;

    @HardwareAnnotations.DisplayData(name = "Version")
    private String mVersion;


    public static final Creator<OperatingSystem> CREATOR = new Creator<OperatingSystem>() {

        @Override
        public OperatingSystem createFromParcel(Parcel in) {
            return new OperatingSystem(in);
        }


        @Override
        public OperatingSystem[] newArray(int size) {
            return new OperatingSystem[size];
        }

    };


    private OperatingSystem(int id, String companyName) {
        super(id, companyName, null);
        setHighlighter(new SimpleHighlighter());
    }


    private OperatingSystem(Parcel in) {
        super(in);
    }


    public int describeContents() {
        return 0;
    }


    @Override
    public PreserveContract.HardwareTypeEntry.HardwareType getHardwareType() {
        return PreserveContract.HardwareTypeEntry.HardwareType.OperatingSystem;
    }


    public String getName() {
        return mName;
    }

    public void setName(String name) {
        mName = name;
    }

    public String getType() {
        return mType;
    }

    public void setType(String type) {
        mType = type;
    }

    public String getBit() {
        return mBit;
    }

    public void setBit(String bit) {
        mBit = bit;
    }

    public String getEdition() {
        return mEdition;
    }

    public void setEdition(String edition) {
        mEdition = edition;
    }

    public String getVersion() {
        return mVersion;
    }

    public void setVersion(String version) {
        mVersion = version;
    }


    interface Builder {

        Builder setModel(String model);

        Builder setName(String name);

        Builder setType(String type);

        Builder setBit(String bit);

        Builder setEdition(String edition);

        Builder setVersion(String version);

        Builder setAttributes(Attribute attributes);

        Builder setOffers(Offer[] offers);

        OperatingSystem createOperatingSystem();

    }


    private static class OperatingSystemBuilder implements Builder {

        OperatingSystem mOperatingSystem;

        OperatingSystemBuilder(int id, String companyName) {
            mOperatingSystem = new OperatingSystem(id, companyName);
        }

        @Override
        public Builder setModel(String model) {
            mOperatingSystem.setModel(model);
            return this;
        }

        @Override
        public Builder setName(String name) {
            mOperatingSystem.setName(name);
            return this;
        }

        @Override
        public Builder setType(String type) {
            mOperatingSystem.setType(type);
            return this;
        }

        @Override
        public Builder setBit(String bit) {
            mOperatingSystem.setBit(bit);
            return this;
        }

        @Override
        public Builder setEdition(String edition) {
            mOperatingSystem.setEdition(edition);
            return this;
        }

        @Override
        public Builder setVersion(String version) {
            mOperatingSystem.setVersion(version);
            return this;
        }

        @Override
        public Builder setAttributes(Attribute attributes) {
            mOperatingSystem.setAttribute(attributes);
            return this;
        }

        @Override
        public Builder setOffers(Offer[] offers) {
            mOperatingSystem.setOffers(offers);
            return this;
        }

        @Override
        public OperatingSystem createOperatingSystem() {
            return mOperatingSystem;
        }

    }


    private static class Deserialize implements JsonDeserializer<OperatingSystem> {

        @Override
        public OperatingSystem deserialize(JsonElement json, Type typeOfT, JsonDeserializationContext context) throws JsonParseException {

            JsonObject jsonObject = json.getAsJsonObject();

            int operatingSystemId = 0;
            String companyName = null, model = null;

            if (jsonObject.has("operating_system_id") && !jsonObject.get("operating_system_id").isJsonNull()) {
                operatingSystemId = jsonObject.get("operating_system_id").getAsInt();
            }

            if (jsonObject.has("company_name") && !jsonObject.get("company_name").isJsonNull()) {
                companyName = jsonObject.get("company_name").getAsString();
            }

            Builder builder = new OperatingSystemBuilder(operatingSystemId, companyName);

            if (jsonObject.has("name") && !jsonObject.get("name").isJsonNull()) {
                builder.setName(jsonObject.get("name").getAsString());
            }

            if (jsonObject.has("type") && !jsonObject.get("type").isJsonNull()) {
                builder.setType(jsonObject.get("type").getAsString());
            }

            if (jsonObject.has("bit") && !jsonObject.get("bit").isJsonNull()) {
                builder.setBit(jsonObject.get("bit").getAsString());
            }

            if (jsonObject.has("edition") && !jsonObject.get("edition").isJsonNull()) {
                builder.setEdition(jsonObject.get("edition").getAsString());
            }

            if (jsonObject.has("version") && !jsonObject.get("version").isJsonNull()) {
                builder.setVersion(jsonObject.get("version").getAsString());
            }

            if (jsonObject.has("attributes") && !jsonObject.get("attributes").isJsonNull()) {
                builder.setAttributes(
                        Attribute.getAttributeFromJsonObject(jsonObject.get("attributes").getAsJsonObject())
                );
            }

            if (jsonObject.has("offers") && !jsonObject.get("offers").isJsonNull()) {
                JsonArray jsonArray = jsonObject.get("offers").getAsJsonArray();
                builder.setOffers(Offer.getOffersFromJsonArray(jsonArray));
            }

            OperatingSystem operatingSystem = builder.createOperatingSystem();
            if (operatingSystem.getVersion() != null) {
                operatingSystem.setModel(operatingSystem.getName() + " " + operatingSystem.getVersion());
            } else {
                operatingSystem.setModel(operatingSystem.getName());
            }

            return builder.createOperatingSystem();

        }

    }

}
