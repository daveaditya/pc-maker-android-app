package com.maker.android.app.activity;

import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.Menu;
import android.view.MenuInflater;

import com.maker.android.app.R;
import com.maker.android.app.fragment.BuildDetailsFragment;
import com.maker.android.app.fragment.BuildsListFragment;


@SuppressWarnings({"unused"})
public class ReviewBuildActivity
        extends AppCompatActivity
        implements BuildsListFragment.OnBuildSelected, BuildDetailsFragment.OpenModelDetails {

    /**
     * A {@link String} constant to use as tag in {@link android.util.Log}
     */
    private static final String LOG_TAG = ReviewBuildActivity.class.getSimpleName();

    public static final String PARENT_CLASS = "ReviewBuildActivity.PARENT_CLASS";
    public static final String SELECTED_BUILD = "SELECTED_BUILD";
    private static final String BUILD_LIST_FRAGMENT = "BUILD_LIST_FRAGMENT";
    private static final String BUILD_DETAILS_FRAGMENT = "BUILD_DETAILS_FRAGMENT";

    private BuildDetailsFragment mBuildDetailsFragment;


    @Override
    protected void onCreate(Bundle savedInstanceState) {

        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_review_build);

        Toolbar toolbar = (Toolbar) findViewById(R.id.activity_build_tb);
        toolbar.setTitle("My Build");
        setSupportActionBar(toolbar);

        if (getSupportActionBar() != null) {
            getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        }

        BuildsListFragment buildsListFragment = new BuildsListFragment();
        mBuildDetailsFragment = new BuildDetailsFragment();

        buildsListFragment.setHasOptionsMenu(true);
        mBuildDetailsFragment.setHasOptionsMenu(true);

        getSupportFragmentManager()
                .beginTransaction()
                .add(R.id.activity_build_fragment, buildsListFragment, BUILD_LIST_FRAGMENT)
                .commit();

    }


    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        MenuInflater menuInflater = new MenuInflater(getBaseContext());
        menuInflater.inflate(R.menu.review_build_action_bar, menu);
        return super.onCreateOptionsMenu(menu);
    }


    @Nullable
    @Override
    public Intent getParentActivityIntent() {
        Intent parentIntent = getIntent();
        String className = parentIntent.getStringExtra(PARENT_CLASS);

        Intent newIntent = null;
        try {
            //you need to define the class with package name
            newIntent = new Intent(getBaseContext(), Class.forName(className));
        } catch (ClassNotFoundException e) {
            e.printStackTrace();
        }
        return newIntent;
    }


    @Override
    public void getSelectedBuild(String buildName) {
        Bundle args = new Bundle();
        args.putString(SELECTED_BUILD, buildName);
        mBuildDetailsFragment.setArguments(args);
        getSupportFragmentManager()
                .beginTransaction()
                .replace(R.id.activity_build_fragment, mBuildDetailsFragment, BUILD_DETAILS_FRAGMENT)
                .commit();
    }


    @Override
    public void openModelDetails(int id, String hardwareType, String model) {

        Intent intent = new Intent(getBaseContext(), ModelDetailsActivity.class);
        intent.putExtra(ModelDetailsActivity.PARENT_CLASS, ReviewBuildActivity.class.getName());
        intent.putExtra(ModelDetailsActivity.TITLE, model);
        intent.putExtra(ModelDetailsActivity.MODEL_TYPE, hardwareType);
        intent.putExtra(ModelDetailsActivity.MODEL_ID, id);
        startActivity(intent);

    }

}
