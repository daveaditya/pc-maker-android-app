package com.maker.android.app.classes;

import android.os.Parcel;
import android.os.Parcelable;

import com.google.gson.JsonArray;
import com.google.gson.JsonObject;

import java.math.BigDecimal;
import java.text.NumberFormat;
import java.util.Locale;


@SuppressWarnings("unused")
public class Offer implements Parcelable {

    /**
     * A {@link String} constant to use as tag in {@link android.util.Log}
     */
    private static final String LOG_TAG = Offer.class.getSimpleName();

    private String mPartnerName;
    private BigDecimal mPrice;
    private String mUrl;
    private static final NumberFormat sCurrencyFormatter = NumberFormat.getCurrencyInstance(
            new Locale("en", "IN")
    );

    public static final Creator<Offer> CREATOR = new Creator<Offer>() {
        @Override
        public Offer createFromParcel(Parcel in) {
            return new Offer(in);
        }

        @Override
        public Offer[] newArray(int size) {
            return new Offer[size];
        }
    };


    public Offer(String mPartnerName, BigDecimal price, String url) {
        this.mPartnerName = mPartnerName;
        this.mPrice = price;
        this.mUrl = url;
    }


    protected Offer(Parcel in) {
        mPartnerName = in.readString();
        mUrl = in.readString();
    }

    @Override
    public int describeContents() {
        return 0;
    }


    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeString(mPartnerName);
        dest.writeString(mPrice.toString());
        dest.writeString(mUrl);
    }


    public String getPartnerName() {
        return mPartnerName;
    }


    public BigDecimal getPrice() {
        return mPrice;
    }


    public String getUrl() {
        return mUrl;
    }


    private static Offer getOfferFromJsonObject(JsonObject data) {

        String partnerName = null;
        if (data.has("partner_name") && !data.get("partner_name").isJsonNull()) {
            partnerName = data.get("partner_name").getAsString();
        }

        BigDecimal price = null;
        if (data.has("price") && !data.get("price").isJsonNull()) {
            price = data.get("price").getAsBigDecimal();
        }

        String url = null;
        if (data.has("link") && !data.get("link").isJsonNull()) {
            url = data.get("link").getAsString();
        }

        return new Offer(partnerName, price, url);

    }

    public static Offer[] getOffersFromJsonArray(JsonArray jsonArray) {

        Offer[] offers = new Offer[jsonArray.size()];

        for (int i = 0; i < jsonArray.size(); i++) {
            JsonObject object = jsonArray.get(i).getAsJsonObject();
            offers[i] = Offer.getOfferFromJsonObject(object);
        }

        return offers;

    }


    public String getFormattedPrice() {
        if (getPrice().compareTo(BigDecimal.ZERO) == 0) {
            return "Free";
        }
        return sCurrencyFormatter.format(getPrice());
    }

}
