package com.maker.android.app.adapter;

import android.content.Context;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.net.Uri;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.PopupMenu;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.ProgressBar;
import android.widget.TextView;
import android.widget.Toast;

import com.bumptech.glide.Glide;
import com.maker.android.app.R;
import com.maker.android.app.activity.ModelListActivity;
import com.maker.android.app.classes.hardware.Hardware;

import java.util.ArrayList;


/**
 * {@link android.support.v7.widget.RecyclerView.Adapter} for the {@link RecyclerView} of
 * {@link ModelListActivity}
 */
@SuppressWarnings({"unused", "WeakerAccess"})
public class ModelListAdapter extends RecyclerView.Adapter<RecyclerView.ViewHolder> {

    /**
     * A {@link String} constant to use as TAG in {@link Log}
     */
    private static final String LOG_TAG = ModelListAdapter.class.getSimpleName();

    public static final String ADD_TO_BUILD = "ADD_TO_BUILD";
    public static final String ADD_TO_COMPARE = "ADD_TO_COMPARE";

    private final int MODEL_ITEM_VIEW = 0;
    private final int PROGRESS_VIEW = 1;

    private Context mContext;
    private ArrayList<Hardware> mHardwareArrayList;

    private int mVisibleThreshold = 2;
    private int mLastVisibleItem, mTotalItemCount;
    private boolean mLoading;
    private boolean mHasNewData;

    private OnLoadMoreListener mOnLoadMoreListener;
    private OnAddOptionClicked mOnAddOptionClicked;
    private OnDetailsClicked mOnDetailsClicked;


    public interface OnLoadMoreListener {
        void loadMore();
    }


    public interface OnAddOptionClicked {
        void getSelectedItem(String inType, int id);
    }


    public interface OnDetailsClicked {
        void openModelDetailsActivity(int id, String hardwareType, String model);
    }


    /**
     * Constructor for {@link ModelListAdapter}
     *
     * @param hardwareArrayList Data set for the {@link ModelListAdapter}
     */
    public ModelListAdapter(Context context, RecyclerView recyclerView, ArrayList<Hardware> hardwareArrayList) {
        mContext = context;
        mHardwareArrayList = hardwareArrayList;
        mHasNewData = false;

        try {
            mOnAddOptionClicked = (OnAddOptionClicked) context;
            mOnDetailsClicked = (OnDetailsClicked) context;
        } catch (ClassCastException exc) {
            exc.printStackTrace();
        }

        if (recyclerView.getLayoutManager() instanceof LinearLayoutManager) {

            final LinearLayoutManager linearLayoutManager
                    = (LinearLayoutManager) recyclerView.getLayoutManager();

            recyclerView.addOnScrollListener(new RecyclerView.OnScrollListener() {
                @Override
                public void onScrolled(RecyclerView recyclerView, int dx, int dy) {
                    super.onScrolled(recyclerView, dx, dy);

                    mTotalItemCount = linearLayoutManager.getItemCount();
                    mLastVisibleItem = linearLayoutManager.findLastCompletelyVisibleItemPosition();

                    if (!isLoading() && mTotalItemCount <= (mLastVisibleItem + mVisibleThreshold) && hasNewData()) {
                        if (mOnLoadMoreListener != null) {
                            mOnLoadMoreListener.loadMore();
                        }
                        setLoading(true);
                    }
                }
            });

        }

    }

    public boolean isLoading() {
        return mLoading;
    }

    public void setLoading(boolean loading) {
        this.mLoading = loading;
    }

    public boolean hasNewData() {
        return mHasNewData;
    }

    public void setHasNewData(boolean hasNewData) {
        mHasNewData = hasNewData;
    }

    public void setOnLoadMoreListener(OnLoadMoreListener onLoadMoreListener) {
        mOnLoadMoreListener = onLoadMoreListener;
    }


    @Override
    public int getItemViewType(int position) {
        return mHardwareArrayList.get(position) != null ? MODEL_ITEM_VIEW : PROGRESS_VIEW;
    }


    /**
     * Creates a new {@link android.support.v7.widget.RecyclerView.ViewHolder}
     *
     * @param parent   The {@link ViewGroup} to which the new {@link View} will be added
     * @param viewType the {@link View} type of the {@link View}
     * @return A new {@link ModelListHolder} containing {@link View}
     */
    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {

        // Creates a new view with the activity_model_list_item.xml layout
        View itemView;
        switch (viewType) {
            case MODEL_ITEM_VIEW:
                itemView = LayoutInflater
                        .from(parent.getContext())
                        .inflate(R.layout.activity_model_list_item, parent, false);

                // Returns a new ModelListHolder with itemView as its view
                return new ModelListHolder(itemView);

            case PROGRESS_VIEW:
                itemView = LayoutInflater
                        .from(parent.getContext())
                        .inflate(R.layout.progress_item, parent, false);

                // Returns a new ModelListHolder with itemView as its view
                return new ProgressViewHolder(itemView);

            default:
                return null;
        }

    }

    /**
     * Called by RecyclerView to display the data at the specified position.
     *
     * @param holder   {@link ModelListHolder} containing the {@link View} that is to be populated
     * @param position Position where the view is to be displayed
     */
    @Override
    public void onBindViewHolder(final RecyclerView.ViewHolder holder, final int position) {

        if (holder instanceof ProgressViewHolder) {

            ((ProgressViewHolder) holder).progressBar.setIndeterminate(true);

        } else if (holder instanceof ModelListHolder) {
            ModelListHolder modelListHolder = (ModelListHolder) holder;

            // Gets the Hardware based on the position
            Hardware currentItem = mHardwareArrayList.get(position);

            // Sets the title, subtitle and highlight details
            modelListHolder.tvTitle.setText(currentItem.getModel());
            modelListHolder.tvSubtitle.setText(currentItem.getCompanyName());
            modelListHolder.tvHighlights.setText(currentItem.getHighlightedData());

            if (currentItem.getAttribute() != null && currentItem.getAttribute().getImageUrls() != null) {
                Glide
                        .with(mContext)
                        .load(currentItem.getAttribute().getImageUrls()[0])
                        .into(modelListHolder.imageViewPreview);
            }

            // Task to perform when the user clicks 'Details' {@link TextView}
            modelListHolder.btnDetails.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {

                    Hardware currentItem = mHardwareArrayList.get(holder.getAdapterPosition());
                    mOnDetailsClicked.openModelDetailsActivity(
                            currentItem.getId(),
                            currentItem.getHardwareType().toString(),
                            currentItem.getModel()
                    );

                }
            });

            modelListHolder.imageBtnMoreOptions.setOnClickListener(new View.OnClickListener() {

                @Override
                public void onClick(View v) {
                    showPopUpMenu(v, holder.getAdapterPosition());
                }

            });

        }

    }

    /**
     * Create a {@link PopupMenu}
     *
     * @param view     view that was clicked
     * @param position position of the view in the {@link android.support.v7.widget.RecyclerView.Adapter}
     */
    private void showPopUpMenu(View view, int position) {

        // Creates a new PopupMenu
        PopupMenu popupMenu = new PopupMenu(view.getContext(), view, Gravity.END | Gravity.BOTTOM,
                R.attr.actionOverflowMenuStyle, 0);

        // Inflate the menu into PopupMenu
        popupMenu.getMenuInflater().inflate(R.menu.model_item_options, popupMenu.getMenu());

        // Sets the OnMenuItemClickListener for the PopupMenu
        popupMenu.setOnMenuItemClickListener(new OnMenuClickListener(view.getContext(), mHardwareArrayList.get(position)));

        // Show the PopupMenu
        popupMenu.show();

    }

    /**
     * Returns the total number of items in the mHardwareItemList
     *
     * @return int representing the number of items
     */
    @Override
    public int getItemCount() {
        return mHardwareArrayList.size();
    }


    /**
     * Holds the {@link ProgressBar} to show loading of data
     */
    private class ProgressViewHolder extends RecyclerView.ViewHolder {

        ProgressBar progressBar;

        ProgressViewHolder(View itemView) {
            super(itemView);
            progressBar = (ProgressBar) itemView.findViewById(R.id.progress_item_pb);
        }

    }


    /**
     * Describes an item view and metadata about its place within the RecyclerView.
     */
    private class ModelListHolder extends RecyclerView.ViewHolder {

        /**
         * A {@link String} constant to use as TAG in {@link Log}
         */
        @SuppressWarnings("unused")
        private final String LOG_TAG = ModelListHolder.class.getSimpleName();

        /**
         * To hold the reference of preview {@link ImageView}
         */
        ImageView imageViewPreview;

        /**
         * To hold the reference of Item title {@link TextView}
         */
        TextView tvTitle;

        /**
         * To hold the reference of the Item subtitle {@link TextView}
         */
        TextView tvSubtitle;

        /**
         * To hold reference to the additional details, i.e. highlights {@link TextView} of Item
         */
        TextView tvHighlights;

        /**
         * To hold the reference to the {@link TextView} acting as Details button
         */
        Button btnDetails;

        /**
         * To hold reference to the {@link ImageButton} acting as more options button
         */
        ImageButton imageBtnMoreOptions;


        /**
         * Constructor for {@link ModelListHolder}
         *
         * @param itemView A new view that is created
         */
        ModelListHolder(View itemView) {
            super(itemView);

            // Gets the reference to the preview image ImageView
            imageViewPreview = (ImageView) itemView.findViewById(R.id.activity_model_item_imgv_preview);

            // Gets the reference to the title TextView
            tvTitle = (TextView) itemView.findViewById(R.id.activity_model_item_tv_title);

            // Gets the reference to the Subtitle TextView
            tvSubtitle = (TextView) itemView.findViewById(R.id.activity_model_item_tv_subtitle);

            // Gets the reference to the Highlights TextView
            tvHighlights = (TextView) itemView.findViewById(R.id.activity_model_item_tv_highlights);

            // Gets the reference to the Subtitle TextView
            btnDetails = (Button) itemView.findViewById(R.id.activity_model_item_btn_details);

            // Gets the reference to the ImageView acting as more options button
            imageBtnMoreOptions = (ImageButton) itemView.findViewById(R.id.activity_model_item_imgbtn_more_options);

            // Makes the highlights TextView visible and on click of the CardView
            (itemView.findViewById(R.id.activity_model_item_cv)).setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    if (tvHighlights.getVisibility() == View.GONE) {
                        tvHighlights.setVisibility(View.VISIBLE);
                    } else {
                        tvHighlights.setVisibility(View.GONE);
                    }
                }
            });
        }

    }


    private class OnMenuClickListener implements PopupMenu.OnMenuItemClickListener {

        private Context mContext;
        private Hardware mClickedHardware;

        OnMenuClickListener(Context context, Hardware clickedHardware) {
            mContext = context;
            mClickedHardware = clickedHardware;
        }

        @Override
        public boolean onMenuItemClick(MenuItem item) {

            switch (item.getItemId()) {

                case R.id.activity_model_item_more_add_to_build:

                    mOnAddOptionClicked.getSelectedItem(ADD_TO_BUILD, mClickedHardware.getId());
                    return true;

                case R.id.activity_model_item_add_to_compare:

                    mOnAddOptionClicked.getSelectedItem(ADD_TO_COMPARE, mClickedHardware.getId());
                    return true;

                case R.id.activity_model_item_go_to_webpage:

                    if (mClickedHardware.getAttribute().getHomeUrl() == null) {
                        Toast.makeText(mContext, "Link not present!", Toast.LENGTH_SHORT).show();
                        return true;
                    }

                    Intent browserIntent = new Intent(Intent.ACTION_VIEW);

                    // If a web browser is not available go to Google Play's chrome app page
                    PackageManager packageManager = mContext.getPackageManager();
                    if (browserIntent.resolveActivity(packageManager) == null) {
                        Uri marketUri = Uri.parse(
                                "market://search?q=pname:com.android.chrome"
                        );
                        Intent marketIntent = new Intent(Intent.ACTION_VIEW, marketUri);

                        if (marketIntent.resolveActivity(packageManager) == null) {
                            Toast.makeText(mContext, "Market Client Not Available!", Toast.LENGTH_SHORT).show();
                        }

                        mContext.startActivity(marketIntent);
                    } else {
                        browserIntent.setData(Uri.parse(mClickedHardware.getAttribute().getHomeUrl()));
                        mContext.startActivity(browserIntent);
                    }
                    return true;

                default:
                    return false;

            }
        }

    }

}

