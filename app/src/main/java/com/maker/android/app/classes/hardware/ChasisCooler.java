package com.maker.android.app.classes.hardware;


import android.os.Parcel;

import com.google.gson.JsonArray;
import com.google.gson.JsonDeserializationContext;
import com.google.gson.JsonDeserializer;
import com.google.gson.JsonElement;
import com.google.gson.JsonObject;
import com.google.gson.JsonParseException;
import com.maker.android.app.annotation.HardwareAnnotations;
import com.maker.android.app.classes.Attribute;
import com.maker.android.app.classes.Offer;
import com.maker.android.app.classes.SimpleHighlighter;
import com.maker.android.app.provider.PreserveContract;

import java.lang.reflect.Type;
import java.math.BigDecimal;


@SuppressWarnings({"unused", "WeakerAccess"})
class ChasisCooler
        extends Hardware {

    /**
     * A {@link String} constant to use as tag for {@link android.util.Log}
     */
    private static final String LOG_TAG = ChasisCooler.class.getSimpleName();

    public static Deserialize sDeserialize;

    static {
        sDeserialize = new Deserialize();
    }

    @HardwareAnnotations.DisplayData(name = "Size", displayInHighlights = true, unit = "mm")
    private int mSize;

    @HardwareAnnotations.DisplayData(name = "Minimum RPM", unit = "rpm")
    private int mMinRpm;

    @HardwareAnnotations.DisplayData(name = "Maximum RPM", unit = "rpm")
    private int mMaxRpm;

    @HardwareAnnotations.DisplayData(name = "LED Color", displayInHighlights = true)
    private String mLedColor;

    @HardwareAnnotations.DisplayData(name = "Connector")
    private ConnectionInterface mConnector;

    @HardwareAnnotations.DisplayData(name = "Maximum Power Consumption", unit = "W")
    private BigDecimal mMaxPowerConsumption;


    public static final Creator<ChasisCooler> CREATOR = new Creator<ChasisCooler>() {

        @Override
        public ChasisCooler createFromParcel(Parcel in) {
            return new ChasisCooler(in);
        }

        @Override
        public ChasisCooler[] newArray(int size) {
            return new ChasisCooler[size];
        }

    };


    private ChasisCooler(int id, String companyName, String model) {
        super(id, companyName, model);
        setHighlighter(new SimpleHighlighter());
    }


    ChasisCooler(Parcel in) {
        super(in);
    }

    @Override
    public int describeContents() {
        return 0;
    }

    public int getSize() {
        return mSize;
    }

    public void setSize(int size) {
        mSize = size;
    }

    public int getMinRpm() {
        return mMinRpm;
    }

    public void setMinRpm(int minRpm) {
        mMinRpm = minRpm;
    }

    public int getMaxRpm() {
        return mMaxRpm;
    }

    public void setMaxRpm(int maxRpm) {
        mMaxRpm = maxRpm;
    }

    public String getLedColor() {
        return mLedColor;
    }

    public void setLedColor(String ledColor) {
        mLedColor = ledColor;
    }

    public ConnectionInterface getConnector() {
        return mConnector;
    }

    public void setConnector(ConnectionInterface connector) {
        mConnector = connector;
    }

    public BigDecimal getMaxPowerConsumption() {
        return mMaxPowerConsumption;
    }

    public void setMaxPowerConsumption(BigDecimal maxPowerConsumption) {
        mMaxPowerConsumption = maxPowerConsumption;
    }

    @Override
    public PreserveContract.HardwareTypeEntry.HardwareType getHardwareType() {
        return PreserveContract.HardwareTypeEntry.HardwareType.ChasisCooler;
    }


    /**
     *
     */
    private static class Deserialize implements JsonDeserializer<ChasisCooler> {

        @Override
        public ChasisCooler deserialize(JsonElement json, Type typeOfT, JsonDeserializationContext context) throws JsonParseException {

            JsonObject jsonObject = json.getAsJsonObject();

            int chasisCoolerId = 0;
            String companyName = null, model = null;

            if (jsonObject.has("chasis_cooler_id") && !jsonObject.get("chasis_cooler_id").isJsonNull()) {
                chasisCoolerId = jsonObject.get("chasis_cooler_id").getAsInt();
            }

            if (jsonObject.has("company_name") && !jsonObject.get("company_name").isJsonNull()) {
                companyName = jsonObject.get("company_name").getAsString();
            }

            if (jsonObject.has("model") && !jsonObject.get("model").isJsonNull()) {
                model = jsonObject.get("model").getAsString();
            }

            Builder builder = new ChasisCoolerBuilder(chasisCoolerId, companyName, model);

            if (jsonObject.has("size") && !jsonObject.get("size").isJsonNull()) {
                builder.setSize(jsonObject.get("size").getAsInt());
            }

            if (jsonObject.has("min_rpm") && !jsonObject.get("min_rpm").isJsonNull()) {
                builder.setMinRpm(jsonObject.get("min_rpm").getAsInt());
            }

            if (jsonObject.has("max_rpm") && !jsonObject.get("max_rpm").isJsonNull()) {
                builder.setMaxRpm(jsonObject.get("max_rpm").getAsInt());
            }

            if (jsonObject.has("led_color") && !jsonObject.get("led_color").isJsonNull()) {
                builder.setLedColor(jsonObject.get("led_color").getAsString());
            }

            if (jsonObject.has("connector") && !jsonObject.get("connector").isJsonNull()) {
                builder.setConnector(ConnectionInterface.getEnumValue(jsonObject.get("connector").getAsString()));
            }

            if (jsonObject.has("max_power_consumption") && !jsonObject.get("max_power_consumption").isJsonNull()) {
                builder.setMaxPowerConsumption(jsonObject.get("max_power_consumption").getAsBigDecimal());
            }

            if (jsonObject.has("attributes") && !jsonObject.get("attributes").isJsonNull()) {
                builder.setAttributes(
                        Attribute.getAttributeFromJsonObject(jsonObject.get("attributes").getAsJsonObject())
                );
            }

            if (jsonObject.has("offers") && !jsonObject.get("offers").isJsonNull()) {
                JsonArray jsonArray = jsonObject.get("offers").getAsJsonArray();
                builder.setOffers(Offer.getOffersFromJsonArray(jsonArray));
            }

            return builder.createChasisCooler();
        }

    }

    interface Builder {

        Builder setSize(int size);

        Builder setMinRpm(int minRpm);

        Builder setMaxRpm(int maxRpm);

        Builder setLedColor(String ledColor);

        Builder setConnector(ConnectionInterface connector);

        Builder setMaxPowerConsumption(BigDecimal maxPowerConsumption);

        Builder setAttributes(Attribute attribute);

        Builder setOffers(Offer[] offers);

        ChasisCooler createChasisCooler();

    }


    private static class ChasisCoolerBuilder implements Builder {

        private ChasisCooler mChasisCooler;

        ChasisCoolerBuilder(int id, String companyName, String model) {
            mChasisCooler = new ChasisCooler(id, companyName, model);
        }

        @Override
        public Builder setSize(int size) {
            mChasisCooler.setSize(size);
            return this;
        }

        @Override
        public Builder setMinRpm(int minRpm) {
            mChasisCooler.setMinRpm(minRpm);
            return this;
        }

        @Override
        public Builder setMaxRpm(int maxRpm) {
            mChasisCooler.setMaxRpm(maxRpm);
            return this;
        }

        @Override
        public Builder setLedColor(String ledColor) {
            mChasisCooler.setLedColor(ledColor);
            return this;
        }

        @Override
        public Builder setConnector(ConnectionInterface connector) {
            mChasisCooler.setConnector(connector);
            return this;
        }

        @Override
        public Builder setMaxPowerConsumption(BigDecimal maxPowerConsumption) {
            mChasisCooler.setMaxPowerConsumption(maxPowerConsumption);
            return this;
        }

        @Override
        public Builder setAttributes(Attribute attribute) {
            mChasisCooler.setAttribute(attribute);
            return this;
        }

        @Override
        public Builder setOffers(Offer[] offers) {
            mChasisCooler.setOffers(offers);
            return this;
        }

        @Override
        public ChasisCooler createChasisCooler() {
            return mChasisCooler;
        }
    }

}
