package com.maker.android.app.adapter;

import android.content.Context;
import android.os.Bundle;
import android.support.v4.app.FragmentManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.bumptech.glide.Glide;
import com.maker.android.app.R;
import com.maker.android.app.activity.CatalogueActivity;
import com.maker.android.app.activity.ModelListActivity;
import com.maker.android.app.classes.CatalogueItem;
import com.maker.android.app.fragment.ProcessorDialogFragment;
import com.maker.android.app.utils.Request;
import com.maker.android.app.utils.Request.RequestBuilder;

import java.util.ArrayList;


/**
 * An {@link android.support.v7.widget.RecyclerView.Adapter} for the {@link CatalogueActivity}'s
 * {@link RecyclerView}
 */
@SuppressWarnings({"unused"})
public class CatalogueAdapter
        extends RecyclerView.Adapter<CatalogueAdapter.HardwareViewHolder> {

    /**
     * A {@link String} constant to use as TAG with {@link android.util.Log}
     */
    private static final String LOG_TAG = CatalogueAdapter.class.getSimpleName();
    /**
     * Gets a reference to the class implementing {@link CatalogueClickListener.OpenModelListActivity}
     */
    private static CatalogueClickListener.OpenModelListActivity sSender;
    /**
     * To Stores the {@link ArrayList} of {@link CatalogueItem}
     */
    private ArrayList<CatalogueItem> mHardwareList;
    private Context mContext;

    /**
     * Constructor for {@link CatalogueAdapter}
     *
     * @param hardwares {@link CatalogueItem} that are to be displayed
     */
    public CatalogueAdapter(Context context, ArrayList<CatalogueItem> hardwares) {

        mContext = context;
        try {
            sSender = (CatalogueClickListener.OpenModelListActivity) context;
        } catch (ClassCastException exc) {
            exc.printStackTrace();
        }

        mHardwareList = hardwares;
    }


    /**
     * Creates a new {@link android.support.v7.widget.RecyclerView.ViewHolder}
     *
     * @param parent   The {@link ViewGroup} to which the new {@link View} will be added
     * @param viewType the {@link View} type of the {@link View}
     * @return A new {@link HardwareViewHolder} containing {@link View}
     */
    @Override
    public HardwareViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {

        // Creates a new View using the component_item.xml Layout
        View itemView = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.activity_catalogue_item, parent, false);

        // Creates a new ComponentViewHolder from the itemView
        // Returns the newly created itemViewHolder
        return new HardwareViewHolder(itemView);

    }


    /**
     * Called by RecyclerView to display the data at the specified position.
     *
     * @param holder   {@link HardwareViewHolder} containing the {@link View} that is to be populated
     * @param position Position where the view is to be displayed
     */
    @Override
    public void onBindViewHolder(HardwareViewHolder holder, int position) {

        // Gets the item that is to be displayed at given position
        CatalogueItem currentItem = mHardwareList.get(position);

        // Sets the title of the CatalogueItem
        holder.textViewHardwareTitle.setText(currentItem.getTitle());

        // set the ImageView's image
        Glide
                .with(mContext)
                .load(mContext.getString(R.string.url_base_for_images) + currentItem.getImageURL())
                .into(holder.imageViewHardwareImage);

        // Creates a new Instance of CatalogueClickListener
        View.OnClickListener openHardwareComponent = new CatalogueClickListener(currentItem);

        // When a ClickEvent occurs open a new Activity which shows model list
        holder.itemView.setOnClickListener(openHardwareComponent);

    }


    /**
     * Returns the total number of items in the mHardwareList
     *
     * @return int representing the number of items
     */
    @Override
    public int getItemCount() {
        return mHardwareList.size();
    }


    /**
     * An {@link View.OnClickListener} to deal with click of a {@link CatalogueItem}
     */
    public static class CatalogueClickListener implements View.OnClickListener {

        /**
         * A {@link String} constant to use as TAG in {@link Log}
         */
        final String LOG_TAG = CatalogueClickListener.class.getSimpleName();

        /**
         * A {@link String} constant to use as tag for the {@link ProcessorDialogFragment}'s data
         */
        static final String REQUESTED_ITEM = "REQUESTED_ITEM";

        /**
         * A {@link String} constant to use as tag for {@link android.support.v4.app.FragmentManager}
         */
        static final String PROCESSOR_DIALOG_FRAGMENT = "PROCESSOR_DIALOG_FRAGMENT";

        /**
         * {@link CatalogueItem} that is related to the view that was clicked
         */
        private CatalogueItem mClickedHardware;


        /**
         * Constructor for {@link CatalogueClickListener}
         *
         * @param clickedItem {@link CatalogueItem} that belongs to the clicked view
         */
        CatalogueClickListener(CatalogueItem clickedItem) {
            mClickedHardware = clickedItem;
        }


        /**
         * Open {@link ModelListActivity} when click is performed
         *
         * @param v {@link View} that was clicked
         */
        @Override
        public void onClick(View v) {

            if (mClickedHardware.getTitle().contains("Processor")) {

                ProcessorDialogFragment processorDialogFragment = new ProcessorDialogFragment();

                Bundle args = new Bundle();
                args.putParcelable(REQUESTED_ITEM, mClickedHardware);

                processorDialogFragment.setArguments(args);
                FragmentManager fragmentManager = ((CatalogueActivity) v.getContext()).getSupportFragmentManager();
                processorDialogFragment.show(fragmentManager, PROCESSOR_DIALOG_FRAGMENT);

            } else {

                // URL of web server to access list of items
                String url = v.getResources().getString(R.string.url_base_for_multi_item_request, mClickedHardware.getEndpoint());

                // Creates a new Request object to access RESTful service
                Request hardwareRequest = new RequestBuilder()
                        .setURL(url)
                        .setHttpType(Request.HttpRequestTypes.GET)
                        .createRequest();

                sSender.modelListActivityStarter(hardwareRequest,
                        mClickedHardware.getTitle(), mClickedHardware.getEndpoint());

            }

        }

        /**
         * An interface that handles opening the {@link ModelListActivity}
         */
        public interface OpenModelListActivity {
            void modelListActivityStarter(Request hardwareRequest, String title, String endpoint);
        }

    }

    /**
     * Describes an item view and metadata about its place within the RecyclerView.
     */
    class HardwareViewHolder extends RecyclerView.ViewHolder {

        /**
         * To hold the reference to the {@link ImageView}
         */
        ImageView imageViewHardwareImage;

        /**
         * To hold the reference to the {@link TextView}
         */
        TextView textViewHardwareTitle;


        /**
         * Constructor for {@link HardwareViewHolder}
         *
         * @param itemView {@link View} to be held
         *                 by the {@link android.support.v7.widget.RecyclerView.ViewHolder}
         */
        HardwareViewHolder(View itemView) {
            super(itemView);

            // Gets the references of the CatalogueItem's image and title
            imageViewHardwareImage = (ImageView) itemView.findViewById(R.id.fragment_compare_list_item_hardware_image);
            textViewHardwareTitle = (TextView) itemView.findViewById(R.id.activity_catalogue_hardware_label);
        }

    }


}

