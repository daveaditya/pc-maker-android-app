package com.maker.android.app.utils;

import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.support.annotation.Nullable;
import android.util.Log;

import org.json.JSONObject;

import java.io.BufferedReader;
import java.io.DataOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.URL;
import java.nio.charset.StandardCharsets;
import java.util.Map;


/**
 * A helper class to download JSON data
 */
@SuppressWarnings({"unused"})
public class DownloaderUtils {


    /**
     * A {@link String} constant to use as TAG in {@link Log}
     */
    private static final String LOG_TAG = DownloaderUtils.class.getSimpleName();


    public static String performNetworkRequest(Request request) throws Exception {

        switch (request.getType()) {
            case GET:
                return performGetRequest(request.getURL());
            case POST:
                return performPostRequest(request.getURL(), request.getData());
            case PUT:
                return performPutRequest(request.getURL(), request.getData());
            case DELETE:
                return performDeleteRequest(request.getURL(), request.getData());
            default:
                throw new Exception("Illegal Request");
        }

    }


    /**
     * Performs a HTTP GET request using the given url
     *
     * @param url url of the source
     * @return String data obtained from given url
     */
    @Nullable
    private static String performGetRequest(String url) {
        // Creates an URL object from the String url
        URL myUrl = createUrlFromString(url);

        // If myUrl is null, no connection can be made. Hence return null
        if (myUrl == null) {
            return null;
        }

        try {
            // Creates a HttpURLConnection from the url
            HttpURLConnection httpURLConnection = (HttpURLConnection) myUrl.openConnection();

            // Sets the HTTP request method as GET
            httpURLConnection.setRequestMethod("GET");

            // Sets the connection timeout as 10 second
            httpURLConnection.setConnectTimeout(10000);

            // Sets the read timeout as 10 second
            httpURLConnection.setReadTimeout(15000);

            httpURLConnection.setRequestProperty("Keep-Alive", "False");

            // If the response code is 404, get the feedback using errorStream
            if (httpURLConnection.getResponseCode() == 404) {
                String error = getStringDataFromInputStream(httpURLConnection.getErrorStream());
                httpURLConnection.disconnect();
                return error;
            }

            // Gets the InputStream associated with the HttpURLConnection
            InputStream inputStream = httpURLConnection.getInputStream();

            // Returns the String response
            String something = getStringDataFromInputStream(inputStream);

            // Close the current InputStream
            inputStream.close();

            // Close the connection
            httpURLConnection.disconnect();

            return something;

        } catch (IOException exc) {
            exc.printStackTrace();
            return null;
        }
    }


    /**
     * Performs a HTTP PUT request using the given url and pass the data as JSON
     *
     * @param url  url of the source
     * @param data data to be passed along with the request
     * @return String response obtained by performing the request
     */
    @Nullable
    private static String performPutRequest(String url, Map<String, Object> data) {
        // Creates an URL object from the String url
        URL myUrl = createUrlFromString(url);

        // If myUrl is null, no connection can be made. Hence return null
        if (myUrl == null) {
            return null;
        }

        // Create a JSONObject from the input Map
        JSONObject jsonData = new JSONObject(data);

        try {
            // Creates a HttpURLConnection from the url
            HttpURLConnection httpURLConnection = (HttpURLConnection) myUrl.openConnection();

            // Sets the HTTP request method as PUT
            httpURLConnection.setRequestMethod("PUT");

            // Sets the Content-Type as JSON
            httpURLConnection.setRequestProperty("Content-Type", "application/json");

            // Sets the charset to UTF-8
            httpURLConnection.setRequestProperty("charset", "utf-8");

            // Sets the Content-Length header of the HTTP request
            httpURLConnection.setRequestProperty("Content-Length", Integer.toString(jsonData.toString().length()));

            // Gets the DataOutputStream connected with the request
            try (DataOutputStream dataOutputStream = new DataOutputStream(httpURLConnection.getOutputStream())) {

                // Writes the JSON data to the stream
                dataOutputStream.write(jsonData.toString().getBytes(StandardCharsets.UTF_8));

                // Flush the data output stream
                dataOutputStream.flush();
            } catch (IOException exc) {
                exc.printStackTrace();
                return null;
            }

            // If the response code is 404, get the feedback using errorStream
            if (httpURLConnection.getResponseCode() == 404) {
                String error = getStringDataFromInputStream(httpURLConnection.getErrorStream());
                httpURLConnection.disconnect();
                return error;
            }

            // Sets the connection timeout as 10 second
            httpURLConnection.setConnectTimeout(10000);

            // Sets the read timeout as 10 second
            httpURLConnection.setReadTimeout(15000);

            // Gets the InputStream associated with the HttpURLConnection
            InputStream inputStream = httpURLConnection.getInputStream();

            // Returns the String response
            String something = getStringDataFromInputStream(inputStream);

            // Close the current InputStream
            inputStream.close();

            // Close the connection
            httpURLConnection.disconnect();

            return something;

        } catch (IOException exc) {
            exc.printStackTrace();
            return null;
        }
    }


    /**
     * Performs a HTTP POST request and sends the data as JSON
     *
     * @param url  url of the source
     * @param data data to be passed to the source
     * @return String data obtained from the response
     */
    @Nullable
    private static String performPostRequest(String url, Map<String, Object> data) {
        // Creates an URL object from the String url
        URL myUrl = createUrlFromString(url);

        // If myUrl is null, no connection can be made. Hence return null
        if (myUrl == null) {
            return null;
        }

        // Create a JSONObject from the input Map
        JSONObject jsonData = new JSONObject(data);

        try {
            // Creates a HttpURLConnection from the url
            HttpURLConnection httpURLConnection = (HttpURLConnection) myUrl.openConnection();

            // Sets the HTTP request method as POST
            httpURLConnection.setRequestMethod("POST");

            // Sets the Content-Type as JSON
            httpURLConnection.setRequestProperty("Content-Type", "application/json");

            // Sets the charset to UTF-8
            httpURLConnection.setRequestProperty("charset", "utf-8");

            // Sets the Content-Length header of the HTTP request
            httpURLConnection.setRequestProperty("Content-Length", Integer.toString(jsonData.toString().length()));

            // Gets the DataOutputStream connected with the request
            try (DataOutputStream dataOutputStream = new DataOutputStream(httpURLConnection.getOutputStream())) {

                // Writes the JSON data to the stream
                dataOutputStream.write(jsonData.toString().getBytes(StandardCharsets.UTF_8));

                // Flush the data output stream
                dataOutputStream.flush();
            } catch (IOException exc) {
                exc.printStackTrace();
                return null;
            }

            // If the response code is 404, get the feedback using errorStream
            if (httpURLConnection.getResponseCode() == 404) {
                String error = getStringDataFromInputStream(httpURLConnection.getErrorStream());
                httpURLConnection.disconnect();
                return error;
            }

            // Sets the connection timeout as 10 second
            httpURLConnection.setConnectTimeout(10000);

            // Sets the read timeout as 10 second
            httpURLConnection.setReadTimeout(15000);

            // Gets the InputStream associated with the HttpURLConnection
            InputStream inputStream = httpURLConnection.getInputStream();

            // Returns the String response
            String something = getStringDataFromInputStream(inputStream);

            // Close the current InputStream
            inputStream.close();

            // Close the connection
            httpURLConnection.disconnect();

            return something;

        } catch (IOException exc) {
            exc.printStackTrace();
            return null;
        }
    }


    /**
     * Performs a HTTP DELETE request and sends the data as JSON
     *
     * @param url  url of the source
     * @param data data to be sent along with the request
     * @return String containing the response obtained from the url
     */
    @Nullable
    private static String performDeleteRequest(String url, Map<String, Object> data) {
        // Creates an URL object from the String url
        URL myUrl = createUrlFromString(url);

        // If myUrl is null, no connection can be made. Hence return null
        if (myUrl == null) {
            return null;
        }

        // Create a JSONObject from the input Map
        JSONObject jsonData = new JSONObject(data);

        try {
            // Creates a HttpURLConnection from the url
            HttpURLConnection httpURLConnection = (HttpURLConnection) myUrl.openConnection();

            // Sets the HTTP request method as DELETE
            httpURLConnection.setRequestMethod("DELETE");

            // Sets the Content-Type as JSON
            httpURLConnection.setRequestProperty("Content-Type", "application/json");

            // Sets the charset to UTF-8
            httpURLConnection.setRequestProperty("charset", "utf-8");

            // Sets the Content-Length header of the HTTP request
            httpURLConnection.setRequestProperty("Content-Length", Integer.toString(jsonData.toString().length()));

            // Gets the DataOutputStream connected with the request
            try (DataOutputStream dataOutputStream = new DataOutputStream(httpURLConnection.getOutputStream())) {

                // Writes the JSON data to the stream
                dataOutputStream.write(jsonData.toString().getBytes(StandardCharsets.UTF_8));

                // Flush the data output stream
                dataOutputStream.flush();
            } catch (IOException exc) {
                exc.printStackTrace();
                return null;
            }

            // If the response code is 404, get the feedback using errorStream
            if (httpURLConnection.getResponseCode() == 404) {
                String error = getStringDataFromInputStream(httpURLConnection.getErrorStream());
                httpURLConnection.disconnect();
                return error;
            }

            // Sets the connection timeout as 10 second
            httpURLConnection.setConnectTimeout(10000);

            // Sets the read timeout as 10 second
            httpURLConnection.setReadTimeout(15000);

            // Gets the InputStream associated with the HttpURLConnection
            InputStream inputStream = httpURLConnection.getInputStream();

            // Returns the String response
            String something = getStringDataFromInputStream(inputStream);

            // Close the current InputStream
            inputStream.close();

            // Close the connection
            httpURLConnection.disconnect();

            return something;

        } catch (IOException exc) {
            exc.printStackTrace();
            return null;
        }
    }


    /**
     * Downloads an image and returns a {@link Bitmap}. If the {@link InputStream}
     * from is {@link URL} is null or cannot be decoded returns null.
     *
     * @param url {@link String} representation url of image
     * @return Returns an image on successful decode of the {@link InputStream}, else
     * returns null.
     */
    public static Bitmap getBitmapFromUrl(String url) {
        // Create an URL object from the String
        URL myUrl = createUrlFromString(url);

        // If the URL returned by createUrlFromString(String url) is null, return null.
        if (myUrl == null) {
            return null;
        }

        try {
            // Create a HttpURLConnection from the URL
            HttpURLConnection httpURLConnection = (HttpURLConnection) myUrl.openConnection();

            // If the link is dead return null
            if (httpURLConnection.getResponseCode() != 200) {
                httpURLConnection.disconnect();
                return null;
            }

            // Set timeout for the connection to 10 second.
            httpURLConnection.setConnectTimeout(10000);

            // Set the read timeout for the connection to 15 second.
            httpURLConnection.setReadTimeout(15000);

            // Indicates that the URL is used for input only
//            httpURLConnection.setDoInput(true);

            // Get the InputStream connected to the HttpURLConnection
            InputStream inputStream = httpURLConnection.getInputStream();

            // Decode the InputStream into a Bitmap
            // and Return the obtained Bitmap
            Bitmap bitmap = BitmapFactory.decodeStream(inputStream);

            // Close the connection
            httpURLConnection.disconnect();

            return bitmap;

            // Handle IOException if any, from the URL.openConnection() method
        } catch (IOException exc) {
            // Log the message returned from the exception
            Log.e(LOG_TAG, exc.getMessage());

            // If the BitmapFactory.decodeStream() fails to do its job, return null.
            return null;
        }
    }


    /**
     * Returns an {@link URL} object created from the {@link String} url param
     *
     * @param url {@link String} representation of the url
     * @return URL object made from the string url
     */
    private static URL createUrlFromString(String url) {
        // If the url is null, return null.
        if (url == null || url.isEmpty()) {
            return null;
        }

        try {
            // Create an URL object from the String url

            // If the creation is successful return the newly created object
            return new URL(url);

            // Handles the MalformedURLException that might occur when using the
            // URL class constructor
        } catch (MalformedURLException exc) {
            exc.printStackTrace();
            // Conversion fails hence return null
            return null;
        }
    }


    /**
     * Returns the data from the {@link InputStream} in the form of a {@link String}
     *
     * @param inputStream {@link InputStream} to get the data from
     * @return String data obtained from the {@link InputStream}
     */
    @Nullable
    private static String getStringDataFromInputStream(InputStream inputStream) throws IOException {
        // Creates a StringBuilder to append the data from the InputStream
        // to the same object
        StringBuilder response = new StringBuilder();

        // Creates a BufferedReader from the InputStream
        try (BufferedReader bufferedReader
                     = new BufferedReader(new InputStreamReader(inputStream, StandardCharsets.UTF_8))) {

            // Creates a new String object to store a single line
            // from the BufferedReader
            String line = "";

            // Iteratively append lines to the response until
            // there is no line in the BufferedReader
            while (line != null) {

                // Appends the line to response
                response.append(line);

                // Reads new line from the BufferedReader
                line = bufferedReader.readLine();

            }

            // Returns the generated response as String
            return response.toString();

            // Handles the IOException that might occur when creating the BufferedReader
        } catch (IOException exc) {
            exc.printStackTrace();
            throw exc;
        }

    }

}
