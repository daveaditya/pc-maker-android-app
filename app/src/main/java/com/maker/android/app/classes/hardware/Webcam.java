package com.maker.android.app.classes.hardware;

import android.os.Parcel;

import com.google.gson.JsonArray;
import com.google.gson.JsonDeserializationContext;
import com.google.gson.JsonDeserializer;
import com.google.gson.JsonElement;
import com.google.gson.JsonObject;
import com.google.gson.JsonParseException;
import com.maker.android.app.annotation.HardwareAnnotations;
import com.maker.android.app.classes.Attribute;
import com.maker.android.app.classes.Offer;
import com.maker.android.app.classes.SimpleHighlighter;
import com.maker.android.app.provider.PreserveContract;

import java.lang.reflect.Type;


@SuppressWarnings({"unused", "WeakerAccess"})
class Webcam
        extends Hardware {

    /**
     * A {@link String} constant to use as tag in {@link android.util.Log}
     */
    private static final String LOG_TAG = Webcam.class.getSimpleName();

    public static Deserialize sDeserialize;

    static {
        sDeserialize = new Deserialize();
    }

    @HardwareAnnotations.DisplayData(name = "Resolution", displayInHighlights = true)
    private String mResolution;

    @HardwareAnnotations.DisplayData(name = "Frame Rate", unit = "Hz")
    private int mFrameRate;

    @HardwareAnnotations.DisplayData(name = "Has Microphone?")
    private boolean mHasMicrophone;

    @HardwareAnnotations.DisplayData(name = "Connectivity", displayInHighlights = true)
    private ConnectionInterface mConnectivity;

    @HardwareAnnotations.DisplayData(name = "Supported OS")
    private String mSupportedOs;

    public static final Creator<Webcam> CREATOR = new Creator<Webcam>() {

        @Override
        public Webcam createFromParcel(Parcel in) {
            return new Webcam(in);
        }

        @Override
        public Webcam[] newArray(int size) {
            return new Webcam[size];
        }

    };


    private Webcam(int id, String companyName, String model) {
        super(id, companyName, model);
        setHighlighter(new SimpleHighlighter());
    }


    Webcam(Parcel in) {
        super(in);
    }


    @Override
    public int describeContents() {
        return 0;
    }


    @Override
    public PreserveContract.HardwareTypeEntry.HardwareType getHardwareType() {
        return PreserveContract.HardwareTypeEntry.HardwareType.Webcam;
    }


    public String getResolution() {
        return mResolution;
    }

    public void setResolution(String resolution) {
        mResolution = resolution;
    }

    public int getFrameRate() {
        return mFrameRate;
    }

    public void setFrameRate(int frameRate) {
        mFrameRate = frameRate;
    }

    public boolean isHasMicrophone() {
        return mHasMicrophone;
    }

    public void setHasMicrophone(boolean hasMicrophone) {
        mHasMicrophone = hasMicrophone;
    }

    public ConnectionInterface getConnectivity() {
        return mConnectivity;
    }

    public void setConnectivity(ConnectionInterface connectivity) {
        mConnectivity = connectivity;
    }

    public String getSupportedOs() {
        return mSupportedOs;
    }

    public void setSupportedOs(String supportedOs) {
        mSupportedOs = supportedOs;
    }


    interface Builder {

        Builder setResolution(String resolution);

        Builder setFrameRate(int frameRate);

        Builder setHasMicrophone(boolean hasMicrophone);

        Builder setConnectivity(ConnectionInterface connectivity);

        Builder setSupportedOs(String supportedOs);

        Builder setAttributes(Attribute attributes);

        Builder setOffers(Offer[] offers);

        Webcam createWebcam();

    }


    private static class WebcamBuilder implements Builder {

        Webcam mWebcam;

        WebcamBuilder(int id, String companyName, String model) {
            mWebcam = new Webcam(id, companyName, model);
        }

        @Override
        public Builder setResolution(String resolution) {
            mWebcam.setResolution(resolution);
            return this;
        }

        @Override
        public Builder setFrameRate(int frameRate) {
            mWebcam.setFrameRate(frameRate);
            return this;
        }

        @Override
        public Builder setHasMicrophone(boolean hasMicrophone) {
            mWebcam.setHasMicrophone(hasMicrophone);
            return this;
        }

        @Override
        public Builder setConnectivity(ConnectionInterface connectivity) {
            mWebcam.setConnectivity(connectivity);
            return this;
        }

        @Override
        public Builder setSupportedOs(String supportedOs) {
            mWebcam.setSupportedOs(supportedOs);
            return this;
        }

        @Override
        public Builder setAttributes(Attribute attributes) {
            mWebcam.setAttribute(attributes);
            return this;
        }

        @Override
        public Builder setOffers(Offer[] offers) {
            mWebcam.setOffers(offers);
            return this;
        }

        @Override
        public Webcam createWebcam() {
            return mWebcam;
        }

    }


    private static class Deserialize implements JsonDeserializer<Webcam> {

        @Override
        public Webcam deserialize(JsonElement json, Type typeOfT, JsonDeserializationContext context) throws JsonParseException {

            JsonObject jsonObject = json.getAsJsonObject();

            int webcamId = 0;
            String companyName = null, model = null;

            if (jsonObject.has("webcam_id") && !jsonObject.get("webcam_id").isJsonNull()) {
                webcamId = jsonObject.get("webcam_id").getAsInt();
            }

            if (jsonObject.has("company_name") && !jsonObject.get("company_name").isJsonNull()) {
                companyName = jsonObject.get("company_name").getAsString();
            }

            if (jsonObject.has("model") && !jsonObject.get("model").isJsonNull()) {
                model = jsonObject.get("model").getAsString();
            }

            Builder builder = new WebcamBuilder(webcamId, companyName, model);

            if (jsonObject.has("resolution") && !jsonObject.get("resolution").isJsonNull()) {
                builder.setResolution(jsonObject.get("resolution").getAsString());
            }

            if (jsonObject.has("frame_rate") && !jsonObject.get("frame_rate").isJsonNull()) {
                builder.setFrameRate(jsonObject.get("frame_rate").getAsInt());
            }

            if (jsonObject.has("has_microphone") && !jsonObject.get("has_microphone").isJsonNull()) {
                builder.setHasMicrophone(jsonObject.get("has_microphone").getAsBoolean());
            }

            if (jsonObject.has("connectivity") && !jsonObject.get("connectivity").isJsonNull()) {
                builder.setConnectivity(ConnectionInterface.getEnumValue(jsonObject.get("connectivity").getAsString()));
            }

            if (jsonObject.has("supported_os") && !jsonObject.get("supported_os").isJsonNull()) {
                builder.setSupportedOs(jsonObject.get("supported_os").getAsString());
            }

            if (jsonObject.has("attributes") && !jsonObject.get("attributes").isJsonNull()) {
                builder.setAttributes(
                        Attribute.getAttributeFromJsonObject(jsonObject.get("attributes").getAsJsonObject())
                );
            }

            if (jsonObject.has("offers") && !jsonObject.get("offers").isJsonNull()) {
                JsonArray jsonArray = jsonObject.get("offers").getAsJsonArray();
                builder.setOffers(Offer.getOffersFromJsonArray(jsonArray));
            }

            return builder.createWebcam();

        }

    }

}
