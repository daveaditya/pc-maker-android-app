package com.maker.android.app.adapter;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.bumptech.glide.Glide;
import com.maker.android.app.R;
import com.maker.android.app.classes.hardware.Hardware;
import com.maker.android.app.fragment.BuildDetailsFragment;
import com.maker.android.app.provider.PreserveContract;

import java.util.ArrayList;
import java.util.LinkedHashMap;


@SuppressWarnings({"unused", "SetTextI18n", "InflateParams"})
public class BuildDetailsAdapter extends RecyclerView.Adapter<BuildDetailsAdapter.BuildComponentHolder> {

    /**
     * A {@link String} constant to use as tag for {@link android.util.Log}
     */
    private static final String LOG_TAG = BuildDetailsAdapter.class.getSimpleName();

    private Context mContext;
    private LinkedHashMap<String, ArrayList<Hardware>> mDataset;
    private ArrayList<String> mKeys;
    private BuildDetailsFragment.OpenModelDetails mOpenModelDetails;


    public BuildDetailsAdapter(Context context, LinkedHashMap<String, ArrayList<Hardware>> dataset) {
        mContext = context;
        mDataset = dataset;
        mKeys = new ArrayList<>(dataset.keySet());

        try {
            mOpenModelDetails = (BuildDetailsFragment.OpenModelDetails) mContext;
        } catch (ClassCastException exc) {
            throw new ClassCastException("Must implement BuildDetailsFragment.OpenModelDetails");
        }

    }


    @Override
    public BuildComponentHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.fragment_builds_detail_item, parent, false);
        return new BuildComponentHolder(view);
    }


    @Override
    public void onBindViewHolder(BuildComponentHolder holder, int position) {

        final String currentHwType = mKeys.get(position);
        ArrayList<Hardware> models = mDataset.get(currentHwType);
        LayoutInflater layoutInflater
                = (LayoutInflater) mContext.getSystemService(Context.LAYOUT_INFLATER_SERVICE);

        if (!currentHwType.equals("Processor")) {
            // Convert Class Name to appropriate display name
            holder.mTextViewHardwareType.setText(
                    PreserveContract.HardwareTypeEntry.HardwareType.valueOf(currentHwType).toString()
            );
        } else {
            holder.mTextViewHardwareType.setText("Processor");
        }

        holder.mLinearLayoutContainer.removeAllViews();
        for (int i = 0; i < models.size(); i++) {

            final Hardware model = models.get(i);
            View item = layoutInflater.inflate(R.layout.build_sub_item, null);

            item.setOnClickListener(new View.OnClickListener() {

                @Override
                public void onClick(View v) {

                    String hardwareType;
                    if (currentHwType.equals("Processor")) {
                        if (model.getCompanyName().equals("Intel")) {
                            hardwareType = "IntelProcessor";
                        } else {
                            hardwareType = "AmdProcessor";
                        }
                    } else {
                        hardwareType = currentHwType;
                    }

                    mOpenModelDetails
                            .openModelDetails(
                                    model.getId(),
                                    PreserveContract.HardwareTypeEntry.HardwareType.valueOf(hardwareType).toString(),
                                    model.getModel()
                            );

                }

            });

            (item.findViewById(R.id.fragment_build_model_delete)).setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {

                    String hardwareType;
                    if (currentHwType.equals("Processor")) {
                        if (model.getCompanyName().equals("Intel")) {
                            hardwareType = "IntelProcessor";
                        } else {
                            hardwareType = "AmdProcessor";
                        }
                    } else {
                        hardwareType = currentHwType;
                    }

                    mContext
                            .getContentResolver()
                            .delete(PreserveContract.BuildEntry.CONTENT_URI,
                                    PreserveContract.BuildEntry.COLUMN_BUILD_HW_TYPE + "=? AND " +
                                            PreserveContract.BuildEntry.COLUMN_BUILD_HW_DESC + " LIKE ?",
                                    new String[]{
                                            hardwareType, "%" + model.getModel() + "\"%"
                                    });
                }

            });

            ((TextView) item.findViewById(R.id.fragment_build_model_title)).setText(model.getModel());
            holder.mLinearLayoutContainer.addView(item);
            if (model.getAttribute().getImageUrls() != null) {
                Glide
                        .with(mContext)
                        .load(model.getAttribute().getImageUrls()[0])
                        .into((ImageView) item.findViewById(R.id.fragment_build_model_img));
            }

        }

    }


    @Override
    public int getItemCount() {
        return mDataset.size();
    }


    class BuildComponentHolder extends RecyclerView.ViewHolder {

        TextView mTextViewHardwareType;
        LinearLayout mLinearLayoutContainer;

        BuildComponentHolder(View itemView) {
            super(itemView);

            mTextViewHardwareType = (TextView) itemView.findViewById(R.id.fragment_build_details_item_tv_hwtype);
            mLinearLayoutContainer = (LinearLayout) itemView.findViewById(R.id.fragment_build_details_item_ll_container);
        }

    }
}
