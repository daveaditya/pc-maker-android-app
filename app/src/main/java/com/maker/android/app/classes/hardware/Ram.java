package com.maker.android.app.classes.hardware;

import android.os.Parcel;

import com.google.gson.JsonArray;
import com.google.gson.JsonDeserializationContext;
import com.google.gson.JsonDeserializer;
import com.google.gson.JsonElement;
import com.google.gson.JsonObject;
import com.google.gson.JsonParseException;
import com.maker.android.app.annotation.HardwareAnnotations;
import com.maker.android.app.classes.Attribute;
import com.maker.android.app.classes.Offer;
import com.maker.android.app.classes.SimpleHighlighter;
import com.maker.android.app.provider.PreserveContract;

import java.lang.reflect.Type;
import java.math.BigDecimal;


@SuppressWarnings({"unused", "WeakerAccess"})
class Ram
        extends Hardware {

    /**
     * A {@link String} constant to use as tag in {@link android.util.Log}
     */
    private static final String LOG_TAG = Ram.class.getSimpleName();

    public static Deserialize sDeserialize;

    static {
        sDeserialize = new Deserialize();
    }

    @HardwareAnnotations.DisplayData(name = "Size", displayInHighlights = true, unit = "GB")
    private BigDecimal mSize;

    @HardwareAnnotations.DisplayData(name = "Type", displayInHighlights = true)
    private String mType;

    @HardwareAnnotations.DisplayData(name = "Kit Configuration", displayInHighlights = true)
    private String mKitConfig;

    @HardwareAnnotations.DisplayData(name = "Speed", unit = "MHz")
    private int mSpeed;

    @HardwareAnnotations.DisplayData(name = "Form Factor")
    private String mFormFactor;

    @HardwareAnnotations.DisplayData(name = "Voltage", unit = "V")
    private BigDecimal mVoltage;

    @HardwareAnnotations.DisplayData(name = "Is Ecc Memory?")
    private boolean mIsEcc;

    @HardwareAnnotations.DisplayData(name = "Is Overclocked?")
    private boolean mIsOverclocked;

    public static final Creator<Ram> CREATOR = new Creator<Ram>() {

        @Override
        public Ram createFromParcel(Parcel in) {
            return new Ram(in);
        }


        @Override
        public Ram[] newArray(int size) {
            return new Ram[size];
        }

    };


    private Ram(int id, String companyName, String model) {
        super(id, companyName, model);
        setHighlighter(new SimpleHighlighter());
    }


    Ram(Parcel in) {
        super(in);
    }


    @Override
    public int describeContents() {
        return 0;
    }


    @Override
    public PreserveContract.HardwareTypeEntry.HardwareType getHardwareType() {
        return PreserveContract.HardwareTypeEntry.HardwareType.Ram;
    }


    public BigDecimal getSize() {
        return mSize;
    }

    public void setSize(BigDecimal size) {
        mSize = size;
    }

    public String getType() {
        return mType;
    }

    public void setType(String type) {
        mType = type;
    }

    public String getKitConfig() {
        return mKitConfig;
    }

    public void setKitConfig(String kitConfig) {
        mKitConfig = kitConfig;
    }

    public int getSpeed() {
        return mSpeed;
    }

    public void setSpeed(int speed) {
        mSpeed = speed;
    }

    public String getFormFactor() {
        return mFormFactor;
    }

    public void setFormFactor(String formFactor) {
        mFormFactor = formFactor;
    }

    public BigDecimal getVoltage() {
        return mVoltage;
    }

    public void setVoltage(BigDecimal voltage) {
        mVoltage = voltage;
    }

    public boolean isEcc() {
        return mIsEcc;
    }

    public void setEcc(boolean isEcc) {
        mIsEcc = isEcc;
    }

    public boolean isOverclocked() {
        return mIsOverclocked;
    }

    public void setOverclocked(boolean overclocked) {
        mIsOverclocked = overclocked;
    }


    interface Builder {

        Builder setSize(BigDecimal size);

        Builder setType(String type);

        Builder setKitConfig(String kitConfig);

        Builder setSpeed(int speed);

        Builder setFormFactor(String formFactor);

        Builder setVoltage(BigDecimal voltage);

        Builder setIsEcc(boolean isEcc);

        Builder setIsOverclocked(boolean isOverclocked);

        Builder setAttributes(Attribute attributes);

        Builder setOffers(Offer[] offers);

        Ram createRam();

    }


    private static class RamBuilder implements Builder {

        Ram mRam;

        RamBuilder(int id, String companyName, String model) {
            mRam = new Ram(id, companyName, model);
        }

        @Override
        public Builder setSize(BigDecimal size) {
            mRam.setSize(size);
            return this;
        }

        @Override
        public Builder setType(String type) {
            mRam.setType(type);
            return this;
        }

        @Override
        public Builder setKitConfig(String kitConfig) {
            mRam.setKitConfig(kitConfig);
            return this;
        }

        @Override
        public Builder setSpeed(int speed) {
            mRam.setSpeed(speed);
            return this;
        }

        @Override
        public Builder setFormFactor(String formFactor) {
            mRam.setFormFactor(formFactor);
            return this;
        }

        @Override
        public Builder setVoltage(BigDecimal voltage) {
            mRam.setVoltage(voltage);
            return this;
        }

        @Override
        public Builder setIsEcc(boolean isEcc) {
            mRam.setEcc(isEcc);
            return this;
        }

        @Override
        public Builder setIsOverclocked(boolean isOverclocked) {
            mRam.setOverclocked(isOverclocked);
            return this;
        }

        @Override
        public Builder setAttributes(Attribute attributes) {
            mRam.setAttribute(attributes);
            return this;
        }

        @Override
        public Builder setOffers(Offer[] offers) {
            mRam.setOffers(offers);
            return this;
        }

        @Override
        public Ram createRam() {
            return mRam;
        }

    }


    private static class Deserialize implements JsonDeserializer<Ram> {

        @Override
        public Ram deserialize(JsonElement json, Type typeOfT, JsonDeserializationContext context) throws JsonParseException {

            JsonObject jsonObject = json.getAsJsonObject();

            int ramId = 0;
            String companyName = null, model = null;

            if (jsonObject.has("ram_id") && !jsonObject.get("ram_id").isJsonNull()) {
                ramId = jsonObject.get("ram_id").getAsInt();
            }

            if (jsonObject.has("company_name") && !jsonObject.get("company_name").isJsonNull()) {
                companyName = jsonObject.get("company_name").getAsString();
            }

            if (jsonObject.has("model") && !jsonObject.get("model").isJsonNull()) {
                model = jsonObject.get("model").getAsString();
            }

            Builder builder = new RamBuilder(ramId, companyName, model);

            if (jsonObject.has("size") && !jsonObject.get("size").isJsonNull()) {
                builder.setSize(jsonObject.get("size").getAsBigDecimal());
            }

            if (jsonObject.has("type") && !jsonObject.get("type").isJsonNull()) {
                builder.setType(jsonObject.get("type").getAsString());
            }

            if (jsonObject.has("kit_config") && !jsonObject.get("kit_config").isJsonNull()) {
                builder.setKitConfig(jsonObject.get("kit_config").getAsString());
            }

            if (jsonObject.has("speed") && !jsonObject.get("speed").isJsonNull()) {
                builder.setSpeed(jsonObject.get("speed").getAsInt());
            }

            if (jsonObject.has("form_factor") && !jsonObject.get("form_factor").isJsonNull()) {
                builder.setFormFactor(jsonObject.get("form_factor").getAsString());
            }

            if (jsonObject.has("voltage") && !jsonObject.get("voltage").isJsonNull()) {
                builder.setVoltage(jsonObject.get("voltage").getAsBigDecimal());
            }

            if (jsonObject.has("is_ecc") && !jsonObject.get("is_ecc").isJsonNull()) {
                builder.setIsEcc(jsonObject.get("is_ecc").getAsBoolean());
            }

            if (jsonObject.has("is_overclocked") && !jsonObject.get("is_overclocked").isJsonNull()) {
                builder.setIsOverclocked(jsonObject.get("is_overclocked").getAsBoolean());
            }

            if (jsonObject.has("attributes") && !jsonObject.get("attributes").isJsonNull()) {
                builder.setAttributes(
                        Attribute.getAttributeFromJsonObject(jsonObject.get("attributes").getAsJsonObject())
                );
            }

            if (jsonObject.has("offers") && !jsonObject.get("offers").isJsonNull()) {
                JsonArray jsonArray = jsonObject.get("offers").getAsJsonArray();
                builder.setOffers(Offer.getOffersFromJsonArray(jsonArray));
            }

            return builder.createRam();

        }

    }

}
