package com.maker.android.app.classes.hardware;

import android.os.Parcel;

import com.google.gson.JsonArray;
import com.google.gson.JsonDeserializationContext;
import com.google.gson.JsonDeserializer;
import com.google.gson.JsonElement;
import com.google.gson.JsonObject;
import com.google.gson.JsonParseException;
import com.maker.android.app.annotation.HardwareAnnotations;
import com.maker.android.app.classes.Attribute;
import com.maker.android.app.classes.Offer;
import com.maker.android.app.classes.SimpleHighlighter;
import com.maker.android.app.provider.PreserveContract;

import java.lang.reflect.Type;
import java.math.BigDecimal;


@SuppressWarnings({"unused", "WeakerAccess"})
class Monitor
        extends Hardware {

    /**
     * A {@link String} constant to use as tag for {@link android.util.Log}
     */
    private static final String LOG_TAG = Monitor.class.getSimpleName();

    public static Deserialize sDeserialize;

    static {
        sDeserialize = new Deserialize();
    }

    @HardwareAnnotations.DisplayData(name = "Screen Size", displayInHighlights = true, unit = "\u2033")
    private BigDecimal mScreenSize;

    @HardwareAnnotations.DisplayData(name = "Resolution", displayInHighlights = true)
    private String mResolution;

    @HardwareAnnotations.DisplayData(name = "Frequency", unit = "Hz")
    private int mFrequency;

    @HardwareAnnotations.DisplayData(name = "Response Time", unit = "ms")
    private int mResponseTime;

    @HardwareAnnotations.DisplayData(name = "Brightness", unit = "cd/m\u00B2")
    private int mBrightness;

    @HardwareAnnotations.DisplayData(name = "Has VGA Port?")
    private boolean mHasVga;

    @HardwareAnnotations.DisplayData(name = "Has USB Port?")
    private boolean mHasUsb;

    @HardwareAnnotations.DisplayData(name = "Has HDMI Port?")
    private boolean mHasHdmi;

    @HardwareAnnotations.DisplayData(name = "Has Mini Display Port?")
    private boolean mHasMiniDisplayPort;

    @HardwareAnnotations.DisplayData(name = "Has Display Port?")
    private boolean mHasDisplayPort;

    @HardwareAnnotations.DisplayData(name = "Has DVI-D?")
    private boolean mHasDviD;

    @HardwareAnnotations.DisplayData(name = "Has D-SUB?")
    private boolean mHasDSub;

    @HardwareAnnotations.DisplayData(name = "Has Speaker?")
    private boolean mHasSpeaker;

    @HardwareAnnotations.DisplayData(name = "Has IPS Display?")
    private boolean mHasIpsDisplay;

    @HardwareAnnotations.DisplayData(name = "Has Curved Screen?")
    private boolean mHasCurvedScreen;

    @HardwareAnnotations.DisplayData(name = "Has Wide Screen?")
    private boolean mHasWidescreen;

    @HardwareAnnotations.DisplayData(name = "Has Headphone Out?")
    private boolean mHasHeadphoneOut;

    @HardwareAnnotations.DisplayData(name = "Has VESA Mounting?")
    private boolean mHasVesaMounting;

    @HardwareAnnotations.DisplayData(name = "Frame Sync")
    private String mFrameSync;


    public static final Creator<Monitor> CREATOR = new Creator<Monitor>() {

        @Override
        public Monitor createFromParcel(Parcel in) {
            return new Monitor(in);
        }


        @Override
        public Monitor[] newArray(int size) {
            return new Monitor[size];
        }

    };


    private Monitor(int id, String companyName, String model) {
        super(id, companyName, model);
        setHighlighter(new SimpleHighlighter());
    }


    Monitor(Parcel in) {
        super(in);
    }


    @Override
    public int describeContents() {
        return 0;
    }


    @Override
    public PreserveContract.HardwareTypeEntry.HardwareType getHardwareType() {
        return PreserveContract.HardwareTypeEntry.HardwareType.Monitor;
    }

    public BigDecimal getScreenSize() {
        return mScreenSize;
    }

    public void setScreenSize(BigDecimal screenSize) {
        mScreenSize = screenSize;
    }

    public String getResolution() {
        return mResolution;
    }

    public void setResolution(String resolution) {
        mResolution = resolution;
    }

    public int getFrequency() {
        return mFrequency;
    }

    public void setFrequency(int frequency) {
        mFrequency = frequency;
    }

    public int getResponseTime() {
        return mResponseTime;
    }

    public void setResponseTime(int responseTime) {
        mResponseTime = responseTime;
    }

    public int getBrightness() {
        return mBrightness;
    }

    public void setBrightness(int brightness) {
        mBrightness = brightness;
    }

    public boolean hasVga() {
        return mHasVga;
    }

    public void setHasVga(boolean hasVga) {
        mHasVga = hasVga;
    }

    public boolean hasUsb() {
        return mHasUsb;
    }

    public void setHasUsb(boolean hasUsb) {
        mHasUsb = hasUsb;
    }

    public boolean hasHdmi() {
        return mHasHdmi;
    }

    public void setHasHdmi(boolean hasHdmi) {
        mHasHdmi = hasHdmi;
    }

    public boolean hasMiniDisplayPort() {
        return mHasMiniDisplayPort;
    }

    public void setHasMiniDisplayPort(boolean hasMiniDisplayPort) {
        mHasMiniDisplayPort = hasMiniDisplayPort;
    }

    public boolean hasDisplayPort() {
        return mHasDisplayPort;
    }

    public void setHasDisplayPort(boolean hasDisplayPort) {
        mHasDisplayPort = hasDisplayPort;
    }

    public boolean hasDviD() {
        return mHasDviD;
    }

    public void setHasDviD(boolean hasDviD) {
        mHasDviD = hasDviD;
    }

    public boolean hasDSub() {
        return mHasDSub;
    }

    public void setHasDSub(boolean hasDSub) {
        mHasDSub = hasDSub;
    }

    public boolean hasSpeaker() {
        return mHasSpeaker;
    }

    public void setHasSpeaker(boolean hasSpeaker) {
        mHasSpeaker = hasSpeaker;
    }

    public boolean hasIpsDisplay() {
        return mHasIpsDisplay;
    }

    public void setHasIpsDisplay(boolean hasIpsDisplay) {
        mHasIpsDisplay = hasIpsDisplay;
    }

    public boolean hasCurvedScreen() {
        return mHasCurvedScreen;
    }

    public void setHasCurvedScreen(boolean hasCurvedScreen) {
        mHasCurvedScreen = hasCurvedScreen;
    }

    public boolean hasWidescreen() {
        return mHasWidescreen;
    }

    public void setHasWidescreen(boolean hasWidescreen) {
        mHasWidescreen = hasWidescreen;
    }

    public boolean hasHeadphoneOut() {
        return mHasHeadphoneOut;
    }

    public void setHasHeadphoneOut(boolean hasHeadphoneOut) {
        mHasVesaMounting = hasHeadphoneOut;
    }

    public boolean hasVesaMounting() {
        return mHasVesaMounting;
    }

    public void setHasVesaMounting(boolean hasVesaMounting) {
        mHasVesaMounting = hasVesaMounting;
    }

    public String getFrameSync() {
        return mFrameSync;
    }

    public void setFrameSync(String frameSync) {
        mFrameSync = frameSync;
    }


    interface Builder {

        Builder setScreenSize(BigDecimal screenSize);

        Builder setResolution(String resolution);

        Builder setFrequency(int frequency);

        Builder setResponseTime(int responseTime);

        Builder setBrightness(int brightness);

        Builder setHasVga(boolean hasVga);

        Builder setHasUsb(boolean hasUsb);

        Builder setHasHdmi(boolean hasHdmi);

        Builder setHasMiniDisplayPort(boolean hasMiniDisplayPort);

        Builder setHasDisplayPort(boolean hasDisplayPort);

        Builder setHasDviD(boolean hasHviD);

        Builder setHasDSub(boolean dSub);

        Builder setHasSpeaker(boolean hasSpeaker);

        Builder setHasIpsDisplay(boolean hasIpsDisplay);

        Builder setHasCurvedScreen(boolean hasCurvedScreen);

        Builder setHasWidescreen(boolean hasWidescreen);

        Builder setHasHeadphoneOut(boolean hasHeadphoneOut);

        Builder setHasVesaMounting(boolean hasVesaMounting);

        Builder setFrameSync(String frameSync);

        Builder setAttribute(Attribute attribute);

        Builder setOffers(Offer[] offers);

        Monitor createMonitor();

    }


    private static class MonitorBuilder implements Builder {

        Monitor mMonitor;

        MonitorBuilder(int id, String companyName, String model) {
            mMonitor = new Monitor(id, companyName, model);
        }

        @Override
        public Builder setScreenSize(BigDecimal screenSize) {
            mMonitor.setScreenSize(screenSize);
            return this;
        }

        @Override
        public Builder setResolution(String resolution) {
            mMonitor.setResolution(resolution);
            return this;
        }

        @Override
        public Builder setFrequency(int frequency) {
            mMonitor.setFrequency(frequency);
            return this;
        }

        @Override
        public Builder setResponseTime(int responseTime) {
            mMonitor.setResponseTime(responseTime);
            return this;
        }

        @Override
        public Builder setBrightness(int brightness) {
            mMonitor.setBrightness(brightness);
            return this;
        }

        @Override
        public Builder setHasVga(boolean hasVga) {
            mMonitor.setHasVga(hasVga);
            return this;
        }

        @Override
        public Builder setHasUsb(boolean hasUsb) {
            mMonitor.setHasUsb(hasUsb);
            return this;
        }

        @Override
        public Builder setHasHdmi(boolean hasHdmi) {
            mMonitor.setHasHdmi(hasHdmi);
            return this;
        }

        @Override
        public Builder setHasMiniDisplayPort(boolean hasMiniDisplayPort) {
            mMonitor.setHasMiniDisplayPort(hasMiniDisplayPort);
            return this;
        }

        @Override
        public Builder setHasDisplayPort(boolean hasDisplayPort) {
            mMonitor.setHasDisplayPort(hasDisplayPort);
            return this;
        }

        @Override
        public Builder setHasDviD(boolean hasHviD) {
            mMonitor.setHasDviD(hasHviD);
            return this;
        }

        @Override
        public Builder setHasDSub(boolean hasDSub) {
            mMonitor.setHasDSub(hasDSub);
            return this;
        }

        @Override
        public Builder setHasSpeaker(boolean hasSpeaker) {
            mMonitor.setHasSpeaker(hasSpeaker);
            return this;
        }

        @Override
        public Builder setHasIpsDisplay(boolean hasIpsDisplay) {
            mMonitor.setHasIpsDisplay(hasIpsDisplay);
            return this;
        }

        @Override
        public Builder setHasCurvedScreen(boolean hasCurvedScreen) {
            mMonitor.setHasWidescreen(hasCurvedScreen);
            return this;
        }

        @Override
        public Builder setHasWidescreen(boolean hasWidescreen) {
            mMonitor.setHasWidescreen(hasWidescreen);
            return this;
        }

        @Override
        public Builder setHasHeadphoneOut(boolean hasHeadphoneOut) {
            mMonitor.setHasHeadphoneOut(hasHeadphoneOut);
            return this;
        }

        @Override
        public Builder setHasVesaMounting(boolean hasVesaMounting) {
            mMonitor.setHasVesaMounting(hasVesaMounting);
            return this;
        }

        @Override
        public Builder setFrameSync(String frameSync) {
            mMonitor.setFrameSync(frameSync);
            return this;
        }

        @Override
        public Builder setAttribute(Attribute attribute) {
            mMonitor.setAttribute(attribute);
            return this;
        }

        @Override
        public Builder setOffers(Offer[] offers) {
            mMonitor.setOffers(offers);
            return this;
        }

        @Override
        public Monitor createMonitor() {
            return mMonitor;
        }

    }


    private static class Deserialize implements JsonDeserializer<Monitor> {

        @Override
        public Monitor deserialize(JsonElement json, Type typeOfT, JsonDeserializationContext context) throws JsonParseException {

            JsonObject jsonObject = json.getAsJsonObject();

            int monitorId = 0;
            String companyName = null, model = null;

            if (jsonObject.has("monitor_id") && !jsonObject.get("monitor_id").isJsonNull()) {
                monitorId = jsonObject.get("monitor_id").getAsInt();
            }

            if (jsonObject.has("company_name") && !jsonObject.get("company_name").isJsonNull()) {
                companyName = jsonObject.get("company_name").getAsString();
            }

            if (jsonObject.has("model") && !jsonObject.get("model").isJsonNull()) {
                model = jsonObject.get("model").getAsString();
            }

            Builder builder = new MonitorBuilder(monitorId, companyName, model);

            if (jsonObject.has("screen_size") && !jsonObject.get("screen_size").isJsonNull()) {
                builder.setScreenSize(jsonObject.get("screen_size").getAsBigDecimal());
            }

            if (jsonObject.has("resolution") && !jsonObject.get("resolution").isJsonNull()) {
                builder.setResolution(jsonObject.get("resolution").getAsString());
            }

            if (jsonObject.has("frequency") && !jsonObject.get("frequency").isJsonNull()) {
                builder.setFrequency(jsonObject.get("frequency").getAsInt());
            }

            if (jsonObject.has("response_time") && !jsonObject.get("response_time").isJsonNull()) {
                builder.setResponseTime(jsonObject.get("response_time").getAsInt());
            }

            if (jsonObject.has("brightness") && !jsonObject.get("brightness").isJsonNull()) {
                builder.setBrightness(jsonObject.get("brightness").getAsInt());
            }

            if (jsonObject.has("has_vga") && !jsonObject.get("has_vga").isJsonNull()) {
                builder.setHasVga(jsonObject.get("has_vga").getAsBoolean());
            }

            if (jsonObject.has("has_usb") && !jsonObject.get("has_usb").isJsonNull()) {
                builder.setHasUsb(jsonObject.get("has_usb").getAsBoolean());
            }

            if (jsonObject.has("has_hdmi") && !jsonObject.get("has_hdmi").isJsonNull()) {
                builder.setHasHdmi(jsonObject.get("has_hdmi").getAsBoolean());
            }

            if (jsonObject.has("has_mini_displayport") && !jsonObject.get("has_mini_displayport").isJsonNull()) {
                builder.setHasMiniDisplayPort(jsonObject.get("has_mini_displayport").getAsBoolean());
            }

            if (jsonObject.has("has_displayport") && !jsonObject.get("has_displayport").isJsonNull()) {
                builder.setHasDisplayPort(jsonObject.get("has_displayport").getAsBoolean());
            }

            if (jsonObject.has("has_dvi_d") && !jsonObject.get("has_dvi_d").isJsonNull()) {
                builder.setHasDviD(jsonObject.get("has_dvi_d").getAsBoolean());
            }

            if (jsonObject.has("has_d_dub") && !jsonObject.get("has_d_dub").isJsonNull()) {
                builder.setHasDSub(jsonObject.get("has_d_dub").getAsBoolean());
            }

            if (jsonObject.has("has_speaker") && !jsonObject.get("has_speaker").isJsonNull()) {
                builder.setHasSpeaker(jsonObject.get("has_speaker").getAsBoolean());
            }

            if (jsonObject.has("is_ips_display") && !jsonObject.get("is_ips_display").isJsonNull()) {
                builder.setHasIpsDisplay(jsonObject.get("is_ips_display").getAsBoolean());
            }

            if (jsonObject.has("is_curved_display") && !jsonObject.get("has_usb").isJsonNull()) {
                builder.setHasCurvedScreen(jsonObject.get("has_usb").getAsBoolean());
            }

            if (jsonObject.has("is_widescreen") && !jsonObject.get("is_widescreen").isJsonNull()) {
                builder.setHasWidescreen(jsonObject.get("is_widescreen").getAsBoolean());
            }

            if (jsonObject.has("has_headphone_out") && !jsonObject.get("has_headphone_out").isJsonNull()) {
                builder.setHasHeadphoneOut(jsonObject.get("has_headphone_out").getAsBoolean());
            }

            if (jsonObject.has("has_vesa_mounting") && !jsonObject.get("has_vesa_mounting").isJsonNull()) {
                builder.setHasVesaMounting(jsonObject.get("has_vesa_mounting").getAsBoolean());
            }

            if (jsonObject.has("framesync") && !jsonObject.get("framesync").isJsonNull()) {
                builder.setFrameSync(jsonObject.get("framesync").getAsString());
            }

            if (jsonObject.has("attributes") && !jsonObject.get("attributes").isJsonNull()) {
                builder.setAttribute(
                        Attribute.getAttributeFromJsonObject(jsonObject.get("attributes").getAsJsonObject())
                );
            }

            if (jsonObject.has("offers") && !jsonObject.get("offers").isJsonNull()) {
                JsonArray jsonArray = jsonObject.get("offers").getAsJsonArray();
                builder.setOffers(Offer.getOffersFromJsonArray(jsonArray));
            }

            return builder.createMonitor();

        }

    }

}
