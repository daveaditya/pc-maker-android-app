package com.maker.android.app.adapter;

import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentPagerAdapter;

import com.maker.android.app.fragment.OrganizationTechSupportFragment;
import com.maker.android.app.fragment.TechnicianTechSupportFragment;


@SuppressWarnings({"unused", "WeakerAccess"})
public class TechSupportPagerAdapter extends FragmentPagerAdapter {

    /**
     * A {@link String} constant to use as tag in {@link android.util.Log}
     */
    private static final String LOG_TAG = TechSupportPagerAdapter.class.getSimpleName();

    private int mTabCount;

    public TechSupportPagerAdapter(FragmentManager fm, int tabCount) {
        super(fm);
        mTabCount = tabCount;
    }

    @Override
    public Fragment getItem(int position) {

        switch (position) {
            case 0:
                return new OrganizationTechSupportFragment();
            case 1:
                return new TechnicianTechSupportFragment();
            default:
                return null;
        }

    }

    @Override
    public int getCount() {
        return mTabCount;
    }

}
