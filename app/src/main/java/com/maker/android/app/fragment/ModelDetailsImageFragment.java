package com.maker.android.app.fragment;

import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.content.res.ResourcesCompat;
import android.support.v4.view.ViewPager;
import android.util.TypedValue;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ProgressBar;
import android.widget.TextView;

import com.google.common.base.CharMatcher;
import com.google.gson.Gson;
import com.google.gson.JsonObject;
import com.maker.android.app.R;
import com.maker.android.app.activity.ModelDetailsActivity;
import com.maker.android.app.adapter.ModelImagePagerAdapter;

import java.util.ArrayList;

/**
 * A simple {@link Fragment} subclass.
 * to handle interaction events.
 */
@SuppressWarnings({"unused"})
public class ModelDetailsImageFragment
        extends Fragment {

    /**
     * A {@link String} constant to use in {@link android.util.Log}
     */
    private static final String LOG_TAG = ModelDetailsImageFragment.class.getSimpleName();

    private LinearLayout mLinearLayout;
    private ViewPager viewPager;


    public ModelDetailsImageFragment() {
        // Required empty public constructor
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {

        View view = inflater.inflate(R.layout.fragment_model_detail_images, container, false);

        TextView textView = (TextView) view.findViewById(R.id.fragment_model_detail_image_tv_not_available);
        viewPager = (ViewPager) view.findViewById(R.id.fragment_model_detail_images_vp);
        ProgressBar progressBar = (ProgressBar) view.findViewById(R.id.fragment_model_detail_image_pb_loading);
        mLinearLayout = (LinearLayout) view.findViewById(R.id.fragment_model_detail_image_ll_page_indicator);

        ArrayList<String> urls = new ArrayList<>();
        try {
            // Creates a GSON instance
            Gson gson = new Gson();

            // Creates a JSONObject from the response
            JsonObject jsonObject = gson.fromJson(getArguments().getString(ModelDetailsActivity.SHOW_DETAILS_DATA), JsonObject.class);

            if (jsonObject.has("attributes") && !jsonObject.get("attributes").isJsonNull()) {
                JsonObject attributes
                        = jsonObject.getAsJsonObject("attributes");

                String[] temp = attributes.get("img_srcs").getAsString().split(",");

                for (String aTemp : temp) {
                    urls.add(CharMatcher.is('"').trimFrom(aTemp));
                }

            } else {
                progressBar.setVisibility(View.GONE);
                textView.setVisibility(View.VISIBLE);
                return view;
            }

            // Fetches JSONException that might occur while parsing JSON data
        } catch (NullPointerException exc) {
            exc.printStackTrace();
        }

        ModelImagePagerAdapter modelImagePagerAdapter = new ModelImagePagerAdapter(getContext(), urls);
        viewPager.setAdapter(modelImagePagerAdapter);
        progressBar.setVisibility(View.GONE);
        viewPager.setVisibility(View.VISIBLE);
        mLinearLayout.setVisibility(View.VISIBLE);

        // Setup the image pagination indicator
        mLinearLayout.setGravity(Gravity.CENTER_VERTICAL | Gravity.CENTER_HORIZONTAL);
        final LinearLayout.LayoutParams layoutParamsSmall = new LinearLayout.LayoutParams(
                (int) TypedValue.applyDimension(TypedValue.COMPLEX_UNIT_DIP, 6, getResources().getDisplayMetrics()),
                (int) TypedValue.applyDimension(TypedValue.COMPLEX_UNIT_DIP, 6, getResources().getDisplayMetrics())
        );
        final LinearLayout.LayoutParams layoutParamsBig = new LinearLayout.LayoutParams(
                (int) TypedValue.applyDimension(TypedValue.COMPLEX_UNIT_DIP, 10, getResources().getDisplayMetrics()),
                (int) TypedValue.applyDimension(TypedValue.COMPLEX_UNIT_DIP, 10, getResources().getDisplayMetrics())
        );
        layoutParamsSmall.setMargins(
                0, 0, (int) TypedValue.applyDimension(TypedValue.COMPLEX_UNIT_DIP, 8, getResources().getDisplayMetrics()), 0
        );
        layoutParamsBig.setMargins(
                0, 0, (int) TypedValue.applyDimension(TypedValue.COMPLEX_UNIT_DIP, 8, getResources().getDisplayMetrics()), 0
        );

        mLinearLayout.removeAllViews();
        for (int i = 0; i < urls.size(); i++) {
            ImageView imageView = new ImageView(getContext());
            if (i == 0) {
                imageView.setImageDrawable(ResourcesCompat.getDrawable(getResources(), R.drawable.filled_circle, null));
                imageView.setLayoutParams(layoutParamsBig);
            } else {
                imageView.setImageDrawable(ResourcesCompat.getDrawable(getResources(), R.drawable.empty_circle, null));
                imageView.setLayoutParams(layoutParamsSmall);
            }
            mLinearLayout.addView(imageView);
        }

        viewPager.addOnPageChangeListener(new ViewPager.OnPageChangeListener() {

            int prev = 0;

            @Override
            public void onPageScrolled(int position, float positionOffset, int positionOffsetPixels) {

            }

            @Override
            public void onPageSelected(int position) {

                ImageView imageViewPrev, imageViewNext;
                int prevpos;
                if (prev < position) {
                    // view is moving to right
                    prevpos = position - 1;
                } else {
                    // view is moving to left
                    prevpos = position + 1;
                }
                prev = position;

                imageViewPrev = (ImageView) mLinearLayout.getChildAt(prevpos);

                imageViewPrev.setImageDrawable(
                        ResourcesCompat.getDrawable(getResources(), R.drawable.empty_circle, null)
                );

                imageViewNext = (ImageView) mLinearLayout.getChildAt(position);
                imageViewNext.setImageDrawable(
                        ResourcesCompat.getDrawable(getResources(), R.drawable.filled_circle, null)
                );

                imageViewPrev.setLayoutParams(layoutParamsSmall);
                imageViewNext.setLayoutParams(layoutParamsBig);

            }

            @Override
            public void onPageScrollStateChanged(int state) {

            }
        });

        return view;
    }

    @Override
    public void onStop() {
        super.onStop();
        viewPager.setCurrentItem(0);
    }

}
