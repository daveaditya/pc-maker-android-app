package com.maker.android.app.classes.hardware;

import android.os.Parcel;

import com.google.gson.JsonArray;
import com.google.gson.JsonDeserializationContext;
import com.google.gson.JsonDeserializer;
import com.google.gson.JsonElement;
import com.google.gson.JsonObject;
import com.google.gson.JsonParseException;
import com.maker.android.app.annotation.HardwareAnnotations;
import com.maker.android.app.classes.Attribute;
import com.maker.android.app.classes.Offer;
import com.maker.android.app.classes.SimpleHighlighter;
import com.maker.android.app.provider.PreserveContract;

import java.lang.reflect.Type;


@SuppressWarnings({"unused", "WeakerAccess"})
class Speaker
        extends Hardware {

    /**
     * A {@link String} constant to use as tag in {@link android.util.Log}
     */
    private static final String LOG_TAG = Speaker.class.getSimpleName();

    public static Deserialize sDeserialize;

    static {
        sDeserialize = new Deserialize();
    }

    @HardwareAnnotations.DisplayData(name = "Configuration", displayInHighlights = true)
    private String mConfiguration;

    @HardwareAnnotations.DisplayData(name = "Is Usb Powered?")
    private boolean mIsUsbPowered;

    @HardwareAnnotations.DisplayData(name = "Has Bluetooth Support?")
    private boolean mHasBluetooth;

    @HardwareAnnotations.DisplayData(name = "Output Power", displayInHighlights = true)
    private String mOutputPower;

    @HardwareAnnotations.DisplayData(name = "Frequency Range")
    private String mFrequencyRange;

    public static final Creator<Speaker> CREATOR = new Creator<Speaker>() {

        @Override
        public Speaker createFromParcel(Parcel in) {
            return new Speaker(in);
        }

        @Override
        public Speaker[] newArray(int size) {
            return new Speaker[size];
        }

    };


    private Speaker(int id, String companyName, String model) {
        super(id, companyName, model);
        setHighlighter(new SimpleHighlighter());
    }


    Speaker(Parcel in) {
        super(in);
    }


    @Override
    public int describeContents() {
        return 0;
    }


    @Override
    public PreserveContract.HardwareTypeEntry.HardwareType getHardwareType() {
        return PreserveContract.HardwareTypeEntry.HardwareType.Speaker;
    }

    public String getConfiguration() {
        return mConfiguration;
    }

    public void setConfiguration(String configuration) {
        mConfiguration = configuration;
    }

    public boolean isUsbPowered() {
        return mIsUsbPowered;
    }

    public void setUsbPowered(boolean usbPowered) {
        mIsUsbPowered = usbPowered;
    }

    public boolean hasBluetooth() {
        return mHasBluetooth;
    }

    public void setHasBluetooth(boolean hasBluetooth) {
        mHasBluetooth = hasBluetooth;
    }

    public String getOutputPower() {
        return mOutputPower;
    }

    public void setOutputPower(String outputPower) {
        mOutputPower = outputPower;
    }

    public String getFrequencyRange() {
        return mFrequencyRange;
    }

    public void setFrequencyRange(String frequencyRange) {
        mFrequencyRange = frequencyRange;
    }


    interface Builder {

        Builder setConfiguration(String configuration);

        Builder setIsUsbPowered(boolean isUsbPowered);

        Builder setHasBluetooth(boolean hasBluetooth);

        Builder setOutputPower(String outputPower);

        Builder setFrequencyRange(String frequencyRange);

        Builder setAttributes(Attribute attributes);

        Builder setOffers(Offer[] offers);

        Speaker createSpeaker();

    }


    private static class SpeakerBuilder implements Builder {

        Speaker mSpeaker;

        SpeakerBuilder(int id, String companyName, String model) {
            mSpeaker = new Speaker(id, companyName, model);
        }

        @Override
        public Builder setConfiguration(String configuration) {
            mSpeaker.setConfiguration(configuration);
            return this;
        }

        @Override
        public Builder setIsUsbPowered(boolean isUsbPowered) {
            mSpeaker.setUsbPowered(isUsbPowered);
            return this;
        }

        @Override
        public Builder setHasBluetooth(boolean hasBluetooth) {
            mSpeaker.setHasBluetooth(hasBluetooth);
            return this;
        }

        @Override
        public Builder setOutputPower(String outputPower) {
            mSpeaker.setOutputPower(outputPower);
            return this;
        }

        @Override
        public Builder setFrequencyRange(String frequencyRange) {
            mSpeaker.setFrequencyRange(frequencyRange);
            return this;
        }

        @Override
        public Builder setAttributes(Attribute attributes) {
            mSpeaker.setAttribute(attributes);
            return this;
        }

        @Override
        public Builder setOffers(Offer[] offers) {
            mSpeaker.setOffers(offers);
            return this;
        }

        @Override
        public Speaker createSpeaker() {
            return mSpeaker;
        }

    }


    private static class Deserialize implements JsonDeserializer<Speaker> {

        @Override
        public Speaker deserialize(JsonElement json, Type typeOfT, JsonDeserializationContext context) throws JsonParseException {

            JsonObject jsonObject = json.getAsJsonObject();

            int speakerId = 0;
            String companyName = null, model = null;

            if (jsonObject.has("speaker_id") && !jsonObject.get("speaker_id").isJsonNull()) {
                speakerId = jsonObject.get("speaker_id").getAsInt();
            }

            if (jsonObject.has("company_name") && !jsonObject.get("company_name").isJsonNull()) {
                companyName = jsonObject.get("company_name").getAsString();
            }

            if (jsonObject.has("model") && !jsonObject.get("model").isJsonNull()) {
                model = jsonObject.get("model").getAsString();
            }

            Builder builder = new SpeakerBuilder(speakerId, companyName, model);

            if (jsonObject.has("configuration") && !jsonObject.get("configuration").isJsonNull()) {
                builder.setConfiguration(jsonObject.get("configuration").getAsString());
            }

            if (jsonObject.has("is_usb_powered") && !jsonObject.get("is_usb_powered").isJsonNull()) {
                builder.setIsUsbPowered(jsonObject.get("is_usb_powered").getAsBoolean());
            }

            if (jsonObject.has("has_bluetooth") && !jsonObject.get("has_bluetooth").isJsonNull()) {
                builder.setHasBluetooth(jsonObject.get("has_bluetooth").getAsBoolean());
            }

            if (jsonObject.has("output_power") && !jsonObject.get("output_power").isJsonNull()) {
                builder.setOutputPower(jsonObject.get("output_power").getAsString());
            }

            if (jsonObject.has("freq_range") && !jsonObject.get("freq_range").isJsonNull()) {
                builder.setFrequencyRange(jsonObject.get("freq_range").getAsString());
            }

            if (jsonObject.has("attributes") && !jsonObject.get("attributes").isJsonNull()) {
                builder.setAttributes(
                        Attribute.getAttributeFromJsonObject(jsonObject.get("attributes").getAsJsonObject())
                );
            }

            if (jsonObject.has("offers") && !jsonObject.get("offers").isJsonNull()) {
                JsonArray jsonArray = jsonObject.get("offers").getAsJsonArray();
                builder.setOffers(Offer.getOffersFromJsonArray(jsonArray));
            }

            return builder.createSpeaker();

        }

    }

}
