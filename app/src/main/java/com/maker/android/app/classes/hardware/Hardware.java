package com.maker.android.app.classes.hardware;


import android.os.Parcel;
import android.os.Parcelable;
import android.support.annotation.Nullable;
import android.text.SpannableStringBuilder;

import com.maker.android.app.annotation.HardwareAnnotations.DisplayData;
import com.maker.android.app.annotation.HardwareAnnotations.DontShowThis;
import com.maker.android.app.classes.Attribute;
import com.maker.android.app.classes.Highlights;
import com.maker.android.app.classes.Offer;
import com.maker.android.app.provider.PreserveContract;
import com.maker.android.app.utils.MyUtils;

import java.lang.annotation.Annotation;
import java.lang.reflect.Field;
import java.util.ArrayList;
import java.util.Collections;
import java.util.LinkedHashMap;


@SuppressWarnings({"unused"})
public abstract class Hardware
        implements Parcelable {

    @DontShowThis
    private static final String LOG_TAG = Hardware.class.getSimpleName();

    /**
     * ID of the {@link Hardware} product
     */
    @DontShowThis
    private int mId;

    /**
     * Company name of the {@link Hardware}
     */
    @DisplayData(name = "Company Name")
    private String mCompanyName;

    /**
     * Name of model of the {@link Hardware}
     */
    @DisplayData(name = "Model")
    private String mModel;

    /**
     * Instance of the {@link Attribute}
     */
    @DontShowThis
    private Attribute mAttribute;

    /**
     * Price of the {@link Hardware}
     */
    @DisplayData(name = "Price", displayInHighlights = true)
    private Offer[] mOffers;

    /**
     * {@link Highlights} for the class
     */
    @DontShowThis
    private Highlights mHighlighter;


    /**
     * Constructor of {@link Hardware}
     *
     * @param id            ID of the {@link Hardware} product
     * @param companyName   Company name of the {@link Hardware}
     * @param model         InSpecSheet of the product
     */
    Hardware(int id, String companyName, String model) {
        mId = id;
        mCompanyName = companyName;
        mModel = model;
    }


    /**
     * Creates a {@link Hardware} object from {@link Parcel}
     *
     * @param in {@link Parcel} containing {@link Hardware} data
     */
    Hardware(Parcel in) {
        this.mId = in.readInt();
        this.mCompanyName = in.readString();
        this.mModel = in.readString();
    }

    /**
     * Returns an HashMap of the Object
     *
     * @param object A class extending {@link Hardware}
     * @param <T> A class extending {@link Hardware}
     * @return A {@link LinkedHashMap} for the given object
     */
    @Nullable
    public static <T extends Hardware> LinkedHashMap<String, Object> getAsLinkedHashMap(T object, boolean forHighlights) {

        // Create a HashMap to store data
        LinkedHashMap<String, Object> data = new LinkedHashMap<>();

        try {
            // Class to represent
            Class clazz = object.getClass();

            ArrayList<Field> fieldCollection = new ArrayList<>();
            Collections.addAll(fieldCollection, clazz.getDeclaredFields());
            Collections.addAll(fieldCollection, clazz.getSuperclass().getDeclaredFields());

            // For every field in the object add it to the HashMap
            for (Field field : fieldCollection) {

                // Makes the private fields accessible
                field.setAccessible(true);

                // If the Marker Annotation DontShowThis is present on the field
                // dont include it, i.e. skip the current field
                if (field.isAnnotationPresent(DontShowThis.class) ||
                        field.get(object) == null || field.getName().equals("serialVersionUID")) {
                        continue;
                }

                // Gets the DisplayData annotation for the current field
                DisplayData displayData = null;
                for (Annotation annotation : field.getDeclaredAnnotations()) {
                    if (annotation.annotationType() == DisplayData.class) {
                        displayData = (DisplayData) annotation;
                    }
                }

                // If DisplayData annotation is not present skip the current field
                if (displayData == null) {
                    continue;
                }

                // If the field is mOffers
                // Add the display name and value to the HashMap
                if (forHighlights && field.getName().equals("mOffers") && displayData.displayInHighlights()) {
                    data.put(displayData.name(), ((Offer[]) field.get(object))[0].getFormattedPrice());
                    continue;
                }

                Object dataValue = field.get(object);
                if (dataValue != null) {
                    if (dataValue.toString().equals("0")) {
                        continue;
                    }
                    if (dataValue.toString().equals("false")) {
                        dataValue = "No";
                    } else if (dataValue.toString().equals("true")) {
                        dataValue = "Yes";
                    }
                }
                if (!displayData.unit().equals("Something")) {
                    assert dataValue != null;
                    dataValue = dataValue.toString() + " " + displayData.unit();
                }


                if (forHighlights && displayData.displayInHighlights()) {
                    // Add the display name and value to the HashMap
                    data.put(displayData.name(), dataValue);
                }

                if (!forHighlights && !displayData.displayInHighlights()) {
                    // Add the display name and value to the HashMap
                    data.put(displayData.name(), dataValue);
                }

            }

        } catch (IllegalAccessException exc) {
            exc.printStackTrace();
            return null;
        }

        // Returns the created HashMap
        return data;
    }


    /**
     * A method that returns the {@link PreserveContract.HardwareTypeEntry.HardwareType}
     *
     * @return HardwareType of this object
     */
    abstract public PreserveContract.HardwareTypeEntry.HardwareType getHardwareType();


    /**
     * Creates a {@link Parcel} from {@link Hardware} object
     *
     * @param dest  {@link Parcel} to send
     * @param flags Special flags if any
     */
    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeInt(getId());
        dest.writeString(getCompanyName());
        dest.writeString(getModel());
    }

    /**
     * Returns the id of the {@link Hardware}
     *
     * @return ID of the {@link Hardware}
     */
    public int getId() {
        return mId;
    }


    /**
     * Sets the ID of the {@link Hardware}
     *
     * @param id ID of the {@link Hardware}
     */
    public void setId(int id) {
        mId = id;
    }


    /**
     * Returns the name of the company of {@link Hardware}
     *
     * @return Name of company
     */
    public String getCompanyName() {
        return mCompanyName;
    }


    /**
     * Sets the name of the company
     *
     * @param companyName Name of the CatalogueItem company
     */
    public void setCompanyName(String companyName) {
        mCompanyName = companyName;
    }

    /**
     * Returns the model name of the {@link Hardware}
     *
     * @return Model of the {@link Hardware}
     */
    public String getModel() {
        return mModel;
    }


    /**
     * Sets the model name of the {@link Hardware}
     *
     * @param model InSpecSheet name of the {@link Hardware}
     */
    public void setModel(String model) {
        mModel = model;
    }


    /**
     * Returns the mOffers of the {@link Hardware}
     *
     * @return Price of the {@link Hardware}
     */
    public Offer[] getOffers() {
        return mOffers;
    }

    /**
     * Sets the mOffers of the {@link Hardware}
     *
     * @param offers Price of the {@link Hardware}
     */
    public void setOffers(Offer[] offers) {
        this.mOffers = offers;
    }


    /**
     * Returns the {@link Attribute} object for the current {@link Hardware}
     *
     * @return Attribute of {@link Hardware}
     */
    public Attribute getAttribute() {
        return mAttribute;
    }

    /**
     * Sets the {@link Attribute} object for current {@link Hardware}
     *
     * @param attribute {@link Attribute} of {@link Hardware}
     */
    public void setAttribute(Attribute attribute) {
        mAttribute = attribute;
    }


    /**
     * Returns the {@link Highlights}
     *
     * @return {@link String} for the {@link Hardware}
     */
    public SpannableStringBuilder getHighlightedData() {
        return mHighlighter.getHighlights(getAsLinkedHashMap(this, true));
    }


    /**
     * Sets the {@link Highlights}
     *
     * @param highlighter {@link Highlights} to apply
     */
    public void setHighlighter(Highlights highlighter) {
        mHighlighter = highlighter;
    }


    /**
     * Returns a {@link String} to send to dealer
     */
    public String getModelOverviewToSend() {
        return MyUtils.removeDuplicates(getCompanyName() + " " + getModel());
    }

}
