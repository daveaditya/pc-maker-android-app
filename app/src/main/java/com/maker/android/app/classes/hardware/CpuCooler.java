package com.maker.android.app.classes.hardware;

import android.os.Parcel;

import com.google.gson.JsonArray;
import com.google.gson.JsonDeserializationContext;
import com.google.gson.JsonDeserializer;
import com.google.gson.JsonElement;
import com.google.gson.JsonObject;
import com.google.gson.JsonParseException;
import com.maker.android.app.annotation.HardwareAnnotations;
import com.maker.android.app.classes.Attribute;
import com.maker.android.app.classes.Offer;
import com.maker.android.app.classes.SimpleHighlighter;
import com.maker.android.app.provider.PreserveContract;

import java.lang.reflect.Type;
import java.math.BigDecimal;


@SuppressWarnings({"unused", "WeakerAccess"})
class CpuCooler
        extends Hardware {

    /**
     * A {@link String} constant to use as tag in {@link android.util.Log}
     */
    private static final String LOG_TAG = CpuCooler.class.getSimpleName();

    public static Deserialize sDeserialize;

    static {
        sDeserialize = new Deserialize();
    }

    @HardwareAnnotations.DisplayData(name = "Type", displayInHighlights = true)
    private String mType;

    @HardwareAnnotations.DisplayData(name = "Minimum RPM", unit = "rpm")
    private int mMinRpm;

    @HardwareAnnotations.DisplayData(name = "Maximum RPM", unit = "rpm")
    private int mMaxRpm;

    @HardwareAnnotations.DisplayData(name = "Connector", displayInHighlights = true)
    private ConnectionInterface mConnector;

    @HardwareAnnotations.DisplayData(name = "Power Input", unit = "W")
    private BigDecimal mPowerInput;

    @HardwareAnnotations.DisplayData(name = "Radiator Length", unit = "mm")
    private BigDecimal mRadiatorLength;

    @HardwareAnnotations.DisplayData(name = "Air Cooler Height", unit = "mm")
    private BigDecimal mAirCoolerHeight;

    @HardwareAnnotations.DisplayData(name = "Supported Sockets")
    private Sockets[] mSupportedSockets;


    private CpuCooler(int id, String companyName, String model) {
        super(id, companyName, model);
        setHighlighter(new SimpleHighlighter());
    }


    CpuCooler(Parcel in) {
        super(in);
    }


    public static final Creator<CpuCooler> CREATOR = new Creator<CpuCooler>() {

        @Override
        public CpuCooler createFromParcel(Parcel in) {
            return new CpuCooler(in);
        }

        @Override
        public CpuCooler[] newArray(int size) {
            return new CpuCooler[size];
        }

    };


    @Override
    public int describeContents() {
        return 0;
    }


    @Override
    public PreserveContract.HardwareTypeEntry.HardwareType getHardwareType() {
        return PreserveContract.HardwareTypeEntry.HardwareType.CpuCooler;
    }

    public String getType() {
        return mType;
    }

    public void setType(String type) {
        mType = type;
    }

    public int getMinRpm() {
        return mMinRpm;
    }

    public void setMinRpm(int minRpm) {
        mMinRpm = minRpm;
    }

    public int getMaxRpm() {
        return mMaxRpm;
    }

    public void setMaxRpm(int maxRpm) {
        mMaxRpm = maxRpm;
    }

    public ConnectionInterface getConnector() {
        return mConnector;
    }

    public void setConnector(ConnectionInterface connector) {
        mConnector = connector;
    }

    public BigDecimal getPowerInput() {
        return mPowerInput;
    }

    public void setPowerInput(BigDecimal powerInput) {
        mPowerInput = powerInput;
    }

    public BigDecimal getRadiatorLength() {
        return mRadiatorLength;
    }

    public void setRadiatorLength(BigDecimal radiatorLength) {
        mRadiatorLength = radiatorLength;
    }

    public BigDecimal getAirCoolerHeight() {
        return mAirCoolerHeight;
    }

    public void setAirCoolerHeight(BigDecimal airCoolerHeight) {
        mAirCoolerHeight = airCoolerHeight;
    }

    public Sockets[] getSupportedSockets() {
        return mSupportedSockets;
    }

    public void setSupportedSockets(Sockets[] supportedSockets) {
        mSupportedSockets = supportedSockets;
    }


    interface Builder {

        Builder setType(String type);

        Builder setMinRpm(int minRpm);

        Builder setMaxRpm(int maxRpm);

        Builder setConnector(ConnectionInterface connector);

        Builder setPowerInput(BigDecimal powerInput);

        Builder setRadiatorLength(BigDecimal radiatorLength);

        Builder setAirCoolerHeight(BigDecimal airCoolerHeight);

        Builder setSupportedSockets(Sockets[] supportedSockets);

        Builder setAttribute(Attribute attribute);

        Builder setOffers(Offer[] offers);

        CpuCooler createCpuCooler();

    }


    private static class CpuCoolerBuilder implements Builder {

        private CpuCooler mCpuCooler;

        CpuCoolerBuilder(int id, String companyName, String model) {
            mCpuCooler = new CpuCooler(id, companyName, model);
        }

        @Override
        public Builder setType(String type) {
            return null;
        }

        @Override
        public Builder setMinRpm(int minRpm) {
            mCpuCooler.setMinRpm(minRpm);
            return this;
        }

        @Override
        public Builder setMaxRpm(int maxRpm) {
            mCpuCooler.setMaxRpm(maxRpm);
            return this;
        }

        @Override
        public Builder setConnector(ConnectionInterface connector) {
            mCpuCooler.setConnector(connector);
            return this;
        }

        @Override
        public Builder setPowerInput(BigDecimal powerInput) {
            mCpuCooler.setPowerInput(powerInput);
            return this;
        }

        @Override
        public Builder setRadiatorLength(BigDecimal radiatorLength) {
            mCpuCooler.setRadiatorLength(radiatorLength);
            return this;
        }

        @Override
        public Builder setAirCoolerHeight(BigDecimal airCoolerHeight) {
            mCpuCooler.setAirCoolerHeight(airCoolerHeight);
            return this;
        }

        @Override
        public Builder setSupportedSockets(Sockets[] supportedSockets) {
            mCpuCooler.setSupportedSockets(supportedSockets);
            return this;
        }

        @Override
        public Builder setAttribute(Attribute attribute) {
            mCpuCooler.setAttribute(attribute);
            return this;
        }

        @Override
        public Builder setOffers(Offer[] offers) {
            mCpuCooler.setOffers(offers);
            return this;
        }

        @Override
        public CpuCooler createCpuCooler() {
            return mCpuCooler;
        }

    }


    private static class Deserialize implements JsonDeserializer<CpuCooler> {

        @Override
        public CpuCooler deserialize(JsonElement json, Type typeOfT, JsonDeserializationContext context) throws JsonParseException {

            JsonObject jsonObject = json.getAsJsonObject();

            int cpuCoolerId = 0;
            String companyName = null, model = null;

            if (jsonObject.has("cpu_cooler_id") && !jsonObject.get("cpu_cooler_id").isJsonNull()) {
                cpuCoolerId = jsonObject.get("cpu_cooler_id").getAsInt();
            }

            if (jsonObject.has("company_name") && !jsonObject.get("company_name").isJsonNull()) {
                companyName = jsonObject.get("company_name").getAsString();
            }

            if (jsonObject.has("model") && !jsonObject.get("model").isJsonNull()) {
                model = jsonObject.get("model").getAsString();
            }

            Builder builder = new CpuCoolerBuilder(cpuCoolerId, companyName, model);

            if (jsonObject.has("type") && !jsonObject.get("type").isJsonNull()) {
                builder.setType((jsonObject.get("type").getAsString()));
            }

            if (jsonObject.has("min_rpm") && !jsonObject.get("min_rpm").isJsonNull()) {
                builder.setMinRpm(jsonObject.get("min_rpm").getAsInt());
            }

            if (jsonObject.has("max_rpm") && !jsonObject.get("max_rpm").isJsonNull()) {
                builder.setMaxRpm(jsonObject.get("max_rpm").getAsInt());
            }

            if (jsonObject.has("connector") && !jsonObject.get("connector").isJsonNull()) {
                builder.setConnector(ConnectionInterface.getEnumValue(jsonObject.get("connector").getAsString()));
            }

            if (jsonObject.has("pwr_input") && !jsonObject.get("pwr_input").isJsonNull()) {
                builder.setPowerInput(jsonObject.get("pwr_input").getAsBigDecimal());
            }

            if (jsonObject.has("radiator_length") && !jsonObject.get("radiator_length").isJsonNull()) {
                builder.setRadiatorLength(jsonObject.get("radiator_length").getAsBigDecimal());
            }

            if (jsonObject.has("air_cooler_height") && !jsonObject.get("air_cooler_height").isJsonNull()) {
                builder.setAirCoolerHeight(jsonObject.get("air_cooler_height").getAsBigDecimal());
            }

            if (jsonObject.has("supported_sockets") && !jsonObject.get("supported_sockets").isJsonNull()) {
                JsonArray jsonArray = jsonObject.getAsJsonArray("supported_sockets");
                Sockets[] supportedSockets = new Sockets[jsonArray.size()];
                for (int i = 0; i < jsonArray.size(); i++) {
                    supportedSockets[i] = Sockets.getEnumFromString(jsonArray.get(i).getAsString());
                }
                builder.setSupportedSockets(supportedSockets);
            }

            if (jsonObject.has("attributes") && !jsonObject.get("attributes").isJsonNull()) {
                builder.setAttribute(
                        Attribute.getAttributeFromJsonObject(jsonObject.get("attributes").getAsJsonObject())
                );
            }

            if (jsonObject.has("offers") && !jsonObject.get("offers").isJsonNull()) {
                JsonArray jsonArray = jsonObject.get("offers").getAsJsonArray();
                builder.setOffers(Offer.getOffersFromJsonArray(jsonArray));
            }

            return builder.createCpuCooler();
        }

    }

}
