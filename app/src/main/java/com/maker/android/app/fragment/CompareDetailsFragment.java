package com.maker.android.app.fragment;

import android.database.Cursor;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v4.app.LoaderManager;
import android.support.v4.content.CursorLoader;
import android.support.v4.content.Loader;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.maker.android.app.R;
import com.maker.android.app.activity.CompareActivity;
import com.maker.android.app.adapter.CompareDetailsAdapter;
import com.maker.android.app.classes.hardware.Hardware;
import com.maker.android.app.classes.hardware.HardwareFactory;
import com.maker.android.app.provider.PreserveContract;

import java.util.ArrayList;
import java.util.LinkedHashMap;


@SuppressWarnings({"unused"})
public class CompareDetailsFragment
        extends Fragment {

    /**
     * A String constant to use as tag in {@link android.util.Log}
     */
    private static final String LOG_TAG = CompareDetailsFragment.class.getSimpleName();

    private static final int COMPARE_DETAILS_LOADER = 88;

    String mSelectedComponent;
    RecyclerView mRecyclerViewSpecs;
    CompareDetailsAdapter mCompareDetailsAdapter;
    ArrayList<LinkedHashMap<String, Object>> mSpecHashMaps;

    public CompareDetailsFragment() {
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_compare_details, container, false);

        mSelectedComponent = getArguments().getString(CompareActivity.SELECTED_COMPONENT);
        mSpecHashMaps = new ArrayList<>();
        mRecyclerViewSpecs = (RecyclerView) view.findViewById(R.id.fragment_model_details_rv_speclist);

        getActivity()
                .getSupportLoaderManager()
                .restartLoader(COMPARE_DETAILS_LOADER, null, new AsyncCompareDataLoader())
                .forceLoad();

        return view;
    }

    @Override
    public void onCreateOptionsMenu(Menu menu, MenuInflater inflater) {
        super.onCreateOptionsMenu(menu, inflater);
        menu.findItem(R.id.activity_compare_menu_action_select_category).setVisible(true);
    }


    @Override
    public boolean onOptionsItemSelected(MenuItem item) {

        switch (item.getItemId()) {

            case R.id.activity_compare_menu_action_discard:

                getContext()
                        .getContentResolver()
                        .delete(
                                PreserveContract.CompareEntry.CONTENT_URI,
                                PreserveContract.CompareEntry.COLUMN_COMPARE_HW_TYPE + "=?",
                                new String[]{
                                        mSelectedComponent
                                }
                        );

                CompareListFragment compareListFragment = new CompareListFragment();
                compareListFragment.setHasOptionsMenu(true);

                getActivity()
                        .getSupportFragmentManager()
                        .beginTransaction()
                        .replace(R.id.activity_compare_fragment, compareListFragment)
                        .commit();
                return true;

            case R.id.activity_compare_menu_action_select_category:

                CompareListFragment compareListFragmentNew = new CompareListFragment();
                compareListFragmentNew.setHasOptionsMenu(true);

                getActivity()
                        .getSupportFragmentManager()
                        .beginTransaction()
                        .replace(R.id.activity_compare_fragment, compareListFragmentNew)
                        .commit();
                return true;

            default:
                return false;
        }

    }


    private final class AsyncCompareDataLoader implements LoaderManager.LoaderCallbacks<Cursor> {

        @Override
        public Loader<Cursor> onCreateLoader(int id, Bundle args) {
            return new CursorLoader(
                    getContext(),
                    PreserveContract.CompareEntry.CONTENT_URI,
                    null,
                    PreserveContract.CompareEntry.COLUMN_COMPARE_HW_TYPE + "=?",
                    new String[]{
                            mSelectedComponent
                    },
                    null
            );
        }

        @Override
        public void onLoadFinished(Loader<Cursor> loader, Cursor data) {

            while (data.moveToNext()) {
                String detailsJSON = data.getString(data.getColumnIndex(PreserveContract.CompareEntry.COLUMN_COMPARE_MODEL_DETAILS));

                try {
                    // Gets the class associated with the current Hardware Type Component
                    final Class clazz = HardwareFactory.getHardwareClass(mSelectedComponent);

                    // If null is returned throw new NullPointerException
                    if (clazz == null) {
                        throw new NullPointerException();
                    }

                    // Creates a GSON instance with specified TypeAdapter
                    Gson gson = new GsonBuilder().registerTypeAdapter(clazz, clazz.getDeclaredField("sDeserialize").get(null)).create();

                    Hardware hardware = gson.fromJson(detailsJSON, HardwareFactory.getHardwareTypeToken(mSelectedComponent));

                    mSpecHashMaps.add(Hardware.getAsLinkedHashMap(hardware, false));

                } catch (NoSuchFieldException | IllegalAccessException exc) {
                    exc.printStackTrace();
                }
            }

            mCompareDetailsAdapter = new CompareDetailsAdapter(getContext(), mSpecHashMaps);
            mRecyclerViewSpecs.setAdapter(mCompareDetailsAdapter);
            mRecyclerViewSpecs.setVisibility(View.VISIBLE);

        }

        @Override
        public void onLoaderReset(Loader<Cursor> loader) {
            mSpecHashMaps.clear();
            if (mCompareDetailsAdapter != null) {
                mCompareDetailsAdapter.notifyDataSetChanged();
            }
        }
    }

}