package com.maker.android.app.classes.hardware;


import android.os.Parcel;

import com.google.gson.JsonArray;
import com.google.gson.JsonDeserializationContext;
import com.google.gson.JsonDeserializer;
import com.google.gson.JsonElement;
import com.google.gson.JsonObject;
import com.google.gson.JsonParseException;
import com.maker.android.app.annotation.HardwareAnnotations;
import com.maker.android.app.classes.Attribute;
import com.maker.android.app.classes.Offer;
import com.maker.android.app.classes.SimpleHighlighter;
import com.maker.android.app.provider.PreserveContract;

import java.lang.reflect.Type;
import java.math.BigDecimal;


@SuppressWarnings({"unused", "WeakerAccess"})
class GraphicsCard
        extends Hardware {

    /**
     * A {@link String} constant to use as tag in {@link android.util.Log}
     */
    private static final String LOG_TAG = GraphicsCard.class.getSimpleName();

    public static Deserialize sDeserialize;

    static {
        sDeserialize = new Deserialize();
    }

    @HardwareAnnotations.DisplayData(name = "Base Clock", displayInHighlights = true, unit = "GHz")
    private BigDecimal mBaseClock;

    @HardwareAnnotations.DisplayData(name = "Boost Clock", unit = "GHz")
    private BigDecimal mBoostClock;

    @HardwareAnnotations.DisplayData(name = "Memory Size", displayInHighlights = true, unit = "GB")
    private BigDecimal mMemorySize;

    @HardwareAnnotations.DisplayData(name = "Memory Type", displayInHighlights = true)
    private ConnectionInterface mMemoryType;

    @HardwareAnnotations.DisplayData(name = "Memory Interface")
    private ConnectionInterface mMemoryInterface;

    @HardwareAnnotations.DisplayData(name = "Power Consumption")
    private int mPowerConsumption;

    @HardwareAnnotations.DisplayData(name = "Length", unit = "mm")
    private int mLength;

    @HardwareAnnotations.DisplayData(name = "Width", unit = "mm")
    private int mWidth;

    @HardwareAnnotations.DisplayData(name = "Height", unit = "mm")
    private int mHeight;

    @HardwareAnnotations.DisplayData(name = "Has HDMI?")
    private boolean mHasHdmi;

    @HardwareAnnotations.DisplayData(name = "Has DVI?")
    private boolean mHasDvi;

    @HardwareAnnotations.DisplayData(name = "Has Display Port?")
    private boolean mHasDisplayport;

    @HardwareAnnotations.DisplayData(name = "Has HDCP?")
    private boolean mHasHdcp;

    @HardwareAnnotations.DisplayData(name = "Is VR Ready?")
    private boolean mIsVrReady;

    @HardwareAnnotations.DisplayData(name = "FrameSync")
    private String mFrameSync;


    public static final Creator<GraphicsCard> CREATOR = new Creator<GraphicsCard>() {

        @Override
        public GraphicsCard createFromParcel(Parcel in) {
            return new GraphicsCard(in);
        }

        @Override
        public GraphicsCard[] newArray(int size) {
            return new GraphicsCard[size];
        }

    };


    private GraphicsCard(int id, String companyName, String model) {
        super(id, companyName, model);
        setHighlighter(new SimpleHighlighter());
    }


    GraphicsCard(Parcel in) {
        super(in);
    }


    @Override
    public int describeContents() {
        return 0;
    }


    @Override
    public PreserveContract.HardwareTypeEntry.HardwareType getHardwareType() {
        return PreserveContract.HardwareTypeEntry.HardwareType.GraphicsCard;
    }

    public BigDecimal getBaseClock() {
        return mBaseClock;
    }

    public void setBaseClock(BigDecimal baseClock) {
        mBaseClock = baseClock;
    }

    public BigDecimal getBoostClock() {
        return mBoostClock;
    }

    public void setBoostClock(BigDecimal boostClock) {
        mBoostClock = boostClock;
    }

    public BigDecimal getMemorySize() {
        return mMemorySize;
    }

    public void setMemorySize(BigDecimal memorySize) {
        mMemorySize = memorySize;
    }

    public ConnectionInterface getMemoryType() {
        return mMemoryType;
    }

    public void setMemoryType(ConnectionInterface memoryType) {
        mMemoryType = memoryType;
    }

    public ConnectionInterface getMemoryInterface() {
        return mMemoryInterface;
    }

    public void setMemoryInterface(ConnectionInterface memoryInterface) {
        mMemoryInterface = memoryInterface;
    }

    public int getPowerConsumption() {
        return mPowerConsumption;
    }

    public void setPowerConsumption(int powerConsumption) {
        mPowerConsumption = powerConsumption;
    }

    public int getLength() {
        return mLength;
    }

    public void setLength(int length) {
        mLength = length;
    }

    public int getWidth() {
        return mWidth;
    }

    public void setWidth(int width) {
        mWidth = width;
    }

    public int getHeight() {
        return mHeight;
    }

    public void setHeight(int height) {
        mHeight = height;
    }

    public boolean hasHdmi() {
        return mHasHdmi;
    }

    public void setHasHdmi(boolean hasHdmi) {
        mHasHdmi = hasHdmi;
    }

    public boolean hasDvi() {
        return mHasDvi;
    }

    public void setHasDvi(boolean hasDvi) {
        mHasDvi = hasDvi;
    }

    public boolean hasDisplayport() {
        return mHasDisplayport;
    }

    public void setHasDisplayport(boolean hasDisplayport) {
        mHasDisplayport = hasDisplayport;
    }

    public boolean hasHdcp() {
        return mHasHdcp;
    }

    public void setHasHdcp(boolean hasHdcp) {
        mHasHdcp = hasHdcp;
    }

    public boolean isVrReady() {
        return mIsVrReady;
    }

    public void setVrReady(boolean vrReady) {
        mIsVrReady = vrReady;
    }

    public String getFrameSync() {
        return mFrameSync;
    }

    public void setFrameSync(String frameSync) {
        mFrameSync = frameSync;
    }

    interface Builder {

        Builder setBaseClock(BigDecimal baseClock);

        Builder setBoostClock(BigDecimal boostClock);

        Builder setMemorySize(BigDecimal memorySize);

        Builder setMemoryType(ConnectionInterface memoryType);

        Builder setMemoryInterface(ConnectionInterface memoryInterface);

        Builder setPowerConsumption(int powerConsumption);

        Builder setLength(int length);

        Builder setWidth(int width);

        Builder setHeight(int height);

        Builder setHasHdmi(boolean hasHdmi);

        Builder setHasDvi(boolean hasDvi);

        Builder setHasDisplayPort(boolean hasDisplayPort);

        Builder setHasHdcp(boolean hasHdcp);

        Builder setVrReady(boolean isVrReady);

        Builder setFrameSync(String frameSync);

        Builder setAttributes(Attribute attribute);

        Builder setOffers(Offer[] offers);

        GraphicsCard createGraphicsCard();

    }


    private static class GraphicsCardBuilder implements Builder {

        private GraphicsCard mGraphicsCard;

        GraphicsCardBuilder(int id, String companyName, String model) {
            mGraphicsCard = new GraphicsCard(id, companyName, model);
        }

        @Override
        public Builder setBaseClock(BigDecimal baseClock) {
            mGraphicsCard.setBaseClock(baseClock);
            return this;
        }

        @Override
        public Builder setBoostClock(BigDecimal boostClock) {
            mGraphicsCard.setBoostClock(boostClock);
            return this;
        }

        @Override
        public Builder setMemorySize(BigDecimal memorySize) {
            mGraphicsCard.setMemorySize(memorySize);
            return this;
        }

        @Override
        public Builder setMemoryType(ConnectionInterface memoryType) {
            mGraphicsCard.setMemoryType(memoryType);
            return this;
        }

        @Override
        public Builder setMemoryInterface(ConnectionInterface memoryInterface) {
            mGraphicsCard.setMemoryInterface(memoryInterface);
            return this;
        }

        @Override
        public Builder setPowerConsumption(int powerConsumption) {
            mGraphicsCard.setPowerConsumption(powerConsumption);
            return this;
        }

        @Override
        public Builder setLength(int length) {
            mGraphicsCard.setLength(length);
            return this;
        }

        @Override
        public Builder setWidth(int width) {
            mGraphicsCard.setWidth(width);
            return this;
        }

        @Override
        public Builder setHeight(int height) {
            mGraphicsCard.setHeight(height);
            return this;
        }

        @Override
        public Builder setHasHdmi(boolean hasHdmi) {
            mGraphicsCard.setHasHdmi(hasHdmi);
            return this;
        }

        @Override
        public Builder setHasDvi(boolean hasDvi) {
            mGraphicsCard.setHasDvi(hasDvi);
            return this;
        }

        @Override
        public Builder setHasDisplayPort(boolean hasDisplayPort) {
            mGraphicsCard.setHasDisplayport(hasDisplayPort);
            return this;
        }

        @Override
        public Builder setHasHdcp(boolean hasHdcp) {
            mGraphicsCard.setHasHdcp(hasHdcp);
            return this;
        }

        @Override
        public Builder setVrReady(boolean isVrReady) {
            mGraphicsCard.setVrReady(isVrReady);
            return this;
        }

        @Override
        public Builder setFrameSync(String frameSync) {
            mGraphicsCard.setFrameSync(frameSync);
            return this;
        }

        @Override
        public Builder setAttributes(Attribute attribute) {
            mGraphicsCard.setAttribute(attribute);
            return this;
        }

        @Override
        public Builder setOffers(Offer[] offers) {
            mGraphicsCard.setOffers(offers);
            return this;
        }

        @Override
        public GraphicsCard createGraphicsCard() {
            return mGraphicsCard;
        }
    }



    private static class Deserialize implements JsonDeserializer<GraphicsCard> {

        @Override
        public GraphicsCard deserialize(JsonElement json, Type typeOfT, JsonDeserializationContext context) throws JsonParseException {

            JsonObject jsonObject = json.getAsJsonObject();

            int graphicsCardId = 0;
            String companyName = null, model = null;

            if (jsonObject.has("graphics_card_id") && !jsonObject.get("graphics_card_id").isJsonNull()) {
                graphicsCardId = jsonObject.get("graphics_card_id").getAsInt();
            }

            if (jsonObject.has("company_name") && !jsonObject.get("company_name").isJsonNull()) {
                companyName = jsonObject.get("company_name").getAsString();
            }

            if (jsonObject.has("model") && !jsonObject.get("model").isJsonNull()) {
                model = jsonObject.get("model").getAsString();
            }

            Builder builder = new GraphicsCardBuilder(graphicsCardId, companyName, model);

            if (jsonObject.has("base_clock") && !jsonObject.get("base_clock").isJsonNull()) {
                builder.setBaseClock(jsonObject.get("base_clock").getAsBigDecimal());
            }

            if (jsonObject.has("boost_clock") && !jsonObject.get("boost_clock").isJsonNull()) {
                builder.setBoostClock(jsonObject.get("boost_clock").getAsBigDecimal());
            }

            if (jsonObject.has("memory_size") && !jsonObject.get("memory_size").isJsonNull()) {
                builder.setMemorySize(jsonObject.get("memory_size").getAsBigDecimal());
            }

            if (jsonObject.has("memory_type") && !jsonObject.get("memory_type").isJsonNull()) {
                builder.setMemoryType(ConnectionInterface.getEnumValue(jsonObject.get("memory_type").getAsString()));
            }

            if (jsonObject.has("memory_interface") && !jsonObject.get("memory_interface").isJsonNull()) {
                builder.setMemoryInterface(ConnectionInterface.getEnumValue(jsonObject.get("memory_interface").getAsString()));
            }

            if (jsonObject.has("power_consumption") && !jsonObject.get("power_consumption").isJsonNull()) {
                builder.setPowerConsumption(jsonObject.get("power_consumption").getAsInt());
            }

            if (jsonObject.has("length") && !jsonObject.get("length").isJsonNull()) {
                builder.setLength(jsonObject.get("length").getAsInt());
            }

            if (jsonObject.has("width") && !jsonObject.get("width").isJsonNull()) {
                builder.setWidth(jsonObject.get("width").getAsInt());
            }

            if (jsonObject.has("height") && !jsonObject.get("height").isJsonNull()) {
                builder.setHeight(jsonObject.get("height").getAsInt());
            }

            if (jsonObject.has("has_hdmi") && !jsonObject.get("has_hdmi").isJsonNull()) {
                builder.setHasHdmi(jsonObject.get("has_hdmi").getAsBoolean());
            }

            if (jsonObject.has("has_dvi") && !jsonObject.get("has_dvi").isJsonNull()) {
                builder.setHasDvi(jsonObject.get("has_dvi").getAsBoolean());
            }

            if (jsonObject.has("has_displayport") && !jsonObject.get("has_displayport").isJsonNull()) {
                builder.setHasDisplayPort(jsonObject.get("has_displayport").getAsBoolean());
            }

            if (jsonObject.has("has_hdcp") && !jsonObject.get("has_hdcp").isJsonNull()) {
                builder.setHasHdcp(jsonObject.get("has_hdcp").getAsBoolean());
            }
            if (jsonObject.has("is_vr_ready") && !jsonObject.get("is_vr_ready").isJsonNull()) {
                builder.setVrReady(jsonObject.get("is_vr_ready").getAsBoolean());
            }
            if (jsonObject.has("frame_sync") && !jsonObject.get("frame_sync").isJsonNull()) {
                builder.setFrameSync(jsonObject.get("frame_sync").getAsString());
            }

            if (jsonObject.has("attributes") && !jsonObject.get("attributes").isJsonNull()) {
                builder.setAttributes(
                        Attribute.getAttributeFromJsonObject(jsonObject.get("attributes").getAsJsonObject())
                );
            }

            if (jsonObject.has("offers") && !jsonObject.get("offers").isJsonNull()) {
                JsonArray jsonArray = jsonObject.get("offers").getAsJsonArray();
                builder.setOffers(Offer.getOffersFromJsonArray(jsonArray));
            }

            return builder.createGraphicsCard();

        }

    }

}
