package com.maker.android.app.fragment;

import android.content.Context;
import android.os.Bundle;
import android.support.annotation.IdRes;
import android.support.annotation.Nullable;
import android.support.v4.app.DialogFragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.RadioGroup;

import com.maker.android.app.R;
import com.maker.android.app.adapter.CatalogueAdapter;
import com.maker.android.app.utils.Request;
import com.maker.android.app.utils.Request.RequestBuilder;


@SuppressWarnings({"unused"})
public class ProcessorDialogFragment extends DialogFragment {

    /**
     * A {@link String} constant to use as tag in {@link android.util.Log}
     */
    private static final String LOG_TAG = ProcessorDialogFragment.class.getSimpleName();

    private CatalogueAdapter.CatalogueClickListener.OpenModelListActivity mOpenModelListActivity;

    public ProcessorDialogFragment() {
    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);

        try {
            mOpenModelListActivity = (CatalogueAdapter.CatalogueClickListener.OpenModelListActivity) getActivity();
        } catch (ClassCastException exc) {
            exc.printStackTrace();
        }

    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {

        View view = inflater.inflate(R.layout.fragment_dialog_processor, container, false);

        RadioGroup radioGroupCompany = (RadioGroup) view.findViewById(R.id.fragment_processor_dialog_rbgrp_company);
        radioGroupCompany.setOnCheckedChangeListener(new RadioGroup.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(RadioGroup group, @IdRes int checkedId) {
                String url = "";
                String title = "";
                String endPoint = "";
                switch (checkedId) {
                    case R.id.fragment_processor_dialog_rb_intel:
                        url = getResources().getString(R.string.url_base_for_multi_item_request,
                                "intel_processors");
                        title = "Intel Processor";
                        endPoint = "intel_processors";
                        dismiss();
                        break;

                    case R.id.fragment_processor_dialog_rb_amd:
                        url = getResources().getString(R.string.url_base_for_multi_item_request,
                                "amd_processors");
                        title = "AMD Processor";
                        endPoint = "amd_processors";
                        dismiss();
                        break;
                }

                // Creates a new Request object to access RESTful service
                Request hardwareRequest = new RequestBuilder()
                        .setURL(url)
                        .setHttpType(Request.HttpRequestTypes.GET)
                        .createRequest();
                mOpenModelListActivity.modelListActivityStarter(hardwareRequest, title, endPoint);

            }
        });


        Button btnCancel = (Button) view.findViewById(R.id.fragment_dialog_processor_btn_cancel);
        btnCancel.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dismiss();
            }
        });

        getDialog().setCanceledOnTouchOutside(false);
        return view;

    }

}
