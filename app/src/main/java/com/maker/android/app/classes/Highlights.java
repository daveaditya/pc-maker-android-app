package com.maker.android.app.classes;

import android.text.SpannableStringBuilder;

import java.util.LinkedHashMap;


/**
 * Denotes that the implementing class has some detail to show
 */
public interface Highlights {

    /**
     * Returns the highlighted data gor the {@link CatalogueItem}
     *
     * @return {@link String} containing highlight data
     */
    SpannableStringBuilder getHighlights(LinkedHashMap<String, Object> data);

}
