package com.maker.android.app.classes.hardware;


import android.os.Parcel;

import com.google.gson.JsonArray;
import com.google.gson.JsonDeserializationContext;
import com.google.gson.JsonDeserializer;
import com.google.gson.JsonElement;
import com.google.gson.JsonObject;
import com.google.gson.JsonParseException;
import com.maker.android.app.annotation.HardwareAnnotations;
import com.maker.android.app.classes.Attribute;
import com.maker.android.app.classes.Offer;
import com.maker.android.app.classes.SimpleHighlighter;
import com.maker.android.app.provider.PreserveContract;

import java.lang.reflect.Type;


@SuppressWarnings({"unused", "WeakerAccess"})
class Headphone
        extends Hardware {

    /**
     * A {@link String} constant to use as tag in {@link android.util.Log}
     */
    private static final String LOG_TAG = Headphone.class.getSimpleName();


    public static Deserialize sDeserialize;


    static {
        sDeserialize = new Deserialize();
    }


    @HardwareAnnotations.DisplayData(name = "Medium", displayInHighlights = true)
    private String mMedium;

    @HardwareAnnotations.DisplayData(name = "Type", displayInHighlights = true)
    private String mType;

    @HardwareAnnotations.DisplayData(name = "Frequency Range")
    private String mFrequencyRange;

    @HardwareAnnotations.DisplayData(name = "Driver Unit", unit = "mm")
    private String mDriverUnit;

    @HardwareAnnotations.DisplayData(name = "Impedance", unit = "\u03A9")
    private String mImpedance;

    @HardwareAnnotations.DisplayData(name = "Has Microphone?")
    private boolean mHasMicrophone;

    @HardwareAnnotations.DisplayData(name = "Connector")
    private ConnectionInterface mConnector;


    private Headphone(int id, String companyName, String model) {
        super(id, companyName, model);
        setHighlighter(new SimpleHighlighter());
    }


    public static final Creator<Headphone> CREATOR = new Creator<Headphone>() {

        @Override
        public Headphone createFromParcel(Parcel in) {
            return new Headphone(in);
        }


        @Override
        public Headphone[] newArray(int size) {
            return new Headphone[size];
        }

    };


    Headphone(Parcel in) {
        super(in);
    }


    @Override
    public int describeContents() {
        return 0;
    }


    @Override
    public PreserveContract.HardwareTypeEntry.HardwareType getHardwareType() {
        return PreserveContract.HardwareTypeEntry.HardwareType.Headphone;
    }

    public String getMedium() {
        return mMedium;
    }

    public void setMedium(String medium) {
        mMedium = medium;
    }

    public String getType() {
        return mType;
    }

    public void setType(String type) {
        mType = type;
    }

    public String getFrequencyRange() {
        return mFrequencyRange;
    }

    public void setFrequencyRange(String frequencyRange) {
        mFrequencyRange = frequencyRange;
    }

    public String getDriverUnit() {
        return mDriverUnit;
    }

    public void setDriverUnit(String driverUnit) {
        mDriverUnit = driverUnit;
    }

    public String getImpedance() {
        return mImpedance;
    }

    public void setImpedance(String impedance) {
        mImpedance = impedance;
    }

    public boolean hasMicrophone() {
        return mHasMicrophone;
    }

    public void setHasMicrophone(boolean hasMicrophone) {
        mHasMicrophone = hasMicrophone;
    }

    public ConnectionInterface getConnector() {
        return mConnector;
    }

    public void setConnector(ConnectionInterface connector) {
        mConnector = connector;
    }


    interface Builder {

        Builder setMedium(String medium);

        Builder setType(String type);

        Builder setFrequencyRange(String frequencyRange);

        Builder setDriverUnit(String driverUnit);

        Builder setImpedance(String impedance);

        Builder setHasMicrophone(boolean hasMicrophone);

        Builder setConnector(ConnectionInterface connector);

        Builder setAttributes(Attribute attribute);

        Builder setOffers(Offer[] offers);

        Headphone createHeadphone();

    }


    private static class HeadphoneBuilder implements Builder {

        Headphone mHeadphone;

        HeadphoneBuilder(int id, String companyName, String model) {
            mHeadphone = new Headphone(id, companyName, model);
        }

        @Override
        public Builder setMedium(String medium) {
            mHeadphone.setMedium(medium);
            return this;
        }

        @Override
        public Builder setType(String type) {
            mHeadphone.setType(type);
            return this;
        }

        @Override
        public Builder setFrequencyRange(String frequencyRange) {
            mHeadphone.setFrequencyRange(frequencyRange);
            return this;
        }

        @Override
        public Builder setDriverUnit(String driverUnit) {
            mHeadphone.setDriverUnit(driverUnit);
            return this;
        }

        @Override
        public Builder setImpedance(String impedance) {
            mHeadphone.setImpedance(impedance);
            return this;
        }

        @Override
        public Builder setHasMicrophone(boolean hasMicrophone) {
            mHeadphone.setHasMicrophone(hasMicrophone);
            return this;
        }

        @Override
        public Builder setConnector(ConnectionInterface connector) {
            mHeadphone.setConnector(connector);
            return this;
        }

        @Override
        public Builder setAttributes(Attribute attribute) {
            mHeadphone.setAttribute(attribute);
            return this;
        }

        @Override
        public Builder setOffers(Offer[] offers) {
            mHeadphone.setOffers(offers);
            return this;
        }

        @Override
        public Headphone createHeadphone() {
            return mHeadphone;
        }

    }


    private static class Deserialize implements JsonDeserializer<Headphone> {

        @Override
        public Headphone deserialize(JsonElement json, Type typeOfT, JsonDeserializationContext context) throws JsonParseException {

            JsonObject jsonObject = json.getAsJsonObject();

            int headphoneId = 0;
            String companyName = null, model = null;

            if (jsonObject.has("headphone_id") && !jsonObject.get("headphone_id").isJsonNull()) {
                headphoneId = jsonObject.get("headphone_id").getAsInt();
            }

            if (jsonObject.has("company_name") && !jsonObject.get("company_name").isJsonNull()) {
                companyName = jsonObject.get("company_name").getAsString();
            }

            if (jsonObject.has("model") && !jsonObject.get("model").isJsonNull()) {
                model = jsonObject.get("model").getAsString();
            }

            Builder builder = new HeadphoneBuilder(headphoneId, companyName, model);

            if (jsonObject.has("medium") && !jsonObject.get("medium").isJsonNull()) {
                builder.setMedium(jsonObject.get("medium").getAsString());
            }

            if (jsonObject.has("type") && !jsonObject.get("type").isJsonNull()) {
                builder.setType(jsonObject.get("type").getAsString());
            }

            if (jsonObject.has("freq_range") && !jsonObject.get("freq_range").isJsonNull()) {
                builder.setFrequencyRange(jsonObject.get("freq_range").getAsString());
            }

            if (jsonObject.has("driver_unit") && !jsonObject.get("driver_unit").isJsonNull()) {
                builder.setDriverUnit(jsonObject.get("driver_unit").getAsString());
            }

            if (jsonObject.has("impedance") && !jsonObject.get("impedance").isJsonNull()) {
                builder.setImpedance(jsonObject.get("impedance").getAsString());
            }

            if (jsonObject.has("microphone") && !jsonObject.get("microphone").isJsonNull()) {
                builder.setHasMicrophone(jsonObject.get("microphone").getAsBoolean());
            }

            if (jsonObject.has("connector") && !jsonObject.get("connector").isJsonNull()) {
                builder.setConnector(ConnectionInterface.getEnumValue(jsonObject.get("connector").getAsString()));
            }

            if (jsonObject.has("attributes") && !jsonObject.get("attributes").isJsonNull()) {
                builder.setAttributes(
                        Attribute.getAttributeFromJsonObject(jsonObject.get("attributes").getAsJsonObject())
                );
            }

            if (jsonObject.has("offers") && !jsonObject.get("offers").isJsonNull()) {
                JsonArray jsonArray = jsonObject.get("offers").getAsJsonArray();
                builder.setOffers(Offer.getOffersFromJsonArray(jsonArray));
            }

            return builder.createHeadphone();

        }

    }

}
