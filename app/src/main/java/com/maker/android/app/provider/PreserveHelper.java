package com.maker.android.app.provider;

import android.content.Context;
import android.database.sqlite.SQLiteCantOpenDatabaseException;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;


@SuppressWarnings({"unused"})
class PreserveHelper extends SQLiteOpenHelper {

    /**
     * A {@link String} constant to use as tag in {@link android.util.Log}
     */
    private static final String LOG_TAG = PreserveHelper.class.getSimpleName();

    private static String DB_NAME = "preserve.db";
    //The Android's default system path of your application database.
    private static String DB_PATH;
    private final Context myContext;


    /**
     * Constructor
     * Takes and keeps a reference of the passed context in order to access to the application assets and resources.
     */
    PreserveHelper(Context context) throws IOException {
        super(context, DB_NAME, null, 1);
        this.myContext = context;
        DB_PATH = myContext.getDatabasePath(DB_NAME).toString();
        createDataBase();
    }


    /**
     * Creates a empty database on the system and rewrites it with your own database.
     */
    private void createDataBase() throws IOException {

        if (!checkDataBase()) {
            //By calling this method an empty database will be created into the default system path
            //of your application so we are gonna be able to overwrite that database with our database.
            this.getReadableDatabase();
            copyDataBase();
        }

    }


    /**
     * Check if the database already exist to avoid re-copying the file each time you open the application.
     *
     * @return true if it exists, false if it doesn't
     */
    private boolean checkDataBase() {

        File file = new File(DB_PATH);
        if (file.exists() && !file.isDirectory()) {
            try (SQLiteDatabase checkDB =
                         SQLiteDatabase.openDatabase(DB_PATH, null, SQLiteDatabase.OPEN_READONLY)) {

                return checkDB != null;

            } catch (SQLiteCantOpenDatabaseException exc) {
                //database does't exist yet.
                return false;
            }
        }
        return false;

    }


    /**
     * Copies your database from your local assets-folder to the just created empty database in the
     * system folder, from where it can be accessed and handled.
     * This is done by transferring ByteStream.
     */
    private void copyDataBase() throws IOException {

        //Open your local db as the input stream
        try (InputStream myInput = myContext.getAssets().open(DB_NAME);

             //Open the empty db as the output stream
             OutputStream myOutput = new FileOutputStream(DB_PATH)) {

            //transfer bytes from the input file to the output file
            byte[] buffer = new byte[1024];
            int length;
            while ((length = myInput.read(buffer)) > 0) {
                myOutput.write(buffer, 0, length);
            }

        } catch (IOException exc) {
            exc.printStackTrace();
        }

    }


    @Override
    public synchronized void close() {
        super.close();
    }


    @Override
    public void onCreate(SQLiteDatabase db) {

    }


    @Override
    public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {

    }

    // Add your public helper methods to access and get content from the database.
    // You could return cursors by doing "return myDataBase.query(....)" so it'd be easy
    // to you to create adapters for your views.

}
