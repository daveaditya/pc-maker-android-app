package com.maker.android.app.classes.hardware;

import android.os.Parcel;

import com.google.gson.JsonArray;
import com.google.gson.JsonDeserializationContext;
import com.google.gson.JsonDeserializer;
import com.google.gson.JsonElement;
import com.google.gson.JsonObject;
import com.google.gson.JsonParseException;
import com.maker.android.app.annotation.HardwareAnnotations;
import com.maker.android.app.classes.Attribute;
import com.maker.android.app.classes.Offer;
import com.maker.android.app.classes.SimpleHighlighter;
import com.maker.android.app.provider.PreserveContract;

import java.lang.reflect.Type;


@SuppressWarnings({"unused", "WeakerAccess"})
class KeyboardMouse
        extends Hardware {

    /**
     * A {@link String} constant to use as tag in {@link android.util.Log}
     */
    private static final String LOG_TAG = KeyboardMouse.class.getSimpleName();

    public static Deserialize sDeserialize;

    static {
        sDeserialize = new Deserialize();
    }

    @HardwareAnnotations.DisplayData(name = "Color")
    private String mColor;

    @HardwareAnnotations.DisplayData(name = "Included Devices", displayInHighlights = true)
    private String mIncludedDevices;

    @HardwareAnnotations.DisplayData(name = "Connectivity", displayInHighlights = true)
    private ConnectionInterface mConnectivity;

    @HardwareAnnotations.DisplayData(name = "Medium", displayInHighlights = true)
    private String mMedium;

    @HardwareAnnotations.DisplayData(name = "Supported OS")
    private String mSupportedOs;


    public static final Creator<GraphicsCard> CREATOR = new Creator<GraphicsCard>() {

        @Override
        public GraphicsCard createFromParcel(Parcel in) {
            return new GraphicsCard(in);
        }


        @Override
        public GraphicsCard[] newArray(int size) {
            return new GraphicsCard[size];
        }

    };


    private KeyboardMouse(int id, String companyName, String model) {
        super(id, companyName, model);
        setHighlighter(new SimpleHighlighter());
    }


    KeyboardMouse(Parcel in) {
        super(in);
    }


    @Override
    public int describeContents() {
        return 0;
    }


    @Override
    public PreserveContract.HardwareTypeEntry.HardwareType getHardwareType() {
        return PreserveContract.HardwareTypeEntry.HardwareType.KeyboardMouse;
    }


    public String getColor() {
        return mColor;
    }

    public void setColor(String color) {
        mColor = color;
    }

    public String getIncludedDevices() {
        return mIncludedDevices;
    }

    public void setIncludedDevices(String includedDevices) {
        mIncludedDevices = includedDevices;
    }

    public ConnectionInterface getConnectivity() {
        return mConnectivity;
    }

    public void setConnectivity(ConnectionInterface connectivity) {
        mConnectivity = connectivity;
    }

    public String getMedium() {
        return mMedium;
    }

    public void setMedium(String medium) {
        mMedium = medium;
    }

    public String getSupportedOs() {
        return mSupportedOs;
    }

    public void setSupportedOs(String supportedOs) {
        mSupportedOs = supportedOs;
    }


    interface Builder {

        Builder setColor(String color);

        Builder setIncludedDevices(String includedDevices);

        Builder setConnectivity(ConnectionInterface connectivity);

        Builder setMedium(String medium);

        Builder setSupportedOs(String supportedOs);

        Builder setAttributes(Attribute attribute);

        Builder setOffers(Offer[] offers);

        KeyboardMouse createKeyboardMouse();

    }


    private static class KeyboardMouseBuilder implements Builder {

        KeyboardMouse mKeyboardMouse;

        KeyboardMouseBuilder(int id, String companyName, String model) {
            mKeyboardMouse = new KeyboardMouse(id, companyName, model);
        }

        @Override
        public Builder setColor(String color) {
            mKeyboardMouse.setColor(color);
            return this;
        }

        @Override
        public Builder setIncludedDevices(String includedDevices) {
            mKeyboardMouse.setIncludedDevices(includedDevices);
            return this;
        }

        @Override
        public Builder setConnectivity(ConnectionInterface connectivity) {
            mKeyboardMouse.setConnectivity(connectivity);
            return this;
        }

        @Override
        public Builder setMedium(String medium) {
            mKeyboardMouse.setMedium(medium);
            return this;
        }

        @Override
        public Builder setSupportedOs(String supportedOs) {
            mKeyboardMouse.setSupportedOs(supportedOs);
            return this;
        }

        @Override
        public Builder setAttributes(Attribute attribute) {
            mKeyboardMouse.setAttribute(attribute);
            return this;
        }

        @Override
        public Builder setOffers(Offer[] offers) {
            mKeyboardMouse.setOffers(offers);
            return this;
        }

        @Override
        public KeyboardMouse createKeyboardMouse() {
            return mKeyboardMouse;
        }

    }


    private static class Deserialize implements JsonDeserializer<KeyboardMouse> {

        @Override
        public KeyboardMouse deserialize(JsonElement json, Type typeOfT, JsonDeserializationContext context) throws JsonParseException {

            JsonObject jsonObject = json.getAsJsonObject();

            int keyboardMouseId = 0;
            String companyName = null, model = null;

            if (jsonObject.has("key_mouse_id") && !jsonObject.get("key_mouse_id").isJsonNull()) {
                keyboardMouseId = jsonObject.get("key_mouse_id").getAsInt();
            }

            if (jsonObject.has("company_name") && !jsonObject.get("company_name").isJsonNull()) {
                companyName = jsonObject.get("company_name").getAsString();
            }

            if (jsonObject.has("model") && !jsonObject.get("model").isJsonNull()) {
                model = jsonObject.get("model").getAsString();
            }

            Builder builder = new KeyboardMouseBuilder(keyboardMouseId, companyName, model);

            if (jsonObject.has("color")) {
                if (jsonObject.get("color").isJsonNull()) {
                    builder.setColor("N/A");
                } else {
                    builder.setColor(jsonObject.get("color").getAsString());
                }
            }

            if (jsonObject.has("included_devices") && !jsonObject.get("included_devices").isJsonNull()) {
                builder.setIncludedDevices(jsonObject.get("included_devices").getAsString());
            }

            if (jsonObject.has("connectivity") && !jsonObject.get("connectivity").isJsonNull()) {
                builder.setConnectivity(ConnectionInterface.getEnumValue(jsonObject.get("connectivity").getAsString()));
            }

            if (jsonObject.has("medium") && !jsonObject.get("medium").isJsonNull()) {
                builder.setMedium(jsonObject.get("medium").getAsString());
            }

            if (jsonObject.has("supported_os") && !jsonObject.get("supported_os").isJsonNull()) {
                builder.setSupportedOs(jsonObject.get("supported_os").getAsString());
            }

            if (jsonObject.has("attributes") && !jsonObject.get("attributes").isJsonNull()) {
                builder.setAttributes(
                        Attribute.getAttributeFromJsonObject(jsonObject.get("attributes").getAsJsonObject())
                );
            }

            if (jsonObject.has("offers") && !jsonObject.get("offers").isJsonNull()) {
                JsonArray jsonArray = jsonObject.get("offers").getAsJsonArray();
                builder.setOffers(Offer.getOffersFromJsonArray(jsonArray));
            }

            return builder.createKeyboardMouse();

        }

    }

}
