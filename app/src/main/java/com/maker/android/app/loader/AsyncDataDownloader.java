package com.maker.android.app.loader;

import android.content.Context;
import android.support.v4.content.AsyncTaskLoader;
import android.util.Log;

import com.maker.android.app.utils.DownloaderUtils;
import com.maker.android.app.utils.Request;


/**
 * An {@link AsyncTaskLoader<String>} to fetch data from WebServer in background
 */
@SuppressWarnings({"unused"})
public class AsyncDataDownloader extends AsyncTaskLoader<String> {

    /**
     * A {@link String} constant to use as TAG in {@link Log}
     */
    private static final String LOG_TAG = AsyncDataDownloader.class.getSimpleName();

    /**
     * Holds the current {@link Request}
     */
    private Request mRequest;


    /**
     * Constructs an {@link AsyncDataDownloader}
     *
     * @param context Context of the Activity
     * @param request {@link Request} object containing connection details
     */
    public AsyncDataDownloader(Context context, Request request) {
        super(context);
        this.mRequest = request;
    }


    /**
     * Works in the background thread
     *
     * @return JSON data fetched from the {@link Request} destination
     */
    @Override
    public String loadInBackground() {
        try {
            // Based on the type of request perform GET, PUT, POST or DELETE
            return DownloaderUtils.performNetworkRequest(mRequest);
        } catch (Exception exc) {
            exc.printStackTrace();
            return null;
        }
    }

}
