package com.maker.android.app.activity;

import android.content.Intent;
import android.os.Bundle;
import android.os.Handler;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;

import com.maker.android.app.R;


@SuppressWarnings({"unused", "FieldCanBeLocal"})
public class SplashActivity extends AppCompatActivity {

    /**
     * A {@link String} constant to use as TAG in {@link Log}
     */
    private static final String LOG_TAG = SplashActivity.class.getSimpleName();

    /**
     * Timeout for the {@link SplashActivity}
     */
    private static int SPLASH_TIMEOUT = 200;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_splash);

        new Handler().postDelayed(
                new Runnable() {
                    @Override
                    public void run() {

                        // Creates an Intent with target as StartActivity
                        Intent start = new Intent(getBaseContext(), StartActivity.class);

                        // Goto StartActivity after initializations
                        startActivity(start);

                        // End the SplashActivity
                        finish();

                    }
                }, SPLASH_TIMEOUT
        );

    }

}
