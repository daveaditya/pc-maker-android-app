package com.maker.android.app.classes.hardware;

import android.os.Parcel;

import com.google.gson.JsonArray;
import com.google.gson.JsonDeserializationContext;
import com.google.gson.JsonDeserializer;
import com.google.gson.JsonElement;
import com.google.gson.JsonObject;
import com.google.gson.JsonParseException;
import com.maker.android.app.annotation.HardwareAnnotations;
import com.maker.android.app.classes.Attribute;
import com.maker.android.app.classes.Offer;
import com.maker.android.app.classes.SimpleHighlighter;
import com.maker.android.app.provider.PreserveContract;

import java.lang.reflect.Type;


@SuppressWarnings({"unused", "WeakerAccess"})
class MousePad
        extends Hardware {

    /**
     * A {@link String} constant to use as tag for {@link android.util.Log}
     */
    private static final String LOG_TAG = MousePad.class.getSimpleName();

    public static Deserialize sDeserialize;

    static {
        sDeserialize = new Deserialize();
    }

    @HardwareAnnotations.DisplayData(name = "Type", displayInHighlights = true)
    private String mType;

    @HardwareAnnotations.DisplayData(name = "Dimensions", unit = "mm")
    private String mDimensions;

    @HardwareAnnotations.DisplayData(name = "Material", displayInHighlights = true)
    private String mMaterial;

    @HardwareAnnotations.DisplayData(name = "Base")
    private String mBase;

    public static final Creator<MousePad> CREATOR = new Creator<MousePad>() {

        @Override
        public MousePad createFromParcel(Parcel in) {
            return new MousePad(in);
        }


        @Override
        public MousePad[] newArray(int size) {
            return new MousePad[size];
        }

    };


    private MousePad(int id, String companyName, String model) {
        super(id, companyName, model);
        setHighlighter(new SimpleHighlighter());
    }


    MousePad(Parcel in) {
        super(in);
    }


    @Override
    public int describeContents() {
        return 0;
    }


    @Override
    public PreserveContract.HardwareTypeEntry.HardwareType getHardwareType() {
        return PreserveContract.HardwareTypeEntry.HardwareType.MousePad;
    }

    public String getType() {
        return mType;
    }

    public void setType(String type) {
        mType = type;
    }

    public String getDimensions() {
        return mDimensions;
    }

    public void setDimensions(String dimensions) {
        mDimensions = dimensions;
    }

    public String getMaterial() {
        return mMaterial;
    }

    public void setMaterial(String material) {
        mMaterial = material;
    }

    public String getBase() {
        return mBase;
    }

    public void setBase(String base) {
        mBase = base;
    }

    interface Builder {

        Builder setType(String type);

        Builder setDimensions(String dimensions);

        Builder setMaterial(String material);

        Builder setBase(String base);

        Builder setAttributes(Attribute attributes);

        Builder setOffers(Offer[] offers);

        MousePad createMousePad();

    }


    private static class MousePadBuilder implements Builder {

        MousePad mMousePad;

        MousePadBuilder(int id, String companyName, String model) {
            mMousePad = new MousePad(id, companyName, model);
        }

        @Override
        public Builder setType(String type) {
            mMousePad.setType(type);
            return this;
        }

        @Override
        public Builder setDimensions(String dimensions) {
            mMousePad.setDimensions(dimensions);
            return this;
        }

        @Override
        public Builder setMaterial(String material) {
            mMousePad.setMaterial(material);
            return this;
        }

        @Override
        public Builder setBase(String base) {
            mMousePad.setBase(base);
            return this;
        }

        @Override
        public Builder setAttributes(Attribute attributes) {
            mMousePad.setAttribute(attributes);
            return this;
        }

        @Override
        public Builder setOffers(Offer[] offers) {
            mMousePad.setOffers(offers);
            return this;
        }

        @Override
        public MousePad createMousePad() {
            return mMousePad;
        }

    }


    private static class Deserialize implements JsonDeserializer<MousePad> {

        @Override
        public MousePad deserialize(JsonElement json, Type typeOfT, JsonDeserializationContext context) throws JsonParseException {

            JsonObject jsonObject = json.getAsJsonObject();

            int mousePadId = 0;
            String companyName = null, model = null;

            if (jsonObject.has("mouse_pad_id") && !jsonObject.get("mouse_pad_id").isJsonNull()) {
                mousePadId = jsonObject.get("mouse_pad_id").getAsInt();
            }

            if (jsonObject.has("company_name") && !jsonObject.get("company_name").isJsonNull()) {
                companyName = jsonObject.get("company_name").getAsString();
            }

            if (jsonObject.has("model") && !jsonObject.get("model").isJsonNull()) {
                model = jsonObject.get("model").getAsString();
            }

            Builder builder = new MousePadBuilder(mousePadId, companyName, model);

            if (jsonObject.has("type") && !jsonObject.get("type").isJsonNull()) {
                builder.setType(jsonObject.get("type").getAsString());
            }

            if (jsonObject.has("dimensions") && !jsonObject.get("dimensions").isJsonNull()) {
                builder.setDimensions(jsonObject.get("dimensions").getAsString());
            }

            if (jsonObject.has("material") && !jsonObject.get("material").isJsonNull()) {
                builder.setMaterial(jsonObject.get("material").getAsString());
            }

            if (jsonObject.has("base") && !jsonObject.get("base").isJsonNull()) {
                builder.setBase(jsonObject.get("base").getAsString());
            }

            if (jsonObject.has("attributes") && !jsonObject.get("attributes").isJsonNull()) {
                builder.setAttributes(
                        Attribute.getAttributeFromJsonObject(jsonObject.get("attributes").getAsJsonObject())
                );
            }

            if (jsonObject.has("offers") && !jsonObject.get("offers").isJsonNull()) {
                JsonArray jsonArray = jsonObject.get("offers").getAsJsonArray();
                builder.setOffers(Offer.getOffersFromJsonArray(jsonArray));
            }

            return builder.createMousePad();

        }

    }

}
